<?php
include dirname(__DIR__).DIRECTORY_SEPARATOR.'prepend.php';

try {
    if ($_SERVER['REQUEST_METHOD'] == 'POST' && $token->validateFormToken() == false) {
        $session->setMsg('L_TOKEN_error','error');
        header('location: '.$urlroot.$route.$params);
        exit();
    }
	$entryPoint = new \Core\Lib\EntryPoint(
        $route, 
        $_SERVER['REQUEST_METHOD'], 
        $params, 
        new \App\Lib\AppRoutes($theme, $session,ROOT_PATH.$mediasDir,$medias,$urlroot, $configuration, $filesTools), 
        new \App\Lib\AppLayout($theme, 'layout.html.php','layout.menu.html.php'),
        new \Core\Lib\Paginator(), 
        '\App\Lib\AppPagination', 
        $session, 
        $token, 
        $filesTools,
        new \Core\Lib\StringTools(),
        $configuration,
        $medias,
        new \Core\Lib\CryptoTools(),
        $theme, 
        $urlroot,
        false,
        'home',
        $lang,
        SODIUM_STRING_KEY
    );
	$entryPoint->run([$theme.'/javascript/visual.js',$theme.'/javascript/prettyPrint.js',$theme.'/javascript/openNew.js']);
}
catch (PDOException $e) {
	$title = 'An error has occurred';

    if (isset($_GET['public/index_php']) ) unset($_GET['public/index_php']);
    $page = current(array_keys($_GET));

	$output = '<h1>Error !</h1><p>Page: '.strip_tags($page).'</p><p>Database error: ' . $e->getMessage() . ' in ' .
	$e->getFile() . ':' . $e->getLine().'</p>';

	showException($title,$output);
}
