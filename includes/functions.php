<?php 
# Fonction qui retourne le timestamp UNIX actuel avec les microsecondes
function getMicrotime() {
    $t = explode(' ',microtime());
    return $t[0]+$t[1];
}
function showException(string $title, string $output)
{
   echo '<!doctype html><html><head><meta charset="utf-8"><title>'.$title.'</title></head><body><main style="padding:20px">'.$output.'</main></body></html>';
}

# Sanitize functions
function mysql_escape_mimic($inp) { 
    if(is_array($inp)) 
        return array_map(__METHOD__, $inp); 

    if(!empty($inp) && is_string($inp)) { 
        return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp); 
    } 

    return $inp; 
} 

function cleanInput($input, $stripOut = []) {

    $search = array(
        'scripts' => '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
        'tags' => '@<[/!]*?[^<>]*?>@si',            // Strip out HTML tags
        'styles' => '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
        'comments' => '@<![sS]*?--[ tnr]*>@'         // Strip multi-line comments
    );
    foreach ($stripOut as $key => $value) {
        if (array_key_exists($value,$search) ){
            unset($search[$value]);
        }
    }

    $output = preg_replace($search, '', $input);
    return str_replace("\0", '', $output);
}

function sanitize($input, $stripOut = [], $escape = true) {
    if (is_array($input)) {
        foreach($input as $var=>$val) {
            unset($input[$var]);
            $input[sanitize($var, $stripOut)] = sanitize($val, $stripOut, $escape);
        }
        $output = $input;
    }
    else {
        if (get_magic_quotes_gpc()) {
            $input = stripslashes($input);
        }
        $input  = cleanInput($input, $stripOut);
        $output = $escape ? mysql_escape_mimic($input) : $input;
    }
    return $output;
}