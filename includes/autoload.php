<?php
function autoloader($className) {
	$fileName = str_replace('\\', '/', $className) . '.php';

    if (!in_array(substr($fileName,0,strpos($fileName,'/')), ['App','Core','Plugins','Vendors'])) {
        if (is_file(dirname(__DIR__) . '/classes/' .'App/Vendors/'.$fileName)) {
            $fileName = 'App/Vendors/'.$fileName;
        } elseif (is_file(dirname(__DIR__) . '/classes/' .'Vendors/'.$fileName)) {
            $fileName = 'Vendors/'.$fileName;
        }
    }

	$file = dirname(__DIR__) . '/classes/' . $fileName;
    
	include $file;
}

spl_autoload_register('autoloader');