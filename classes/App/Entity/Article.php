<?php
namespace App\Entity;

use \Core\Interfaces\DatabaseTable;

final class Article {
	public $id;
	public $authorId;
    public $articledate;
	public $articletitle;
	public $articletext;
    
	private $authorsTable;
	private $author;
	private $articleCategoriesTable;
	private $categoryTable;

	public function __construct(DatabaseTable $authorsTable, DatabaseTable $articleCategoriesTable, DatabaseTable $categoryTable) {
		$this->authorsTable = $authorsTable;
		$this->articleCategoriesTable = $articleCategoriesTable;
		$this->categoryTable = $categoryTable;
	}

	/**
     * To avoid $var to be changed because object should be immuable
     */
    public function __set($var,$value)
    {
        return null;
    }
    public function __call($name, $arguments)
    {
        return null;
    }
    public function __isset($name)
    {
        return null;
    }
    public function __unset($name)
    {
        return $name;
    }
    public function __get($var)
    {
        return $var;
    }


	public function getAuthor() {
		if (empty($this->author)) {
			$this->author = $this->authorsTable->findById($this->authorId);
		}
		
		return $this->author;
	}

	public function addCategory($categoryId) {
		$articleCat = ['articleId' => $this->id, 'categoryId' => $categoryId];

		$this->articleCategoriesTable->save($articleCat);
	}

    public function getCategory($categoryId) {
        return $this->categoryTable->findById($categoryId);
    }

	public function getCategories() {
		$articleCategories = $this->articleCategoriesTable->find(['articleId'], [$this->id]);
        $cat = [];
        foreach ($articleCategories as $k => $articleCategory) {
                $cat[] = $this->getCategory($articleCategory->categoryId);
        }
        return $cat;
	}

	public function hasCategory($categoryId) {
		$articleCategories = $this->articleCategoriesTable->find(['articleId'], [$this->id]);

		foreach ($articleCategories as $articleCategory) {
			if ($articleCategory->categoryId == $categoryId) {
				return true;
			}
		}
	}

	public function clearCategories() {
		$this->articleCategoriesTable->deleteWhere('articleId', $this->id);
	}
}