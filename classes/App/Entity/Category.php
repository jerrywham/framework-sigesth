<?php
namespace App\Entity;

use Core\Interfaces\DatabaseTable;

final class Category {
	public $id;
	public $name;
	private $articlesTable;
	private $articleCategoriesTable;

	public function __construct(DatabaseTable $articlesTable, DatabaseTable $articleCategoriesTable) {
		$this->articlesTable = $articlesTable;
		$this->articleCategoriesTable = $articleCategoriesTable;
	}

	/**
     * To avoid $var to be changed because object should be immuable
     */
    public function __set($var,$value)
    {
        return null;
    }
    public function __call($name, $arguments)
    {
        return null;
    }
    public function __isset($name)
    {
        return null;
    }
    public function __unset($name)
    {
        return $name;
    }
    public function __get($var)
    {
        return $var;
    }


	public function getArticles($limit = null, $offset = null) {
		$articleCategories = $this->articleCategoriesTable->find(['categoryId'], [$this->id], ['='], '*', null, $limit, $offset);

		$articles = [];

		foreach ($articleCategories as $articleCategory) {
			$article =  $this->articlesTable->findById($articleCategory->articleId);
			$cat = $article->getCategory($articleCategory->categoryId);
			$article->categoryName = $cat->name;
			if ($article) {
				$articles[] = $article;
			}			
		}

		usort($articles, [$this, 'sortArticles']);

		return $articles;
	}

	public function getAllArticles($limit = null, $offset = null) {
		$articleCategories = $this->articleCategoriesTable->findAll(null, $limit, $offset);

		$articles = [];

		foreach ($articleCategories as $articleCategory) {
			$article =  $this->articlesTable->findById($articleCategory->articleId);
			if ($article) {
				$articles[] = $article;
			}			
		}

		usort($articles, [$this, 'sortArticles']);

		return $articles;
	}

	public function getNumArticles() {
		return $this->articleCategoriesTable->total(['categoryId'=> $this->id]);
	}

	private function sortArticles($a, $b) {

		$aDate = $this->articlesTable->pdo->formatDate($a->articledate,'');
		$bDate = $this->articlesTable->pdo->formatDate($b->articledate,'');

		if ($aDate->getTimestamp() == $bDate->getTimestamp()) {
			return 0;
		}
		return $aDate->getTimestamp() < $bDate->getTimestamp() ? -1 : 1;
	}
}