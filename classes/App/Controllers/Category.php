<?php
namespace App\Controllers;

final class Category {
	private $categoriesTable;
	private $urlroot;

	public function __construct(\Core\Interfaces\DatabaseTable $categoriesTable, string $urlroot) {
		$this->categoriesTable = $categoriesTable;
		$this->urlroot = $urlroot;
	}
	
	/**
     * To avoid $var to be changed because object should be immuable
     */
    public function __set($var,$value)
    {
        return null;
    }
    public function __call($name, $arguments)
    {
        return null;
    }
    public function __isset($name)
    {
        return null;
    }
    public function __unset($name)
    {
        return $name;
    }
    public function __get($var)
    {
        return $this->$var;
    }


	public function edit() {

		if (isset($_GET['id'])) {
			$category = $this->categoriesTable->findById($_GET['id']);
		}

		$title = 'L_Edit_Category';

		return ['template' => 'category.edit.html.php',
				'title' => $title,
				'category' => ($category ?? null),
				'token' => true
		];
	}

	public function saveEdit() {
		$category = $_POST['category'];

		$this->categoriesTable->save($category);

		header('location: '.$this->urlroot.'category/list');
        exit();
	}

	public function list() {
		$categories = $this->categoriesTable->findAll();

		$title = 'L_Article_Categories';

		return ['template' => 'categories.html.php', 
			'title' => $title, 
			'categories' => $categories,
			'token' => true
		];
	}

	public function delete() {
		$this->categoriesTable->delete($_POST['id']);

		header('location: '.$this->urlroot.'category/list');
        exit(); 
	}
}