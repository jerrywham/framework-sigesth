<?php
namespace App\Controllers;

use \Core\Lib\DatabaseTable;
use HybridLogic\Validation\Rule;


final class Register {
	private $usersTable;
	private $session;
	private $theme;
	private $urlroot;
	private $validator;

	public function __construct(DatabaseTable $usersTable, string $theme, \Core\Interfaces\Auth\Session $session, string $urlroot, \App\Interfaces\Validator $validator) {
		$this->usersTable = $usersTable;
		$this->session = $session;
		$this->theme = $theme;
		$this->urlroot = $urlroot;

		$this->validator = $validator;

		$this->validator
			->add_rule('name', new Rule\NotEmpty())
			->add_rule('email', new Rule\NotEmpty())
			->add_rule('email', new Rule\Email())
			->add_rule('password', new Rule\NotEmpty())
			->add_rule('password', new Rule\MinLength(12))
			->add_rule('password', new Rule\AlphaNumAndSpecialChars())
		;
	}
	
	/**
     * To avoid $var to be changed because object should be immuable
     */
    public function __set($var,$value)
    {
        return null;
    }
    public function __call($name, $arguments)
    {
        return null;
    }
    public function __isset($name)
    {
        return null;
    }
    public function __unset($name)
    {
        return $name;
    }
    public function __get($var)
    {
        return $this->$var;
    }



	public function registrationForm() {
		$userToEdit = null;

		$js = [$this->theme.'/javascript/pwdStrength.js'];


		if (isset($_GET['id'])) {
			$userToEdit = $this->usersTable->findById($_GET['id']);
			$js = array_merge($js,[$this->theme.'/javascript/pwdVerif.js']);
		}
		
		return ['template' => 'register.html.php', 
				'title' => (isset($_GET['id']) ? 'L_Edit_account' : 'L_Register_an_account'),
				'userToEdit' => $userToEdit,
				'javascript' => $js,
				'token' => true
			   ];
	}


	public function success() {
		return ['template' => 'register.success.html.php', 
			    'title' => 'L_Registration_Successful'];
	}

    public function changePwd()
    {
        $user = $_POST['user'];
        $user['email'] = strtolower($user['email']);
        $userExists = $this->usersTable->find(['email'], [$user['email']] );
        if (!empty($userExists)) {
        	$user['id'] = current($userExists)->id;
        	$user['name'] = current($userExists)->name;
        	if ($this->validator->is_valid($user)) {
        		//Hash the password before saving it in the database
        		$user['password'] = password_hash($user['password'], PASSWORD_DEFAULT);

        		//When submitted, the $user variable now contains a lowercase value for email
        		//and a hashed password
        		$this->usersTable->save($user);
        		$this->session->setMsg('L_Data_recorded_successfully');
        		header('Location: '.$this->urlroot.'login');
        		exit();
        	} else {
				$errors = $this->validator->get_errors();
				$user = $this->validator->get_data();
				//If the data is not valid, show the form again
				return [
					'template' => 'login.finalize.html.php',
	                'title' => 'L_Change_password',
	                'token' => true,
	                'javascript' => [$this->theme.'/javascript/pwdStrength.js'],
			    	'errors' => $errors,
			    	'user' => $user
				 ]; 
			}
        }
    }

	public function registerUser() {
		$user = $_POST['user'];

		//Assume the data is valid to begin with
		$valid = true;
		$errors = [];


		if($this->validator->is_valid($user)) {
			$this->validator->get_data();
			//convert the email to lowercase
			$user['email'] = strtolower($user['email']);

			//search for the lowercase version of `$user['email']`
			if (count($this->usersTable->find(['email'], [$user['email']] )) > 0) {
				$valid = false;
				$errors[] = 'L_That_email_address_is_already_registered';
			}
		} else {
			$valid = false;
			$errors = $this->validator->get_errors();
			$user = $this->validator->get_data();
		}

		//If $valid is still true, no fields were blank and the data can be added
		if ($valid == true) {

			$all = $this->usersTable->total();
			# For the first user, all privileges
			if ($all == 0) {
				$reflected = new \ReflectionClass('\Core\Lib\User');
				$user['permissions'] = array_sum($reflected->getConstants());
			}


			//Hash the password before saving it in the database
			$user['password'] = password_hash($user['password'], PASSWORD_DEFAULT);

			//When submitted, the $user variable now contains a lowercase value for email
			//and a hashed password
			$this->usersTable->save($user);
			$this->session->setMsg('L_Data_recorded_successfully');
			header('Location: '.$this->urlroot.'users/success');
			exit();
		}
		else {
			//If the data is not valid, show the form again
			return ['template' => 'register.html.php', 
				    'title' => 'L_Register_an_account',
			    	'errors' => $errors,
			    	'user' => $user,
					'token' => true
				   ]; 
		}
	}

	public function editUser()
	{
		//Assume the data is valid to begin with
		$valid = true;
		$errors = [];

		$user = $_POST['user'];

		//But if any of the fields have been left blank, set $valid to false
		$this->validator
			->add_rule('password_confirmation', new Rule\NotEmpty())
			->add_rule('password', new Rule\Equal($user['password_confirmation']))
			->add_rule('old-email', new Rule\NotEmpty())
			->add_rule('old-email', new Rule\Email());

		if($this->validator->is_valid($user) && !empty($this->usersTable->find(['email'], [$user['old-email']]) ) ) {
			$this->validator->get_data();
			//convert the email to lowercase
			$user['email'] = strtolower($user['email']);
		} else {
			$valid = false;
			$errors = $this->validator->get_errors();
			$user = $this->validator->get_data();
			unset($user['password']);
		}
		unset($user['password_confirmation']);
		unset($user['old-email']);

		//If $valid is still true, no fields were blank and the data can be added
		if ($valid == true) {
			if (!empty($user['password'])) {
				//Hash the password before saving it in the database
				$user['password'] = password_hash($user['password'], PASSWORD_DEFAULT);
			}
			//When submitted, the $user variable now contains a lowercase value for email
			//and a hashed password
			$this->usersTable->save($user);
			$this->session->set('username',$user['email']);
			$this->session->setMsg('L_Data_recorded_successfully');
			header('Location: '.$this->urlroot.'users/list');
			exit();
		}
		else {
			//If the data is not valid, show the form again
			return ['template' => 'register.html.php', 
				    'title' => 'L_Edit_account',
			    	'errors' => $errors,
			    	'user' => $user,
					'token' => true
				   ]; 
		}
	}

	public function list() {
		$users = $this->usersTable->findAll();

		return ['template' => 'users.list.html.php',
				'title' => 'L_Users_List',
				'users' => $users,
				'javascript' => [$this->theme.'/javascript/LightJavascriptTableFilter.js']
				];
	}

	public function permissions() {

		$user = $this->usersTable->findById($_GET['id']);

		$reflected = new \ReflectionClass('\Core\Lib\User');
		$constants = $reflected->getConstants();

		return ['template' => 'permissions.html.php',
				'title' => 'L_Edit_Permissions',
				'user' => $user,
				'permissions' => $constants,
				'token' => true
				];	
	}

	public function savePermissions() {
		$user = [
			'id' => $_GET['id'],
			'permissions' => array_sum($_POST['permissions'] ?? [])
		];

		$this->usersTable->save($user);
		$this->session->setMsg('L_Data_recorded_successfully');
		header('location: '.$this->urlroot.'users/list');
		exit();
	}
}