<?php
namespace App\Controllers;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

final class Mailer implements \Core\Interfaces\Mailer {

    public function getMailer(bool $bool)
    {
        return new PHPMailer($bool);
    }

}