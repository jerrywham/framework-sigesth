<?php

namespace App\Controllers;

use HybridLogic\Validation\Validator;

final class AppValidator implements \App\Interfaces\Validator {

    private $validator;

    function __construct()
    {
        $this->validator = new Validator();
    }

    public function add_rule(string $input, \HybridLogic\Validation\Rule $rule){
        return $this->validator->add_rule($input,$rule);
    }
    public function is_valid(array $data) {
        return $this->validator->is_valid($data);
    }
    public function get_data($field = null, $default = null) {
        return $this->validator->get_data($field,$default);
    }
    public function get_errors() {
        return $this->validator->get_errors();
    }

}