<?php

namespace App\Controllers;

final class Medias implements \App\Interfaces\Medias {

    private $mediasDir;
    private $allowExtensions = [
        'aac' => 'audio/aac',
        'avi' => 'video/x-msvideo',
        'bz' => 'application/x-bzip',
        'bmp' => 'image/bmp',
        'csv' => 'text/csv',
        'doc' => 'application/msword',
        'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'dot' => 'application/msword',
        'gif' => 'image/gif',
        'ico' => 'image/x-icon',
        'ics' => 'text/calendar',
        'png' => 'image/png',
        'jpeg' => 'image/jpeg',
        'jpg' => 'image/jpeg',
        'key' => 'application/octet-stream',
        'md' => 'text/plain',
        'mkv' => 'video/x-matroska',
        'mp3' => 'audio/mpeg3',
        'mpeg' => 'video/mpeg',
        'numbers' => 'application/octet-stream',
        'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        'odt' => 'application/vnd.oasis.opendocument.text',
        'odp' => 'application/vnd.oasis.opendocument.presentation',
        'otp' => 'application/vnd.oasis.opendocument.presentation-template',
        'ott' => 'application/vnd.oasis.opendocument.text-template',
        'ogg' => 'application/ogg',
        'pages' => 'application/octet-stream',
        'pdf' => 'application/pdf',
        'pot' => 'application/vnd.ms-powerpoint',
        'pps' => 'application/vnd.ms-powerpoint',
        'ppsx' => 'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
        'ppt' => 'application/vnd.ms-powerpoint',
        'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
        'sig' => 'application/octet-stream',
        'sql' => 'application/octet-stream',
        'tar' => 'application/x-tar', 
        'txt' => 'text/plain',
        'wav' => 'audio/x-wav',
        'xls' => 'application/vnd.ms-excel',
        'xla' => 'application/vnd.ms-excel',
        'xlb' => 'application/vnd.ms-excel',
        'xlb' => 'application/x-excel',
        'xlc' => 'application/vnd.ms-excel',
        'xld' => 'application/vnd.ms-excel',
        'xlk' => 'application/vnd.ms-excel',
        'xll' => 'application/vnd.ms-excel',
        'xlm' => 'application/vnd.ms-excel',
        'xl' => 'application/vnd.ms-excel',
        'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'xlt' => 'application/vnd.ms-excel',
        'xlv' => 'application/vnd.ms-excel',
        'xlw' => 'application/x-excel',
        'zip' => 'application/zip',
    ];
    private $fileIcons = [
        'aac' => 'audio-x-generic.png',
        'avi' => 'vlc.png',
        'bmp' => 'image-bmp.png',
        'bz' => 'palimpsest.png',
        'csv' => 'ods.png',
        'doc' => 'doc.png',
        'dot' => 'dotx.png',
        'docx' => 'docx.png',
        'gif' => 'image-gif.png',
        'ico' => 'image-x-ico.png',
        'ics' => 'evolution-calendar.png',
        'jpeg' => 'image-jpeg.png',
        'jpg' => 'image-jpeg.png',
        'key' => 'numbers2.png',
        'md' => 'txt.png',
        'mkv' => 'vlc.png',
        'mp3' => 'audio-x-generic.png',
        'mpeg' => 'vlc.png',
        'numbers' => 'numbers.png',
        'ods' => 'ods2.png',
        'odt' => 'ooo-template.png',
        'odp' => 'impress.png',
        'ogg' => 'audio-x-vorbis+ogg.png',
        'otp' => 'libreoffice-main.png',
        'ott' => 'libreoffice-main.png',
        'pages' => 'pages.png',
        'pdf' => 'pdf2.png',
        'png' => 'image-png.png',
        'pot' => 'ppt.png',
        'pps' => 'pps.png',
        'ppsx' => 'pptx.png',
        'ppt' => 'ppt.png',
        'pptx' => 'pptx.png',
        'sig' => 'txt.png',
        'sql' => 'ooo-base.png',
        'txt' => 'txt.png',
        'wav' => 'musique.png',
        'xl' => 'xls.png',
        'xla' => 'xls.png',
        'xlb' => 'xls.png',
        'xlc' => 'xls.png',
        'xld' => 'xls.png',
        'xlk' => 'xls.png',
        'xll' => 'xls.png',
        'xlm' => 'xls.png',
        'xls' => 'xls.png',
        'xlsx' => 'xlsx.png',
        'xlt' => 'xls.png',
        'xlv' => 'xls.png',
        'xlw' => 'xls.png',
        'zip' => 'package-x-generic.png'
        // 'psd' => 'psd.gif',
    ];
    private $dirForIcons;
    private $separator = FILE_SEPARATOR;
    private $filesTools;
    private $theme;
    private $urlroot;
    private $session;

    public function __construct(string $mediasDir,string $theme, string $dirForIcons, string $urlroot, \Core\Interfaces\Auth\Session $session, array $allowExtensions = [], array $fileIcons = [], \Core\Interfaces\FilesTools $filesTools = null)
    {
        $this->mediasDir = $mediasDir;
        $this->theme = $theme;
        $this->fileIcons = (!empty($fileIcons) ? $fileIcons : $this->fileIcons);
        $this->dirForIcons = $dirForIcons;
        $this->filesTools = $filesTools;
        $this->separator = ($filesTools->getFileSeparator() ?? $this->separator);
        $this->allowExtensions = (!empty($allowExtensions) ? $allowExtensions : $this->allowExtensions);
        $this->urlroot = $urlroot;
        $this->session = $session;
    }
    /**
     * To avoid $var to be changed because object should be immuable
     */
    public function __set($var,$value)
    {
        return null;
    }
    public function __call($name, $arguments)
    {
        return null;
    }
    public function __isset($name)
    {
        return null;
    }
    public function __unset($name)
    {
        return $name;
    }
    public function __get($var)
    {
        return $var;
    }

    public function getMediasAllowedExtensions():array
    {
        return $this->allowExtensions;
    }

    public function getMediasFilesIcons():array
    {
        return $this->fileIcons;
    }

    public function getMediasFilesIconsPath():string
    {
        return '/assets/'.$this->dirForIcons;
    }

    public function displayPict()
    {
        return $this->display('pict');
    }
    public function displayFile()
    {
        return $this->display('files');
    }

    public function list()
    {

        $pictroot = 'pict';
        $filesroot = 'files';
        $dirRoot = basename($this->urlroot);
        $mediasDir = '/'.substr($this->mediasDir,strpos($this->mediasDir,$dirRoot));
            
        foreach ($_GET as $key => $value) {
            if (strpos($key, 'medias') !== false) {
                if (strpos($key,'medias/pict/') !== false) {
                    $pictroot = str_replace('medias/','',$key);
                }
                if (strpos($key,'medias/files/') !== false) {
                    $filesroot = str_replace('medias/','',$key);
                }
            }
        }

        $pict = $this->filesTools->listADir($this->mediasDir.$pictroot, false, ['.gitkeep','.htaccess','index.html','thumbs'],true);
        $files = $this->filesTools->listADir($this->mediasDir.$filesroot, false, ['.gitkeep','.htaccess','index.html'],true);

        return [
            'title' => 'L_Medias',
            'template' => 'medias.html.php',
            'javascript' => [$this->theme.DIRECTORY_SEPARATOR.'javascript'.DIRECTORY_SEPARATOR.'onglets.js',$this->theme.DIRECTORY_SEPARATOR.'javascript'.DIRECTORY_SEPARATOR.'bulles.js',$this->theme.DIRECTORY_SEPARATOR.'javascript'.DIRECTORY_SEPARATOR.'toogleOnDoubleClick.js',$this->theme.DIRECTORY_SEPARATOR.'javascript'.DIRECTORY_SEPARATOR.'LightJavascriptTableFilter.js',$this->theme.DIRECTORY_SEPARATOR.'javascript'.DIRECTORY_SEPARATOR.'tableSort.js',$this->theme.DIRECTORY_SEPARATOR.'javascript'.DIRECTORY_SEPARATOR.'checkAll.js'],
            'mediasDir' => substr($this->mediasDir,0,-1),
            'pict' => $pict,
            'pictroot' => str_replace('//','/','/'.$pictroot.'/'),
            'files' => $files,
            'filesroot' => '/'.$filesroot,
            'filesTools' => $this->filesTools,
            'filesIcons' => $this->fileIcons,
            'dirForIcons' => $this->dirForIcons,
            'token' => true
        ];
    }

    private function isMediaPrivate($file):bool
    {
        return (bool)strpos($file,$this->filesTools->getPrivateTag().'.');
    }

    /**
     * @param  string $type [pict or files]
     * @return [media]       [display media]
     */
    private function display(string $type)
    {

        $media = $this->realRoute($type.'/');
        if (($this->isMediaPrivate($media) && $this->session->isLoggedIn()) || !$this->isMediaPrivate($media)) {
            if (is_file($this->mediasDir.$media)) {
                $fileInfos = new \finfo(FILEINFO_MIME_TYPE);
                $info = $fileInfos->file($this->mediasDir.$media);
                if (in_array($info, $this->allowExtensions)) {
                    $f = $this->filesTools->getFilename($media);
                    switch ($f['extension']) {
                        case 'png':
                        case 'jpg':
                        case 'jpeg':
                        case 'gif':
                        case 'bmp':
                        case 'tiff':
                        case 'tif': 
                            $this->filesTools->readPict($this->mediasDir.$media,$this->getMediasAllowedExtensions());
                            break;
                        default:
                            $content = file_get_contents($this->mediasDir.$media);
                            $name = $f['name'];
                            header('Content-Description: File Transfer');
                            header("Content-Disposition: inline; filename=$name");//attachment for display outside the browser
                            header("Content-type: $info");
                            header('Cache-Control: private, max-age=0, must-revalidate');
                            header('Pragma: public');
                            echo $content;
                        break;
                    }
                }
            } else {
                http_response_code(404);
                header('location: '.$this->urlroot.'error');
                exit();
            }
        } else {
            http_response_code(401);
            header('location: '.$this->urlroot.'unauthorized');
            exit();
        }
        exit();
    }

    private function realRoute($route)
    {
        foreach ($_GET as $key => $value) {
            if (strpos($key,$route) !== false) {
                $e = substr($key,strrpos($key,'_')+1);
                return str_replace([$route.'/'.$route,'_'.$e],[$route.'/','.'.$e],$key);
            }
        }
    }

    public function new()
    {

        foreach ($_GET as $key => $value) {
            if (strpos($key, 'medias/add') !== false) {
                $leadon = str_replace(['medias/add/pict','medias/add/files'],'',$key);
                if (strpos($key,'medias/add/pict') !== false) {
                    $baseurl = 'pict';
                }
                if (strpos($key,'medias/add/files') !== false) {
                    $baseurl = 'files';
                }
            }
        }
        return [
            'title' => 'L_Add_media',
            'template' => 'medias.add.html.php',
            'token' => true,
            'allowuploads' => (bool) ini_get('file_uploads'),// check if upload is enabled on server
            'baseurl' => $baseurl,
            'leadon' => $leadon,
            'allowExtensions' => array_keys($this->allowExtensions),
            'javascript' => [$this->theme.DIRECTORY_SEPARATOR.'javascript'.DIRECTORY_SEPARATOR.'previewFiles.js'],
        ];
    }

    public function uploadByFilesTools(string $root, string $baseurl, string $postlead, string $visibility, bool $allowuploads, bool $browsedirs, string $separator):bool
    {
        return $this->filesTools->upload($root,$baseurl,$postlead,$visibility,$allowuploads,$this->allowExtensions,$browsedirs,$separator);
    }

    public function addNew()
    {
        # baseurl = pict or files
        # leadon = selected dir
        
        # Directory where to put the file
        if (!isset($_POST['baseurl'])) {
            return false;
        } else {
            $baseurl = trim(str_replace('.','',$_POST['baseurl'])).DIRECTORY_SEPARATOR;
            if (!is_dir($this->mediasDir.$baseurl)) {
                return false;
            }
        }

        # Visibility of the file (public or private)
        if (!isset($_POST['visibility'])) {
            return false;
        } else {
            $visibility = ($_POST['visibility'] == 'public' ? $_POST['visibility'] : 'private');
        }

        $allowuploads = isset($_SESSION['uid']);

        $upload = $this->filesTools->upload($this->mediasDir,$baseurl,$_POST['lead'],$visibility,$allowuploads,$this->allowExtensions,true,$this->separator);
        $msg = '';
        if (isset($upload['errors'])) {
            $msg .= 'L_Error : L_Failed_to_move_uploaded_files';
            foreach ($upload['errors'] as $key => $value) {
                $msg .= "\n".$value;
            }
            $type = 'error';
        } else {
            $msg .= 'L_Data_recorded_successfully';
            foreach ($upload['files'] as $key => $value) {
                $msg .= "\n".$value;
            }
            if (isset($type)) {
                $type = 'warning';
            } else {
                $type = 'success';
            }
        }


        if ($baseurl == 'pict/') {
            $lib = 'pict';
        } else {
            $lib = 'files';
        }

        $this->session->setMsg($msg,$type);
        
        header('location: '.$this->urlroot.'medias/'.$lib.$_POST['lead'].'/#'.$lib);
        exit();
        
    }

    public function addFolder()
    {
        
        # Remove html tags
        $_POST = sanitize($_POST);

        $newdir = $this->filesTools->title2filename(str_replace(['.',"\0"],'',$_POST['newfolder']));
        $dir = str_replace(['.',"\0"],'',$_POST['dir']);
        $lib = substr($dir,1,strpos(substr($dir,1),DIRECTORY_SEPARATOR));
        if ($lib != '') $lib = '#'.$lib;

        if (is_dir($this->mediasDir.$dir)) {
            if (!is_dir($this->mediasDir.$dir.$newdir)) {
                if (mkdir($this->mediasDir.$dir.$newdir)) {
                    $this->session->setMsg('L_Folder_created_successfully');
                    header('location: '.$this->urlroot.'medias'.$dir.$newdir.$lib);
                    exit();
                } else {
                    $this->session->setMsg('L_Folder_creation_failed');
                    header('location: '.$this->urlroot.'medias'.$dir.$lib);
                    exit();
                }
            } else {
                $this->session->setMsg('L_Folder_already_exists');
                header('location: '.$this->urlroot.'medias'.$dir.$newdir.$lib);
                exit();
            }
        } else {
            $this->session->setMsg('L_Folder_creation_failed');
            header('location: '.$this->urlroot.'medias'.$dir.$lib);
            exit();
        }
    }

    private function return_bytes($size_str)
    {
        switch (substr ($size_str, -1))
        {
            case 'M': case 'm': return (int)$size_str * 1048576;
            case 'K': case 'k': return (int)$size_str * 1024;
            case 'G': case 'g': return (int)$size_str * 1073741824;
            default: return $size_str;
        }
    }
    private function bytes_for_human($size_int) {
        if (!empty($size_int) && is_int($size_int)){
            $unit = array('B','K','M','G');
            try {
                return round($size_int / pow(1024, ($i = floor(log($size_int, 1024))))).$unit[$i];
            } catch (Exception $e) {
                return $size_int;
            }
        } else {
            return $size_int;
        }
    }

    public function edit()
    {
        $file = null;
        $action = null;
         foreach ($_GET as $key => $value) {
            if (strpos($key, 'medias') !== false) {
                if (strpos($key,'medias/edit/rename/') !== false) {
                    $file = str_replace('medias/edit/rename/','',$this->realRoute($key));
                    $action = 'rename';
                    break;
                }
            }
        }

        if ((is_dir($this->mediasDir.$file) ||is_file($this->mediasDir.$file)) && $action !== null) {
            $f = basename($file);
            $f = $this->filesTools->getFilename($f);
            return [
                'template' => 'medias.edit.html.php',
                'title' => 'L_Edit_Informations',
                'filename' => ($f['name'][0] == '.' ? substr($f['name'],1) : $f['name']),
                'dir' => dirname($file),
                'action' => $action,
                'token' => true,
                'ftoken' => $f['token'],
                'fvisibility' => (int) $f['visibility']
            ];
        } else {
            http_response_code(404);
            header('location: '.$this->urlroot.'error');
            exit();
        }
    }

    public function rename()
    {
        # Remove html tags
        $_POST = sanitize($_POST);

        $ftoken = $_POST['ftoken'];
        $dir = str_replace(['/','.'],[DIRECTORY_SEPARATOR,''],$_POST['dir']);
        $lib = substr($dir,1,strpos(substr($dir,1),DIRECTORY_SEPARATOR));
        if ($lib != '') $lib = '#'.$lib;

        # it's a file else it's a folder
        if ($ftoken != '') {
            $file = str_replace(['/','\\',DIRECTORY_SEPARATOR],'',substr($_POST['filename'],0,strrpos($_POST['filename'],'.')));

            $ext = $this->filesTools->getExtension($_POST['filename']);
            $fvisibility = ($_POST['fvisibility'] == true ? '' : $this->filesTools->getPrivateTag());
            $visibility = (isset($_POST['visibility']) && ($_POST['visibility'] == 'private' || $_POST['visibility'] == 0) ? $this->filesTools->getPrivateTag() : '');

            $new = $this->filesTools->title2filename(substr($_POST['new'],0,strrpos($_POST['new'],'.')));
            
            $file .= $this->separator.$ftoken.$fvisibility.'.'.$ext;
            $new .= $this->separator.$this->filesTools->generate_hash($new.$visibility.$ftoken).$visibility.'.'.$ext;
        } else {
            $file = $_POST['filename'];
            $new = $this->filesTools->title2filename($_POST['new']);
        }

        if (is_file($this->mediasDir.$dir.DIRECTORY_SEPARATOR.$file) || is_dir($this->mediasDir.$dir.DIRECTORY_SEPARATOR.$file) && $file != $new) {
            try {
                rename($this->mediasDir.$dir.DIRECTORY_SEPARATOR.$file , $this->mediasDir.$dir.DIRECTORY_SEPARATOR.$new);
            } catch (Exception $e) {
                http_response_code(404);
                header('location: '.$this->urlroot.'error'.$lib);
                exit();
            }
            $dir = '/'.$dir;
            if ($dir == '/pict' || $dir == '/files') $dir = '';
            $this->session->setMsg('L_File_rename_successfully');
            header('location: '.$this->urlroot.'medias'.$dir.$lib); 
            exit();
        } else {
            http_response_code(404);
            header('location: '.$this->urlroot.'error'.$lib);
            exit();
        }
       
    }

    public function delete()
    {

        $_POST = sanitize($_POST);

        echo '<pre style="position:relative;z-index:2000000000;opacity:0.6;border: 1px solid #e3af43; background-color: #f8edd5; padding: 10px;color:#111;white-space: pre;white-space: pre-wrap;word-wrap: break-word;">Fichier '.__FILE__.', LIGNE '.__LINE__.'<br/><strong>$_POST :</strong> '; print_r($_POST);exit('</pre>');
        
        $f = $this->filesTools->getFilename($_POST['file']);
        $isDir = false;
        $lib = substr($_POST['dir'],1,strpos(substr($_POST['dir'],1),DIRECTORY_SEPARATOR));
        if ($lib != '') $lib = '#'.$lib;

        if ($f['name'][0] == '.' && $f['token'] == '') {
            $isDir = true;
        }

        if ($isDir && is_dir($this->mediasDir.$_POST['dir'].$_POST['file'])) {
            if($this->filesTools->delTree($this->mediasDir.$_POST['dir'].$_POST['file'],true,true)) {
                $this->filesTools->delTree($this->mediasDir.'thumbs'.DIRECTORY_SEPARATOR.$_POST['dir'].$_POST['file'],true,true);
                $d = substr($_POST['dir'],0,strrpos($_POST['dir'],'/'));
                if ($d == '/pict' || $d == '/files') $d = '';

                $this->session->setMsg('L_Directory_deleted_successfully');
                header('location: '.$this->urlroot.'medias'.$d.$lib ); 
                exit();
            } else {
                $this->session->setMsg('L_Directory_deletion_failed', 'error');
                header('location: '.$this->urlroot.'medias'.$_POST['dir'].$lib); 
                exit();
            }
        } elseif ($isDir === false && is_file($this->mediasDir.$_POST['dir'].$_POST['file'])) {
            if (unlink($this->mediasDir.$_POST['dir'].$_POST['file'])) {
                unlink(str_replace('pict'.DIRECTORY_SEPARATOR,'pict'.DIRECTORY_SEPARATOR.'thumbs'.DIRECTORY_SEPARATOR,$this->mediasDir.$_POST['dir'].$_POST['file']));
                $this->session->setMsg('L_File_deleted_successfully');
                header('location: '.$this->urlroot.'medias'.$_POST['dir'].$lib); 
                exit();
            } else {
                $this->session->setMsg('L_File_deletion_failed', 'error');
                header('location: '.$this->urlroot.'medias'.$_POST['dir'].$lib); 
                exit();
            }
        } else {
            header('location: '.$this->urlroot.'medias'.$_POST['dir'].$lib); 
            exit();
        }
    }

}