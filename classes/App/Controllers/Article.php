<?php
namespace App\Controllers;
use \Core\Interfaces\DatabaseTable;
use \Core\Interfaces\Auth\Authentication;
use \Core\Interfaces\Auth\Session;

final class Article {
	private $authorsTable;
	private $articlesTable;
	private $categoriesTable;
	private $authentication;
	private $session;
	private $theme;
	private $urlroot;
	private $objByPage = 10;

	public function __construct(DatabaseTable $articlesTable, DatabaseTable $authorsTable, DatabaseTable $categoriesTable, Authentication $authentication, Session $session, string $theme, string $urlroot, int $objByPage = 10) {
		$this->articlesTable = $articlesTable;
		$this->authorsTable = $authorsTable;
		$this->categoriesTable = $categoriesTable;
		$this->authentication = $authentication;
		$this->session = $session;
		$this->theme = $theme;
		$this->urlroot = $urlroot;
		if ($objByPage > 0) {
			$this->objByPage = $objByPage;
		}
	}
	
	/**
     * To avoid $var to be changed because object should be immuable
     */
    public function __set($var,$value)
    {
        return null;
    }
    public function __call($name, $arguments)
    {
        return null;
    }
    public function __isset($name)
    {
        return null;
    }
    public function __unset($name)
    {
        return $name;
    }
    public function __get($var)
    {
        return $this->$var;
    }



	public function home() {
		return [
			'template' => 'home.html.php', 
			'title' => 'L_Internet_Article_Database',
			'token' => true
		];
	}

	public function error404() {
		return [
			'template' => 'error.html.php', 
			'title' => 'L_Error : L_Page_not_found !', 
			'bodytitle' => 'L_Error',
			'bodytext' => 'L_Page_not_found !'
		];
	}

	public function error401() {
		return [
			'template' => 'error.html.php', 
			'title' => 'L_Error : L_Unauthorized !', 
			'bodytitle' => 'L_Error',
			'bodytext' => 'L_Unauthorized !'
		];
	}

	public function list() {

		$page = $_GET['page'] ?? 1;

		$offset = ($page-1)*$this->objByPage;

		if (isset($_GET['category'])) {
			$category = $this->categoriesTable->findById($_GET['category']);
			$categoryName = $category->name;
			$articles = $category->getArticles($this->objByPage, $offset);
			$totalArticles = $category->getNumArticles();
		}
		else {
			$articles = $this->articlesTable->findInAllTables(
				'category', 
				'`article`.`id`, `article`.`articletitle`, `article`.`articletext`, DATE_FORMAT(`article`.`articledate`,\'%d/%m%/%Y\') AS articledate, `article`.`authorId`, (`category`.`id`) AS categoryid, `category`.`name` AS categoryName',
				'`article`.`id` ASC',
				$this->objByPage,
				$offset,
				'LEFT'
			);
			
			$totalArticles = $this->articlesTable->total();
		}	

		$title = 'L_Articles_list';

		$author = $this->authentication->getUser();

		return ['template' => 'articles.html.php', 
				'title' => $title, 
				'total' => $totalArticles,
				'currentPage' => $page,
				'elementsByPage' => $this->objByPage,
				'articles' => $articles,
				'user' => $author,
				'categories' => $this->categoriesTable->findAll(),
				'categoryId' => $_GET['category'] ?? null,
				'categoryName' => $categoryName ?? null,
				'token' => true
				];
	}

	public function admin_list() {
		$r = $this->list();
		$r['template'] = 'articles.admin.html.php';
		$r['javascript'] = [$this->theme.'/javascript/tableSort.js',$this->theme.'/javascript/checkAll.js'];
		return $r;
	}

	public function delete() {

		$author = $this->authentication->getUser();

		$article = $this->articlesTable->findById($_POST['id']);

		if ($article->authorId != $author->id && !$author->hasPermission(\Core\Lib\User::DELETE_JOKES) ) {

			$this->session->setMsg('L_You_do_not_have_permission_to_view_this_page');
			header('location: '.$this->urlroot.'article/list'); 
			exit();
		}
		
		$article->clearCategories();
		$this->articlesTable->delete($_POST['id']);

		$this->session->setMsg('L_Data_deleted_successfully');
		header('location: '.$this->urlroot.'article/list'); 
		exit();
	}

	public function saveEdit() {
		$author = $this->authentication->getUser();

		$article = $_POST['article'];
		if ($article['articletext'] == '<br>') {
            header('location: '.$this->urlroot.'article/edit'); 
            exit();
        }
        
		$article['articledate'] = new \DateTime();

		$articleEntity = $author->addData($article);

		$articleEntity->clearCategories();
		if (isset($_POST['category'])) {
			foreach ($_POST['category'] as $categoryId) {
				$articleEntity->addCategory($categoryId);
			}
		}

		$this->session->setMsg('L_Data_recorded_successfully');
		header('location: '.$this->urlroot.'article/list'); 
		exit();
	}

	public function edit() {
		$author = $this->authentication->getUser();
		$categories = $this->categoriesTable->findAll();

		if (isset($_GET['id'])) {
			$article = $this->articlesTable->findById($_GET['id']);
			if (isset($article->articledate)) {
				$article->articledate = $this->articlesTable->pdo->formatDate($article->articledate,'Y-m-d');
			}
		}
		$title = 'L_Edit_article';

		return ['template' => 'article.edit.html.php',
				'title' => $title,
				'article' => $article ?? null,
				'user' => $author,
				'categories' => $categories,
				'token' => true
				];
	}
	
}