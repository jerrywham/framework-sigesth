<?php
namespace App\Controllers;

final class Login {
	private $authentication;
	private $session;
	private $ban;
	private $urlroot;
    private $theme;
    private $postMail;

	public function __construct(\Core\Interfaces\Auth\Authentication $authentication, \Core\Interfaces\Auth\Session $session, \Core\Interfaces\Auth\Ban $ban, \Core\Interfaces\PostMail $postMail, string $urlroot, string $theme) {
		$this->authentication = $authentication;
		$this->session = $session;
		$this->ban = $ban;
		$this->urlroot = $urlroot;
        $this->theme = $theme;
        $this->postMail = $postMail;
	}
	
	/**
     * To avoid $var to be changed because object should be immuable
     */
    public function __set($var,$value)
    {
        return null;
    }
    public function __call($name, $arguments)
    {
        return null;
    }
    public function __isset($name)
    {
        return null;
    }
    public function __unset($name)
    {
        return $name;
    }
    public function __get($var)
    {
        return $var;
    }



	public function loginForm() {
        
        if ($this->authentication->firstConnexion()) {
            header('location: '.$this->urlroot.'users/register');
            exit();
        }
		$notSpamCode = $_GET['sp'] ?? '';
		if ($this->ban->ban_canLogin($notSpamCode) === false) { 
            $pass = false; 
        } else { 
            $pass = true; 
        }
		if($pass){
			return ['template' => 'login.html.php', 
					'title' => 'Log In',
					'token' => true,
                    'javascript' => [$this->theme.'/javascript/focusOnLogin.js'],
                    'loginPage' => true,
				    ];
		} else {
			$this->ban->ban_loginFailed();
			return ['template' => 'login.html.php', 
					'title' => 'Log In',
					'token' => true,
                    'javascript' => [$this->theme.'/javascript/focusOnLogin.js'],
                    'loginPage' => true,
				    ];
		}
	}

    public function newPassword()
    {
        return [
            'template' => 'login.newpassword.html.php',
            'title' => 'L_New_password',
            'token' => true
        ];
    }

    public function sendToken()
    {
        if ($this->authentication->userExists($_POST['login'])) {
            $this->postMail->sendTo($_POST['login']);
        }
        $this->session->setMsg('L_Token_sent','info');
        header('location: '.$this->urlroot.'login');
        exit();
    }

    public function controlToken()
    {
        $route = ltrim(str_replace(DIR_ROOT,'',strtok($_SERVER['REQUEST_URI'], '?')), '/');
        $token = new \Core\Lib\Token($this->session, $route, 3600, new \Core\Lib\FilesTools(new \Vendors\J20Uuid()));
        if ($token->validateEmailToken()) {
            return [
                'template' => 'login.finalize.html.php',
                'title' => 'L_New_password',
                'token' => true,
                'javascript' => [$this->theme.'/javascript/pwdStrength.js']
            ];
        } else {
            return [
                'template' => 'login.newpassword.html.php',
                'title' => 'L_New_password',
                'token' => true,
                'error' => 'L_TOKEN_error. L_Try_again'
            ];
        }
    }

	public function processLogin() {

		$notSpamCode = $_GET['sp'] ?? '';
		if ($this->ban->ban_canLogin($notSpamCode) === false) { $pass = false; } else { $pass = true; }
		if($pass){
			if ($this->authentication->login($_POST['login'], $_POST['password'])) {
				$this->ban->ban_loginOk();
				$this->session->setMsg('L_Login_Successful');
				header('location: '.$this->urlroot.'home');
				exit();
			}
			else {
				$this->ban->ban_loginFailed();
				return ['template' => 'login.html.php',
						'title' => 'L_Log_In',
						'error' => 'L_Invalid_username/password.',
                        'token' => true,
                        'javascript' => [$this->theme.'/javascript/focusOnLogin.js']
						];
			}
		} else { 
	        $this->ban->ban_loginFailed(); 
			return ['template' => 'login.html.php',
					'title' => 'L_Log_In',
					'error' => 'L_Invalid_username/password.',
                    'token' => true,
                    'javascript' => [$this->theme.'/javascript/focusOnLogin.js']
					];
	    }
	}

	public function success() {
		header('Location: '.$this->urlroot.'home');
		exit();
	}

	public function error() {
		return ['template' => 'login.error.html.php', 'title' => 'L_You_are_not_logged_in'];
	}

	public function permissionsError() {
		return ['template' => 'permissions.error.html.php', 'title' => 'L_Access_Denied'];
	}

	public function logout() {
		$this->ban->logout();
		unset($_SESSION);
		session_destroy();
		$this->session->remove(null);
		$this->session->setMsg('L_You_have_been_logged_out');
		header('Location: '.$this->urlroot.'home');
		exit();
	}
}
