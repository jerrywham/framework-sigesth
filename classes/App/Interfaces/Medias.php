<?php
namespace App\Interfaces;

interface Medias {

    public function getMediasAllowedExtensions():array;
    public function getMediasFilesIcons():array;
    public function getMediasFilesIconsPath():string;
    public function displayPict();
    public function displayFile();
    public function list();
    public function new();
    public function addNew();
    public function uploadByFilesTools(string $root, string $baseurl, string $postlead, string $visibility, bool $allowuploads, bool $browsedirs, string $separator):bool;
}