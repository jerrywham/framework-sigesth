<?php
namespace App\Interfaces;

interface Validator {
    public function add_rule(string $input, \HybridLogic\Validation\Rule $rule);
    public function is_valid(array $data);
    public function get_data($field = null, $default = null);
    public function get_errors();
}