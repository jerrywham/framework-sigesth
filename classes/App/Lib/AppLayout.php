<?php
namespace App\Lib;

final class AppLayout implements \Core\Interfaces\Layout {

    private $theme = '';
    private $template = '';
    private $menuTemplate = '';
    private $vars = [];

    public function __construct(string $theme, string $template, string $menuTemplate)
    {
        $this->theme = $theme;
        $this->template = is_file($this->theme.DIRECTORY_SEPARATOR.$template) ? $this->theme.DIRECTORY_SEPARATOR.$template : '';
        $this->menuTemplate = is_file($this->theme.DIRECTORY_SEPARATOR.$menuTemplate) ? $this->theme.DIRECTORY_SEPARATOR.$menuTemplate : '';
    }
    /**
     * To avoid $var to be changed because object should be immuable
     */
    public function __set($var,$value)
    {
        return null;
    }
    public function __call($name, $arguments)
    {
        return null;
    }
    public function __isset($name)
    {
        return null;
    }
    public function __unset($name)
    {
        return $name;
    }
    public function __get($var)
    {
        return $var;
    }
    public function template(string $template=''):string
    {
        return ($template != '' ? (is_file($this->theme.DIRECTORY_SEPARATOR.$template) ? $this->theme.DIRECTORY_SEPARATOR.$template : $this->template) : $this->template);
    }
    public function menu(array $vars):string
    {
        ob_start();
        extract($vars);
        include $this->menuTemplate;
        $menu = ob_get_clean();
        return $menu;
    }
    public function vars(bool $noReturn, array $vars):array
    {
        if ($noReturn === false) {
            //Minimal params
            $this->vars['absolutePath'] = ($vars['absolutePath'] ?? true);
            $this->vars['javascript'] = ($vars['javascript'] ?? []);
            $this->vars['javascriptInline'] = ($vars['javascriptInline'] ?? '');
            $this->vars['isIframeAllowed'] = ($vars['isIframeAllowed'] ?? false);
            $this->vars['headers'] = ($vars['headers'] ?? ['secure' => true]);
            $this->vars['theme'] = $this->theme;
            foreach ($vars as $key => $value) {
                if (!isset($this->vars[$key])) {
                    $this->vars[$key] = $value;
                }
            }
            $this->vars['menu'] = $this->menu($this->vars);
            if (isset($this->vars['authentication']) && $this->vars['authentication']->isLoggedIn()) {
                $this->vars['javascript'][] = $this->theme.'/javascript/countdown.js';
                $this->vars['javascript'][] = $this->theme.'/javascript/confirmAction.js';
            }
            if (isset($this->vars['msg']) && !empty($this->vars['msg']['id'])) {
                $this->vars['javascript'][] = ['fadeOut(\'alert_'.$this->vars['msg']['id'].'\');'];
            }
            return $this->vars;
        }
        return [];
    }
}