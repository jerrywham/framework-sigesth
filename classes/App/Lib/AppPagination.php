<?php
namespace App\Lib;

final class AppPagination implements \Core\Interfaces\Pagination {

    private $currentpage = 1;
    private $page = 0;
    private $elementsByPage = 10;
    private $variables = array();

    public function __construct(int $total, int $page, int $elementsByPage, array $variables = array())
    {
        if ($elementsByPage > 0) {
             $this->elementsByPage = $elementsByPage;
        }
        $this->currentpage = ceil($total/$elementsByPage);
        $this->page = $page;
        $this->variables = $variables;
    }
    /**
     * To avoid $var to be changed because object should be immuable
     */
    public function __set($var,$value)
    {
        return null;
    }
    public function __call($name, $arguments)
    {
        return null;
    }
    public function __isset($name)
    {
        return null;
    }
    public function __unset($name)
    {
        return $name;
    }
    public function __get($var)
    {
        return $var;
    }


    public function displayPagination(string $page = 'home', string $classCurrentPage = 'currentpage', string $classPages = ''): string {

        $vars = '';
        $p = array();
        foreach ($this->variables as $key => $value) {
            $vars .= !empty($value) ? '&'.$key.'=' . $value : '';
        }

        for ($i = 1; $i <= $this->currentpage; $i++){
            if ($i == $this->page) {
                $p[] = '<span'.(!empty($classCurrentPage) ? ' class="'.$classCurrentPage.'"' : ' class="currentpage"').'>&nbsp;'.$i.'&nbsp;</span>';
            }
            else{
                $p[] = '<a href="'.$page.'?page='.$i.$vars.'"'.(!empty($classPages) ? ' class="'.$classPages.'"' : '').'>&nbsp;'.$i.'&nbsp;</a>';
            }
        }

        $p = implode(' ',$p);
        
        return $p;
    }

    public function getPage(): int {

    }
}