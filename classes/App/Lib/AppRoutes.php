<?php
namespace App\Lib;

final class AppRoutes implements \Core\Interfaces\Routes {
	private $usersTable;
	private $articlesTable;
	private $categoriesTable;
	private $articleCategoriesTable;
	private $authentication;
	private $session;
	private $pluginsManagerController;
	private $pluginsConfigurationController;
	private $theme;
	private $mediasDir;
	private $medias;
	private $urlroot;
	private $configuration;
	private $filesTools;

	public function __construct(string $theme, \Core\Interfaces\Auth\Session $session, string $mediasDir, \App\Interfaces\Medias $medias, string $urlroot, \Core\Interfaces\Configuration $configuration, \Core\Interfaces\FilesTools $filesTools) {

		$this->configuration = $configuration;
		
		include dirname(dirname(dirname(__DIR__))).DIRECTORY_SEPARATOR.'includes'.DIRECTORY_SEPARATOR.'DatabaseConnection.php';

		$this->articlesTable = new \Core\Lib\DatabaseTable($pdo, 'article', 'id', '\App\Entity\Article', [&$this->usersTable, &$this->articleCategoriesTable, &$this->categoriesTable], new \Vendors\J20Uuid(), new \Core\Lib\CryptoTools(), SODIUM_STRING_KEY, [], APP_UUID );
 		$this->categoriesTable = new \Core\Lib\DatabaseTable($pdo, 'category', 'id', '\App\Entity\Category', [&$this->articlesTable, &$this->articleCategoriesTable], new \Vendors\J20Uuid(), new \Core\Lib\CryptoTools(), SODIUM_STRING_KEY, [], APP_UUID );
 		$this->articleCategoriesTable = new \Core\Lib\DatabaseTable($pdo, 'article_category', 'categoryId','\stdClass',[], new \Vendors\J20Uuid(), new \Core\Lib\CryptoTools(), SODIUM_STRING_KEY, [], APP_UUID);
 		
 		$this->usersTable = new \Core\Lib\DatabaseTable($pdo, 'user', 'id', '\Core\Lib\User', [&$this->articlesTable, 'authorId'], new \Vendors\J20Uuid(), new \Core\Lib\CryptoTools(), SODIUM_STRING_KEY, ['name','email'], APP_UUID);
		$this->session = $session;
		
		$this->authentication = new \Core\Lib\Authentication($this->usersTable, new \Core\Lib\PasswordTools(), $this->session, 'email', 'password');

		$this->theme = $theme;	
		$this->mediasDir = $mediasDir;
		$this->medias = $medias;
		$this->urlroot = $urlroot;	
		$this->filesTools = $filesTools;

		$this->pluginsManagerController = new \Core\Lib\PluginsManager($this->theme, $this->authentication, $this->session, $this->urlroot);
		$J20Uuid = new \Vendors\J20Uuid();
		$CryptoTools = new \Core\Lib\CryptoTools();
		$this->pluginsConfigurationController = new \Core\Lib\PluginsConfiguration($this->authentication, $this->session, $this->pluginsManagerController->getPluginsAvailable(), $this->urlroot, $this->theme, $this->medias, new \Core\Lib\DatabaseTable($pdo, 'plugins', 'id', '\stdClass', ['id', 'plugin_name','params_name','params_value'], $J20Uuid, $CryptoTools, SODIUM_STRING_KEY, [], APP_UUID), $this->mediasDir,$J20Uuid, $CryptoTools, SODIUM_STRING_KEY, [], APP_UUID);
	}
	
	/**
     * To avoid $var to be changed because object should be immuable
     */
    public function __set($var,$value)
    {
        return null;
    }
    public function __call($name, $arguments)
    {
        return null;
    }
    public function __isset($name)
    {
        return null;
    }
    public function __unset($name)
    {
        return $name;
    }
    public function __get($var)
    {
        return $var;
    }


	public function getMediasDir()
	{
		return $this->mediasDir;
	}

	public function getRoutes(): array {
		$articleController = new \App\Controllers\Article($this->articlesTable, $this->usersTable, $this->categoriesTable, $this->authentication, $this->session, $this->theme, $this->urlroot);
		$categoryController = new \App\Controllers\Category($this->categoriesTable, $this->urlroot);

		$usersController = new \App\Controllers\Register($this->usersTable, $this->theme, $this->session, $this->urlroot, new \App\Controllers\AppValidator());

		$css = new \Core\Lib\Css($this->theme. '/theme/main.css', $this->theme);
		$medias = new \App\Controllers\Medias($this->mediasDir, $this->theme, $this->theme.DIRECTORY_SEPARATOR.'filesIcons', $this->urlroot, $this->session, [], [], $this->filesTools);

		$loginController = new \App\Controllers\Login(
			$this->authentication, 
			$this->session, 
			new \Core\Lib\Ban('login?sp=', dirname(dirname(dirname(__DIR__)))),
			new \Core\Lib\PostMail(
				new \Core\Lib\Token(
					$this->session, '', 3600, 
					new \Core\Lib\FilesTools(new \Vendors\J20Uuid(),FILE_SEPARATOR)
				), 
				new \App\Controllers\Mailer(), 
				$this->configuration, 
				new \Core\Lib\StringTools(),
				new \Core\Lib\Request()
			), 
			$this->urlroot, 
			$this->theme);

		$routes = [
			/////////////////
			/// Generics routes in alphabetic order
			///////////////// 
			'' => [
				'GET' => [
					'controller' => $articleController,
					'action' => 'list'
				]
			],
			'home' => [
				'GET' => [
					'controller' => $articleController,
					'action' => 'list'
				]
			],
			'assets/css/style.css' => [
				'GET' => [
					'controller' => $css,
					'action' => 'compressCss'
				]
			],
			'assets/fonts/[a-z-]*\.(eot|svg|ttf|woff|woff2)' => [
				'GET' => [
					'controller' => $css,
					'action' => 'fonts'
				]
			],
			'configuration' => [
				'GET' => [
					'controller' => $this->configuration,
					'action' => 'getConfig'
				],
				'POST' => [
					'controller' => $this->configuration,
					'action' => 'saveConfig'
				],
				'login' => true,
				'permissions' => \Core\Lib\User::CONFIG_ACCESS
			],
			'error' => [
				'GET' => [
					'controller' => $articleController,
					'action' => 'error404'
				]
			],
			'files/.*' => [
				'GET' => [
					'controller' => $medias,
					'action' => 'displayFile'
				],
				'login' => true,
			],
			'login' => [
				'GET' => [
					'controller' => $loginController,
					'action' => 'loginForm'
				],
				'POST' => [
					'controller' => $loginController,
					'action' => 'processLogin'
				]
			],
			'login/newpassword' => [
				'GET' => [
					'controller' => $loginController,
					'action' => 'newPassword'
				],
				'POST' => [
					'controller' => $loginController,
					'action' => 'sendToken'
				]
			],
			'login/finalizepassword' => [
				'GET' => [
					'controller' => $loginController,
					'action' => 'controlToken'
				],
				'POST' => [
					'controller' => $usersController,
					'action' => 'changePwd'
				]
			],
			'login/error' => [
				'GET' => [
					'controller' => $loginController,
					'action' => 'error'
				]
			],
			'login/permissionserror' => [
				'GET' => [
					'controller' => $loginController,
					'action' => 'permissionsError'
				]
			],
			'login/success' => [
				'GET' => [
					'controller' => $loginController,
					'action' => 'success'
				]
			],
			'logout' => [
				'GET' => [
					'controller' => $loginController,
					'action' => 'logout'
				]
			],
			'plugins/list' => [
				'GET' => [
					'controller' => $this->pluginsManagerController,
					'action' => 'list'
				],
				'POST' => [
					'controller' => $this->pluginsManagerController,
					'action' => 'activatePlugins'
				],
				'login' => true,
				'permissions' => \Core\Lib\User::SUPERUSER
			],
			'plugin/configuration/.*' => [
				'GET' => [
					'controller' => $this->pluginsConfigurationController,
					# Access to configuration panel for the plugin
					'action' => 'configuration'
				],
				'POST' => [
					'controller' => $this->pluginsConfigurationController,
					# Configuration recorder
					'action' => 'editConfiguration'
				],
				# User must be logged in to see the page
				'login' => true,
				# and he have to have configuration permission
				'permissions' => \Core\Lib\User::CONFIG_ACCESS
			],
			// For class Ban 
			'sp.*' => [
				'GET' => [
					'controller' => $loginController,
					'action' => 'loginForm'
				]
			],
			'users/register' => [
				'GET' => [
					'controller' => $usersController,
					'action' => 'registrationForm'
				],
				'POST' => [
					'controller' => $usersController,
					'action' => 'registerUser'
				],
				'login' => true,
				'permissions' => \Core\Lib\User::EDIT_USER_ACCESS
			],
			'unauthorized' => [
				'GET' => [
					'controller' => $articleController,
					'action' => 'error401'
				]
			],
			'users/edit' => [
				'GET' => [
					'controller' => $usersController,
					'action' => 'registrationForm'
				],
				'POST' => [
					'controller' => $usersController,
					'action' => 'editUser'
				],
				'login' => true,
				'permissions' => \Core\Lib\User::EDIT_USER_ACCESS
			],
			'users/success' => [
				'GET' => [
					'controller' => $usersController,
					'action' => 'success'
				],
				'login' => true,
				'permissions' => \Core\Lib\User::EDIT_USER_ACCESS
			],
			'users/permissions' => [
				'GET' => [
					'controller' => $usersController,
					'action' => 'permissions'
				],
				'POST' => [
					'controller' => $usersController,
					'action' => 'savePermissions'
				],
				'login' => true,
				'permissions' => \Core\Lib\User::EDIT_USER_ACCESS
			],
			'users/list' => [
				'GET' => [
					'controller' => $usersController,
					'action' => 'list'
				],
				'login' => true,
				'permissions' => \Core\Lib\User::EDIT_USER_ACCESS
			],
			/////////////////
			/// App routes
			///////////////// 
			'article/edit' => [
				'POST' => [
					'controller' => $articleController,
					'action' => 'saveEdit'
				],
				'GET' => [
					'controller' => $articleController,
					'action' => 'edit'
				],
				'login' => true,
				'permissions' => \Core\Lib\User::EDIT
			],
			'article/delete' => [
				'POST' => [
					'controller' => $articleController,
					'action' => 'delete',
				'permissions' => \Core\Lib\User::DELETE
				],
				'login' => true
			],
			'article/list' => [
				'GET' => [
					'controller' => $articleController,
					'action' => 'admin_list'
				],
				'login' => true,
				'permissions' => \Core\Lib\User::ADMIN
			],
			'category/edit' => [
				'POST' => [
					'controller' => $categoryController,
					'action' => 'saveEdit'
				],
				'GET' => [
					'controller' => $categoryController,
					'action' => 'edit'
				],
				'login' => true,
				'permissions' => \Core\Lib\User::ADMIN
			],
			'category/delete' => [
				'POST' => [
					'controller' => $categoryController,
					'action' => 'delete'
				],
				'login' => true,
				'permissions' => \Core\Lib\User::ADMIN
			],
			'category/list' => [
				'GET' => [
					'controller' => $categoryController,
					'action' => 'list'
				]
			],
			'medias/add/(pict|files)*[a-z0-9-_/]*' => [
				'GET' => [
					'controller' => $medias,
					'action' => 'new'
				],
				'POST' => [
					'controller' => $medias,
					'action' => 'addNew'
				],
				'login' => true,
				'permissions' => \Core\Lib\User::ADMIN
			],
			'medias/edit/rename/(pict|files)*[a-z0-9-_/.]*' => [
				'GET' => [
					'controller' => $medias,
					'action' => 'edit'
				],
				'POST' => [
					'controller' => $medias,
					'action' => 'rename'
				],
				'login' => true,
				'permissions' => \Core\Lib\User::ADMIN
			],
			'medias/delete' => [
				'POST' => [
					'controller' => $medias,
					'action' => 'delete'
				],
				'login' => true,
				'permissions' => \Core\Lib\User::ADMIN
			],
			'medias(/(pict|files)?/[a-z0-9-_/#]*)?' => [
				'GET' => [
					'controller' => $medias,
					'action' => 'list'
				],
				'POST' => [
					'controller' => $medias,
					'action' => 'rename'
				],
				'login' => true,
				'permissions' => \Core\Lib\User::ADMIN
			],
			'medias/add/folder' => [
				'POST' => [
					'controller' => $medias,
					'action' => 'addFolder'
				],
				'login' => true,
				'permissions' => \Core\Lib\User::ADMIN
			],
			'pict/.*' => [
				'GET' => [
					'controller' => $medias,
					'action' => 'displayPict'
				]
			],
		];

		return $routes;
	}

	public function getAuthentication(): \Core\Interfaces\Auth\Authentication {
		return $this->authentication;
	}

	public function checkPermission($permission): bool {
		$user = $this->authentication->getUser();

		if ($user && $user->hasPermission($permission)) {
			return true;
		} else {
			return false;
		}
	}

	public function getAssetsFilesPath()
	{
		return [
			$this->medias->getMediasFilesIconsPath(),
			'assets/favicons',
		];
	}

}