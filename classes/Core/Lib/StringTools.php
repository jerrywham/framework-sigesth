<?php 

namespace Core\Lib;

final class StringTools implements \Core\Interfaces\StringTools {

    private $LANG = [];

    public function __construct(){}
    
    /**
     * To avoid $var to be changed because object should be immuable
     */
    public function __set($var,$value)
    {
        return null;
    }
    public function __call($name, $arguments)
    {
        return null;
    }
    public function __isset($name)
    {
        return null;
    }
    public function __unset($name)
    {
        return $name;
    }
    public function __get($var)
    {
        return $var;
    }

    public function setLang(array $lang): array
    {
        return $this->LANG = $lang;
    }

    public function initLang(string $lang): array
    {
        $lang = str_replace('/',DIRECTORY_SEPARATOR,$lang);
        $file = dirname(dirname(dirname(__DIR__))).DIRECTORY_SEPARATOR.'lang'.DIRECTORY_SEPARATOR.$lang.'.php';
        # Generic indexes file
        if (is_file($file)) {
            include $file;
            $this->LANG = $CORE_LANG ?? array();
        }
        return $this->LANG;
    }

    public function l($value)
    {
        return $this->LANG[$value] ?? $value;
    }

    public function t(array $args,string $singular,string $plural):string
    {
        $singular = $this->LANG[$singular] ?? $singular;
        $plural = $this->LANG[$plural] ?? $plural;
        return ((isset($args['total']) && $args['total'] > 1) ? vsprintf($plural,$args) : vsprintf($singular,$args));
    }

    public function mb_ucfirst(string $str):string {
        $fc = mb_strtoupper(mb_substr($str, 0, 1));
        return $fc.mb_substr($str, 1);
    }
}