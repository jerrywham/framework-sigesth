<?php
namespace Core\Lib;

final class Configuration implements \Core\Interfaces\Configuration {

    private $config;
    private $appName = 'Sigesth';
    private $dataPath = __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.'config.php';
    private $theme = 'default';
    private $lang = 'fr/fr';
    private $mediasDir = 'datas'.DIRECTORY_SEPARATOR.'medias'.DIRECTORY_SEPARATOR;
    private $dirForIcons = 'filesIcons'.DIRECTORY_SEPARATOR;
    private $defaultJs = 'visual;prettyPrint;openNew;svgxuse';
    private $emailParams = '{}';
    private $databaseType;
    private $cryptoTools;
    private $urlroot;
    private $session;
    private $touch;

    public function __construct(string $dataPath, \Core\Interfaces\Auth\Session $session, \Core\Interfaces\CryptoTools $cryptoTools, array $config)
    {
        if (is_file($dataPath)) {
            $this->dataPath = $dataPath;
        }
        foreach ($config as $key => $value) {
            if (isset($this->$key)) {
                $this->$key = $value;
            }
        }
        $this->config = $config;

        $this->cryptoTools = $cryptoTools;
        $this->session = $session;
        $this->touch = fileatime($this->dataPath);
    }

    /**
     * To avoid $var to be changed because object should be immuable
     */
    public function __set($var,$value)
    {
        return null;
    }
    public function __call($name, $arguments)
    {
        return null;
    }
    public function __isset($name)
    {
        return null;
    }
    public function __unset($name)
    {
        return $name;
    }
    public function __get($var)
    {
        return $var;
    }

    public function getConfig()
    {      

        include $this->dataPath;

        $databaseType = file_get_contents(dirname(dirname($this->dataPath)).DIRECTORY_SEPARATOR.'DatabaseConnection.php');
        $databaseType = str_replace("config'.DIRECTORY_SEPARATOR.'",'',substr($databaseType,strpos($databaseType,'config'),(strpos($databaseType,'.ini')-strpos($databaseType,'config'))) );

        $email = json_decode($emailParams ?? $this->emailParams,true);
        $email['mp'] = $this->safeDecrypt($email['mp']);
        return [
            'dbType' => $databaseType,
            'template' => 'configuration.html.php', 
            'title' => 'L_Configuration',
            'appName' => $appName ?? $this->appName,
            'ctheme' => $theme ?? $this->theme,
            'languages' => $this->getLanguages(),
            'language' => $lang ?? $this->lang,
            'mediasDir' => $mediasDir ?? $this->mediasDir,
            'dirForIcons' => $dirForIcons ?? $this->dirForIcons,
            'defaultJs' => $defaultJs ?? $this->defaultJs,
            'emailParams' => $email,
            'token' => true
        ];
    }

    public function getInfos(string $value)
    {
        include $this->dataPath;
        if (isset($this->$value)) {
            return $this->$value;
        }
    }

    public function safeEncrypt($string)
    {
        return $this->cryptoTools->safeEncrypt($string,SODIUM_STRING_KEY);
    }
    public function safeDecrypt($string)
    {
        return $this->cryptoTools->safeDecrypt($string,SODIUM_STRING_KEY);
    }
    

    public function saveConfig()
    {
        $root = dirname(dirname(dirname(__DIR__)));
        foreach ($_POST as $key => $value) {
            if (!empty($value)) {
                $value = str_replace("\0", '', (is_string($value) ? strip_tags($value) : $value));
                switch ($key) {
                    case 'appName':
                        $this->appName = $value;
                        break;
                    case 'ctheme':
                        if (is_dir($root.DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.$value) ) {
                            $this->theme = $value;
                        }
                        break;
                    case 'lang':
                        if (strpos($value,'/') !== false 
                            && (is_dir($root.DIRECTORY_SEPARATOR.'lang'.DIRECTORY_SEPARATOR.str_replace('/',DIRECTORY_SEPARATOR, $value)) 
                                || 
                                is_dir($root.DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.$this->theme.DIRECTORY_SEPARATOR.'lang'.DIRECTORY_SEPARATOR.str_replace('/',DIRECTORY_SEPARATOR, $value)) ) ) {
                            $this->lang = $value;
                        }
                        break;
                    case 'mediasDir':
                        if (is_dir($root.DIRECTORY_SEPARATOR.str_replace('/',DIRECTORY_SEPARATOR, $value)) ) {
                            $this->mediasDir = (substr($value,-1) != DIRECTORY_SEPARATOR ? $value.DIRECTORY_SEPARATOR : $value);
                        }
                        break;
                    case 'dirForIcons':
                        if (is_dir($root.DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.$this->theme.DIRECTORY_SEPARATOR.str_replace('/',DIRECTORY_SEPARATOR, $value)) ) {
                            $this->dirForIcons = (substr($value,-1) != DIRECTORY_SEPARATOR ? $value.DIRECTORY_SEPARATOR : $value);
                        }
                        break;
                    case 'defaultJs':
                        $this->defaultJs = $value;
                        break;
                    case 'emailParams':
                        foreach ($value as $k => $v) {
                            if ($k == 'mp') {
                                $v = $this->safeEncrypt($v);
                            }
                            $value[$k] = str_replace("\0", '', strip_tags($v));
                        }
                        $this->emailParams = json_encode($value);
                        break;
                    case 'dbType':
                        if (in_array($value, ['mysql','sqlite'])) {
                            $db = "<?php\n\n\$pdo = new Core\Lib\MyPDO(ROOT_PATH.'datas'.DIRECTORY_SEPARATOR, ROOT_PATH.'includes'.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'$value.ini');\n\$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);";
                            $dbFile = dirname(dirname($this->dataPath)).DIRECTORY_SEPARATOR.'DatabaseConnection.php';

                            file_put_contents($dbFile,$db,LOCK_EX);
                            chmod($dbFile,0644);
                        }
                        break;
                }
            }
        }
        $dirConfig = str_replace('config.php','',$this->dataPath);
        
        $config = md5(file_get_contents($this->dataPath));
        if (is_file($dirConfig.$config.'.php')) {
            unlink($dirConfig.$config.'.php');
        }

        $content = "<?php\n\$appName = ".var_export($this->appName,true).";\n\$theme = ".var_export($this->theme,true).";\n\$lang = ".var_export($this->lang,true).";\n\$mediasDir = ".var_export($this->mediasDir,true).";\n\$dirForIcons = ".var_export($this->dirForIcons,true).";\n\$defaultJs = ".var_export($this->defaultJs,true).";\n\$emailParams = ".var_export($this->emailParams,true).";";

        file_put_contents($this->dataPath, $content);

        $config = md5(file_get_contents($this->dataPath));

        file_put_contents($dirConfig.$config.'.php', $content);
        
        $this->session->setMsg('L_Data_recorded_successfully');
        header('location: '.$this->urlroot.'configuration');
        exit();
        
    }

    private function getLanguages():array
    {
        $ritit = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator(dirname(dirname(dirname(__DIR__))).DIRECTORY_SEPARATOR.'lang'), \RecursiveIteratorIterator::CHILD_FIRST); 
        $r = array(); 
        foreach ($ritit as $splFileInfo) {
            if (!in_array($splFileInfo->getFilename(), ['.','..','index.html','.htaccess'])) {
                $path = $splFileInfo->isDir()
                ? array($splFileInfo->getFilename() => array()) 
                : array($splFileInfo->getFilename()); 
                for ($depth = $ritit->getDepth() - 1; $depth >= 0; $depth--) { 
                    $path = array($ritit->getSubIterator($depth)->current()->getFilename() => $path);                     
                }
                $r = array_merge_recursive($r, $path);
            }
            unset($path);
        }
        $lang = array();
        foreach ($r as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $k => $v) {
                    $lang[] = $key.'/'.strtok($v,'.');
                }
            } else {
                $lang[] = $key.'/'.strtok($v,'.');
            }
        }
        return $lang;
    }
}