<?php
namespace Core\Lib;

final class DatabaseTable implements \Core\Interfaces\DatabaseTable {
	private $pdo;
	private $table;
	private $primaryKey;
	private $className;
	private $constructorArgs;
	private $lastQuery;
	private $columnsToEncrypt = [];
	private $encryptKey;
	private $cryptoTools;
	private $J20Uuid;
	private $uuid;

	public function __construct(\Core\Interfaces\MyPDO $pdo, string $table, string $primaryKey, string $className = '\stdClass', array $constructorArgs = [], \Core\Interfaces\J20Uuid $J20Uuid, \Core\Interfaces\CryptoTools $cryptoTools, string $encryptKey = null, array $columnsToEncrypt = [], string $uuid = '') {
		$this->pdo = $pdo;
		$this->table = $table;
		$this->primaryKey = $primaryKey;
		$this->className = $className;
		$this->constructorArgs = $constructorArgs;
		$this->columnsToEncrypt = $columnsToEncrypt;
		$this->encryptKey = $encryptKey;
		$this->cryptoTools = $cryptoTools;
		$this->J20Uuid = $J20Uuid;
		$this->uuid = $uuid;

		# if uuid column is not in table, we add it (see anomynisation)
		$this->pdo->checkUUIDColumns($this->table, $this->columnsToEncrypt, $this->primaryKey);
	}
	
	/**
     * To avoid $var to be changed because object should be immuable
     */
    public function __set($var,$value)
    {
        return null;
    }
    public function __call($name, $arguments)
    {
        return null;
    }
    public function __isset($name)
    {
        return null;
    }
    public function __unset($name)
    {
        return $name;
    }
    public function __get($var)
    {
        return $this->$var;
    }

    public function getEncrypt()
    {
    	return $this->encrypt;
    }


	public function total(array $fields = [], $table2 = null, $join = null) {

		if ($table2 !== null && !in_array($join, ['INNER','LEFT','RIGHT','FULL'])) $join = 'INNER';

		$sql = 'SELECT COUNT(*) FROM `' . $this->table .( ($table2 !== null && $join !== null) ? '` '.$join.' JOIN `'. $this->table . '_'. $table2 .'` ON `'. $this->table .'`.`id` = `'. $this->table . '_'. $table2 .'`.`'. $this->table .'Id` '.$join.' JOIN `'. $table2 .'` ON `'. $this->table . '_'. $table2. '`.`' . $table2 .'Id` = `'. $table2 .'`.`id`': '`');
		$parameters = [];

		if (!empty($fields)) {
			$sql .= ' WHERE `';
			$parameters = $this->anonymise($fields);
			foreach ($parameters as $key => $value) {
				if (in_array($key, $this->columnsToEncrypt)) {
					$sql .= 'uuid_'.$key.'` = :uuid_'.$key.' AND `';
				} elseif (substr($key,0,4) != 'uuid') {
					$sql .= $key . '` = :'.$key.' AND `';
				}
			}
			$sql = substr($sql,0,-6);
		}
		
		$this->lastQuery = ['query' => $sql, 'params' => $parameters];
		$query = $this->query($sql, $parameters, __LINE__);
		$row = $query->fetch();
		return $row[0];
	}

	public function findById($value, string $select = '*') {
		$query = 'SELECT '. $select .' FROM `' . $this->table . '` WHERE `' . $this->primaryKey . '` = :value';

		$parameters = [
			'value' => $value
		];

		$this->lastQuery = ['query' => $query, 'params' => $parameters];
		$query = $this->query($query, $parameters, __LINE__);

		$results = $query->fetchObject($this->className, $this->constructorArgs);

		$results = $this->deanonymise($results);

		return $results;
	}

	public function findInAllTables(string $table2, string $select = '*', $orderBy = null, $limit = null, $offset = null, $join = 'INNER')
	{
		if (!in_array($join, ['INNER','LEFT','RIGHT','FULL'])) $join = 'INNER';

		$query = 'SELECT '. $select .' FROM `' . $this->table . '` '.$join.' JOIN `'. $this->table . '_'. $table2 .'` ON `'. $this->table .'`.`id` = `'. $this->table . '_'. $table2 .'`.`'. $this->table .'Id` '.$join.' JOIN `'. $table2 .'` ON `'. $this->table . '_'. $table2. '`.`' . $table2 .'Id` = `'. $table2 .'`.`id`';

		if ($orderBy != null) {
			$query .= ' ORDER BY ' . $orderBy;
		}

		if ($limit != null) {
			$query .= ' LIMIT ' . $limit;
		}

		if ($offset != null) {
			$query .= ' OFFSET ' . $offset;
		}
		
		$this->lastQuery = ['query' => $query, 'params' => null];
		$q = $this->query($query, [], __LINE__);
		$results = $q->fetchAll(\PDO::FETCH_CLASS, $this->className, $this->constructorArgs);
		$id = [];
		foreach ($results as $key => $result) {
			if (!in_array($result->id, $id)) {
				$id[] = $result->id;
			} else {
				unset($results[$key]);
			}
		}
		$results = $this->deanonymise($results);

		return $results;
		
	}

	public function find(array $columns, array $values, array $comparator = ['='], string $select = '*', $orderBy = null, $limit = null, $offset = null) {

		$column = '';
		$fields = array_combine($columns,$values);
		$fields = $this->anonymise($fields);
		foreach ($fields as $key => $col) {
			if (in_array($key, $this->columnsToEncrypt)) {
					$parameters['uuid_'.$key] = $fields['uuid_'.$key];
					$column .= 'uuid_'.$key.'` '.($comparator[$key]?? '=').' :uuid_'.$key.' AND `';
			} elseif (substr($key,0,4) != 'uuid') {
				$column .= $key . '` '.($comparator[$key]?? '=').' :'.$key.' AND `';
				$parameters[$key] = $col;
			}
		}
		$column = substr($column, 0 , -6);

		$query = 'SELECT '. $select .' FROM ' . $this->table . ' WHERE `' . $column;

		if ($orderBy != null) {
			$query .= ' ORDER BY ' . $orderBy;
		}

		if ($limit != null) {
			$query .= ' LIMIT ' . $limit;
		}

		if ($offset != null) {
			$query .= ' OFFSET ' . $offset;
		}

		$this->lastQuery = ['query' => $query, 'params' => $parameters];
		$query = $this->query($query, $parameters, __LINE__);

		$results = $query->fetchAll(\PDO::FETCH_CLASS, $this->className, $this->constructorArgs);
		$results = $this->deanonymise($results);

		return $results;
	}

	public function findAll(string $select = '*', $orderBy = null, $limit = null, $offset = null) {
		$query = 'SELECT '. $select .' FROM ' . $this->table;

		if ($orderBy != null) {
			$query .= ' ORDER BY ' . $orderBy;
		}

		if ($limit != null) {
			$query .= ' LIMIT ' . $limit;
		}

		if ($offset != null) {
			$query .= ' OFFSET ' . $offset;
		}

		$this->lastQuery = ['query' => $query, 'params' => null];
		$result = $this->query($query, [], __LINE__);

		$results = $result->fetchAll(\PDO::FETCH_CLASS, $this->className, $this->constructorArgs);
		$results = $this->deanonymise($results);

		return $results;
	}

	public function delete($id) {
		$parameters = [':id' => $id];
		$query = 'DELETE FROM `' . $this->table . '` WHERE `' . $this->primaryKey . '` = :id';

		$this->lastQuery = ['query' => $query, 'params' => $parameters];
		$this->query($query, $parameters, __LINE__);
	}

	public function deleteWhere($column, $value) {

		if (in_array($column, $this->columnsToEncrypt)) {
			$value = $this->J20Uuid->v5($this->uuid,$value);
			$column = 'uuid'; 
		}

		$query = 'DELETE FROM `' . $this->table . '` WHERE `' . $column . '` = :value';

		$parameters = [
			'value' => $value
		];

		$this->lastQuery = ['query' => $query, 'params' => $parameters];
		$query = $this->query($query, $parameters, __LINE__);
	}

	public function save($record) {
		$entity = new $this->className(...$this->constructorArgs);

		if (is_object($record)) {
			$record = json_decode(json_encode($record),true);
		}

		try {
			if ($record[$this->primaryKey] == '') {
				unset($record[$this->primaryKey]);
			}
			$insertId = $this->insert($record);

			$entity->{$this->primaryKey} = $insertId;
		}
		catch (\PDOException $e) {
			$this->update($record);
		}

		foreach ($record as $key => $value) {
			if (!empty($value)) {
				$entity->$key = $value;	
			}			
		}

		return $entity;	
	}

	public function lastQuery()
	{
		return $this->lastQuery;
	}

	private function query($sql, $parameters = [], $ligne = null) {
		$query = $this->pdo->prepare($sql);
		$query->execute($parameters);
		return $query;
	}	

	private function insert($fields) {
		$query = 'INSERT INTO `' . $this->table . '` (';
		$fields = $this->anonymise($fields);

		foreach ($fields as $key => $value) {
			$query .= '`' . $key . '`,';
		}

		$query = rtrim($query, ',');

		$query .= ') VALUES (';

		foreach ($fields as $key => $value) {
			$query .= ':' . $key . ',';
		}

		$query = rtrim($query, ',');

		$query .= ')';

		$fields = $this->processDates($fields);

		$this->lastQuery = ['query' => $query, 'params' => $fields];
		$this->query($query, $fields, __LINE__);

		return $this->pdo->lastInsertId();
	}

	private function update($fields) {
		$query = ' UPDATE `' . $this->table .'` SET ';
		$fields = $this->anonymise($fields);

		foreach ($fields as $key => $value) {
			$query .= '`' . $key . '` = :' . $key . ',';
		}

		$query = rtrim($query, ',');

		$query .= ' WHERE `' . $this->primaryKey . '` = :primaryKey';

		//Set the :primaryKey variable
		$fields['primaryKey'] = $fields[$this->primaryKey];

		$fields = $this->processDates($fields);

		$this->lastQuery = ['query' => $query, 'params' => $fields];
		$this->query($query, $fields, __LINE__);
	}

	private function processDates($fields) {
		foreach ($fields as $key => $value) {
			if ($value instanceof \DateTime) {
				$fields[$key] = $value->format('Y-m-d');
			}
		}
		return $fields;
	}

	private function anonymise(array $fields)
	{
		foreach ($fields as $key => $value) {
			if (in_array($key, $this->columnsToEncrypt)) {
				$fields[$key] = $this->cryptoTools->safeEncrypt($value,$this->encryptKey); 
				if (!array_key_exists('uuid_'.$key,$fields)) {
					$fields['uuid_'.$key] = $this->J20Uuid->v5($this->uuid,$value);
				}
			}
		}
		return $fields;
	}

	private function deanonymise($results)
	{
		if (is_array($results)) {
			foreach ($results as $key => $result) {
				if (is_object($result)) {
					foreach ($result as $k => $col) {
						if (in_array($k, $this->columnsToEncrypt)) {
							$results[$key]->$k = $this->cryptoTools->safeDecrypt($col,$this->encryptKey);
						}
						if (strpos($k,'date') !== false) {
							$results[$key]->$k = $this->pdo->formatDate($col,'d/m/Y');
						}
					}
				} elseif (is_array($result)) {
					foreach ($result as $k => $col) {
						if (in_array($k, $this->columnsToEncrypt)) {
							$results[$key][$k] = $this->cryptoTools->safeDecrypt($col,$this->encryptKey);
						}
						if (strpos($k,'date') !== false) {
							$results[$key][$k] = $this->pdo->formatDate($col,'d/m/Y');
						}
					}
				} else {
					if (in_array($key, $this->columnsToEncrypt)) {
						$results[$key] = $this->cryptoTools->safeDecrypt($result,$this->encryptKey);
					}
					if (strpos($key,'date') !== false) {
						$results[$key] = $this->pdo->formatDate($result,'d/m/Y');
					}
				}
			}
		}
		if (is_object($results)) {
			foreach ($results as $key => $result) {
				if (is_object($result)) {
					foreach ($result as $k => $col) {
						if (in_array($k, $this->columnsToEncrypt)) {
							$results->$key->$k = $this->cryptoTools->safeDecrypt($col,$this->encryptKey);
						}
						if (strpos($k,'date') !== false) {
							$results->$key->$k = $this->pdo->formatDate($col,'d/m/Y');
						}
					}
				} elseif (is_array($result)) {
					foreach ($result as $k => $col) {
						if (in_array($k, $this->columnsToEncrypt)) {
							$results->$key[$k] = $this->cryptoTools->safeDecrypt($col,$this->encryptKey);
						}
						if (strpos($k,'date') !== false) {
							$results->$key[$k] = $this->pdo->formatDate($col,'d/m/Y');
						}
					}
				} else {
					if (in_array($key, $this->columnsToEncrypt)) {
						$results->$key = $this->cryptoTools->safeDecrypt($result,$this->encryptKey);
					}
					if (strpos($key,'date') !== false) {
						$results->$key = $this->pdo->formatDate($result,'d/m/Y');
					}
				}
			}
		}
		return $results;
	}
}