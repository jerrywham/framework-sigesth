<?php
namespace Core\Lib;

final class MyPDO extends \PDO implements \Core\Interfaces\MyPDO 
{
    private $driver;
    private $host;
    private $port;
    private $dbname;
    private $databaseDir;

    public function __construct(string $databaseDir,string $file = 'my_setting.ini')
    {
        if (!$settings = parse_ini_file($file, TRUE)) throw new exception('Unable to open ' . $file . '.');

        $this->databaseDir = $databaseDir;

        $this->driver = $settings['database']['driver'];
        $this->host = $this->driver != 'sqlite' ? ':host=' . $settings['database']['host']  : ':'. $this->realpath($settings['database']['host']);
        $this->port = isset($settings['database']['port']) ? ';port=' . $settings['database']['port'] : '';
        $this->dbname = isset($settings['database']['schema']) ? ';dbname=' . $settings['database']['schema'] . ($this->driver != 'sqlite' ?  ';charset=utf8' : '') : '';


        $dns = $this->driver . $this->host . $this->port . $this->dbname;
        
        parent::__construct($dns, $settings['database']['username'] ?? 'charset=UTF-8', $settings['database']['password'] ?? null);
    }

    public function checkUUIDColumns(string $table,array $columnsToEncrypt,string $primaryKey)
    {
        $uuidPresent = false;
        $uuidMustBePresent = false;

        $sql = $this->getTableSchema($table);
        $query = $this->prepare($sql[0]);
        $query->execute($sql[1]);
        $cols = $query->fetchAll(\PDO::FETCH_OBJ);

        foreach ($cols as $key => $col) {
            switch ($this->driver) {
                case 'mysql':
                    if (in_array(substr($col->COLUMN_NAME,5),$columnsToEncrypt) ) {
                        $uuidPresent = true;
                    }
                    if (in_array($col->COLUMN_NAME, $columnsToEncrypt)) {
                        $uuidMustBePresent = true;
                    }
                    break;
                case 'sqlite':
                    if (in_array(substr($col->name,5),$columnsToEncrypt) ) {
                        $uuidPresent = true;
                    }
                    if (in_array($col->name, $columnsToEncrypt)) {
                        $uuidMustBePresent = true;
                    }
                    break;
                
                default:
                    break;
            }
        }
        if ($uuidPresent === false && $uuidMustBePresent === true) {
            foreach ($columnsToEncrypt as $key => $value) {
                $query = $this->prepare('ALTER TABLE `'.$table.'` ADD `uuid_'.$value.'` VARCHAR(36) NOT NULL DEFAULT ""'.($this->driver != 'sqlite' ? ' AFTER `'.$primaryKey.'`' : ''));
                $query->execute([]);
            }
        }
    }

    public function prepare($sql, $options = null)
    {
        if ($this->driver == 'sqlite') {
            if (strpos($sql,'DATE_FORMAT') !== false) {
                $SQL = explode(',',$sql);
                foreach ($SQL as $key => $value) {
                    if (strpos($value,'DATE_FORMAT')) {
                        $timestring = str_replace('DATE_FORMAT(','',$value);
                        $v = substr($value,0,strpos($value,'(')+1); 
                        $SQL[$key] = str_replace('DATE_FORMAT(',$timestring,$v); 
                        unset($SQL[$key+1]);
                    }
                }
                $sql = implode(',',$SQL);
            }
        }
        return parent::prepare($sql);
    }

    public function formatDate($date, string $format = '')
    {
        if ($this->driver == 'sqlite') {
            if ($this->validateDate($date,'d/m/Y') !== false) {
                $d = new \DateTime();
                $aDate = explode('/',$date);
                $d->setDate($aDate[2], $aDate[1], $aDate[0]);
            } elseif ($date instanceof \DateTime) {
                $d = $date;
            } elseif ($this->validateDate($date,'Y-m-d')) {
                $d = new \DateTime($date);
            }
            if ($format == '') {
                return $d;
            } else {
                return $d->format($format);
            }
        }
        return $date;
    }

    private function validateDate($date, $format = 'Y-m-d H:i:s') 
    {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    private function getTableSchema(string $table='')
    {
        switch ($this->driver) {
            case 'mysql':
                $query = 'SELECT COLUMN_NAME, DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = :table';
                $params = [':table' => $table];
                break;
            case 'sqlite':
                $query = 'PRAGMA table_info('.$table.')';
                $params = [];
                break;
            default :
                $query = '';
                $params = [];
                break;
        }
        return [$query,$params];
    }

    private function realpath($path)
    {
        return str_replace(['./','/'],[$this->databaseDir,DIRECTORY_SEPARATOR],$path);
    }
}
