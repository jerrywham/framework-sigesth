<?php
namespace Core\Lib;

final class PluginsConfiguration implements \Core\Interfaces\PluginsConfiguration {

    private $authentication;
    private $session;
    private $pluginsTable;
    private $urlroot;
    private $theme;
    private $medias;
    private $defaultParams = [];
    private $id;
    private $plugin_name;
    private $plugin;
    private $params_name;
    private $params_value;
    private $pluginsAvailable;
    private $J20Uuid;
    private $cryptoTools;
    private $encryptKey;
    private $columnsToEncrypt;
    private $uuid;

    public function __construct(\Core\Interfaces\Auth\Authentication $authentication, \Core\Interfaces\Auth\Session $session, $pluginsAvailable, string $urlroot, string $theme, \App\Interfaces\Medias $medias, \Core\Interfaces\DatabaseTable $pluginsTable, string $mediasDir, \Core\Interfaces\J20Uuid $J20Uuid, \Core\Interfaces\CryptoTools $cryptoTools, string $encryptKey = null, array $columnsToEncrypt = [], string $uuid = '')
    {
        include dirname(dirname(dirname(__DIR__))).DIRECTORY_SEPARATOR.'includes'.DIRECTORY_SEPARATOR.'DatabaseConnection.php';

        $this->pluginsAvailable = $pluginsAvailable;
        $this->plugin_name = $this->getPlugin();
        $this->authentication = $authentication;
        $this->session = $session;
        $this->urlroot = $urlroot;
        $this->medias = $medias;
        $this->J20Uuid = $J20Uuid;
        $this->cryptoTools = $cryptoTools;
        $this->encryptKey = $encryptKey;
        $this->columnsToEncrypt = $columnsToEncrypt;
        $this->uuid = $uuid;
        if ($this->plugin_name != null) {
            $pluginClassName = '\Plugins\\'.$this->plugin_name.'\\'.$this->plugin_name;
            
            $this->pluginsTable = new \Core\Lib\DatabaseTable(
                $pdo, 
                'plugins', 
                'id', 
                $pluginClassName, 
                [
                    $this->session->getLang(), 
                    $theme,
                    $this->getHooks(), 
                    $this->authentication, 
                    $this->session, 
                    $this->urlroot,
                    $this->medias,
                    &$pluginsTable, 
                    $mediasDir,
                    'id', 
                    'plugin_name',
                    'params_name', 
                    'params_value'
                ],
                $this->J20Uuid,
                $this->cryptoTools,
                $this->encryptKey,
                $this->columnsToEncrypt,
                $this->uuid
            );

            $this->plugin = new $pluginClassName(
                $this->session->getLang(), 
                $theme,
                $this->getHooks(), 
                $this->authentication, 
                $this->session, 
                $this->urlroot,
                $this->medias, 
                $this->pluginsTable,
                $mediasDir
            );
        }
    }
    /**
     * To avoid $var to be changed because object should be immuable
     */
    public function __set($var,$value)
    {
        return null;
    }
    public function __call($name, $arguments)
    {
        return null;
    }
    public function __isset($name)
    {
        return null;
    }
    public function __unset($name)
    {
        return $name;
    }
    public function __get($var)
    {
        return $var;
    }


    private function getPlugin()
    {
        foreach (array_keys($_GET) as $key => $value) {
            if (strpos($value,'plugin/configuration') !== false) {
                $plugin = str_replace('plugin/configuration/','',$value);
                if (array_key_exists($plugin,$this->pluginsAvailable)) {
                    return $plugin;
                } else {
                    http_response_code(404);
                    header('location: '.$this->urlroot.'error');
                    exit();
                }
            } 
        }   
    }

    private function getHooks()
    {
        $hooks = array();
        $hooksFile = dirname(dirname(__DIR__)).DIRECTORY_SEPARATOR.'Plugins'.DIRECTORY_SEPARATOR.$this->plugin_name.DIRECTORY_SEPARATOR.'hooks.php';
        if (is_file($hooksFile))
        {
            include $hooksFile;
        }
        return $hooks;
    }

    
    private function getConfig()
    {
        $c = $this->pluginsTable->find(['plugin_name'],[$this->plugin_name]);
        $config = [];
        $defaultConfig = $this->plugin->defaultParams;            
        if (empty($c)) {
            foreach ($defaultConfig as $key => $param) {
                $c[$key]['params_name'] = $param['params_name'];
                $c[$key]['params_value'] = (is_bool($param['params_value'])) ? intval($param['params_value']): json_encode($param['params_value'],JSON_PRETTY_PRINT);
                $c[$key]['params_mandatory'] = $param['params_mandatory'];
            }
            $config = $c;
        } else {
            foreach ($c as $key => $param) {
                $config[$param->params_name]['id'] = $param->id;
                $config[$param->params_name]['params_name'] = $param->params_name;
                $json = json_decode($param->params_value); 
                if (json_last_error() === JSON_ERROR_NONE) {
                    $config[$param->params_name]['params_value'] = $param->params_value;
                } else {
                    $config[$param->params_name]['params_value'] = '"'.$param->params_value.'"';
                }
                $config[$param->params_name]['params_mandatory'] = $defaultConfig[$param->params_name]['params_mandatory']; 
            }
        }
        return $config;
    }

    public function getParam($param)
    {
        $c = $this->getConfig();
        if (array_key_exists($param,$c)) {
            return json_decode($c[$param]['params_value'],true);
        } else {
            return null;
        }
    }

    /**
     * Method to display configuration panel
     */
    public function configuration():array
    {
        $reflected = new \ReflectionObject($this->plugin);
        if( $reflected->hasConstant('configuration') === false) {
            http_response_code(404);
            header('location: '.$this->urlroot.'error');
            exit();
        }
        $config = $this->getConfig();

        return [
            'template' => 'plugins.configuration.html.php',
            'title' => 'L_Plugin_configuration : '.$this->plugin_name,
            'token' => true,
            'config' => $config,
            'plugin' => $this->plugin_name
        ];
    }

    /**
     * Method to save or edit configuration
     */
    public function editConfiguration()
    {
        $data = $_POST['config'];

        //Assume the data is valid to begin with
        $valid = true;
        $errors = [];
        foreach ($data as $key => $param) {
            //But if any of the fields have been left blank, set $valid to false
            if ($param['params_value'] == '' && $param['params_mandatory'] == 1) {
                $valid = false;
                $errors[] = 'L_Param_cannot_be_blank : '.$param['params_name'];
            } else {
                $param['params_value'] = strip_tags($param['params_value']);
                if (is_array($this->plugin->__get('defaultParams')[$param['params_name'] ]['params_value'] ) ) {
                    $json = json_decode($param['params_value']); 
                    switch (json_last_error()) {
                            case JSON_ERROR_NONE:
                                'No errors';
                            break;
                            case JSON_ERROR_DEPTH:
                                $valid = false;
                                $errors[] = 'L_Param "'.$param['params_name'].'" : L_Maximum_stack_depth_exceeded';
                            break;
                            case JSON_ERROR_STATE_MISMATCH:
                                $valid = false;
                                $errors[] = 'L_Param "'.$param['params_name'].'" : L_Underflow_or_the_modes_mismatch';
                            break;
                            case JSON_ERROR_CTRL_CHAR:
                                $valid = false;
                                $errors[] = 'L_Param "'.$param['params_name'].'" : L_Unexpected_control_character_found';
                            break;
                            case JSON_ERROR_SYNTAX:
                                $valid = false;
                                $errors[] = 'L_Param "'.$param['params_name'].'" : L_Syntax_error_malformed_JSON';
                            break;
                            case JSON_ERROR_UTF8:
                                $valid = false;
                                $errors[] = 'L_Param "'.$param['params_name'].'" : L_Malformed_UTF-8_characters_possibly_incorrectly_encoded';
                            break;
                            default:
                                $valid = false;
                                $errors[] = 'L_Param "'.$param['params_name'].'" : L_Unknown_error';
                            break;
                    }
                }
            }
        }
        //If $valid is still true, no fields were blank and the data can be added
        if ($valid == true) {
            foreach ($data as $key => $param) {
                unset($param['params_mandatory']);
                $param['plugin_name'] = $this->plugin_name;
                $param['id'] = ($param['id'] ?? '');
                $this->pluginsTable->save($param);
            }
        }
        else {
            //If the data is not valid, show the form again
            return ['template' => 'plugins.configuration.html.php', 
                    'title' => 'L_Plugin_configuration_page : '.$this->plugin_name,
                    'token' => true,
                    'errors' => $errors,
                    'config' => $data,
                    'plugin' => $this->plugin_name,
                   ]; 
        }
        $this->session->setMsg('L_Data_recorded_successfully');
        header('Location: '.$this->urlroot.'plugin/configuration/'.$this->plugin_name);
        exit();        
    }
}