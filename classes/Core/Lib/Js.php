<?php
namespace Core\Lib;

final class Js implements \Core\Interfaces\JsPaker {

    private $files = array();
    private $paker;
    private $encoding;
    private $fastDecode;
    private $specialChars;
    private $theme;

    public function __construct(array $files, string $paker, string $theme, int $_encoding = 62, bool $_fastDecode = true, bool $_specialChars = false)
    {
        $this->files = $files;
        $this->paker = $paker;
        $this->theme = $theme;
        $this->encoding = $_encoding;
        $this->fastDecode = $_fastDecode;
        $this->specialChars = $_specialChars;
    }
    
    /**
     * To avoid $var to be changed because object should be immuable
     */
    public function __set($var,$value)
    {
        return null;
    }
    public function __call($name, $arguments)
    {
        return null;
    }
    public function __isset($name)
    {
        return null;
    }
    public function __unset($name)
    {
        return $name;
    }
    public function __get($var)
    {
        return $var;
    }


    public function pack($minification = true): string
    {
        $js = '';
        foreach ($this->files as $key => $file) {
            if (is_array($file)) {
                foreach ($file as $key => $content) {
                   $js .= $content."\n";
                }
            } elseif (is_file($file)) {
                $content = file_get_contents($file);
                $js .= $content."\n";
            }
        }
        return $this->recordJs($js,$minification);
    }

    public function recordJs(string $js,bool $minification = false): string
    {
        if ($minification === true) {
            $js = $this->minification($js);
        }
        $f = md5($js).'.js';
        if (!is_file($this->theme .'/javascript/packed/'.$f)) {
            file_put_contents($this->theme .'/javascript/packed/'.$f,$js);
        }
        return $f;
    }

    private function minification(string $js): string
    {
        $paker = new $this->paker($js, $this->encoding, $this->fastDecode, $this->specialChars);
        return $paker->pack($js);
    }
}