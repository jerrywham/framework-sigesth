<?php
namespace Core\Lib;

final class User {

	const EDIT = 1;
	const DELETE = 2;
	const ADD = 4;
    const EDIT_USER_ACCESS = 8;
    const ADMIN = 16;
	const CONFIG_ACCESS = 32;
	const SUPERUSER = 64;

    # If you need to use a column from your users DB, a variable with the same name must be defined
	public $id;
	public $name;
	public $email;
	public $password;

	private $table;
	private $userIdColumn;
    private $permissions;

	public function __construct(\Core\Interfaces\DatabaseTable $table, string $userIdColumn) {
		$this->table = $table;
		$this->userIdColumn = $userIdColumn;
	}
	
    /**
     * To avoid $var to be changed because object should be immuable
     */
    public function __set($var,$value)
    {
        return null;
    }
    public function __call($name, $arguments)
    {
        return null;
    }
    public function __isset($name)
    {
        return null;
    }
    public function __unset($name)
    {
        return $name;
    }
    public function __get($var)
    {
        return $var;
    }


	public function getData() {
		return $this->table->find([$this->userIdColumn], [$this->id]);
	}

	public function addData($data) {
		$data[$this->userIdColumn] = $this->id;

		return $this->table->save($data);
	}

	public function hasPermission($permission) {
		return $this->permissions & $permission;  
	}
}