<?php
namespace Core\Lib;

final class Session implements \Core\Interfaces\Auth\Session {

    private $MsgId;
    private $sessionName;
    private $request;
    private $lang;
    private $types = ['primary','success','warning','danger','info','inverse','ghost','error'];

    public function __construct(\Core\Interfaces\Request $request, string $sessionName = '', string $lang, array $types = []) {
        $this->sessionName = $sessionName;
        $this->request = $request;
        $this->lang = $lang;
        if (!empty($types)) {
            $this->types = $types;
        }

        // Force cookie path (but do not change lifetime)
        $cookie = session_get_cookie_params();
        // Default cookie expiration and path.
        $cookiedir = '';
        if (dirname($this->request->getServer('SCRIPT_NAME', ''))!='/') {
            $cookiedir = dirname(dirname($this->request->getServer('SCRIPT_NAME', ''))).'/';
        }
        // To reduce risk of session ID injection
        ini_set('session.referer_check', $this->request->getSchemeAndHttpHost());
        // Make sure HTTP contents are not cached for authenticated session. Allow caching only when contents are not private.
        ini_set('session.cache_limiter', 'nocache');
        if (PHP_VERSION >= '7.1.0') {
            // Longer session ID results in stronger session ID
            ini_set('session.sid_length', 48);
            // The more bits in a session ID char, session module generates stronger session ID for the same session ID length.
            ini_set('session.sid_bits_per_character', 6);
        }
        if (PHP_VERSION < '7.1.0') {
            // Stronger hash function will generates stronger session ID. 
            ini_set('session.hash_function', 'sha256');
        }
        
        $ssl = true;
        if ($this->request->getServer('HTTPS','') !== 'on') {
            $ssl = false;
        } else {
            // Allows access to session ID cookie only when protocol is HTTPS.
            ini_set('session.cookie_secure', 1);
        }
        session_set_cookie_params($cookie['lifetime'], $cookiedir, $this->request->getServer('HTTP_HOST'), $ssl);

        # On Debian based system, a cronjob script exists to clean session because the Debian package maintainers of PHP
        # believed that the solution for cleaning up session data in PHP is insecure.
        # To avoid "ps_files_cleanup_dir: opendir(/var/lib/php/sessions) failed: Permission denied (13)" warning, the setting
        # session.gc_probability should be set to 0 instead of normally 1 in others systems.
        if (is_file('/usr/lib/php/sessionclean')) {
            ini_set('session.gc_probability', 0);
        }

        // Browsers not to store cookie to permanent storage.
        ini_set('session.cookie_lifetime', 0);
        // Use cookies to store session.
        ini_set('session.use_cookies', 1);
        // This prevents session module to use uninitialized session ID.
        ini_set('session.use_strict_mode', 1);
        // Disallow access to session cookie by JavaScript.
        ini_set('session.cookie_httponly', 1);
        // Force cookies for session  (phpsessionID forbidden in URL)
        ini_set('session.use_only_cookies', 1);
        if (phpversion() >= '7.3') {
            ini_set('session.cookie_samesite', 'Strict');
        }
        // Prevent php to use sessionID in URL if cookies are disabled.
        ini_set('session.use_trans_sid', 0);
        
        $this->start();
    }

    /**
     * To avoid $var to be changed because object should be immuable
     */
    public function __set($var,$value)
    {
        return null;
    }
    public function __call($name, $arguments)
    {
        return null;
    }
    public function __isset($name)
    {
        return null;
    }
    public function __unset($name)
    {
        return $name;
    }
    public function __get($var)
    {
        return $var;
    }

    public function getLang()
    {
        return $this->lang;
    }

    public function start() {
        if ($this->isActive() === false) {
            if (!empty($this->sessionName) ) session_name($this->sessionName);
            session_start();
        }
    }

    public function get($key) {
        $this->autostart();
        if ($key){
            if (strpos($key, '.') !== false) {
                $k = explode('.', $key);
                foreach ($k as $cle => $v) {
                    if ($cle == 0) {
                        $key = $v;
                    } else {
                        if (isset($_SESSION[$key][$v])){
                            return $_SESSION[$key][$v];
                        }else{
                            return false;
                        }
                    }
                }
            } else {
                if (isset($_SESSION[$key])){
                    return $_SESSION[$key];
                }else{
                    return false;
                }
            }
        } else {
            return $_SESSION;
        }
    }

    /**
     * Méthode permettant de lire un ou tous les index du tableau de session
     * @param $key string la clé de l'index à lire
     * 
     * @return mixed
     *
     * @author Cyril MAGUIRE
     */
    public function read($key=null) {
        $this->autostart();
        if ($key){
            if (strpos($key, '.') !== false) {
                $k = explode('.', $key);
                foreach ($k as $cle => $v) {
                    if ($cle == 0) {
                        $key = $v;
                    } else {
                        if (isset($_SESSION[$key][$v])){
                            return $_SESSION[$key][$v];
                        }else{
                            return false;
                        }
                    }
                }
            } else {
                if (isset($_SESSION[$key])){
                    return $_SESSION[$key];
                }else{
                    return false;
                }
            }
        } else {
            return $_SESSION;
        }
    }

    public function set($key, $value) {
        $this->autostart();
        if (strpos($key, '.') !== false) {
            $k = explode('.', $key);
            foreach ($k as $cle => $v) {
                if ($cle == 0) {
                    $key = $v;
                } else {
                    $_SESSION[$key][$v] = $value;
                }
            }
        } else {
            $_SESSION[$key] = $value;
        }   
    }

    public function remove($keys) {
        $token = isset($_SESSION['formtoken']) ? $_SESSION['formtoken'] : null;
        if (is_string($keys) && strpos($keys, '.') !== false) {
            $k = explode('.',$keys);
            foreach ($k as $cle => $v) {
                if ($cle == 0) {
                    $keys = $v;
                } else {
                    if (isset($_SESSION[$keys][$v])) {
                        unset($_SESSION[$keys][$v]);
                    }
                }
            }
        } elseif (!empty($keys)) {
            if (is_string($keys)) {
                $keys = [$keys];
            }
            foreach ($keys as $key) {
                if (isset($_SESSION[$key])) {
                    unset($_SESSION[$key]);
                }
            }
        } else { 
            $_SESSION = array();
            session_destroy();
            session_start();
        }
        $_SESSION['formtoken'] = $token;
    }

    public function isActive() {
        if ( php_sapi_name() !== 'cli' ) {
            if ( version_compare(phpversion(), '5.4.0', '>=') ) {
                return session_status() === PHP_SESSION_ACTIVE ? true : false;
            } else {
                return session_id() === '' ? false : true;
            }
        }
        return false;
    }

    public function setMsg($msg,$type = 'success') {
        $this->autostart();
        $_SESSION['msg'] = array(
            'msg' => $msg,
            'type' => $type,
            'id' => mt_rand(1,50)
        );
        return true;
    }

    public function msg():array {
        $this->autostart();
        $r = array(
            'type' => false,
            'id' => false,
            'msg' => false
        );
        if (isset($_SESSION['msg']['msg'])) {
            $r = array(
                'type' => $_SESSION['msg']['type'],
                'id' => $_SESSION['msg']['id'],
                'msg' => $_SESSION['msg']['msg']
            );
            $this->MsgId = 'id_'.$_SESSION['msg']['id'];
            $_SESSION['msg'] = array();
        }
        return $r;
    }

    public function alert(array $msg,string $theme, string $alertFile):string {
        if (in_array($msg['type'], $this->types) ) {
            ob_start();
            if (is_file($theme . DIRECTORY_SEPARATOR. $alertFile)) {
                include  $theme . DIRECTORY_SEPARATOR. $alertFile;
            }
            return ob_get_clean();
        }
        return '';
    }

    public function getServer($server = null, $default = null) {
        return $this->request->getServer($server, $default);
    }

    public function getRequest() {
        return $this->request;
    }

    public function isLoggedIn():bool {

        if (!(bool)$this->get('uid')
            || ($this->get('ip') !== $this->allIPs())
            || time() >= $this->get('expires_on')) {
            $this->remove('');
            return false;
        }
        // User accessed a page : Update his/her session expiration date.
        $this->set('expires_on', time() + ini_get('session.gc_maxlifetime') );
        if (!empty($this->get('longlastingsession'))) {
            $this->set('expires_on', $this->get('expires_on') + $this->get('longlastingsession'));
        }

        return true;
    }

    private function autostart() {
        if ($this->isActive() === false){
            $this->start();
        }
    }

    private function allIPs() {
        return $this->getServer('REMOTE_ADDR','')
            .'_'.$this->getServer('HTTP_X_FORWARDED_FOR', '')
            .'_'.$this->getServer('HTTP_CLIENT_IP', '');
    }


}
