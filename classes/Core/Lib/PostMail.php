<?php

namespace Core\Lib;

final class PostMail implements \Core\Interfaces\PostMail {

    private $token;
    private $mailer;
    private $configuration;
    private $stringTools;
    private $request;

    public function __construct(\Core\Interfaces\Token\Token $token, \Core\Interfaces\Mailer $mailer, \Core\Interfaces\Configuration $configuration, \Core\Interfaces\StringTools $stringTools, \Core\Interfaces\Request $request)
    {
        $this->token = $token;
        $this->mailer = $mailer;
        $this->configuration = $configuration;
        $this->stringTools = $stringTools;
        $this->request = $request;
    }

    public function sendTo(string $email)
    {
        $this->stringTools->initLang($this->configuration->getInfos('lang'));
        $infos = json_decode($this->configuration->getInfos('emailHost'), true);
        $link = $this->request->getSchemeAndHttpHost().'/'.str_replace(DIRECTORY_SEPARATOR,'/',DIR_ROOT).'login/finalizepassword?token='.$this->token->getTokenEmailMethod();

        $body = '<h1>'.$this->stringTools->l('L_New_password').'</h1>';
        $body .= '<p>'.$this->stringTools->l('L_New_password_email_message').'</p>';
        $body .= '<p>'.$this->stringTools->l('L_New_password_email_message_sent_by_error').' <a href="mailto:'.$infos['email'].'">'.$infos['email'].'</a></p>';
        $body .= '<p>'.$this->stringTools->l('L_New_password_email_message_ok').'</p><p><a href="'.$link.'">'.$link.'</a></p>';

        $mail = $this->mailer->getMailer(true);                             // Passing `true` enables exceptions


        $mail->SMTPDebug = $infos['PHPMailerDebug'];                        // Enable verbose debug output

        $mail->CharSet = 'UTF-8';
        $mail->isSMTP();   
        $mail->AuthType = 'LOGIN';                                          // Set mailer to use SMTP
        $mail->Host = $infos['host'];                                       // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                                             // Enable SMTP authentication
        $mail->Username = $infos['username'];                                  // SMTP username
        $mail->Password = $this->configuration->safeDecrypt($infos['mp']);  // SMTP password
        $mail->SMTPSecure = $infos['SMTPSecure'];                           // Enable TLS encryption, `ssl` also accepted
        $mail->SMTPAutoTLS = false;
        $mail->Port = $infos['port'];                                       // TCP port to connect to


        $mail->setFrom($infos['no-reply'], $this->configuration->getInfos('appName'));
        $mail->addAddress($email);                                          // Add a recipient // Name is optional
        $mail->addReplyTo($infos['no-reply'], 'No reply');

        // $mail->addAttachment('/var/tmp/file.tar.gz');                    // Add attachments
        // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');               // Optional name
        $mail->isHTML(true);                                                // Set email format to HTML

        $mail->Subject = $this->stringTools->l('L_New_password');
        $mail->Body    = $body;
        $mail->AltBody = strip_tags(str_replace(array(
                '<h1>','</h1>','<p>','</p>','<ul>','</ul>','<li>','<li style="color:red">','</li>','<hr/>'
            ),array(
                '## ',"\n",'',"\n",'','','* ','* ',"\n",''
            ),$body));
        try {
            $mail->send();
            $return =true;
        } catch (Exception $e) {
            $return = false;
        }
        return $return;
    }
}