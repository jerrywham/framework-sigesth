<?php
namespace Core\Lib;

final class Css {

    private $cssFile;
    private $cssFinal;
    private $variables;
    private $theme;

    public function __construct(string $cssOrigin, string $theme, array $variables = array())
    {
        if (is_file($cssOrigin)) {
            $finfo = new \finfo(FILEINFO_MIME_TYPE);
            if($finfo->file($cssOrigin) == 'text/css' || $finfo->file($cssOrigin) == 'text/plain') {
                $this->cssFile = $cssOrigin;
            } else {
                throw new \Exception("Error: no css file", 1);
            }
        } else {
            throw new \Exception("Error: no css file", 1);
        }
        $this->theme = $theme;
        $this->variables = $variables;
    }
    
    /**
     * To avoid $var to be changed because object should be immuable
     */
    public function __set($var,$value)
    {
        return null;
    }
    public function __call($name, $arguments)
    {
        return null;
    }
    public function __isset($name)
    {
        return null;
    }
    public function __unset($name)
    {
        return $name;
    }
    public function __get($var)
    {
        return $var;
    }

    public function fonts()
    {
        $route = $this->realRoute('fonts');
        if (is_file($this->theme.DIRECTORY_SEPARATOR.$route)) {
            include_once $this->theme.DIRECTORY_SEPARATOR.$route;
            exit();
        }
    }

    private function realRoute($route):string
    {
        foreach ($_GET as $key => $value) {
            if (strpos($key,$route) !== false) {
                $e = substr($key,strrpos($key,'_')+1);
                return str_replace(['assets/',$route.'/'.$route,'_'.$e],['',$route.'/','.'.$e],$key);
            }
        }
        return '';
    }


    public function compressCss()
    {
        # On inclue le contenu et on le formate (suppression espaces, sauts de ligne, commentaires )
        $css = file_get_contents($this->cssFile);
        // Suppression des commentaires
        $css = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $css);

        // Importation du contenu de fichiers à inclure (syntaxe APACHE)
        preg_match_all('!\<\!\-\-\#(?:include virtual)\=\"([a-z\/\.]*\.css)\" \-\-\>!i',$css,$includes);
        if (isset($includes[1])) {
            $startOfBuffer = '';
            foreach ($includes[1] as $key => $file) {
                if (is_file($this->theme. DIRECTORY_SEPARATOR .$file)) {
                    $css = str_replace($includes[0][$key],'',$css);
                    $startOfBuffer .= file_get_contents($this->theme. DIRECTORY_SEPARATOR .$file);
                }
            }
        }
        if (isset($startOfBuffer)) {
            $css = $startOfBuffer.$css;
        }

        // Les variables seront du type : $MAVARIABLE
            
        // // On remplace les variables par leur valeur
        foreach($this->variables as $variable => $valeur) {
            $css = str_replace('$'.$variable, $valeur, $css);
        }
     
        // Suppression des tabulations, espaces multiples, retours à la ligne, etc.
        $css = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $css);
     
        // Suppression des derniers espaces inutiles
        $css = str_replace(array(' { ',' {','{ '), '{', $css);
        $css = str_replace(array(' } ',' }','} '), '}', $css);
        $css = str_replace(array(' : ',' :',': '), ':', $css);

        return $css;
    }
        
}