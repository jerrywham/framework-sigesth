<?php
namespace Core\Lib;

final class PluginsManager implements \Core\Interfaces\PluginsManager {

    public $icon_default = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAMAAABg3Am1AAACiFBMVEUAAAAIGQMAAAAAAAAFDgJeyzhi0DoAAAAshA0AAAAAAAAAAAA6mBhi0js3jxlm1D8uhRAjZQwbTwoAAAAndQ0+lCAAAABu40YxiRMAAAB741YiYAx851aP3HRj0T0ZRgpw5EcvghJDnSIncQ08mBwuhw9XvzE/mR4AAAAAAAAAAABw5UdIpCdl1j0FCwMAAABv40aFwHCo6pGE3GUrgA5NtChm00BWvDEvhxFi0TojWQ9StS4vhhB24FBgzzk1khQqfw1v4UZLsicndAxLsidh0Dk8nRoaSwogXgsAAAAshA4sgw44lRdgzzlOtSoAAABz6Upp20AaSwpj1DxtuVFv1kpXvzG286CU63Wj7omK8GWS63I3lRZ46lB25E9Tui1x40l6x19HoiaN33CA2WIaSQtVvjBt4EQUOAglbgwdUwpDpSEaRwp4705w5EdHqiNk1DwRLgZ25E9UvS5470517Etz6Umn8o2x9Zmu9JWr8pKl8Yq09p24+KKc736W73aR7m+h8IXM/7m99amp8o+j8IiM7mkshQ6P7mxx5kiZ7nqJ7WZ861W/+qq8+aee8IKW63iC61x150xv5EZOsylEnCM/lR8ncwzD+rCb6n6S7nN36k9VvDFPrixNpixEkydCjyZJrSVFqCFCpB8vghIqeQ4qfg0lbgwcTwrd+9LI/bS/9a2f7YSM422J8WSH6mRdxThSuSxIoic/nB05lhg1iRYyjxIlaA7K9LqR5HOS8XCN7GuG7WGC4GF02lBrs1BewDtTtTA8jh0gWwvh/9fP/72p6JSm7IyX4n6a8nuI4meC0WRr20NgsUJcpUFRoDM9gSI1exoeVgqazImEx2xwv1RvzU9qw0tOFNlgAAAAdHRSTlMABAkQFfj1IHU9MyYVCwr+24htTzguLCUgG/7+/fv7+/ft7OnIx6aXi4Z8dnFmYVYa/Pr59vLq1tLGw7mwrq6qnpqakYeGfHx7a2NTSkhFRTovKBP6+fb19PPv6eno5uXf29fU0s7FwKalmJWJhn5jW1tOJVbr87AAAAOqSURBVEjHpZSHU9pQHIAjpaVAobZWq917V+3ee++99947EiEIAiIioICyynIh7lmt2rq79+6/018CNR0vPe76HXdwue97P967JNj/Ip56vLk5elvkwdqi4gdNJSURF/fq8xttXa5n+7dHGOz0eLtTcLym/FyEwaAFDXockDqHR1hc8+IUhHx3pCMm0EHa0eS+S1Hx8f8qVr0Bvy33BvwUxcdu37hqcvT88cPYffHiRti06sT6tVMunlpQWFTc+KBZF5iIPqFbszZOndRcUofjL+uKC/OBQltP04eCd3PmIIPxhfSCVFDT0dTU43//9uvr0XOfybS5LchgYkmDzW63+1JwKEaMqE1vazNYjG6JROI+hAwWB7+Zq6RpNXiIFBoJhXEuMlhe+gZnZEaX6C1HkMHq0jF/6la3ZNw4a63yMDoI1FE6YxstylEzk4ffHFF7Ehmsc3T94ruNjw0viZkYcHfNbnQQ9DB/3fg4MzNTc0yE/YMhwe6futViAD9Ls4K+vaLYgodeSgfcsPyjrKx0zRq4PGvCpFh0sHmkL3zsBlpPvw8TROvhmY0ehA56fWGfssFXyudNWVSfb//w+SwyGPqpgfKthrCuVJJab329/WN1+WB0UGaT6PV6Q0gHsrOJ1vcF76rJSrbAD77lUW1mWFcoFKSqSkWyBXvHFuTlWUedvjRmFK0DuSqVVksSlWMw9AgI8maKsCE+iyKkg0+SBFE5AEMzNW/cbPjaVDY4+xedkGkgQLOHfhtdH+s1q3J/6hkyKROgWVrmV2oZXZrKFoiHiTFg2MjeBgXJ6KlylmDQZF30VvAnBnu7tH06+GkswVadzhGYvHx+qePhqwxGl0PQHxkM1AW/tHQGwP9OZGTIwA/paTmsQY9Z43zR2TmakDE6YGINbGaShLWlv+k5zgonS1BqM1NbDeug5pjg46yqbu/P8sTZFTJYn5LlqTKtq/WVy2SqcK3cMHsHMthU5q+CY5ESWnN7x4GFS1YuK+p+XlF+HmODe6HApahu71i4BJYcLsLix+sCreonZzBW9k7ov2jK7T37+g7B8TFV/aRlFnuRHHajaK44Rr5Wq9Xlo8VRIVAJ7XI4nH7A1U9+EwRPn8dyKBAF7fbjchN4PJ5QGHdn7Fs1FcyL5fZDF5SdIIwTCPj8XTExSUmXfVK1uuLFMoEgjseF4O8JHEgSeEIBPyZxy4zp06Yt9Xg8ByclxvCFMAKxBWbDHC4vblcSFU2fkchP4GAMPwC2ctECg5QvwwAAAABJRU5ErkJggg==';

    private $pluginsAvailable = [];
    private $pluginsEnabled = [];
    private $pluginsDisabled = [];
    private $plugins = [];
    private $theme;
    private $authentication;
    private $session;
    private $urlroot;
    private $plugAppDir;

    public function __construct(string $theme, \Core\Interfaces\Auth\Authentication $authentication, \Core\Interfaces\Auth\Session $session, string $urlroot)
    {   
        $this->plugAppDir = dirname(dirname(__DIR__)).DIRECTORY_SEPARATOR.'App'.DIRECTORY_SEPARATOR.'Plugins'.DIRECTORY_SEPARATOR;
        $this->theme = $theme;
        $this->authentication = $authentication;
        $this->session = $session;
        $this->urlroot = $urlroot;
        $this->pluginsAvailable = $this->listPlugins($this->plugAppDir.'pluginsAvailable'.DIRECTORY_SEPARATOR);
        $this->pluginsEnabled = $this->listPlugins($this->plugAppDir.'pluginsEnabled'.DIRECTORY_SEPARATOR);
        $this->pluginsDisabled = array_diff($this->pluginsAvailable,$this->pluginsEnabled);

        foreach ($this->pluginsAvailable as $key => $plugin) {
            $this->plugins[$plugin] = $this->getInfos($plugin);
        }
        ksort($this->plugins);
    }

    /**
     * To avoid $var to be changed because object should be immuable
     */
    public function __set($var,$value)
    {
        return null;
    }
    public function __call($name, $arguments)
    {
        return null;
    }
    public function __isset($name)
    {
        return null;
    }
    public function __unset($name)
    {
        return $name;
    }
    public function __get($var)
    {
        return $var;
    }

    public function getPluginsAvailable()
    {
        return $this->plugins;
    }

    public function list()
    {
        $title = 'L_List_of_plugins';

        return [
            'title' => $title,
            'template' => 'plugins.html.php',
            'plugins' => $this->plugins,
            'javascript' => [$this->theme.'/javascript/openNew.js', $this->theme.'/javascript/filter.js', $this->theme.'/javascript/checkAll.js'],
            'javascriptInline' => '

            document.getElementById("plugins-search").addEventListener("keyup", function(e) {
                    return doFilter(this.id, "plugins-table");
                }
            );

            document.getElementById("checkAll").addEventListener("click", function(e) {
                    return checkAll(this.form, "chkAction[]");
                }
            );
            
            if (typeof(Storage) !== "undefined" && localStorage.getItem("plugins-search") !== "undefined") {
                input = document.getElementById("plugins-search");
                input.value = localStorage.getItem("plugins-search");
                doFilter("plugins-search", "plugins-table");
            }

            ',
            'token' => true
        ];
    }

    public function activatePlugins()
    {
        if (isset($_POST['chkAction'])) {
            foreach ($_POST['chkAction'] as $key => $plugin) {
                if (in_array($plugin, $this->pluginsAvailable)) {
                    if (!in_array($plugin, $this->pluginsEnabled)) {
                        touch($this->plugAppDir.'pluginsEnabled'.DIRECTORY_SEPARATOR.$plugin);
                        $this->pluginsEnabled[] = $plugin;
                    } 
                }
            }
            $this->onActivate($this->pluginsEnabled);
            $pluginsToDisabled = array_diff($this->pluginsEnabled,$_POST['chkAction']);
            foreach ($pluginsToDisabled as $key => $plugin) {
                $this->remove($plugin);
            }
            $this->onDeactivate($pluginsToDisabled);
        } else {
            foreach ($this->pluginsEnabled as $key => $plugin) {
                $this->remove($plugin);
            }
            $this->onDeactivate($this->pluginsEnabled);
        }
        $this->session->setMsg('L_Data_recorded_successfully');
        header('Location: '.$this->urlroot.'plugins/list');
        exit();    
    }

    private function remove($plugin)
    {
        $pFile = $this->plugAppDir.'pluginsEnabled'.DIRECTORY_SEPARATOR.$plugin;
        if (is_file($pFile)) {
            unlink($pFile);
        }
    }

    private function onActivate(array $plugins = [])
    {
        foreach ($plugins as $key => $plugin) {
            $p = new \ReflectionClass('\Plugins\\'.$plugin.'\\'.$plugin);
            ($p->newInstance($this->session->getLang(), $this->theme,[], $this->authentication, $this->session, $this->urlroot))->onActivate();
            unset($pl);
            unset($p);
        }
    }

    private function onDeactivate(array $plugins = [])
    {
        foreach ($plugins as $key => $plugin) {
            $p = new \ReflectionClass('\Plugins\\'.$plugin.'\\'.$plugin);
            ($p->newInstance($this->session->getLang(), $this->theme,[], $this->authentication, $this->session, $this->urlroot))->onDeactivate();
            unset($pl);
            unset($p);
        }
    }

    private function getInfos(string $plugin):array
    {
        $plugDir = dirname(dirname(__DIR__)).DIRECTORY_SEPARATOR.'Plugins'.DIRECTORY_SEPARATOR.$plugin.DIRECTORY_SEPARATOR;
        if (is_file($plugDir.'icon.png')) {
            $icon = $this->base64_encode_image($plugDir.'icon.png');
        } elseif (is_file($plugDir.'icon.jpg')) {
            $icon = $this->base64_encode_image($plugDir.'icon.jpg','jpg');
        } else {
            $icon = $this->icon_default;
        }

        if(is_file($plugDir.'infos.json')) {
            $infos = json_decode(file_get_contents($plugDir.'infos.json'),true);
            $infos['enabled'] = (in_array($plugin, $this->pluginsEnabled) ? true : false);
            $infos['title'] = strtolower($infos['title']);
            $infos['icon'] = $icon;
            $infos['configuration'] = $this->getConfig($plugin);
            return $infos;
        } else {
            return [
                'title' => strtolower($plugin),
                'icon' => $icon,
                'description' => '',
                'version' => '',
                'compatibility' => '',
                'author' => '',
                'contact' => '',
                'depot' => '',
                'updateOn' => '',
                'enabled' => (in_array($plugin, $this->pluginsEnabled) ? true : false),
                'configuration' => $this->getConfig($plugin)
            ];
        }
    }

    private function getConfig(string $plugin)
    {
        $reflected = new \ReflectionClass('\Plugins\\'.$plugin.'\\'.$plugin);
        return $reflected->hasConstant('configuration');
    }

    /**
     * Méthode permettant d'encoder une image en base64
     * 
     * @param $filename string le chemin vers le fichier image
     * @param $filetype string l'extension de l'image
     * @return string
     * 
     * @author luke at lukeoliff.com
     */
    public function base64_encode_image ($filename,$filetype='png') {
        if (is_string($filename) && is_file($filename) ) {
            $imgbinary = fread(fopen($filename, "r"), filesize($filename));
            return 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);
        } else {
            return $filename;
        }
    }

    private function listPlugins(string $path)
    {
        $plugins = array();
        $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path));
        foreach($files as $name => $object){
            $name = basename($name);
            if (!in_array($name, ['.','..','.htaccess','index.html'])) {
                $plugins[] = $name;
            }
        }
        return $plugins;
    }

}