<?php 
namespace Core\Lib;

final class PasswordTools implements \Core\Interfaces\Auth\PasswordTools {

    private $weak;
    private $CHARSET;

    public function __construct($weak=false,$CHARSET='UTF-8') {
        $this->weak = $weak;
        $this->CHARSET = $CHARSET;
    }
    
    /**
     * To avoid $var to be changed because object should be immuable
     */
    public function __set($var,$value)
    {
        return null;
    }
    public function __call($name, $arguments)
    {
        return null;
    }
    public function __isset($name)
    {
        return null;
    }
    public function __unset($name)
    {
        return $name;
    }
    public function __get($var)
    {
        return $var;
    }


    private function options() {
        if (version_compare(PHP_VERSION, 7.0, '<')) {
            return array(
                'cost' => $this->getOptimalBcryptCostParameter(),
                'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM)
            );
        } else {
            return array(
                'cost' => $this->getOptimalBcryptCostParameter(),
            );
        }
    }

    /**
     * This code will benchmark your server to determine how high of a cost you can
     * afford. You want to set the highest cost that you can without slowing down
     * you server too much. 8-10 is a good baseline, and more is good if your servers
     * are fast enough. The code below aims for ≤ 50 milliseconds stretching time,
     * which is a good baseline for systems handling interactive logins.
     * @Param int $min_ms Minimum amount of time in milliseconds that it should take
     * to calculate the hashes
     */
    private function getOptimalBcryptCostParameter($timeTarget = 0.25) {// 250 milliseconds
        $cost = 8; 
        do {
            $cost++;
            if (version_compare(PHP_VERSION, 7.0, '<') ) {
                $options = array(
                    'cost' => $cost,
                    'salt' => 'usesomesillystringforsalt'
                );
            } else {
                $options = array('cost' => $cost);
            }
            $start = microtime(true);
            \password_hash("rasmuslerdorf", PASSWORD_DEFAULT, $options);
            $end = microtime(true);
        } while (($end - $start) < $timeTarget);

        return $cost;
    }

    /**
     * Note that the salt here is randomly generated.
     * Never use a static salt or one that is not randomly generated.
     *
     * For the VAST majority of use-cases, let password_hash generate the salt randomly for you
     */
    public function create_hash($password) {
        return \password_hash($password, PASSWORD_DEFAULT, $this->options());
    }

    public function validate_password($password, $good_hash) {
        if (\password_verify($password, $good_hash)) {
            return true;
        }
        return false;
    }

    public function isPasswordNeedsRehash($password,$hash) {
        if (\password_needs_rehash($hash, PASSWORD_DEFAULT, $this->options())) {
            return $this->create_hash($password);
        }
        return false;
    } 

    public function isPasswordStrong(string $pw,int $length, $compare=null)
    {
        $compare = mb_strtolower($compare,$this->CHARSET);
        $p = mb_strtolower($pw,$this->CHARSET);
        similar_text($compare, $p, $percent);
        if ($percent > 50) return false;
        
        if (mb_strlen($pw,$this->CHARSET) < $length) return false;
        $regex = '/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)+,\/,°,`,£,€,\',§,ç,é,è,ù,à,=,:,;, ]/';
        $r = false;
        // // If the password length is greater than $length and contain any lowercase alphabet or any number or any special character
        $r = (preg_match('/[a-z]/',$pw) || preg_match('/[A-Z]/',$pw) || preg_match('/\d+/',$pw) || preg_match($regex,$pw) );
        // // If the password length is greater than $length and contain alphabet,number,special character respectively
        $r = (( ((preg_match('/[a-z]/',$pw) || preg_match('/[A-Z]/',$pw)) && preg_match('/\d+/',$pw)) || (preg_match('/\d+/',$pw) && preg_match($regex,$pw)) || ((preg_match('/[a-z]/',$pw) || preg_match('/[A-Z]/',$pw)) && preg_match($regex,$pw)) ));
        // // If the password length is greater than $length and must contain alphabets,numbers and special characters
        if ($this->weak == false) {
            $r =(preg_match('/[a-z]/',$pw) && preg_match('/[A-Z]/',$pw) && preg_match('/\d+/',$pw) && preg_match($regex,$pw));
        }
        return $r;
    }   
} 
