<?php 
# ------------------ BEGIN LICENSE BLOCK ------------------
#
# Copyright (c) 2013 SebSauvage
# See http://sebsauvage.net/paste/?36dbd6c6be607e0c#M5uR8ixXo5rXBpXx32gOATLraHPffhBJEeqiDl1dMhs
# 
# Modification and OPP style from Cyril MAGUIRE contact@ecyseo.net
#
# Instructions d'utilisation:
# • Faites un require_once de ce script.
# • à l'endroit où vous testez la validité du mot de passe:
#     • Si ban_canLogin()==false, l'utilisateur est banni. Ne testez même pas le mot de passe: Rejetez l'utilisateur.
#    • Si ban_canLogin()==true, vérifiez le mot de passe.
#          • Si le mot de passe est ok, appelez ban_loginOk(), sinon appelez ban_loginFailed()
# La lib s'occupe de compter le nombre d'échecs et de gérer la durée de bannissement 
# (bannissement/levée de ban).
# Cette lib créé un sous-répertoire "data" qui contient les données de bannissement 
# (ipbans.php) et un log de connexion (date('Ymd').log.txt).
#
# Exemple
#        $ban = new Core\Lib\Ban('http://mon.domaine.net/index.php?action=login&sp=', '.'.DIRECTORY_SEPARATOR.'data');
#        $notSpamCode = (isset($_GET['sp']) ? $_GET['sp'] : '');
#        if ($ban->ban_canLogin($notSpamCode) === false) { $pass = false; } else { $pass = true; }
#        if($pass){ 
#            if (CODE AUTHENTIFICATION = OK) {
#                $ban->ban_loginOk();
#                $session->setMsg(L_LOGIN_OK);
#                header('location:index.php');
#                exit();
#            } else {
#                $ban->ban_loginFailed();
#                $session->setMsg(L_ERROR_CONNEXION, 'error');
#                header('location:index.php?action=login');
#                exit();
#            }
#        } else { 
#            $ban->ban_loginFailed(); 
#            $session->setMsg(L_ERROR_CONNEXION, 'error');
#            header('location:index.php?action=login');
#            exit();
#        }
# ------------------- END LICENSE BLOCK -------------------

namespace Core\Lib;

final class Ban  implements \Core\Interfaces\Auth\Ban {

    private $timezone = 'Europe/Paris';
    private $connexionPage = 'login';
    private $DATADIR = './';
    private $config = array(
        'DATADIR' => '.'.DIRECTORY_SEPARATOR.'.ipbans'.DIRECTORY_SEPARATOR,
        'IPBANS_FILENAME' => '',
        'BAN_AFTER' => 3,
        'BAN_DURATION' => 1800,
    );
    private $IPBANS = array('FAILURES'=>array(),'BANS'=>array());

    private $BAN_DOCTYPE = '<!DOCTYPE html><html lang="en"><head><meta charset="utf8"></head><body><p>';
    private $BAN_END = '</p></body></html>';
    private $msg = array(
        'MSG_COME_BACK_IN' => 'Come back in',
        'MSG_MIN_OR_NOT' => 'minutes or not...</p>',
        'MSG_IF_NOT_SPAMMER' => '<p>If you are not a robot',
        'CLICK_HERE' => 'click here'
    );
    private $SECURITY_SALT = null;

    public function __construct(string $connexionPage = 'login', string $DATADIR = './', int $BAN_AFTER = 3, int $BAN_DURATION = 1800, string $timezone = 'Europe/Paris')
    {
        if (!empty($connexionPage)) $this->connexionPage = $connexionPage;
        if (!$this->checkTimeZone($timezone)) $this->timezone = 'Europe/Paris';

        date_default_timezone_set($this->timezone);

        if (!empty($DATADIR)) $this->DATADIR = rtrim($DATADIR,DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR;
        $this->config['DATADIR'] = $this->DATADIR.'.ipbans'.DIRECTORY_SEPARATOR; // Data subdirectory
        $this->config['IPBANS_FILENAME'] = $this->config['DATADIR'].'ipbans.php'; // File storage for failures and bans.
        if (!empty($BAN_AFTER)) $this->config['BAN_AFTER'] = $BAN_AFTER; // Ban IP after this many failures.
        if (!empty($BAN_DURATION)) $this->config['BAN_DURATION'] = $BAN_DURATION; // Ban duration for IP address after login failures (in seconds) (1800 sec. = 30 minutes)

        $this->SECURITY_SALT = $this->generateurMot(100);

        if (!is_dir($this->config['DATADIR'])) { mkdir($this->config['DATADIR'],0705); chmod($this->config['DATADIR'],0705); }
        if (!is_file($this->config['DATADIR'].DIRECTORY_SEPARATOR.'.htaccess')) { file_put_contents($this->config['DATADIR'].DIRECTORY_SEPARATOR.'.htaccess',"Allow from none\nDeny from all\n"); } // Protect data files.
        if (!is_file($this->config['DATADIR'].DIRECTORY_SEPARATOR.date('Ymd').'log.txt')) { file_put_contents($this->config['DATADIR'].DIRECTORY_SEPARATOR.date('Ymd').'log.txt',''); } // log file.
        // ------------------------------------------------------------------------------------------
        // Brute force protection system
        // Several consecutive failed logins will ban the IP address for 30 minutes.
        if (!is_file($this->config['IPBANS_FILENAME'])) $this->recBan(array('FAILURES'=>array(),'BANS'=>array()));
        include $this->config['IPBANS_FILENAME'];
    }
    
    /**
     * To avoid $var to be changed because object should be immuable
     */
    public function __set($var,$value)
    {
        return null;
    }
    public function __call($name, $arguments)
    {
        return null;
    }
    public function __isset($name)
    {
        return null;
    }
    public function __unset($name)
    {
        return $name;
    }
    public function __get($var)
    {
        return $var;
    }

    private function logm($message)
    {
        $t = strval(date('Y/m/d_H:i:s')).' - '.$_SERVER["REMOTE_ADDR"].' - '.strval($message)."\n";
        file_put_contents($this->config['DATADIR'].DIRECTORY_SEPARATOR.date('Ymd').'log.txt',$t,FILE_APPEND);
    }

    private function generateurMot($longueur = 8,$nbCaracteres = 4,$caracteresSup = array(),$nombresSup = array(),$voyellesSup = array(),$consonnesSupp = array()) 
    {
        $mot = '';
        $consonnes = array('b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','z');
        $voyelles = array('a','e','i','o','u','y');
        $caracteres = array('@','#','?','!','+','=','-','%','&','*');
        $nombres = array('0','1','2','3','4','5','6','7','8','9');
        $caracteresDejaChoisis = array();
        
        if (!empty($consonnesSupp)) {
            $consonnes = array_diff($this->consonnes,$consonnesSupp);
        }
        if (!empty($voyellesSup)) {
            $voyelles = array_diff($this->voyelles,$voyellesSup);
        }
        if (!empty($caracteresSup)) {
            $caracteres = array_diff($this->caracteres,$caracteresSup);
        }
        if (!empty($nombresSup)) {
            $nombres = array_diff($this->nombres,$nombresSup);
        }
        
        if (empty($consonnes)) {
            $consonnes = array('b');
        }
        if (empty($voyelles)) {
            $voyelles = $this->consonnes;
        }
        if (empty($nombres)) {
            $nombres = $this->consonnes;
        }
        
        if ($nbCaracteres == 0) {
            $caracteres = $this->consonnes;
        }
        $choix = array('0'=>$consonnes,'1'=>$voyelles,'2'=>$caracteres,'3'=>$nombres);
        $j = 0;
        for($i=0;$i<$longueur;$i++) {
            if (count($caracteresDejaChoisis) == $nbCaracteres) {
                $caracteres = $caracteresDejaChoisis;
            }
            //choix aléatoire entre consonnes et voyelles
            $rand = array_rand($choix,1);
            $tab = $choix[$rand];
            //on recherche l'index d'une lettre, au hasard dans le tableau choisi
            $lettre = array_rand($tab,1);
            if (in_array($lettre, $caracteresDejaChoisis)) {
                $lettre = array_rand($consonnes,1);
                $tab = $consonnes;
            }
            //On ajoute le caractère au tableau des caractères déjà choisis
            if ($tab == $caracteres) {
                $caracteresDejaChoisis[] = $lettre;
            }
            //on recherche la dernière lettre du mot généré
            if (strlen($mot) > 0) {
                $derniereLettre = $mot[strlen($mot)-1];
            } else {
                $derniereLettre = '';
            }
            
            //si la lettre choisie est déjà à la fin du mot généré, on relance la boucle
            if ($tab[$lettre] == $derniereLettre || in_array($derniereLettre,$tab)) {
                $i--;
            } else {//sinon on l'ajoute au mot généré
                $maj = mt_rand(0,10);
                if ($maj<2) {
                    $mot .= strtoupper($tab[$lettre]);  
                } else {
                    $mot .= $tab[$lettre];  
                }
            }
        }
        
        return $mot;
    }

    private function recBan($var)
    {
        $ok = file_put_contents($this->config['IPBANS_FILENAME'], "<?php\n\$this->IPBANS=".var_export($var,true).";\n");
        if ($ok === false) {
            echo '<pre style="border: 1px solid #e3af43; background-color: #f8edd5; padding: 10px;color:#111;white-space: pre;white-space: pre-wrap;word-wrap: break-word;">Erreur : '; print_r('Vérifiez les droits en écriture !');exit('</pre>');
        }
    }

    
    // Signal a failed login. Will ban the IP if too many failures:
    public function ban_loginFailed()
    {
        $ip=$_SERVER["REMOTE_ADDR"]; 
        $gb=$this->IPBANS;
        $notSpamCode = str_replace('/','_',base64_encode($ip.time().$this->SECURITY_SALT));

        if (!isset($gb['FAILURES'][$ip])) $gb['FAILURES'][$ip]=0;
        $gb['FAILURES'][$ip]++;
        if ($gb['FAILURES'][$ip]>($this->config['BAN_AFTER']-1))
        {
            $gb['BANS'][$ip]=time()+$this->config['BAN_DURATION'];
            if (!isset($gb['NOTSPAM'][$ip])) {
                $gb['NOTSPAM'][$ip]=$notSpamCode;
            }
            if (empty($gb['NOTSPAM'][$ip])) {
                $gb['NOTSPAM'][$ip]=$notSpamCode;
            }
            $this->logm('IP address banned from login');
            $this->recBan($gb);
            echo $this->BAN_DOCTYPE.$this->msg['MSG_COME_BACK_IN'].'&nbsp;'.($this->config['BAN_DURATION']/60).'&nbsp;'.$this->msg['MSG_MIN_OR_NOT'];
            echo $this->msg['MSG_IF_NOT_SPAMMER'].'<a href='.$this->connexionPage.(isset($gb['NOTSPAM'][$ip])? $gb['NOTSPAM'][$ip]:$notSpamCode).'>&nbsp;'.$this->msg['CLICK_HERE'].'</a>'.$this->BAN_END;
            exit();
        }
        $this->IPBANS = $gb;
        $this->logm('IP address login failed');
        $this->recBan($gb);
    }

    // Signals a successful login. Resets failed login counter.
    public function ban_loginOk()
    {
        $ip=$_SERVER["REMOTE_ADDR"]; 
        $gb=$this->IPBANS;
        unset($gb['FAILURES'][$ip]); 
        unset($gb['BANS'][$ip]);
        unset($gb['NOTSPAM'][$ip]);
        $this->IPBANS = $gb;
        $this->recBan($gb);
        $this->logm('Login ok.');
    }

    public function logout()
    {
        $this->logm('Logout done.');
    }

    // Checks if the user CAN login. If 'true', the user can try to login.
    public function ban_canLogin($notSpamCode=''):bool
    {
        $ip=$_SERVER["REMOTE_ADDR"]; 
        $gb=$this->IPBANS;
        if (isset($gb['BANS'][$ip]))
        {
            // User is banned. Check if the ban has expired:
            if ($gb['BANS'][$ip]<=time())
            { // Ban expired, user can try to login again.
                $this->logm('Ban lifted.');
                $this->recBan($gb);
                return true; // Ban has expired, user can login.
            }
            // User is banned. Check if he clicked on notspam link
            if ($notSpamCode == $gb['NOTSPAM'][$ip]) {
                // Ban expired, user can try to login again.
                $this->logm('Ban lifted.');
                $this->recBan($gb);
                return true; // Ban has expired, user can login.
            }
            return false; // User is banned.
        }
        return true; // User is not banned.
    }

    private function checkTimeZone($tz)
    {
        $TimeZones = array('Australia/Adelaide','Australia/Broken_Hill','Australia/Darwin','Australia/North','Australia/South','Australia/Yancowinna','America/Goose_Bay','America/Pangnirtung','America/Halifax','America/Barbados','America/Blanc-Sablon','America/Glace_Bay','America/Martinique','America/Moncton','America/Thule','Atlantic/Bermuda','Canada/Atlantic','Australia/Melbourne','Antarctica/Macquarie','Australia/ACT','Australia/Brisbane','Australia/Canberra','Australia/Currie','Australia/Hobart','Australia/Lindeman','Australia/NSW','Australia/Queensland','Australia/Sydney','Australia/Tasmania','Australia/Victoria','Australia/LHI','Australia/Lord_Howe','America/Anchorage','America/Adak','America/Atka','America/Juneau','America/Metlakatla','America/Nome','America/Sitka','America/Yakutat','America/Asuncion','Europe/Amsterdam','Europe/Athens','America/Puerto_Rico','America/Anguilla','America/Antigua','America/Aruba','America/Curacao','America/Dominica','America/Grand_Turk','America/Grenada','America/Guadeloupe','America/Kralendijk','America/Lower_Princes','America/Marigot','America/Miquelon','America/Montserrat','America/Port_of_Spain','America/Santo_Domingo','America/St_Barthelemy','America/St_Kitts','America/St_Lucia','America/St_Thomas','America/St_Vincent','America/Tortola','America/Virgin','Australia/Perth','Australia/West','Europe/London','Europe/Belfast','Europe/Gibraltar','Europe/Guernsey','Europe/Isle_of_Man','Europe/Jersey','GB','Europe/Tiraspol','America/Bogota','Europe/Brussels','Asia/Baghdad','Europe/Busingen','Europe/Vaduz','Europe/Zurich','Asia/Bangkok','Asia/Phnom_Penh','Asia/Vientiane','Asia/Jakarta','Europe/Bucharest','Europe/Chisinau','America/La_Paz','Europe/Dublin','Africa/Juba','Africa/Khartoum','Africa/Blantyre','Africa/Bujumbura','Africa/Gaborone','Africa/Harare','Africa/Kigali','Africa/Lubumbashi','Africa/Lusaka','Africa/Maputo','Africa/Windhoek','America/Rankin_Inlet','America/Resolute','America/Chicago','Asia/Shanghai','America/Havana','America/Atikokan','America/Bahia_Banderas','America/Belize','America/Cambridge_Bay','America/Cancun','America/Chihuahua','America/Coral_Harbour','America/Costa_Rica','America/El_Salvador','America/Fort_Wayne','America/Guatemala','America/Indiana/Indianapolis','America/Indiana/Knox','America/Indiana/Marengo','America/Indiana/Petersburg','America/Indiana/Tell_City','America/Indiana/Vevay','America/Indiana/Vincennes','America/Indiana/Winamac','America/Indianapolis','America/Iqaluit','America/Kentucky/Louisville','America/Kentucky/Monticello','America/Knox_IN','America/Louisville','America/Managua','America/Matamoros','America/Menominee','America/Merida','America/Mexico_City','America/Monterrey','America/North_Dakota/Beulah','America/North_Dakota/Center','America/North_Dakota/New_Salem','America/Ojinaga','America/Rainy_River','America/Tegucigalpa','America/Winnipeg','Canada/Central','Mexico/General','Asia/Chongqing','Asia/Chungking','Asia/Harbin','Asia/Macao','Asia/Macau','Asia/Taipei','PRC','ROC','Europe/Berlin','Europe/Kaliningrad','Africa/Algiers','Africa/Ceuta','Africa/Tripoli','Africa/Tunis','Arctic/Longyearbyen','Atlantic/Jan_Mayen','Europe/Andorra','Europe/Belgrade','Europe/Bratislava','Europe/Budapest','Europe/Copenhagen','Europe/Kiev','Europe/Lisbon','Europe/Ljubljana','Europe/Luxembourg','Europe/Madrid','Europe/Malta','Europe/Minsk','Europe/Monaco','Europe/Oslo','Europe/Paris','Europe/Podgorica','Europe/Prague','Europe/Riga','Europe/Rome','Europe/San_Marino','Europe/Sarajevo','Europe/Simferopol','Europe/Skopje','Europe/Sofia','Europe/Stockholm','Europe/Tallinn','Europe/Tirane','Europe/Uzhgorod','Europe/Vatican','Europe/Vienna','Europe/Vilnius','Europe/Warsaw','Europe/Zagreb','Europe/Zaporozhye','Africa/Casablanca','America/Argentina/Buenos_Aires','America/Argentina/Catamarca','America/Argentina/ComodRivadavia','America/Argentina/Cordoba','America/Argentina/Jujuy','America/Argentina/La_Rioja','America/Argentina/Mendoza','America/Argentina/Rio_Gallegos','America/Argentina/Salta','America/Argentina/San_Juan','America/Argentina/San_Luis','America/Argentina/Tucuman','America/Argentina/Ushuaia','America/Buenos_Aires','America/Catamarca','America/Cordoba','America/Jujuy','America/Mendoza','America/Rosario','America/Caracas','America/Cayman','America/Panama','America/Detroit','America/Hermosillo','America/Mazatlan','America/Regina','America/Swift_Current','America/Thunder_Bay','Canada/Saskatchewan','Mexico/BajaSur','Pacific/Guam','Pacific/Saipan','Africa/Addis_Ababa','Africa/Asmara','Africa/Asmera','Africa/Dar_es_Salaam','Africa/Djibouti','Africa/Kampala','Africa/Mogadishu','Africa/Nairobi','Indian/Antananarivo','Indian/Comoro','Indian/Mayotte','America/New_York','America/Jamaica','America/Montreal','America/Nassau','America/Nipigon','America/Port-au-Prince','America/Toronto','Canada/Eastern','Europe/Helsinki','Africa/Cairo','Asia/Amman','Asia/Beirut','Asia/Damascus','Asia/Famagusta','Asia/Gaza','Asia/Hebron','Asia/Istanbul','Asia/Nicosia','Europe/Istanbul','Europe/Mariehamn','Europe/Moscow','Europe/Nicosia','Chile/EasterIsland','Pacific/Easter','Atlantic/Madeira','Africa/Abidjan','Africa/Accra','Africa/Bamako','Africa/Banjul','Africa/Bissau','Africa/Conakry','Africa/Dakar','Africa/Freetown','Africa/Lome','Africa/Monrovia','Africa/Nouakchott','Africa/Ouagadougou','Africa/Sao_Tome','Africa/Timbuktu','America/Danmarkshavn','Atlantic/Reykjavik','Atlantic/St_Helena','Etc/GMT','Etc/Greenwich','Pacific/Honolulu','Pacific/Johnston','Asia/Hong_Kong','Atlantic/Azores','Asia/Calcutta','Asia/Dacca','Asia/Dhaka','Asia/Kolkata','Asia/Jerusalem','Asia/Tel_Aviv','Asia/Irkutsk','Asia/Tokyo','Asia/Pyongyang','Asia/Seoul','ROK','America/Yellowknife','America/Denver','America/Boise','America/Edmonton','America/Inuvik','America/Phoenix','America/Shiprock','Canada/Mountain','America/Montevideo','Indian/Maldives','Asia/Colombo','Asia/Makassar','Asia/Ujung_Pandang','America/Creston','America/Dawson_Creek','America/Ensenada','America/Fort_Nelson','America/Santa_Isabel','America/Tijuana','Mexico/BajaNorte','America/St_Johns','Canada/Newfoundland','Pacific/Auckland','Antarctica/McMurdo','Antarctica/South_Pole','NZ','America/Los_Angeles','America/Dawson','America/Vancouver','America/Whitehorse','Canada/Pacific','Canada/Yukon','Asia/Karachi','Asia/Ho_Chi_Minh','Asia/Saigon','Pacific/Bougainville','Pacific/Port_Moresby','America/Paramaribo','Asia/Yekaterinburg','Asia/Pontianak','America/Guayaquil','Asia/Rangoon','Asia/Yangon','Africa/Johannesburg','Africa/Maseru','Africa/Mbabane','Atlantic/Stanley','America/Punta_Arenas','America/Santiago','Chile/Continental','Asia/Kuala_Lumpur','Asia/Singapore','Pacific/Samoa','Pacific/Midway','Pacific/Pago_Pago','Asia/Tbilisi','Asia/Tehran','Etc/UCT','Etc/Universal','Etc/UTC','Etc/Zulu','UTC','Africa/Ndjamena','Africa/Brazzaville','Africa/Bangui','Africa/Douala','Africa/Kinshasa','Africa/Lagos','Africa/Libreville','Africa/Luanda','Africa/Malabo','Africa/Niamey','Africa/Porto-Novo','Africa/El_Aaiun','Atlantic/Canary','Atlantic/Faeroe','Atlantic/Faroe','Asia/Jayapura');
        if (in_array($tz, $TimeZones)) {
            return true;
        }
        return false;
    }
}
?>