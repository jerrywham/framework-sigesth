<?php

namespace Core\Lib;

final class FilesTools implements \Core\Interfaces\FilesTools {

    private $J20Uuid;
    private $separator = FILE_SEPARATOR;
    private $widthOfThumbs;
    private $qualityOfThumbs;
    private $privateTag;

    public function __construct(\Core\Interfaces\J20Uuid $J20Uuid, string $separator, int $widthOfThumbs = 96, int $qualityOfThumbs = 80, string $privateTag = '_priv')
    {
        $this->J20Uuid = new $J20Uuid();
        $this->separator = mb_strtolower($separator);
        $this->widthOfThumbs = $widthOfThumbs;
        $this->qualityOfThumbs = $qualityOfThumbs;
        $this->privateTag = $privateTag;
    }

    /**
     * To avoid $var to be changed because object should be immuable
     */
    public function __set($var,$value)
    {
        return null;
    }
    public function __call($name, $arguments)
    {
        return null;
    }
    public function __isset($name)
    {
        return null;
    }
    public function __unset($name)
    {
        return $name;
    }
    public function __get($var)
    {
        return $var;
    }

    public function getFileSeparator():string
    {
        return $this->separator;
    }
    public function getPrivateTag():string
    {
        return $this->privateTag;
    }

    public function getExtension(string $file):string {
        $e = explode('.',$file);
        return trim(end($e));
    }

    public function getFilename(string $file):array {
        $ext = $this->getExtension($file);
        return [
            'name'=> substr($file,0,strpos($file,$this->separator)).'.'.$ext, 
            'extension' => $ext,
            'token' => substr($file,strpos($file,$this->separator)+strlen($this->separator),(-strlen($ext)-1-(strpos($file,$this->privateTag) !== false ? strlen($this->privateTag) : 0) )),
            'visibility' => ((bool)!strpos($file,$this->privateTag))
        ];
    }

    public function removeAccents(string $string,$removeSpaces=false)
    {
        $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ',"'");
        $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o','&#039;');
        $str = str_replace($a, $b, $string);
        $str = preg_replace('#\&([A-za-z])(?:acute|cedil|circ|grave|ring|tilde|uml|uro)\;#', '\1', $str);
        $str = preg_replace('#\&([A-za-z]{2})(?:lig)\;#', '\1', $str); # pour les ligatures e.g. '&oelig;'
        $str = preg_replace('#\&[^;]+\;#', '', $str); # supprime les autres caractères
        if ($removeSpaces === true) {
            $str = str_replace(' ','',$str);
        }
        return $str;
    }

    /**
     * Méthode qui convertit une chaine de caractères au format valide pour un nom de fichier
     *
     * @param   str         chaine de caractères à formater
     * @return  string      nom de fichier valide
     **/
    public function title2filename($str) {

        $str = $this->nullbyteRemove($str);
        $str = strtolower($this->removeAccents($str, true));
        $str = str_replace('|','',$str);
        $str = preg_replace('/\.{2,}/', '.', $str);
        $str = preg_replace('/[^[:alnum:]|.|_]+/',' ',$str);
        return strtr(ltrim(trim($str),'.'), ' ', '-');
    }

    public function cronDel(string $dir,array $exclude=array(), int $conservationInDays=30):bool
    {
        if (!is_dir($dir)) return false;
        $filenames = array();
        $now = new \DateTime(date('Y-m-d H:i:s', time()));
        foreach (new \DirectoryIterator($dir) as $fileInfo) {
            if($fileInfo->isDot() || in_array($fileInfo->getFilename(), $exclude)) {
                continue;
            } else {
                if ($fileInfo->isFile()) {
                    $fileDate = new \DateTime(date('Y-m-d H:i:s', $fileInfo->getMTime()));
                    if ($now->diff($fileDate)->days > $conservationInDays) {
                        $p = $fileInfo->getPathname();
                        unlink($p);
                    }
                }
            }
        }
        return true;
    }


    public function createFileIfUnexists(string $path):bool
    {
        if (!is_file($path)) {
            if (!is_dir(dirname($path))) {
                if($this->createDirIfUnexists(dirname($path)) === true) {
                    if(file_put_contents($this->nullbyteRemove($path),'',LOCK_EX) !== false) {
                        return true;
                    }
                } 
            } else {
                if(file_put_contents($this->nullbyteRemove($path),'',LOCK_EX) !== false) {
                    return true;
                }
            } 
            return false;
        }
        return true;
    }

    public function createDirIfUnexists(string $path):bool
    {
        $return = false;
        if (is_file($path)) {
            $path = dirname($path);
        }   
        $dirtocreate = array();
        while (!is_dir($path)) {
            $dirtocreate[] = $path;
            $path = dirname($path);
        }
        $dirtocreate = array_reverse($dirtocreate);
        foreach ($dirtocreate as $key => $dir) {
            $return = false;
            try {
                mkdir($dir, 0755, true);
                if (!is_file($dir.DIRECTORY_SEPARATOR.'.htaccess')) {
                    try {
                        file_put_contents($dir.DIRECTORY_SEPARATOR.'.htaccess', "Allow from none\nDeny from all\n");
                        $return = true;
                    } catch (\Exception $e) {
                        echo 'Unable to create .htaccess in '.$dir.' directory';
                    }
                }
                if (!is_file($dir.DIRECTORY_SEPARATOR.'index.html')) {
                    try {
                        file_put_contents($dir.DIRECTORY_SEPARATOR.'index.html', '');
                        $return = true;
                    } catch (\Exception $e) {
                        echo 'Unable to create index.html in '.$dir.' directory';
                    }
                }
            } catch (\Exception $e) {
                echo 'Unable to create '.$dir.': ',  $e->getMessage(), "\n";
            }
        }
        return $return;
    }
    
    public function delTree(string $dir,bool $isAdmin=false, bool $delDir=true, array $exclude=[]):bool { 
        if ($isAdmin === false) return false;
        if (is_dir($dir)) {
            $files = array_diff(scandir($dir), array('.','..')); 
            foreach ($files as $file) { 
                if (!in_array($file, $exclude)) {
                    (is_dir($dir.DIRECTORY_SEPARATOR.$file)) ? $this->delTree($dir.DIRECTORY_SEPARATOR.$file, true, true) : @unlink($dir.DIRECTORY_SEPARATOR.$file); 
                }
            } 
            if ($delDir === true) {
                return rmdir($dir); 
            } else {
                return true;
            }
        }
        return false;
    } 

    public function listADir(string $startpath, bool $onlydir=false, array $exclude=array(), bool $order = false):array
    {
        if (!is_array($exclude)) $exclude = array();
        $ritit = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($startpath), \RecursiveIteratorIterator::CHILD_FIRST); 
        $r = array(); 
        foreach ($ritit as $splFileInfo) {
            if ($splFileInfo->getFilename() != '.' && $splFileInfo->getFilename() != '..' && !in_array($splFileInfo->getFilename(), $exclude)) {
                $path = $splFileInfo->isDir()
                ? array($splFileInfo->getFilename() => array()) 
                : array($splFileInfo->getFilename()); 
                for ($depth = $ritit->getDepth() - 1; $depth >= 0; $depth--) { 
                    $path = array($ritit->getSubIterator($depth)->current()->getFilename() => $path);                     
                }
                $r = array_merge_recursive($r, $path);
            }
            unset($path);
        } 
        if ($onlydir) {
            foreach ($r as $key => $path) {
                if (!is_array($path)) {
                    unset($r[$key]);
                }
            }
        }
        foreach ($exclude as $key => $value) {
            if (isset($r[$value])) {
                unset($r[$value]);
            }
        }
        if ($order) {
            $tmp = [];
            foreach ($r as $k => $v) {
                if (is_array($v)) {
                    $tmp[$k] = $v;
                    unset($r[$k]);
                }
            }
            natsort($r);
            ksort($tmp);
            $r = $tmp + $r;
        }
        return $r;
    }

    
    public function createJsonTree($path,$filename='tree.json',$exclude=array('.htaccess','index.html','index.php'))
    {
        $path = rtrim($path,'/');
        if ($filename == '') {
            $filename = 'tree.json';
        }
        $filename = basename($filename);
        if ($this->createFileIfUnexists($path.DIRECTORY_SEPARATOR.$filename) === true) {
            $tree = json_encode($this->listADir($path,true,$exclude),JSON_FORCE_OBJECT|JSON_PRETTY_PRINT);
            if (file_put_contents($this->nullbyteRemove($path.DIRECTORY_SEPARATOR.$filename),$tree, LOCK_EX) !== false) {
                return true;
            }
        }
        return false;
    }

    public function createJson($path,$data,$filename='file.json',$prettyPrint=true)
    {
        if ($prettyPrint === true) {
            $pretty = JSON_FORCE_OBJECT|JSON_PRETTY_PRINT;
        } else {
            $pretty = JSON_FORCE_OBJECT;
        }
        $path = rtrim($path,'/');
        if ($filename == '') {
            $filename = 'file.json';
        }
        $filename = basename($filename);
        if ($this->createFileIfUnexists($path.DIRECTORY_SEPARATOR.$filename) === true) {
            $data = json_encode($data,$pretty);
            if (file_put_contents($path.DIRECTORY_SEPARATOR.$filename,$data,LOCK_EX) !== false) {
                return true;
            }
        }
        return false;
    }

    public function getJson($path)
    {
        $f = array();
        if (is_file($path)) {
            $f = json_decode(file_get_contents($path),true);
            if ($f === null) {
                $f = array();
            }
        }
        return $f;
    }

    # fonction de chargement d'un fichier de langue
    public function loadLang($filename) {
        if(file_exists($filename)) {
            $LANG = json_decode(file_get_contents($filename),true);
            foreach($LANG as $key => $value) {
                if(!defined($key)) define($key,$value);
            }
            return $LANG;
        }
    }


    /**
     * Protège une chaine contre un null byte
     *
     * @param   string chaine à nettoyer
     * @return  string chaine nettoyée
    */
    public function nullbyteRemove($string) {
        return str_replace("\0", '', $string);
    }


    public function rawurlencode(string $string)
    {
        return rawurlencode($this->nullbyteRemove($string));
    }

    public function strtolower(string $string)
    {
        return strtolower($this->nullbyteRemove($string));
    }

    public function rawurldecode(string $string)
    {
        return rawurldecode($this->nullbyteRemove($string));
    }


    public function uploadImg($DIR,$index = 'upfile') {
        header('Content-Type: text/plain; charset=utf-8');

        try {
            
            // Undefined | Multiple Files | $_FILES Corruption Attack
            // If this request falls under any of them, treat it invalid.
            if (
                !isset($_FILES[$index]['error']) ||
                is_array($_FILES[$index]['error'])
            ) {
                throw new \RuntimeException('INVALID PARAMETERS');
            }

            // Check $_FILES[$index]['error'] value.
            switch ($_FILES[$index]['error']) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new \RuntimeException('NO FILE SENT');
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new \RuntimeException('EXCEEDED FILESIZE LIMIT');
                default:
                    throw new \RuntimeException('UNKNOWN ERRORS');
            }

            // You should also check filesize here. 
            if ($_FILES[$index]['size'] > 1000000) {
                throw new \RuntimeException('EXCEEDED FILESIZE LIMIT');
            }

            // DO NOT TRUST $_FILES[$index]['mime'] VALUE !!
            // Check MIME Type by yourself.
            $finfo = new \finfo(FILEINFO_MIME_TYPE);
            if (false === $ext = array_search(
                $finfo->file($_FILES[$index]['tmp_name']),
                array(
                    'jpg' => 'image/jpeg',
                    'png' => 'image/png',
                    'gif' => 'image/gif',
                ),
                true
            )) {
                throw new \RuntimeException('INVALID FILE FORMAT');
            }

            // You should name it uniquely.
            // DO NOT USE $_FILES[$index]['name'] WITHOUT ANY VALIDATION !!
            // On this example, obtain safe unique name from its binary data.
            $name = $this->generate_hash($_FILES[$index]['tmp_name']);
            if (!move_uploaded_file(
                $_FILES[$index]['tmp_name'],
                sprintf($DIR.'%s.%s',
                    $name,
                    $ext
                )
            )) {
                throw new \RuntimeException('FAILED TO MOVE UPLOADED FILE');
            }

            return array($name,$ext);

        } catch (\RuntimeException $e) {

            return $e->getMessage();

        }
    }

    public function return_bytes(string $size_str):int
    {
        switch (substr ($size_str, -1))
        {
            case 'M': case 'm': return (int)$size_str * 1048576;
            case 'K': case 'k': return (int)$size_str * 1024;
            case 'G': case 'g': return (int)$size_str * 1073741824;
            default: return $size_str;
        }
    }
    public function bytes_for_human(int $size_int):string {
        if (!empty($size_int) && is_int($size_int)){
            $unit = array('B','K','M','G');
            try {
                return round($size_int / pow(1024, ($i = floor(log($size_int, 1024))))).$unit[$i];
            } catch (Exception $e) {
                return $size_int;
            }
        } else {
            return $size_int;
        }
    }
    public function human_filesize($bytes, $decimals = 2) {
        $sz = 'BKMGTP';
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
    }

    public function upload(string $rootDir, string $baseurl, string $postlead, string $visibility, bool $allowuploads, array $supportedextentions, bool $browsedirs = true, string $separator = ''):array
    {
        $finfo = new \finfo(FILEINFO_MIME_TYPE);

        $lead = '';

        // upload file
        if($allowuploads && isset($_FILES['file']) && is_array($_FILES['file'])) {
            $phpmaxsize = $this->return_bytes(trim(ini_get('upload_max_filesize')));
            $upload = true;
            $lead = trim($this->nullbyteRemove($postlead));
            $root = substr($lead, 0, strpos($lead, '/'));
            $ssdir = substr($lead, strpos($lead, '/') + 1);
            $FILES = $_FILES['file'];
            $msg['errors'] = [];
            $msg['files'] = [];
            foreach ($FILES['name'] as $kFile => $nFile) {
                $msg['errors'][$nFile] = [];
                try {
                    
                    // Undefined | Multiple Files | $_FILES Corruption Attack
                    // If this request falls under any of them, treat it invalid.
                    if ( !isset($FILES['error'][$kFile]) || is_array($FILES['error'][$kFile])) {
                        $upload = false;
                        // throw new \RuntimeException('Invalid parameters.');
                        return false;
                    }

                    // Check $FILES['error'][$kFile] value.
                    switch ($FILES['error'][$kFile]) {
                        case UPLOAD_ERR_OK:
                            break;
                        case UPLOAD_ERR_NO_FILE:
                            // throw new \RuntimeException('No file sent.');
                            $msg['errors'][$nFile][] = 'UPLOAD_ERR_NO_FILE';
                        case UPLOAD_ERR_INI_SIZE:
                        case UPLOAD_ERR_FORM_SIZE:
                            // throw new \RuntimeException('L_FILE L_TOO_BIG (max : '.ini_get('upload_max_filesize').')');
                            $msg['errors'][$nFile][] = 'UPLOAD_ERR_FORM_SIZE';
                        default:
                            // throw new \RuntimeException('Unknown errors.');
                            $msg['errors'][$nFile][] = 'UNKNOWN_ERRORS';                        
                    }

                    // You should also check filesize here. 
                    if ($FILES['size'][$kFile] > $phpmaxsize) {
                        $upload = false;
                        // throw new \RuntimeException('L_FILE L_TOO_BIG (max : '.ini_get('upload_max_filesize').')');
                        $msg['errors'][$nFile][] = 'FILE_TOO_BIG';
                    }

                    // DO NOT TRUST $FILES['mime'] VALUE !!
                    // Check MIME Type by yourself.
                    $mimetype = $finfo->file($FILES['tmp_name'][$kFile]);
                    $arrayExt = explode('.', $FILES['name'][$kFile]);
                    $extension = end($arrayExt);
                    $ext = strtolower(array_search($mimetype,$supportedextentions,true)); 
                    if ($extension != $ext) {
                        if (isset($supportedextentions[$extension])) {
                            $ext = $extension;
                        } else {
                            $ext = false;
                        }
                    }

                    if (false === $ext) {
                        $upload = false;
                        // throw new \RuntimeException('L_INVALID_TYPE_FILE');
                        $msg['errors'][$nFile][] = 'INVALID_TYPE_FILE'; ;
                    }

                    if ($root != null) {
                        $key = 0;
                        if ($handle = opendir($rootDir.$baseurl.$root)) {
                            while (false !== ($file = readdir($handle))) { 
                                //first see if this file is required in the listing
                                if ($file == "." || $file == "..")  continue;
                                if (@filetype($rootDir.$baseurl.$root.DIRECTORY_SEPARATOR.$file) == "dir") {
                                    if(!$browsedirs) continue;
                                    $key++;
                                    $exploreDir[$key] = trim($file) . "/";
                                }
                            }
                            closedir($handle); 
                        }
                        if($ssdir != null && $exploreDir != null && !in_array($ssdir, $exploreDir)) {
                            $upload = false;
                            // throw new \RuntimeException('Unknown errors.');
                            $msg['errors'][$nFile][] = 'UNKNOWN_ERRORS';
                        }
                    }

                    if (is_dir($rootDir.$baseurl.$lead)) {
                        if(file_exists($rootDir.$baseurl.$lead.$FILES['name'][$kFile])) {
                            if($this->overwrite === false) {
                                $upload = true;
                                $count = 1;
                                while(file_exists($rootDir.$baseurl.$lead.$FILES['name'][$kFile])) {
                                    $FILES['name'][$kFile] = $FILES['name'][$kFile].'('.$count.')';
                                    $count++;
                                }
                            } else{
                                $upload = true;
                            } 
                        }
                        if($upload && empty($msg['errors'][$nFile])) {
                            // You should name it uniquely.
                            // DO NOT USE $FILES['name'] WITHOUT ANY VALIDATION !!
                            // On this example, obtain safe unique name from its binary data.
                            if (!move_uploaded_file($FILES['tmp_name'][$kFile], 
                                    sprintf($rootDir.$baseurl.$lead.'%s.%s',
                                        $this->title2filename(str_replace(['.','_'.$ext],['_',''],$FILES['name'][$kFile]).$separator.$this->generate_hash($FILES['tmp_name'][$kFile]).($visibility == 'private' ? $this->privateTag : '')),
                                        $ext
                                    ))
                            ) {
                                $msg['errors'][$nFile][] = 'UPLOAD_ERROR';
                            } else {
                                unset($msg['errors'][$nFile]);
                                $msg['files'][$nFile];
                            }
                        } else {
                            $msg['errors'][$nFile][] = 'UPLOAD_ERROR';
                        }
                    }

                } catch (\RuntimeException $e) { 
                    return false;
                }
            }
            if (!empty($msg['errors'])) {
                if (empty($msg['files'])) {
                    unset($msg['files']);
                }
                return $msg;
            } else {
                unset($msg['errors']);
                return $msg;
            }
        }
        return $msg['errors'][$nFile][] = 'UNKNOWN_ERRORS';
    }

    /**
    * 
    * Modified by Cyril MAGUIRE for files
    * Generate a secure hash for a given password. The cost is passed
    * to the blowfish algorithm. Check the PHP manual page for crypt to
    * find more information about this setting.
    * 
    * @author Marten Jacobs
    */
    public function generate_hash(string $password, int $cost=11):string {
        /* To generate the salt, first generate enough random bytes. Because
         * base64 returns one character for each 6 bits, the we should generate
         * at least 22*6/8=16.5 bytes, so we generate 17. Then we get the first
         * 22 base64 characters
         */
        $salt=substr(base64_encode(openssl_random_pseudo_bytes(17)),0,22);
        /* As blowfish takes a salt with the alphabet ./A-Za-z0-9 we have to
         * replace any '+' in the base64 string with '.'. We don't have to do
         * anything about the '=', as this only occurs when the b64 string is
         * padded, which is always after the first 22 characters.
         */
        $salt=str_replace("+",".",$salt);
        /* Next, create a string that will be passed to crypt, containing all
         * of the settings, separated by dollar signs
         */
        $param='$'.implode('$',array(
                "2y", //select the most secure version of blowfish (>=PHP 5.3.7)
                str_pad($cost,2,"0",STR_PAD_LEFT), //add the cost in two digits
                $salt //add the salt
        ));
       
        //now do the actual hashing
        return str_replace(['+','.','/','$'],random_int(0, 9),strtolower(crypt($password,$param)));
    }

    /** 
    * Get the directory size 
    * @param directory $directory 
    * @return integer 
    * @author itsrool at gmail dot com
    */ 
    public function dirSize($directory) { 
        $size = 0; 
        foreach(new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($directory)) as $file){ 
            if($file->getFileName() != '.' && $file->getFileName() != '..') $size+=$file->getSize(); 
        } 
        return $size; 
    } 


    public function displayFiles(string $mediasDir,string $dir,array $files,string $type, array $filesIcon, string $dirForIcons, string $token, string $folderIcon = '', string $extOfFolderIcon = 'png', string $class = 'targetBlank', string $output = '#path #Dirname #class #thumb #file #mime-type #size #dimensions #dir #token #img'):string
    {

        $finfo = new \finfo(FILEINFO_MIME_TYPE);

        $return = '';
        foreach ($files as $key => $file) {
            $bubbleId = $this->J20Uuid->v4(false);
            if (is_array($file)) {
                # Directory
                $d = $this->getFilename($key);
                $visibility = ($d['visibility'] === true ? '<span class="badge--success">L_Public</span>' : '<span class="badge--warning">&nbsp;&nbsp;L_Private&nbsp;&nbsp;</span>');
                $cvisibilityTrue = ($d['visibility'] === true ? ' checked="checked"' : '');
                $cvisibilityFalse = ($d['visibility'] === true ? '' : ' checked="checked"');
                $fvisibility = (int)$d['visibility'];
                $ftoken = $d['token'];

                $return .= str_replace(
                    ['#dir','#file','#name','#mime-type','#dimensions','#size','#path','#Dirname','#thumb','#img','#token','#cvisibilityTrue','#cvisibilityFalse','#fvisibility','#visibility','#ftoken'],
                    [$dir,$key,$key,'L_Directory','L_Not_Applicable',$this->human_filesize($this->dirsize($mediasDir.$dir.$key)),strtolower('/medias'.str_replace('medias/pict','',$dir).$key).'#'.$type,$key,$this->base64_encode_image($folderIcon,$extOfFolderIcon),'<img src="'.$this->base64_encode_image($folderIcon,$extOfFolderIcon).'" alt="'.$key.'"/>',$token,$cvisibilityTrue,$cvisibilityFalse,$fvisibility,$visibility,$ftoken],
                    $output);

            } else {
                # File
                $f = $this->getFilename($file);
                $visibility = ($f['visibility'] === true ? '<span class="badge--success">L_Public</span>' : '<span class="badge--warning">&nbsp;&nbsp;L_Private&nbsp;&nbsp;</span>');
                $cvisibilityTrue = ($f['visibility'] === true ? ' checked="checked"' : '');
                $cvisibilityFalse = ($f['visibility'] === true ? '' : ' checked="checked"');
                $fvisibility = (int)$f['visibility'];
                $ftoken = $f['token'];
                $name = $f['name'];

                # Picture
                if ($type == 'pict') {
                    $idpA = $this->J20Uuid->v4(false);
                    $idpB = $this->J20Uuid->v4(false);
                    $idpC = $this->J20Uuid->v4(false);
                    $idpD = $this->J20Uuid->v4(false);
                    list($width, $height) = getimagesize($mediasDir.$dir.$file);
                    $thumb = $mediasDir.DIRECTORY_SEPARATOR.'pict'.DIRECTORY_SEPARATOR.'thumbs'.str_replace('pict/','',$dir).$file;
                    if (!is_file($thumb) && $width > $this->widthOfThumbs) {
                        if (!is_dir(dirname($thumb))) {
                            mkdir(dirname($thumb),0755,true);
                        }
                        $this->makeThumb($mediasDir.$dir.$file,$thumb,$this->widthOfThumbs,0,$this->qualityOfThumbs);
                    } elseif ($width < $this->widthOfThumbs) {
                        $thumb = $mediasDir.$dir.$file;
                    }
                    $thumb = $this->base64_encode_image($thumb,$this->getExtension($thumb));

                    $return .= str_replace(
                        ['#dir','#file','#mime-type','#size','#path','#class','#thumb','#name','#dimensions','#token','#cvisibilityTrue','#cvisibilityFalse','#fvisibility','#visibility','#ftoken','idfora','forida','idforb','foridb','idforc','foridc','idford','foridd'],
                        [$dir,$file,$finfo->file($mediasDir.$dir.$file),$this->human_filesize(filesize($mediasDir.$dir.$file)),$dir.$file,$class,$thumb,$name,$width.'X'.$height,$token,$cvisibilityTrue,$cvisibilityFalse,$fvisibility,$visibility,$ftoken,'id="'.$idpA.'"','for="'.$idpA.'"','id="'.$idpB.'"','for="'.$idpB.'"','id="'.$idpC.'"','for="'.$idpC.'"','id="'.$idpD.'"','for="'.$idpD.'"'],
                        $output);

                } else {
                    $idA = $this->J20Uuid->v4(false);
                    $idB = $this->J20Uuid->v4(false);
                    $idC = $this->J20Uuid->v4(false);
                    $idD = $this->J20Uuid->v4(false);
                    # Other file
                    $ext = $f['extension'];
                    if (substr($dir,-1) != DIRECTORY_SEPARATOR) $dir .= DIRECTORY_SEPARATOR;

                    $return .= str_replace(
                        ['#dir','#file','#name','#mime-type','#size','#path','#class','#img','#token','#cvisibilityTrue','#cvisibilityFalse','#fvisibility','#visibility','#ftoken','idfora','forida','idforb','foridb','idforc','foridc','idford','foridd'],
                        [str_replace('medias/files','',$dir),$file,$name,$finfo->file($mediasDir.$dir.$file),$this->human_filesize(filesize($mediasDir.$dir.$file)),$dir.$file,$class,(isset($filesIcon[$ext]) ? '<img src="'.$this->base64_encode_image($dirForIcons.DIRECTORY_SEPARATOR.$filesIcon[$ext],$filesIcon[$ext]).'" alt="'.$name.'" title="'.$file.'"/>' : $file),$token,$cvisibilityTrue,$cvisibilityFalse,$fvisibility,$visibility,$ftoken,'id="'.$idA.'"','for="'.$idA.'"','id="'.$idB.'"','for="'.$idB.'"','id="'.$idC.'"','for="'.$idC.'"','id="'.$idD.'"','for="'.$idD.'"'],
                        $output);
                }
            }
        }
        return $return;
    }


    private function scaleimage($location, $w=null, $h=null, $maxw=null, $maxh=null){
        if ($w == null || $h == null) {
            $img = @getimagesize($location);
        } else {
            $img = [$w,$h];
        }
        if($img){
            $w = $img[0];
            $h = $img[1];

            $dim = array('w','h');
            foreach($dim AS $val){
                $max = "max{$val}";
                if(${$val} > ${$max} && ${$max}){
                    $alt = ($val == 'w') ? 'h' : 'w';
                    $ratio = ${$alt} / ${$val};
                    ${$val} = ${$max};
                    ${$alt} = ${$val} * $ratio;
                }
            }

            return("<img src='{$location}' alt='image' width='{$w}' height='{$h}' />");
        }
    }


    /**
     * Méthode qui crée la miniature d'une image
     *
     * @param   filename            emplacement et nom du fichier source
     * @param   filename_out        emplacement et nom de la miniature créée
     * @param   width               largeur de la miniature
     * @param   height              hauteur de la miniature
     * @param   quality             qualité de l'image
     * @param   ratio               si vrai conserve le ratio largeur x hauteur
     * @return  boolean             vrai si image créée
     **/
    private function makeThumb(string $filename, string $filename_out, int $width, int $height, int $quality = 80, bool $ratio = true)
    {
        if (!function_exists('imagecreatetruecolor')) {
            return $this->scaleimage($filename,$width,$height);
        }

        # Informations sur l'image
        list($width_orig, $height_orig, $type) = getimagesize($filename);

        if ($ratio) {
            # Calcul du ratio
            $ratio_w = $width / $width_orig;
            $ratio_h = $height / $height_orig;
            if ($width == 0) {
                $width = $width_orig * $ratio_h;
            } elseif ($height == 0) {
                $height = $height_orig * $ratio_w;
            } elseif ($ratio_w < $ratio_h and $ratio_w < 1) {
                $width = $ratio_w * $width_orig;
                $height = $ratio_w * $height_orig;
            } elseif ($ratio_h < 1) {
                $width = $ratio_h * $width_orig;
                $height = $ratio_h * $height_orig;
            } else {
                $width = $width_orig;
                $height = $height_orig;
            }
        }

        # Création de l'image
        $image_p = imagecreatetruecolor($width, $height);

        if ($type == 1) {
            $image = imagecreatefromgif($filename);
            $color = imagecolortransparent($image_p, imagecolorallocatealpha($image_p, 0, 0, 0, 127));
            imagefill($image_p, 0, 0, $color);
            imagesavealpha($image_p, true);
            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
            imagegif($image_p, $filename_out);
        } elseif ($type == 2) {
            $image = imagecreatefromjpeg($filename);
            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
            imagejpeg($image_p, $filename_out, $quality);
        } elseif ($type == 3) {
            $image = imagecreatefrompng($filename);
            $color = imagecolortransparent($image_p, imagecolorallocatealpha($image_p, 0, 0, 0, 127));
            imagefill($image_p, 0, 0, $color);
            imagesavealpha($image_p, true);
            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
            imagepng($image_p, $filename_out);
        }

        return is_file($filename_out);
    }

    public function readPict(string $img,array $AllowedExtensions):string
    {
        $f = $this->getFilename($img);
        $ext = $f['extension'];
        if (array_key_exists($ext, $AllowedExtensions)) {
            switch ($ext) {
                case 'png':
                case 'jpg':
                case 'jpeg':
                case 'gif':
                case 'bmp':
                case 'tiff':
                case 'tif': 
                    $headers = apache_request_headers(); 
                    // Checking if the client is validating his cache and if it is current.
                    if (isset($headers['If-Modified-Since']) && (strtotime($headers['If-Modified-Since']) == filemtime($img))) {
                        // Client's cache IS current, so we just respond '304 Not Modified'.
                        header('Last-Modified: '.gmdate('D, d M Y H:i:s', filemtime($img)).' GMT', true, 304);
                    } else {
                        $name = basename($f['name']);
                        // Image not cached or cache outdated, we respond '200 OK' and output the image.
                        header('Last-Modified: '.gmdate('D, d M Y H:i:s', filemtime($img)).' GMT', true, 200);
                        header('Content-Length: '.filesize($img));
                        header("Content-Disposition: inline; filename=$name");//attachment for display outside the browser
                        header('Content-Type: '.$AllowedExtensions[$ext]);
                        print file_get_contents($img);
                    }
                break;
            }
        } else {
            return null;
        }
    }

    /**
     * Méthode permettant d'encoder une image en base64
     * 
     * @param $filename string le chemin vers le fichier image
     * @param $filetype string l'extension de l'image
     * @return string
     * 
     * @author luke at lukeoliff.com
     */
    public function base64_encode_image(string $filename,string $filetype):string
    {
        if (is_string($filename) && is_file($filename)) {
            $imgbinary = fread(fopen($filename, "r"), filesize($filename));
            return 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);
        } else {
            return $filename;
        }
    }
    /**
     * Méthode permettant d'encoder un fichier en base 64
     * 
     * @param $source string le chemin vers le fichier à encoder
     *
     *@author Cyril MAGUIRE
     */
    public function file2base64($source):string
    {
        $bin_data_source = fread(fopen($source, "r"), filesize($source));  // reading as binary string
        $b64_data_source = base64_encode($bin_data_source);              // BASE64 encodage
        fclose($source);
        return $b64_data_source;
    }
    /**
     * Méthode permettant de décoder un fichier précédemment encodé en base 64
     * 
     * @param $source string le chemin vers le fichier à encoder
     * @param $output string le chemin vers le fichier de sortie
     * @param $md5 bool vérification des sommes de contrôle md5
     *
     *@author Cyril MAGUIRE
     */
    public function base642file($source, $output,bool $md5=true):bool
    {
        $bin_data_target = base64_decode($source);             // base64 decodage
        if (file_put_contents($output, $bin_data_target)) {
            if ($md5) {
                $md5 = md5_file($source);
                if (md5_file($output) != $md5) {
                    unlink($output);
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public function ariane(string $path, string $root, string $separator = ' >> ', string $anchor = ''):string {
        $dirs = explode('/',trim($path,'/'));
        $last = end($dirs);
        $lastkey = key($dirs);
        $ariane = '';
        $links = '/'.$root;
        if (count($dirs) > 1) {
            foreach ($dirs as $key => $dir) {
                if($dir != $last && $key != 0) {
                    $links .= '/'.$dir;
                    $ariane .= '<a href="'.$links.($anchor != '' ? '#'.$anchor : '').'">'.$dir.'</a>'.$separator;
                } else {
                    if ($key == 0) {
                        $links .= '/'.$dir;
                        $ariane .= '<a href="/'.$root.($anchor != '' ? '#'.$anchor : '').'">'.$dir.'</a>'.$separator;
                    } elseif ($key != $lastkey) {
                       $links .= '/'.$dir;
                        $ariane .= '<a href="'.$links.($anchor != '' ? '#'.$anchor : '').'">'.$dir.'</a>'.$separator;
                    } else {
                        $ariane .= '<span>'.$dir.'</span>';
                    }
                }
            }
        }
        return $ariane;
    }
}