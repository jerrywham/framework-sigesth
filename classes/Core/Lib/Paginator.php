<?php
namespace Core\Lib;

final class Paginator implements \Core\Interfaces\Paginator {

    public function __construct() {}

    /**
     * To avoid $var to be changed because object should be immuable
     */
    public function __set($var,$value)
    {
        return null;
    }
    public function __call($name, $arguments)
    {
        return null;
    }
    public function __isset($name)
    {
        return null;
    }
    public function __unset($name)
    {
        return $name;
    }
    public function __get($var)
    {
        return $var;
    }


    public function make(\Core\Interfaces\Pagination $pagination)
    {
       return $pagination;
    }
}