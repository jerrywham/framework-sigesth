<?php

namespace Core\Lib;

final class Token implements \Core\Interfaces\Token\Token {


    /**
     * Control the number of tokens a user can keep open.
     * This is most useful with one-time use tokens.  Since new tokens
     * are created on each request, having a hard limit on the number of open tokens
     * can be useful in controlling the size of the session file.
     *
     * When tokens are evicted, the oldest ones will be removed, as they are the most likely
     * to be dead/expired.
     *
     * @var integer
     */
    private $csrfLimit = 100;
    private $delay = 3600;
    private $baseUrl = '';
    private $session;
    private $filesTools;

    public function __construct(\Core\Interfaces\Auth\Session $session, string $baseUrl = 'index.php',int $delay = 3600, \Core\Interfaces\FilesTools $filesTools = null)
    { 
        $this->session = $session;
        $this->delay = $delay;
        $this->baseUrl = $baseUrl;
        $this->filesTools = $filesTools;
    }
    
    /**
     * To avoid $var to be changed because object should be immuable
     */
    public function __set($var,$value)
    {
        return null;
    }
    public function __call($name, $arguments)
    {
        return null;
    }
    public function __isset($name)
    {
        return null;
    }
    public function __unset($name)
    {
        return $name;
    }
    public function __get($var)
    {
        return $var;
    }

    /**
     * Return a new token, to put on a hidden input field
     * @return [string] [token]
     */
    public function getTokenPostMethod(): string {

        $overflow = count($this->session->get('formtoken')) - $this->csrfLimit;

        if ($overflow > 0 && empty($_POST)) {
            $this->session->set('formtoken', array_slice($this->session->get('formtoken'), $overflow + 1, $this->csrfLimit - 1, true));
        }

        $token = sha1(mt_rand(0, 1000000));
        $this->session->set('formtoken.'.$token, time());

        return $token;
    } 

    /**
     * Return a new token, to sent to given email
     * @return [string] [token]
     */
    public function getTokenEmailMethod(): string {

        $token = sha1(mt_rand(0, 1000000)).sha1(mt_rand(0, 1000000));
        $tokenArray = $this->getTokenArray();
        foreach ($tokenArray as $date => $value) {
            $tokenArray = $this->isValidToken($date,$tokenArray)['tokenArray'];
        }
        $tokenArray[date('Y-m-d\TH:i:s')] = $token;
        $this->filesTools->createJson(dirname(dirname(dirname(__DIR__))),$tokenArray,'token.json');

        return $token;
    }

    public function validateEmailToken():bool 
    {
        if ($_SERVER['REQUEST_METHOD']=='GET' && isset($_GET['token'])) {
            $tokenArray = $this->getTokenArray();
            if (in_array($_GET['token'], $tokenArray)) {
                $date = array_search($_GET['token'],$tokenArray);
                $isValidToken = $this->isValidToken($date,$tokenArray);
                $tokenArray = $isValidToken['tokenArray'];
                if ($isValidToken['valid']) {
                    # Token can be used only one time
                    unset($tokenArray[$date]);
                }
                $this->filesTools->createJson(dirname(dirname(dirname(__DIR__))),$tokenArray,'token.json');
                return $isValidToken['valid'];
            }
        }
        return false;
    }

    public function isValidToken(string $date,array $tokenArray):array
    {
        $now = new \DateTime('now + '.$this->delay.' seconds');
        $limit = (new \DateTime($date))->add(new \DateInterval('PT'.$this->delay.'S'));
        $valid = true;
        if ($now >= $limit) {
            $valid = false;
            unset($tokenArray[$date]);
        }
        return ['valid' => $valid, 'tokenArray' => $tokenArray];
    }

    private function getTokenArray()
    {
        $tokenArray = [];
        if (is_file(dirname(dirname(dirname(__DIR__))).DIRECTORY_SEPARATOR.'token.json')) {
            $tokenArray = json_decode(file_get_contents(dirname(dirname(dirname(__DIR__))).DIRECTORY_SEPARATOR.'token.json'),true);
        }
        return $tokenArray;
    }

    /**
     * Méthode qui valide la durée de vide d'un token
     *
     * @parm    $delay (timelife of token in seconds)
     * @return  stdio/null
     * @author  Stephane F
     **/
    public function validateFormToken():bool {

        if($_SERVER['REQUEST_METHOD']=='POST' 
            AND $this->session->get('formtoken') !== null 
            AND !isset($_SERVER['HTTP_X_CSRF_TOKEN']) 
            AND !isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
            if(empty($_POST['token']) 
                OR $this->session->get('formtoken.'.$_POST['token']) < (time() - $this->delay) ) { # seconds
                return false;
            } else {
                $this->session->remove('formtoken.'.$_POST['token']);
                return true;
            }
        }
        if ($_SERVER['REQUEST_METHOD']=='POST' 
            AND isset($_SERVER['HTTP_X_CSRF_TOKEN'])) {
            if(empty($_SERVER['HTTP_X_CSRF_TOKEN']) 
                OR $_SERVER['HTTP_X_CSRF_TOKEN'] != $_POST['token'] 
                OR $this->session->get('formtoken.'.$_SERVER['HTTP_X_CSRF_TOKEN']) < (time() - $this->delay) ) {  # seconds
                if (!empty($_SERVER['HTTP_X_CSRF_TOKEN'])) {
                    $this->session->remove('formtoken.'.$_SERVER['HTTP_X_CSRF_TOKEN']);
                }
                return false;
            }
            if (!array_key_exists($_SERVER['HTTP_X_CSRF_TOKEN'], $this->session->get('formtoken'))){
                $this->session->set('formtoken',array());
            }
            $this->session->remove('formtoken.'.$_SERVER['HTTP_X_CSRF_TOKEN']);
            return true;
        }
        return false;
    }
}
?>