<?php 

namespace Core\Lib;

// PECL libsodium 0.2.1 and newer
// Installation 
//      apt-get install libsodium-dev
//      sudo apt-get install php-pear
//      sudo apt-get install php5-dev (or php7.0-dev)
//      pecl install libsodium
// And add the following line to your php.ini file:
// extension=sodium.so
//      sudo service apache2 restart
// 
// After installing both the library and the PHP extension, 
// make a quick test script to verify that you have the correct version of libsodium installed.
// 
// var_dump([
//     SODIUM_LIBRARY_MAJOR_VERSION,
//     SODIUM_LIBRARY_MINOR_VERSION,
//     SODIUM_LIBRARY_VERSION
// ]);
// 
// If you're using libsodium 1.0.14, you should see this when you run this test script:

// user@hostname:~/dir$ php version_check.php
// array(2) {
//   [0] =>
//   int(9)
//   [1] =>
//   int(6)
//   [2] =>
//   string(6) "1.0.14"
// }
// To test :
// 1) include library
// 
// 2) Do this once then store it somehow:
// $string_key = \Sodium\randombytes_buf(\Sodium\CRYPTO_SECRETBOX_KEYBYTES);
// $file_key = \Sodium\sodium_crypto_secretstream_xchacha20poly1305_keygen();
// 
// $message = 'We are all living in a yellow submarine';
// $cryptoTools = new cryptoTools();
//
// 3) For string
// $ciphertext = $cryptoTools->safeEncrypt($message,$string_key);
// $plaintext = $cryptoTools->safeDecrypt($ciphertext,$string_key);

// var_dump($ciphertext);
// var_dump($plaintext);
// 
// 4) For file
// $input_file = '/tmp/example.original';
// $encrypted_file = '/tmp/example.enc';
// $decrypted_file = '/tmp/example.dec';
// 
// $cryptoTools->safeEncryptFile($input_file,$encrypted_file, $file_key);
// $cryptoTools->safeDecryptFile($encrypted_file,decrypted_file, $file_key);

final class CryptoTools implements \Core\Interfaces\CryptoTools{

    private $chunk_size = 4096;
    private $block_size = 16;

    public function __construct($chunk_size = 4096, $block_size = 16){
        if ($chunk_size > $this->chunk_size) {
            $this->chunk_size = $chunk_size;
        }
        if ($block_size > $this->block_size) {
            $this->block_size = $block_size;
        }
    }

    /**
     * Encrypt a message
     * 
     * @param string $message - message to encrypt
     * @param string $key - encryption key
     * @return string
     */
    public function safeEncrypt($message, $key) {

        if (!function_exists('sodium_pad')) {
            return $this->encrypt($message,$key);
        }

        $nonce = random_bytes(SODIUM_CRYPTO_SECRETBOX_NONCEBYTES);
        $padded_message = sodium_pad($message, $this->block_size);
        $cipher = base64_encode(
            $nonce.
            sodium_crypto_secretbox(
                $padded_message,
                $nonce,
                $key
            )
        );
        if (is_numeric($message)) $message = strval($message);
        sodium_memzero($message);
        sodium_memzero($key);
        return $cipher;
    }

      /**
       * Decrypt a message
       * 
       * @param string $encrypted - message encrypted with safeEncrypt()
       * @param string $key - encryption key
       * @return string
       */
    public function safeDecrypt($encrypted,$key)
      {   

        if (!function_exists('sodium_unpad')) {
            return $this->decrypt($encrypted,$key);
        }
        
        $e = base64_decode($encrypted);
        if ($e === false) {
            throw new \Exception('The encoding failed');
        }
        if (mb_strlen($e, '8bit') < (SODIUM_CRYPTO_SECRETBOX_NONCEBYTES + SODIUM_CRYPTO_SECRETBOX_MACBYTES)) {
            throw new \Exception('The message was truncated');
        }
        $nonce = mb_substr($e, 0, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, '8bit');
        $message = mb_substr($e, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, null, '8bit');
        $decrypted_padded_message = sodium_crypto_secretbox_open($message, $nonce, $key);
        if ($decrypted_padded_message === false) {
             throw new \Exception('The message was tampered with in transit');
        }
        if (is_numeric($message)) $message = strval($message);
        sodium_memzero($message);
        sodium_memzero($key);
        return sodium_unpad($decrypted_padded_message, $this->block_size);
      }  
    
    /*
    * Needs php 7.2+
    */
    public function safeEncryptFile($input_file, $encrypted_file, $key)
    {
        $fd_in = fopen($input_file, 'rb');
        $fd_out = fopen($encrypted_file, 'wb');

        list($stream, $header) = sodium_crypto_secretstream_xchacha20poly1305_init_push($key);

        fwrite($fd_out, $header);

        $tag = SODIUM_CRYPTO_SECRETSTREAM_XCHACHA20POLY1305_TAG_MESSAGE;
        do {
            $chunk = fread($fd_in, $this->chunk_size);
            if (feof($fd_in)) {
                $tag = SODIUM_CRYPTO_SECRETSTREAM_XCHACHA20POLY1305_TAG_FINAL;
            }
            $encrypted_chunk = sodium_crypto_secretstream_xchacha20poly1305_push($stream, $chunk, '', $tag);
            fwrite($fd_out, $encrypted_chunk);
        } while ($tag !== SODIUM_CRYPTO_SECRETSTREAM_XCHACHA20POLY1305_TAG_FINAL);
        sodium_memzero($key);
        fclose($fd_out);
        fclose($fd_in);
    }
    

    /*
    * Needs php 7.2+
    */
    public function safeDecryptFile($encrypted_file, $decrypted_file, $key)
    {
        $fd_in = fopen($encrypted_file, 'rb');
        $fd_out = fopen($decrypted_file, 'wb');

        $header = fread($fd_in, SODIUM_CRYPTO_SECRETSTREAM_XCHACHA20POLY1305_HEADERBYTES);

        $stream = sodium_crypto_secretstream_xchacha20poly1305_init_pull($header, $key);
        do {
            $chunk = fread($fd_in, $this->chunk_size + SODIUM_CRYPTO_SECRETSTREAM_XCHACHA20POLY1305_ABYTES);
            list($decrypted_chunk, $tag) = sodium_crypto_secretstream_xchacha20poly1305_pull($stream, $chunk);
            fwrite($fd_out, $decrypted_chunk);
        } while (!feof($fd_in) && $tag !== SODIUM_CRYPTO_SECRETSTREAM_XCHACHA20POLY1305_TAG_FINAL);
        $ok = feof($fd_in);
        
        sodium_memzero($key);
        fclose($fd_out);
        fclose($fd_in);

        if (!$ok) {
            die('Invalid/corrupted input');
        }
    }

    public function encrypt($plaintext, $key) {
        $method = "AES-256-CBC";
        $key = hash('sha256', $key, true);
        $iv = openssl_random_pseudo_bytes(16);

        $ciphertext = openssl_encrypt($plaintext, $method, $key, OPENSSL_RAW_DATA, $iv);
        $hash = hash_hmac('sha256', $ciphertext, $key, true);

        return $iv . $hash . $ciphertext;
    }

    public function decrypt($ivHashCiphertext, $key) {
        $method = "AES-256-CBC";
        $iv = substr($ivHashCiphertext, 0, 16);
        $hash = substr($ivHashCiphertext, 16, 32);
        $ciphertext = substr($ivHashCiphertext, 48);
        $key = hash('sha256', $key, true);

        if (hash_hmac('sha256', $ciphertext, $key, true) !== $hash) return null;

        return openssl_decrypt($ciphertext, $method, $key, OPENSSL_RAW_DATA, $iv);
    }
}