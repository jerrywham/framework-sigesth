<?php
namespace Core\Lib;

final class Authentication implements \Core\Interfaces\Auth\Authentication{
	private $users;
	private $passwordTools;
	private $session;
	private $usernameColumn;
	private $passwordColumn;
	// options
    private $inactivityTimeout = 3600;//1 hour
    private $disableSessionProtection = false;

	public function __construct(\Core\Interfaces\DatabaseTable $users,\Core\Interfaces\Auth\PasswordTools $passwordTools,\Core\Interfaces\Auth\Session $session, $usernameColumn, $passwordColumn, $options = []) {
		(session_id() === '' ? session_start() : null);
		$this->users = $users;
		$this->passwordTools = $passwordTools;
		$this->session = $session;
		$this->usernameColumn = $usernameColumn;
		$this->passwordColumn = $passwordColumn;

        $this->loadOptions($options);
        ini_set('session.gc_maxlifetime', $this->inactivityTimeout);

	}

	/**
     * To avoid $var to be changed because object should be immuable
     */
    public function __set($var,$value)
    {
        return null;
    }
    public function __call($name, $arguments)
    {
        return null;
    }
    public function __isset($name)
    {
        return null;
    }
    public function __unset($name)
    {
        return $name;
    }
    public function __get($var)
    {
        return $var;
    }


	public function login(string $username, string $password):bool {
		$user = current($this->users->find([$this->usernameColumn], [strtolower($username)]));
		if (!empty($user) && $this->passwordTools->validate_password($password, $user->{$this->passwordColumn})) {
			session_regenerate_id();
			if ($this->passwordTools->isPasswordNeedsRehash($password, $user->{$this->passwordColumn}) ) {
				$user->{$this->passwordColumn} = $this->passwordTools->create_hash($password);
                foreach ($user as $key => $value) {
                    if (!in_array($key, ['id','password'])) {
                        unset($user->$key);
                    }
                }
				$this->users->save($user);
			}
			// Generate unique random number to sign forms (HMAC)
	        $this->session->set('uid', sha1(uniqid('', true).'_'.mt_rand()));
	        $this->session->set('ip', $this->allIPs());
	        $this->session->set('username', strtolower($username));
	        // Set session expiration.
	        $this->session->set('expires_on', time() + $this->inactivityTimeout);
			return true;
		}
		else {
			return false;
		}
	}

    public function userExists(string $login):bool
    {   
        return (bool) $this->users->find([$this->usernameColumn], [ strtolower(strip_tags($login)) ] );
    }

    public function firstConnexion()
    {
        return !(bool) $this->users->total();
    }

	public function isLoggedIn():bool {

		if (!(bool)$this->session->get('uid')
            || ($this->disableSessionProtection === false
                && $this->session->get('ip') !== $this->allIPs())
            || time() >= $this->session->get('expires_on')) {
			$this->session->remove('');
            return false;
        }
        // User accessed a page : Update his/her session expiration date.
        $this->session->set('expires_on', time() + $this->inactivityTimeout);
        if (!empty($this->session->get('longlastingsession'))) {
            $this->session->set('expires_on', $this->session->get('expires_on') + $this->session->get('longlastingsession'));
        }

        return true;
	}

    public function loggedInUntil(string $format = 'd/m/Y H\hi\ms\s'):string
    {
        if ($this->isLoggedIn()) {
            $expire = $this->session->get('expires_on');
            return date($format, $expire);
        }
        return '';
    }
	
	public function getUser(string $column = '') {
		if ($this->isLoggedIn()) {
            $user = current($this->users->find([$this->usernameColumn], [strtolower($_SESSION['username'])] ));
            if (isset($user->{$column})) {
                return $user->{$column};
            }
            return $user;
		}
		else {
			return null;
		}
	}

    public function msg(string $format = '%1$s L_connected %2$s',string $dateFormat = 'H \h i \m' ):string
    {
       if($this->isLoggedIn()) {
            return sprintf($format,$this->getUser('name'),'. <span id="clock" class="clockt">L_Time_before_disconnection : <span id="countdown"></span></span>');
       }
       return '';
    }

    private function allIPs() {
        return $this->session->getServer('REMOTE_ADDR','')
            .'_'.$this->session->getServer('HTTP_X_FORWARDED_FOR', '')
            .'_'.$this->session->getServer('HTTP_CLIENT_IP', '');
    }

    private function loadOptions($options) {
        foreach($options as $key => $option) {
            if (in_array($key, ['disableSessionProtection', 'inactivityTimeout'])) {
                $this->$key = $option;
            }
        }
    }
}