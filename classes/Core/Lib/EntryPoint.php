<?php
namespace Core\Lib;

# unique id to authorize scripts/css inline and eval in js (for emails)
# we don't make it a constant so that the id can change with each page loading
define('CSP_RAND', md5(mt_rand(0, 1000000)));

final class EntryPoint {

	private $root;
	private $route;
	private $method;
	private $params;
	private $routes;
	private $layout;
	private $theme;
	private $urlroot;
	private $charset;
	private $paginator;
	private $pagination;
	private $session;
	private $token;
	private $filesTools;
	private $stringTools;
	private $configuration;
	private $medias;
	private $lang = 'fr/fr';
	private $Lang = [];
	private $loginBeforeDisplay;
	private $routeByDefault;
	private $pluginsAvailable = [];
	private $pluginsEnabled = [];
	private $pluginsHooks = [];
	private $encryptKey;
	private $cryptoTools;


	public function __construct(
		string $route, 
		string $method, 
		string $params,
		\Core\Interfaces\Routes $routes, 
		\Core\Interfaces\Layout $layout, 
		\Core\Interfaces\Paginator $paginator, 
		string $pagination,
		\Core\Interfaces\Auth\Session $session,
		\Core\Interfaces\Token\Token $token, 
		\Core\Interfaces\FilesTools $filesTools, 
		\Core\Interfaces\StringTools $stringTools,
		\Core\Interfaces\Configuration $configuration,
		\App\Interfaces\Medias $medias, 
		\Core\Interfaces\CryptoTools $cryptoTools, 
		string $theme, 
		string $urlroot,
		bool $loginBeforeDisplay = true, 
		string $routeByDefault = 'home', 
		string $lang = 'fr/fr', 
		string $encryptKey = null,
		string $charset = 'utf-8'

	) {
		$this->root = dirname(dirname(dirname(__DIR__)));
		$this->route = $route;
		$this->routes = $routes;
		$this->layout = $layout;
		$this->method = $method;
		$this->params = $params;
		$this->paginator = $paginator;
		$this->pagination = $pagination;
		$this->theme = $theme;
		$this->urlroot = $urlroot;
		$this->session = $session;
		$this->token = $token;
		$this->filesTools = $filesTools;
		$this->stringTools = $stringTools;
		$this->configuration = $configuration;
		$this->medias = $medias;

		
		$this->pluginsAvailable = $this->getPluginsAvailable();
		# Traduction
		$this->lang = $lang;
		$this->Lang = $this->Lang($lang,$theme);
		$this->stringTools->setLang($this->Lang);
		$this->charset = $charset;
		$this->loginBeforeDisplay = $loginBeforeDisplay;
		$this->routeByDefault = ($loginBeforeDisplay === true ? 'login' : (empty($routeByDefault) ? 'home' : $routeByDefault) );
		$this->encryptKey = $encryptKey;
		$this->cryptoTools = $cryptoTools;
		$this->checkUrl();
		
		$this->loadPlugins();

		
	}

	private function Lang(string $lang,string $theme):array
	{
		$Lang = [];
		$lang = str_replace('/',DIRECTORY_SEPARATOR,$lang);
		# Generic indexes file
		$genericFileLang = $this->root.DIRECTORY_SEPARATOR.'lang'.DIRECTORY_SEPARATOR.$lang.'.php';
		if (is_file($genericFileLang)) {
			include $genericFileLang;
			$Lang = $CORE_LANG ?? array();
			# Specific indexes file (for the theme)
			$themeFileLang = $theme.DIRECTORY_SEPARATOR.'lang'.DIRECTORY_SEPARATOR.$lang.'.php';
			if (is_file($themeFileLang)) {
				include $themeFileLang;
				$Lang = array_merge($CORE_LANG, ($LANG ?? array()));
			}
			# Add specific plugins indexes lang
			foreach ($this->pluginsAvailable as $key => $plugin) {
				$pluginFileLang = dirname(dirname(__DIR__)).DIRECTORY_SEPARATOR.'Plugins'.DIRECTORY_SEPARATOR.$plugin.DIRECTORY_SEPARATOR.'lang'.DIRECTORY_SEPARATOR.$lang.'.php';
				if (is_file($pluginFileLang)) {
					include $pluginFileLang;
					$Lang = array_merge($Lang, ($PLUGIN_LANG ?? array()));
				}
			}
			return $Lang;
		} else {
			throw new Exception('Error : '.$lang.' file doesn\'t exist', 1);
		}
	}

	/**
     * To avoid $var to be changed because object should be immuable
     */
    public function __set($var,$value)
    {
        return null;
    }
    public function __call($name, $arguments)
    {
        return null;
    }
    public function __isset($name)
    {
        return null;
    }
    public function __unset($name)
    {
        return $name;
    }
    public function __get($var)
    {
        return $var;
    }


	private function checkUrl() {
		if ($this->route !== strtolower($this->route)) {
			http_response_code(301);
			header('location: ' . $this->getRoot() .'/'. strtolower($this->route));
			exit();
		}
	}

	private function loadTemplate($templateFileName, $variables = []) {

		$view = $this->theme . DIRECTORY_SEPARATOR. $templateFileName;

		if (!is_file($this->theme . DIRECTORY_SEPARATOR. $templateFileName)) {
			if (!is_file($templateFileName)) {
				'';
			} else {
				$view = $templateFileName;
			}
		}

		if (!is_file($view)) {
			return false;
		}

		$paginator = $this->paginator;
		$pagination = $this->pagination;

		$stringTools = $this->stringTools;

		$LANG = $this->Lang;

		extract($variables);

		if (isset($menu) && is_file($menu)) {
			ob_start();
			include_once $menu;
			$menu = ob_get_clean();
		}
		
		ob_start();
		include $view;

		$template = ob_get_clean();

		$template = (isset($absolutePath) && $absolutePath === true ? $this->absolutePath($template) : $template);
		$template = strtr($template,$LANG);
		// $template = str_replace(array_keys($this->lang),array_values($this->lang), $template);

		# we compresse if possible
		if($encoding == $this->httpEncoding()) {
			header('Content-Encoding: '.$encoding);
			$template = gzencode($template,-1,FORCE_GZIP);
	    }

		$this->headers($headers, $isIframeAllowed);

	    return $template;

	}
	/**
	 * Display page
	 * @param  array  $javascriptFiles [Javascript files loaded on every pages]
	 * @param  string $alertFile       [html file with code for alert block]
	 * @return [type]                  [display page on media]
	 */
	public function run(array $javascriptFiles, string $alertFile = 'alert.html.php') {

		$routes = $this->getRoutes();

		$authentication = $this->routes->getAuthentication();
		$isFirstConnexion = $authentication->firstConnexion();

		$isCss = $this->checkDisplay($routes,['authentication'=>$authentication,'isFirstConnexion'=>$isFirstConnexion]);

		$controller = $routes[$this->route][$this->method]['controller'];
		$action = $routes[$this->route][$this->method]['action'];
		$page = $this->getPage($routes, $controller, $action, $authentication, $alertFile, $javascriptFiles, $isCss);

		# Compress and display css
		if ($isCss === true) {
			$this->headers(['cache' => 'text/css']);
			echo $page;			
			exit();
		}
		$page['title'] = $page['title'];
		$template = $this->getTemplate($controller,$routes,$page);

		unset($page['template']);

		$page['encoding'] = ($page['encoding'] ?? false);

		$page['output'] = $this->loadTemplate($template,$this->layout->vars($isCss, $page));
		
		if (isset($page['break']) && $page['break'] == true) {
			echo $page['output'];
			exit();
		}

		$page['encoding'] = ($page['encoding'] ?? 'gzip');
		
		echo $this->loadTemplate($this->layout->template(), $this->layout->vars($isCss, $page));
	}

	private function javascript()
	{	
		header('Content-Type: text/javascript');
		foreach ($this->pluginsEnabled as $key => $plugin) {
			if (method_exists($plugin,'javascript') ) {
				$plugin->javascript();
			}
		}
		# For javascript of plugins
		if (strpos($this->route,'assets/javascript/') === false) {
			$this->route = dirname($this->route);
		}
		# Compress and display javascript
		$jsFile = str_replace('assets/javascript/','',$this->route);
		$packed = $this->theme. DIRECTORY_SEPARATOR.'javascript'.DIRECTORY_SEPARATOR.'packed'.DIRECTORY_SEPARATOR.$jsFile;
		if (is_file($packed)) {
			$js = file_get_contents($packed);
			if ('assets/javascript/'.md5($js).'.js' == $this->route) {
				# Translate js
				echo strtr($js,$this->Lang);
			}
		}
		exit();
	}

	private function loadJavascript(array $javascriptFiles, array $page)
	{
		$js = $javascriptFiles;
		if (isset($page['javascript']) && !empty($page['javascript'])) {
			foreach ($page['javascript'] as $key => $file) {
				if (!in_array($file, $javascriptFiles)) {
					$js[] = $file;
				}
			}
		}
		return $js;
	}

	private function isMedia()
	{
		$assets = $this->routes->getAssetsFilesPath();
		foreach ($assets as $key => $asset) {
			if (strpos($this->route, trim(strtolower($asset),'/')) !== false) {
				return $key;
			}
		}
		return false;
	}

	private function displayMedia($isMedia)
	{
		if ($isMedia !== false) {
			$assets = $this->routes->getAssetsFilesPath();
			$img = $this->theme.str_replace(['assets/','/',basename(strtolower($assets[$isMedia] )).DIRECTORY_SEPARATOR ],['',DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR.basename($assets[$isMedia]).DIRECTORY_SEPARATOR ],$this->route);
			if (is_file($img)) {
				$this->filesTools->readPict($img,$this->medias->getMediasAllowedExtensions());
			}
			exit();
		}
	}

	private function includeFile($routes)
	{
		$includeFile = $this->root.DIRECTORY_SEPARATOR.$routes[$this->route]['include']['dir'].$this->route;
		if (is_file($includeFile)) {
			$this->headers(['secure'], false);
			echo file_get_contents($includeFile);
		}
		exit;
	}

	private function checkDisplay($routes,$controls=[])
	{
		$isJs = preg_match('!([a-z0-9\/]*\.js)!',$this->route);
		$isCss = preg_match('!([a-z\/]*\.css)!',$this->route);
		$isFont = preg_match('!([a-z\/]*\.(eot|svg|ttf|woff|woff2))!',$this->route);
		if ($isJs == true) {
			$this->javascript();
		}

		$this->displayMedia($this->isMedia());

		extract($controls);

		if (isset($routes[$this->route]['include']) ) { 
			$this->includeFile($routes);
		}
		if (!isset($routes[$this->route]) && $isJs == false) {
			http_response_code(404);
			header('location: '.$this->urlroot.'error');
			exit();
		}
		if ( $this->loginBeforeDisplay === true  && !$authentication->isLoggedIn() && $isCss == false) {
			if ($isFirstConnexion === false && $this->route != $this->routeByDefault) {
				header('location: '.$this->urlroot.$this->routeByDefault);
				exit();
			}
		}
		else if ($isFirstConnexion === false && isset($routes[$this->route]['login']) && !$authentication->isLoggedIn() && $isCss == false) {
			header('location: '.$this->urlroot.'login/error');
			exit();
		}
		else if ($isFirstConnexion === false && isset($routes[$this->route]['permissions']) && !$this->routes->checkPermission($routes[$this->route]['permissions'])) {
			header('location: '.$this->urlroot.'login/permissionserror');	
			exit();
		}
		return (bool) $isCss;
	}

	private function pluginsHooks(string $name, \Core\Interfaces\Plugins $plugin, array $hooks)
	{
		$this->pluginsHooks[$name] = array('plugin' => $plugin,'hooks' => $hooks);
	}

    public function callHook(string $plugin, string $hook, array $variables = [])
    {
    	if (isset($this->pluginsHooks[$plugin])) {
	        if (in_array($hook,$this->pluginsHooks[$plugin]['hooks'])) {
	            if (in_array($hook, \get_class_methods(\get_class($this->pluginsHooks[$plugin]['plugin'])))) {
	                return $this->pluginsHooks[$plugin]['plugin']->{$hook}($variables);
	            }
	        }
	    } else {
	    	return null;
	    }
    }


    private function getTemplatePath($plugin)
    {
        return dirname(dirname(__DIR__)).DIRECTORY_SEPARATOR.dirname(str_replace('\\',DIRECTORY_SEPARATOR,\get_class($plugin))).DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR;
    }

    /**
     * fmmarzoa at librexpresion dot org
     * see http://php.net/manual/en/function.get-object-vars.php
     */
    private function obj2array ( &$Instance ) {
        $clone = (array) $Instance;
        $rtn = array ();
        $rtn['___SOURCE_KEYS_'] = $clone;

        while ( list ($key, $value) = each ($clone) ) {
            $aux = explode ("\0", $key);
            $newkey = $aux[count($aux)-1];
            $rtn[$newkey] = &$rtn['___SOURCE_KEYS_'][$key];
        }

        return $rtn;
    }

    private function getRoutes():array
    {
		$routes = $this->routes->getRoutes();

		foreach ($routes as $route => $params) {			
			if ($route != '' && preg_match('!'.$route.'!', $this->route, $matches)) {
				$new = $matches[0];
			} else {
				$new = $route;
			}
			$routes[$new] = $params;
		}

		# Add Plugins routes
		foreach ($this->pluginsEnabled as $key => $plugin) {
			$new = '';
			$pluginRoutes = $plugin->getRoutes();
			foreach ($pluginRoutes as $url => $params) {
				$new = '';
				if ($url != '') {
					if (preg_match('!'.$url.'!', $this->route, $matches)) {
						$new = $matches[0];
					} else {
						$new = $url;
					}
				}
				if (!isset($routes[$new])) {
					$routes[$new] = $params;
				}
				if(isset($params['overwright']) && $params['overwright'] === true) {
					if (isset($routes[$new]['origin'])) {
						$params['origin']['obj'] = $routes[$new]['origin']['obj'];
						$params['origin']['array'] = $routes[$new]['origin']['array'];
						$params['origin'][$this->method]['action'] = $routes[$new][$this->method]['action'];
						(isset($params['prev_plugin']) ? $params['prev_plugin'][] = get_class($routes[$new][$this->method]['controller']) : ($params['prev_plugin'] = [get_class($routes[$new][$this->method]['controller'])] ) );
						$routes[$new] = $params;
					} else {
						$params['origin']['obj'] = $routes[$new][$this->method]['controller'];
						$params['origin']['array'] = $this->obj2array($routes[$new][$this->method]['controller']);
						$params['origin'][$this->method]['action'] = $routes[$new][$this->method]['action'];
						$routes[$new] = $params;
					}
				} 
				$routes[$new] = $params;
			}
		}
		return $routes;
    }

    private function getPage(array $routes, $controller, string $action,  \Core\Interfaces\Auth\Authentication $authentication, string $alertFile, array $javascriptFiles, bool $isCss)
    {
    	# If a plugin have a method with the same name as controller have, origin controller is switched to the method
    	if ( isset($routes[$this->route]['origin']) ) {
			# origin without plugin treatment
			$OriginController = $routes[$this->route]['origin']['obj'];
			$OriginAction = $routes[$this->route]['origin'][$this->method]['action'];
			$OriginPage = $OriginController->$OriginAction();

			if (isset($routes[$this->route]['prev_plugin'])) {
				$page = $controller->$action($routes[$this->route]['origin'],$routes[$this->route]['origin'][$this->method]['action']);
				foreach ($routes[$this->route]['prev_plugin'] as $key => $prev) {
					$prev = new \ReflectionClass($prev);
					$p = ($prev->newInstance($this->lang, $this->theme,[], $authentication, $this->session, $this->urlroot))->$action($routes[$this->route]['origin'],$routes[$this->route]['origin'][$this->method]['action']);
					foreach ($p as $key => $value) {
						# if plugin change the value of OriginPage[key]
						if (array_key_exists($key,$OriginPage) && $OriginPage[$key] != $value) {
							$page[$key] = $value;
						}
						# if plugin add new index
						if (!array_key_exists($key,$OriginPage)) {
							if (array_key_exists($key,$page) && is_array($page[$key]) && $page[$key] != $value) {
								$page[$key][] = $value;
							} else {
								$page[$key] = $value;
							}
						}
					}
				}
			} else {
				$page = $controller->$action($routes[$this->route]['origin'],$routes[$this->route]['origin'][$this->method]['action']);
			}
		} else {
			$page = $controller->$action(null,null);
		}
		if ($isCss === false) {
			//Minimal params
			$page['authentication'] = $authentication;
		    $page['token'] = (isset($page['token']) ? $this->token->getTokenPostMethod() : false);
		    $page['lang'] = strtok($this->lang,'/');
		    $page['encryptKey'] = $this->encryptKey;
		    $page['cryptoTools'] = $this->cryptoTools;
			$page['mainTitle'] = $this->configuration->getInfos('appName');
			$page['sprintf'] = ($page['sprintf'] ?? true);
			$page['msg'] = $this->session->msg();
			$page['alert'] = $this->session->alert($page['msg'],$this->theme, $alertFile);
			$page['javascript'] = $this->loadJavascript($javascriptFiles,$page);//Packing can be done on layout page with Js object
	        $page['hooks'] = $this;
	    }
		return $page;
    }

    private function getTemplate($controller,$routes,$page)
    {
		if (strpos(\get_class($controller), 'Plugins\\') !== false && !isset($routes[$this->route]['origin'])) {
			return $this->getTemplatePath($controller).$page['template'];
		} else {
			return $this->layout->template((isset($page['template']) ? $page['template'] : ''));
		}
    }

	/**
	 * Check if url have a good format
	 *
	 * @param	site		url
	 * @return	boolean		true if url is good
	 **/
	private function checkSite(&$site, $reset=true) {

		$site = preg_replace('#([\'"].*)#', '', $site);
		# On vérifie le site via une expression régulière
		# Méthode Jeffrey Friedl - http://mathiasbynens.be/demo/url-regex
		# modifiée par Amaury Graillat pour prendre en compte la valeur localhost dans l'url
		if(preg_match('@\b((ftp|https?)://([-\w]+(\.\w[-\w]*)+|localhost)|(?:[a-z0-9](?:[-a-z0-9]*[a-z0-9])?\.)+(?: com\b|edu\b|biz\b|gov\b|in(?:t|fo)\b|mil\b|net\b|org\b|[a-z][a-z]\b))(\:\d+)?(/[^.!,?;"\'<>()\[\]{}\s\x7F-\xFF]*(?:[.!,?]+[^.!,?;"\'<>()\[\]{}\s\x7F-\xFF]+)*)?@iS', $site))
				return true;
		else {
			if($reset) $site='';
			return false;
		}
	}

	/**
	 * Display root url of the website
	 * @param  boolean $truncate [display protocol (http://, https://, ftp://) or not]
	 * @return [string]          [return root url]
	 */
	private function getRoot($truncate=false) {

		$protocol = ( (!empty($_SERVER['HTTPS']) AND $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['REQUEST_SCHEME']) && $_SERVER['REQUEST_SCHEME'] == 'https') ) ? 'https://' : "http://";
		$servername = $_SERVER['HTTP_HOST'];
		$serverport = (preg_match('/:[0-9]+/', $servername) OR $_SERVER['SERVER_PORT'])=='80' ? '' : ':'.$_SERVER['SERVER_PORT'];
		$dirname = preg_replace('/\/(core|plugins)\/(.*)/', '', dirname($_SERVER['SCRIPT_NAME']));
		$racine = rtrim($protocol.$servername.$serverport.$dirname, '/').'/';
		$racine = str_replace(array('webroot/','install/'), '', $racine);
		if(!$this->checkSite($racine, false))
			throw new Exception('Error: wrong or invalid url');
		if ($truncate){ 
			$root = substr($racine,strpos($racine, '://')+3,strpos($racine,basename($racine))+4);
			$racine = substr($root,strpos($root,'/'));
		}

		return dirname($racine);
	}

	/**
	 * Transform relatives urls in absolutes urls
	 * @param  [string] $template [content to change]
	 * @return [string]           [content to return]
	 */
	private function absolutePath($template)
	{
		$dirRoot = $this->getRoot(true);
		return str_replace(
			array('href="/','url=/','action="/','src="/', $dirRoot.'/'.$dirRoot, $dirRoot.$dirRoot),
			array('href="'.$this->getRoot().'/','url='.$this->getRoot().'/','action="'.$this->getRoot().'/','src="'.$this->getRoot().'/',$dirRoot,$dirRoot),
			$template);
	}

	private function headers(array $headers, bool $isIframeAllowed = false)
	{
		if (isset($headers['secure'])) {
			# Mise en place de headers permettant d'augmenter la sécurité
			# Les scripts ne peuvent provenir que du serveur
			header('Content-Security-Policy: default-src  \'self\'; script-src \'self\' '.$this->getRoot().' nonce-'.CSP_RAND.' \'unsafe-inline\' \'unsafe-eval\'; connect-src  \'self\' '.$this->getRoot().'; font-src  \'self\'; frame-src  \'self\'; img-src  \'self\' data:; media-src  \'self\'; object-src  \'self\'; style-src \'self\' '.$this->getRoot().' nonce-'.CSP_RAND.' \'unsafe-inline\'');
			if ($isIframeAllowed === false){
				# Les pages ne pourront pas être mise dans une iframe
				header('X-Frame-Options: DENY');
			}
			# Les données transiteront pas défaut en https (si la connexion sécurisée est disponible)
			header('Strict-Transport-Security: max-age=31536000; includeSubDomains');

			# On impose le charset
			header('Content-Type: text/html; charset='.$this->charset);
		}
		if (isset($headers['cache'])) {

			# On force la mise en cache et le type du fichier
			header('Content-Type: '.$headers['cache']);
			header('Expires: ' . gmdate( "D, d M Y H:i:s", time() + 31536000 ) . ' GMT');
			header("Cache-Control: public, max-age=31536000");

		}
		
	}

	/**
	 * Méthode qui retourne le type de compression disponible
	 *
	 * @return	stout
	 * @author	Stephane F., Amaury Graillat
	 **/
	private function httpEncoding() {
		if(headers_sent()){
			$encoding = false;
		}elseif(strpos($_SERVER['HTTP_ACCEPT_ENCODING'],'gzip') !== false){
			$encoding = 'gzip';
		}else{
			$encoding = false;
		}
		return $encoding;
	}

	private function getPluginsAvailable()
	{
		return array_keys($this->filesTools->listADir(dirname(dirname(__DIR__)).DIRECTORY_SEPARATOR.'Plugins', true,['.htaccess', 'index.html']));
	}


	private function loadPlugins()
	{
		$root = dirname(dirname(__DIR__));
		include dirname($root).DIRECTORY_SEPARATOR.'includes/DatabaseConnection.php';
		$root .= DIRECTORY_SEPARATOR;

		$pluginsDir = $root.'App'.DIRECTORY_SEPARATOR.'Plugins'.DIRECTORY_SEPARATOR;

		$this->filesTools->delTree($pluginsDir.'pluginsAvailable',true, false, ['index.html', '.htaccess']);
		$this->pluginsAvailable = $this->getPluginsAvailable();
		foreach ($this->pluginsAvailable as $key => $plugin) {
			# Plugins available
			$this->filesTools->createFileIfUnexists($pluginsDir.'pluginsAvailable'.DIRECTORY_SEPARATOR.$plugin);
			
			if (is_file($pluginsDir.'pluginsEnabled'.DIRECTORY_SEPARATOR.$plugin)) {
				$hookFile = $root.'Plugins'.DIRECTORY_SEPARATOR.$plugin.DIRECTORY_SEPARATOR.'hooks.php';
				if (is_file($hookFile))
				{
					include $hookFile;
				}
				if (!isset($hooks)) {
					$hooks = array();
				}
				# Plugins enabled
				$pluginClassName = '\Plugins\\'.$plugin.'\\'.$plugin;
				$pluginsTable =  new \Core\Lib\DatabaseTable($pdo, 'plugins', 'id', '\stdClass', ['id', 'plugin_name','params_name','params_value'], new \Vendors\J20Uuid(), new \Core\Lib\CryptoTools(), SODIUM_STRING_KEY, [], APP_UUID);
				$pluginActivate = new $pluginClassName(
					$this->lang, 
					$this->theme, 
					$hooks, 
					$this->routes->getAuthentication(), 
					$this->session, 
					$this->urlroot, 
					$this->medias,
					new \Core\Lib\DatabaseTable(
						$pdo, 
						'plugins', 
						'id', 
						$pluginClassName, 
						[
							$this->lang, 
							$this->theme, 
							$hooks, 
							$this->routes->getAuthentication(), 
							$this->session, 
							$this->urlroot,
							&$pluginsTable, 
							$this->routes->getMediasDir(),
							'id', 
							'plugin_name',
							'params_name', 
							'params_value'
						], new \Vendors\J20Uuid(), new \Core\Lib\CryptoTools(), SODIUM_STRING_KEY, [], APP_UUID), 
					$this->routes->getMediasDir()
				);
				# Add hooks
				$this->pluginsHooks($pluginClassName, $pluginActivate, $hooks);

				$this->pluginsEnabled[$plugin] = $pluginActivate;
			} else {

			}

		}
	}
}