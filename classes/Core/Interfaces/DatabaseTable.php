<?php
namespace Core\Interfaces;

interface DatabaseTable {
    public function total(array $fields = [], $table2 = null, $join = null);
    public function findById($value, string $select = '*');
    public function findInAllTables(string $table2, string $select = '*', $orderBy = null, $limit = null, $offset = null, $join = 'INNER');
    public function find(array $columns, array $values, array $comparator = ['='], string $select = '*', $orderBy = null, $limit = null, $offset = null);
    public function findAll(string $select = '*', $orderBy = null, $limit = null, $offset = null);
    public function delete($id);
    public function deleteWhere($column, $value);
    public function save($record);
}