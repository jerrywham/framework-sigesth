<?php 

namespace Core\Interfaces\Token;

interface Token {
    public function getTokenPostMethod():string;
    public function validateFormToken():bool;
}