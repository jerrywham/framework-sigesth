<?php
namespace Core\Interfaces\Auth;

interface Ban {
    public function ban_loginFailed();
    public function ban_loginOk();
    public function logout();
    public function ban_canLogin($notSpamCode=''):bool;
}