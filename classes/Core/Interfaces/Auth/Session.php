<?php
namespace Core\Interfaces\Auth;

interface Session {
    public function get($key);
    public function set($key, $value);
    public function remove($keys);
    public function start();
    public function isActive();
    public function setMsg($msg,$type);
    public function msg():array;
    public function alert(array $msg,string $theme, string $alertFile):string;
    public function isLoggedIn():bool;
}
