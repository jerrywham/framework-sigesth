<?php 

namespace Core\Interfaces\Auth;

interface PasswordTools {
    public function create_hash($password);
    public function validate_password($password, $good_hash);
    public function isPasswordNeedsRehash($password,$hash);
}