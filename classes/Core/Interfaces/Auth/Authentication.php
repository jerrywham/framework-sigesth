<?php
namespace Core\Interfaces\Auth;

interface Authentication {
    public function login(string $username, string $password): bool;
    public function isLoggedIn(): bool;
    public function getUser();
}



