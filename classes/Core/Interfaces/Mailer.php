<?php
namespace Core\Interfaces;

interface Mailer {
    public function getMailer(bool $bool);
}