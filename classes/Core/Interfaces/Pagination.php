<?php
namespace Core\Interfaces;

interface Pagination {
    public function __construct(int $total, int $page, int $elementsByPage, array $variables = array());
    public function displayPagination(string $page, string $classCurrentPage, string $classPages): string;
    public function getPage(): int;
}