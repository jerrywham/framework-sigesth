<?php
namespace Core\Interfaces;

interface StringTools {
    public function setLang(array $lang): array;
    public function t(array $args,string $singular,string $plural):string;
    public function mb_ucfirst(string $string):string;
}