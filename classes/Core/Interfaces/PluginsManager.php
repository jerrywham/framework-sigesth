<?php
namespace Core\Interfaces;

interface PluginsManager {
    public function __construct(string $theme, \Core\Interfaces\Auth\Authentication $authentication, \Core\Interfaces\Auth\Session $session, string $urlroot);
    public function list();
    public function activatePlugins();
}