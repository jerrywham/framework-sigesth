<?php
namespace Core\Interfaces;

interface PostMail {
    public function sendTo(string $email);
}