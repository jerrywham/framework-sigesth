<?php
namespace Core\Interfaces;

interface Paginator {
    public function make(\Core\Interfaces\Pagination $pagination);
}