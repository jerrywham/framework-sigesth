<?php
namespace Core\Interfaces;

interface Routes {
	public function getRoutes(): array;
	public function getAuthentication(): \Core\Interfaces\Auth\Authentication;
	public function checkPermission($permission): bool;
}