<?php
namespace Core\Interfaces;

interface Plugins {
    public function __construct(string $default_lang, string $theme, array $hooks, \Core\Interfaces\Auth\Authentication $authentication, \Core\Interfaces\Auth\Session $session, string $urlroot, \App\Interfaces\Medias $medias = null, \Core\Interfaces\DatabaseTable $pluginsTable = null, string $mediasDir = '');
    public function onActivate();
    public function onDeactivate();
    public function getRoutes():array;
}