<?php
namespace Core\Interfaces;

interface FilesTools {
    public function getExtension(string $file):string;
    public function getFilename(string $file):array;
    public function getFileSeparator():string;
    public function getPrivateTag():string;
    public function cronDel(string $dir, array $exclude=array(), int $conservationInDays=30);
    public function createFileIfUnexists(string $path):bool;
    public function createDirIfUnexists(string $path):bool;
    public function delTree(string $dir,bool $isAdmin=false,bool $delDir=true,array $exclude=[]):bool; 
    public function listADir(string $startpath, bool $onlydir=false, array $exclude=array(), bool $order = false):array;
    public function return_bytes(string $size_str):int;
    public function bytes_for_human(int $size_int):string;
    public function upload(string $rootDir, string $baseurl, string $postlead, string $visibility, bool $allowuploads, array $supportedextentions, bool $browsedirs = true, string $separator = ''):array;
    public function generate_hash(string $password, int $cost=11):string;
    public function displayFiles(string $mediasDir,string $dir,array $files,string $type, array $filesIcon, string $dirForIcons, string $token, string $folderIcon = '', string $extOfFolderIcon = 'png', string $class = 'targetBlank', string $output = '#bubbleId #path #Dirname #folderIcon #class #thumb #file #mime-type #size #dimensions #dir #token #img'):string;
    public function base64_encode_image(string $filename, string $filetype):string;
    public function file2base64($source):string;
    public function base642file($source, $output,bool $md5=true):bool;
    public function ariane(string $path, string $root, string $separator = ' >> '):string;
    public function readPict(string $img,array $AllowedExtensions):string;
}