<?php
namespace Core\Interfaces;

interface JsPaker {
    public function pack($minification = true): string;
}