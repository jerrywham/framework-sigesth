<?php
namespace Core\Interfaces;

interface Configuration {
    public function getConfig();
    public function getInfos(string $value);
    public function safeEncrypt($string);
    public function safeDecrypt($string);
    public function saveConfig();
}