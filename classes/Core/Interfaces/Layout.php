<?php
namespace Core\Interfaces;

interface Layout {
    public function __construct(string $theme, string $template, string $menuTemplate);
    public function template(string $template=''): string;
    public function menu(array $vars): string;
    public function vars(bool $noReturn, array $vars): array;
}