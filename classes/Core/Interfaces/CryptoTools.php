<?php
namespace Core\Interfaces;

interface CryptoTools {
    public function safeEncrypt($message, $key);
    public function safeDecrypt($encrypted,$key);
    public function safeEncryptFile($input_file, $encrypted_file, $key);
    public function safeDecryptFile($encrypted_file, $decrypted_file, $key);
    public function encrypt($plaintext, $key);
    public function decrypt($ivHashCiphertext, $key);
}