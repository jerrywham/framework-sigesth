<?php
namespace Core\Interfaces;

interface MyPDO {
    public function __construct(string $databaseDir,string $file = 'my_setting.ini');
    public function checkUUIDColumns(string $table, array $columnsToEncrypt, string $primaryKey);
    public function prepare($sql, $options = null);
    public function formatDate($date, string $format = 'd/m/Y');
}