<?php
namespace Core\Interfaces;

interface PluginsConfiguration {

    public function __construct(\Core\Interfaces\Auth\Authentication $authentication, \Core\Interfaces\Auth\Session $session, $pluginsAvailable, string $urlroot, string $theme, \App\Interfaces\Medias $medias, \Core\Interfaces\DatabaseTable $pluginsTable, string $mediasDir, \Core\Interfaces\J20Uuid $J20Uuid, \Core\Interfaces\CryptoTools $cryptoTools, string $encryptKey = null, array $columnsToEncrypt = [], string $uuid = '');
    public function getParam($param);
    public function configuration():array;
    public function editConfiguration();
}