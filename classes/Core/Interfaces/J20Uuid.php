<?php
namespace Core\Interfaces;

interface J20Uuid {
    public function v4(bool $dashes = true): string;
    public function v5(string $namespace, string $name): string;
    public function is_valid(string $uuid): bool;
}