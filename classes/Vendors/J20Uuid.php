<?php 

namespace Vendors;

/**
 * UUID
 *
 * Generate a Universally Unique Identifier
 */

class J20Uuid implements \Core\Interfaces\J20Uuid {

    /**
     * Generate a UUID v4
     *
     * The UUID is 36 characters with dashes, 32 characters without.
     *
     * @return string  E.g. 67f71e26-6d76-4d6b-9b6b-944c28e32c9d
     */
    public function v4(bool $dashes = true) :string
    {
        if ($dashes)
        {
            $format = '%s-%s-%04x-%04x-%s';
        }
        else
        {
            $format = '%s%s%04x%04x%s';
        }

        return sprintf($format,

            // 8 hex characters
            bin2hex(openssl_random_pseudo_bytes(4)),

            // 4 hex characters
            bin2hex(openssl_random_pseudo_bytes(2)),

            // "4" for the UUID version + 3 hex characters
            mt_rand(0, 0x0fff) | 0x4000,

            // (8, 9, a, or b) for the UUID variant + 3 hex characters
            mt_rand(0, 0x3fff) | 0x8000,

            // 12 hex characters
            bin2hex(openssl_random_pseudo_bytes(6))
        );
    }

    public function v5(string $namespace, string $name):string {
       if(!$this->is_valid($namespace)) return false;

       // Get hexadecimal components of namespace
       $nhex = str_replace(array('-','{','}'), '', $namespace);

       // Binary Value
       $nstr = '';

       // Convert Namespace UUID to bits
       for($i = 0; $i < strlen($nhex); $i+=2) {
         $nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
       }

       // Calculate hash value
       $hash = sha1($nstr . $name);

       return sprintf('%08s-%04s-%04x-%04x-%12s',

         // 32 bits for "time_low"
         substr($hash, 0, 8),

         // 16 bits for "time_mid"
         substr($hash, 8, 4),

         // 16 bits for "time_hi_and_version",
         // four most significant bits holds version number 5
         (hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x5000,

         // 16 bits, 8 bits for "clk_seq_hi_res",
         // 8 bits for "clk_seq_low",
         // two most significant bits holds zero and one for variant DCE1.1
         (hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

         // 48 bits for "node"
         substr($hash, 20, 12)
       );
     }

     public function is_valid(string $uuid):bool {
       return preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?'.
                         '[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $uuid) === 1;
     }

}