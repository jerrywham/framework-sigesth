<?php
/**
 * namespace must be Plugins\nameOfThePlugin
 */
namespace Plugins\jquery;

class jquery implements \Core\Interfaces\Plugins {

    # To enable configuration.
    // const configuration = true;

    # Variables must be private for immutability of object
    # and initiate by constructor
    # see Tom Butler's blog for more explanations: https://r.je

    /**
     * mandatory variables
     */
    private $lang;
    private $theme;
    private $hooks = [];
    private $authentication;
    private $session;
    private $pluginsTable;
    private $id;
    private $plugin_name;
    private $params_name;
    private $params_value;
    private $mediasDir;
    private $urlroot;
    /**
     * if config is available
     * Respect structure of the array even if there is only one param
     */
    private $defaultParams = [
        
    ];

    public function __construct(string $default_lang,string $theme, array $hooks, \Core\Interfaces\Auth\Authentication $authentication, \Core\Interfaces\Auth\Session $session, string $urlroot, \App\Interfaces\Medias $medias = null, \Core\Interfaces\DatabaseTable $pluginsTable = null, string $mediasDir = '') {
        # lang file must be in pluginName/lang/lang.php (for example pluginName/fr/fr.php) directory
        # see sample/fr/fr.php for details
        $this->lang = $default_lang;
        $this->theme = $theme;
        $this->hooks = $hooks;
        $this->authentication = $authentication;
        $this->session = $session;
        $this->pluginsTable = $pluginsTable;
        $this->urlroot = $urlroot;

        $this->mediasDir = $mediasDir;
        if (!is_dir($this->mediasDir)) {
            @mkdir($this->mediasDir, 0777, true);
        }
    }
    /**
     * To avoid $var to be changed because object should be immuable
     */
    public function __set($var,$value)
    {
        return null;
    }
    public function __call($name, $arguments)
    {
        return null;
    }
    public function __isset($name)
    {
        return null;
    }
    public function __unset($name)
    {
        return $name;
    }
    /**
     * Exception for __get to allow pluginsConfiguration to access private properties in read mode
     * @param  [mixed] $var [var to access to]
     * @return [mixed]      [var]
     */
    public function __get($var)
    {
        return $this->$var;
    }

    /**
     * This method is called on plugin activation
     * 
     */
    public function onActivate() {
        
    }

     /**
     * This method is called on plugin deactivation
     * 
     */
    public function onDeactivate(){
        
    }
    /**
     * This method add routes to the initial application
     * Controller (in GET or POST method) should always be "$this" or must be defined before return
     */
    public function getRoutes():array {
        return [
        ];
    }

    ######################################
    #                                    #
    # ██  ██  ████   ████  ██  ██  ████  #
    # ██  ██ ██  ██ ██  ██ ██  ██ ██  ██ #
    # ██  ██ ██  ██ ██  ██ ██ ██   ██    #
    # ██████ ██  ██ ██  ██ ████     ██   #
    # ██  ██ ██  ██ ██  ██ ██ ██     ██  #
    # ██  ██ ██  ██ ██  ██ ██  ██ ██  ██ #
    # ██  ██  ████   ████  ██  ██  ████  #
    #                                    #
    ######################################
    /**
     * This method is called only if its name is declared on hooks.php file of the plugin
     */
    public function addJquery($variables = [])
    {
        $pages = $variables['page'] ?? ['medias([a-z0-9-_/]*)'];
        $version = $variables['version'] ?? 'jQuery-v3.3.1.js';
        $route = ltrim(str_replace(DIR_ROOT,'',strtok($_SERVER['REQUEST_URI'], '?')), '/');
        foreach ($pages as $key => $page) {
            if (preg_match('!'.$page.'!', $route)) {
                echo '<script src="/assets/javascript/'.(new \Core\Lib\Js([__DIR__.DIRECTORY_SEPARATOR.$version], 'Vendors\JavaScriptPacker', $this->theme))->pack(false).'" nonce="'.CSP_RAND.'"></script>
            ';
            } else {
                echo '';
            }
        }
    }
}