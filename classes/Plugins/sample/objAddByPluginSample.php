<?php
/**
 * namespace must be Plugins\nameOfThePlugin
 */
namespace Plugins\sample;

class objAddByPluginSample {

    public function __construct() {}

    public function __set($var,$value)
    {
        return null;
    }
    public function __get($var)
    {
        return null;
    }


    public function sampleMethodCalledByTemplate($value='')
    {
        return 'L_Methods_can_be_rewrited';
    }

    public function sample()
    {
        return [
                # template must be in pluginName/templates directory
                'template' => 'sample.html.php', 
                'title' => 'L_sample',
                'sampleHooks' => ['L_Data_recorded_successfully via $_POST' => true] + $_POST,
                'sampleObj' => $this,
                # Mandatory for all POST
                'token' => true
        ];
    }
}