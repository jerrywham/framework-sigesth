<?php
/**
 * This array list all the traduction terms available for this plugin
 * Name of the array must be $PLUGIN_LANG (with uppercase)
 */
$PLUGIN_LANG = array(
    'L_sample' => 'Example de plugin',
    'L_You_can_put_php_to_open_variables_of_the_plugin' => 'Vous pouvez appeler une variable php si elle est définie par le plugin.',
    'L_Or_a_method' => 'Ou une méthode du plugin',
    'L_var_defined_in_"layout.html.php"_template_of_default_theme,_called_by_hook_"sampleHook"_method._The_result_is_displayed_on_every_pages' => 'variable définie dans le fichier "layout.html.php" du thème par défaut, appelées par la méthode crochet "sampleHook". Le résultat est affiché sur toutes les pages.',
    'L_var_defined_in_"home.html.php"_template_of_default_theme,_called_by_hook_"sampleHook"_method._The_result_is_displayed_only_on_home_page' => 'variable définie dans le fichier "home.html.php" du thème par défaut, appelées par la méthode crochet "sampleHook". Le résultat est affiché uniquement sur la page d\'accueil.',
    'L_Menu_sample' => 'Exemple de menu',
    'L_Menu_added_by_sample_hook"sampleMenu"' => 'Exemple de menu ajouté par le crochet sampleMenu',
    'L_Plugin_configuration_page' => 'Page de configuration du plugin',
    'L_Sample' => 'Exemple',
    'L_Methods_can_be_rewrited' => 'Les méthodes du plugin peuvent être réécrites',
    'This is a sample plugin. Look at the commented code to know how it works.' => 'Voici un plugin d&#039;exemple. Étudiez le code commenté pour comprendre comment il fonctionne. Vous pourrez ainsi créer vos propres plugins. Voir également le wiki pour plus d&#039;explications.',
);