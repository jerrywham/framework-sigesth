<h2>L_sample</h2>
<p>L_You_can_put_php_to_open_variables_of_the_plugin :</p>
<pre style="position:relative;z-index:2000000000;opacity:0.6;border: 1px solid #e3af43; background-color: #f8edd5; padding: 10px;color:#111;white-space: pre;white-space: pre-wrap;word-wrap: break-word;"><strong>$sampleHooks :</strong> <?php print_r($sampleHooks);?>
</pre>
<p>L_Or_a_method</p>
<pre style="position:relative;z-index:2000000000;opacity:0.6;border: 1px solid #e3af43; background-color: #f8edd5; padding: 10px;color:#111;white-space: pre;white-space: pre-wrap;word-wrap: break-word;"><strong>$sample->sampleMethodCalledByTemplate('var passed to method') :</strong> <?=$sampleObj->sampleMethodCalledByTemplate('var passed to method');?>
</pre>
<form action="" method="post">
     <p>
        <label for="sample">L_Sample</label>
        <input name="sample" id="sample" type="text" value="sample">
    </p>
    <input type="submit" value="L_Submit">
    <input type="hidden" name="token" value="<?=$token;?>">
</form>