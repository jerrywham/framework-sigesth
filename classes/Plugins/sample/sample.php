<?php
/**
 * This plugin do nothing in particular but you can study the code to know how to do a new plugin
 *
 * 
 * namespace must be Plugins\nameOfThePlugin
 */
namespace Plugins\sample;

class sample implements \Core\Interfaces\Plugins {

    # To enable configuration. This constant is not mandatory
    const configuration = true;

    # Variables must be private for immutability of object
    # and initiate by constructor
    # see Tom Butler's blog for more explanations: https://r.je

    /**
     * mandatory variables
     */
    private $lang;
    private $theme;
    private $hooks = [];
    private $authentication;
    private $session;
    private $pluginsTable;
    private $id;
    private $plugin_name;
    private $params_name;
    private $params_value;
    private $mediasDir;
    private $urlroot;
    /**
     * Optionnal if config is available
     * Respect structure of the array even if there is only one param
     */
    private $defaultParams = [
        'param1' => [
            'params_name' => 'param1',  //the name of the param (the same as the key)
            'params_value' => null,     //the value of the param (mixed)
            'params_mandatory' => true, //(bool) if it is true, the param can not be blank 
        ],
        'param2' => [
            'params_name' => 'param2', 
            'params_value' => null,
            'params_mandatory' => false,
        ]
    ];
    private $param1;
    private $param2;

    public function __construct(string $default_lang,string $theme, array $hooks, \Core\Interfaces\Auth\Authentication $authentication, \Core\Interfaces\Auth\Session $session, string $urlroot, \App\Interfaces\Medias $medias = null, \Core\Interfaces\DatabaseTable $pluginsTable = null, string $mediasDir = '') {
        # lang file must be in pluginName/lang/lang.php (for example pluginName/fr/fr.php) directory
        # see sample/fr/fr.php for details
        $this->lang = $default_lang;
        $this->theme = $theme;
        $this->hooks = $hooks;
        $this->authentication = $authentication;
        $this->session = $session;
        $this->urlroot = $urlroot;
        $this->pluginsTable = $pluginsTable;
        $this->param1 = $this->defaultParams['param1']['params_value'];
        $this->param2 = $this->defaultParams['param2']['params_value'];
    }
    /**
     * To avoid $var to be changed because object should be immuable
     */
    public function __set($var,$value)
    {
        return null;
    }
    public function __call($name, $arguments)
    {
        return null;
    }
    public function __isset($name)
    {
        return null;
    }
    public function __unset($name)
    {
        return $name;
    }
    /**
     * Exception for __get to allow pluginsConfiguration to access private properties in read mode
     * @param  [mixed] $var [var to access to]
     * @return [mixed]      [var]
     */
    public function __get($var)
    {
        return $this->$var;
    }


    /**
     * This method is called on plugin activation
     * 
     */
    public function onActivate() {
        # In this example, a file "activated" is create on sample folder
        $this->mark('activated','deactivated');
        return 'Plugin other activated';
    }

     /**
     * This method is called on plugin deactivation
     * 
     */
    public function onDeactivate(){
        # In this example, a file "deactivated" is create on sample folder
        $this->mark('deactivated','activated');
        return 'Plugin other deactivated';
    }

    /**
     * This method add routes to the initial application
     * Controller (in GET or POST method) should always be "$this" or must be defined before return
     * Be careful : route MUST always be in lowercase. In case of uppercase, $_REQUEST could be empty
     */
    public function getRoutes():array {
        $newControllerSample = new \Plugins\sample\objAddByPluginSample();
        return [
            'sampletest' => [
                'GET' => [
                    'controller' => $this,
                    # Method sample of this object
                    'action' => 'sample'
                ],
                'POST' => [
                    'controller' => $newControllerSample,
                    # Method sample of objAddByPluginSample object
                    'action' => 'sample'
                ],
                # User must be logged in to see the page
                'login' => true,
                # and he have to have edit permission
                'permissions' => \Core\Lib\User::EDIT
            ],
            '[a-z]*/edit' => [
                'GET' => [
                    'controller' => $this,
                    # Access to edit method of the plugin
                    'action' => 'edit'
                ],
                'POST' => [
                    'controller' => $this,
                    # Recorder
                    'action' => 'saveEdit'
                ],
                # User must be logged in to see the page
                'login' => true,
                # and he have to have configuration permission
                'permissions' => \Core\Lib\User::EDIT,
                # Overwright the initial route
                'overwright' => true
            ],
        ];
    }

    public function edit(array $originController, string $originMethod)
    {

        $origin = $originController['obj'];
        $table = $origin->defaultTable->table;
        $content_index = $table.'text';
        
        $obj = (new \ReflectionObject($origin))->newInstance($origin->__get('defaultTable'),$origin->__get('authorsTable'), $origin->__get('categoriesTable'),$this->authentication, $this->session,$this->theme, $this->urlroot);

        $data = $origin->$originMethod();

        if (isset($_GET['id'])) {
            $content = $obj->defaultTable->findById($_GET['id']);
            $data[$table]->{$content_index} = '-->This is content added by plugin "sample"<--<br/><br/>'.$content->{$content_index};
        } 

        return $data;
    }

    public function saveEdit(array $originController, string $originMethod)
    {
        if (isset($originController['obj'])) {
            return $originController['obj']->$originMethod();
        }   
    }

    /**
     * Only for the example (used in onActivate and onDeactivate methods)
     */
    private function mark($touch,$unlink)
    {
        if (is_file(__DIR__.DIRECTORY_SEPARATOR.$unlink)) unlink(__DIR__.DIRECTORY_SEPARATOR.$unlink);
        touch(__DIR__.DIRECTORY_SEPARATOR.$touch);
    }

    ######################################
    #                                    #
    # ██  ██  ████   ████  ██  ██  ████  #
    # ██  ██ ██  ██ ██  ██ ██  ██ ██  ██ #
    # ██  ██ ██  ██ ██  ██ ██ ██   ██    #
    # ██████ ██  ██ ██  ██ ████     ██   #
    # ██  ██ ██  ██ ██  ██ ██ ██     ██  #
    # ██  ██ ██  ██ ██  ██ ██  ██ ██  ██ #
    # ██  ██  ████   ████  ██  ██  ████  #
    #                                    #
    ######################################

    /**
     * This method is called only if its name is declared on hooks.php file of the plugin
     */
    public function sampleHook($variables = [])
    {
       echo '<pre style="position:relative;z-index:2000000000;opacity:0.6;border: 1px solid #e3af43; background-color: #f8edd5; padding: 10px;color:#111;white-space: pre;white-space: pre-wrap;word-wrap: break-word;">Fichier '.__FILE__.', LIGNE '.__LINE__.'<br/><strong>sampleHook method :</strong> '; print_r($variables);echo('</pre>');
    }

    /**
     * This method is called in the sample.html.php template of the plugin
     */
    public function sampleMethodCalledByTemplate($value)
    {
        return $value;
    }

    /**
     * This method is called by the new route add by the plugin (see getRoutes method)
     */
    public function sample()
    {
        return [
                # template must be in pluginName/templates directory
                'template' => 'sample.html.php', 
                'title' => 'L_sample',
                'sampleHooks' => $this->hooks,
                'sampleObj' => $this,
                # Mandatory for all POST
                'token' => true
        ];
    }

    /**
     * This method add a new entry in nav on layout.html.php template
     */
    public function sampleMenu()
    {
        # This menu is called by sampleMenu hook, add in layout.html.php template.
        return '<li><a href="/sampletest" title="L_Menu_added_by_sample_hook"sampleMenu"">L_Menu_sample</a></li>';
    }

}