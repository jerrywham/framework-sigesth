<?php
/**
 * namespace must be Plugins\nameOfThePlugin
 */
namespace Plugins\openwysiwyg;

class openwysiwyg implements \Core\Interfaces\Plugins {

    # To enable configuration.
    const configuration = true;

    # Variables must be private for immutability of object
    # and initiate by constructor
    # see Tom Butler's blog for more explanations: https://r.je

    /**
     * mandatory variables
     */
    private $lang;
    private $theme;
    private $hooks = [];
    private $authentication;
    private $session;
    private $pluginsTable;
    private $id;
    private $plugin_name;
    private $params_name;
    private $params_value;
    private $mediasDir;
    private $medias;
    private $urlroot;
    private $version = 'v1.4.7';
    private $dirSrc = '';
    /**
     * if config is available
     * Respect structure of the array even if there is only one param
     */
    private $defaultParams = [
        'browsedirs' => [
            'params_name' => 'browsedirs',
            'params_value' => true,
            'params_mandatory' => true,
        ],
        'overwrightFilename' => [
            'params_name' => 'overwrightFilename',
            'params_value' => false,
            'params_mandatory' => true,
        ],
        'separator' => [
            'params_name' => 'separator',
            'params_value' => FILE_SEPARATOR,
            'params_mandatory' => true,
        ]
    ];

    private $supportedextentions;
    private $filetypes;
    private $filetypesPath;
    private $browsedirs = true;
    private $separator;

    public function __construct(string $default_lang,string $theme, array $hooks, \Core\Interfaces\Auth\Authentication $authentication, \Core\Interfaces\Auth\Session $session, string $urlroot, \App\Interfaces\Medias $medias = null, \Core\Interfaces\DatabaseTable $pluginsTable = null, string $mediasDir = '') {
        # lang file must be in pluginName/lang/lang.php (for example pluginName/fr/fr.php) directory
        # see openwysiwyg/fr/fr.php for details
        $this->lang = $default_lang;
        $this->theme = $theme;
        $this->hooks = $hooks;
        $this->authentication = $authentication;
        $this->session = $session;
        $this->pluginsTable = $pluginsTable;
        $this->medias = $medias;
        if ($this->medias != null) {
            $this->filetypesPath = $this->medias->getMediasFilesIconsPath();
            $this->filetypes = $this->medias->getMediasFilesIcons();
            $this->supportedextentions =  $this->medias->getMediasAllowedExtensions();
        }
        $this->browsedirs =  $this->defaultParams['browsedirs']['params_value'];
        $this->overwrightFilename =  $this->defaultParams['overwrightFilename']['params_value'];
        $this->separator =  $this->defaultParams['separator']['params_value'];
        $this->urlroot = $urlroot;

        $this->mediasDir = $mediasDir;
        if (!is_dir($this->mediasDir)) {
            @mkdir($this->mediasDir, 0777, true);
        }
        $this->dirSrc = __DIR__.DIRECTORY_SEPARATOR.'openwysiwyg_'.$this->version.DIRECTORY_SEPARATOR;
    }
    /**
     * To avoid $var to be changed because object should be immuable
     */
    public function __set($var,$value)
    {
        return null;
    }
    public function __call($name, $arguments)
    {
        return null;
    }
    public function __isset($name)
    {
        return null;
    }
    public function __unset($name)
    {
        return $name;
    }
    /**
     * Exception for __get to allow pluginsConfiguration to access private properties in read mode
     * @param  [mixed] $var [var to access to]
     * @return [mixed]      [var]
     */
    public function __get($var)
    {
        return $this->$var;
    }

    /**
     * This method is called on plugin activation
     * 
     */
    public function onActivate() {
        
    }

     /**
     * This method is called on plugin deactivation
     * 
     */
    public function onDeactivate(){
        
    }

    public function getSeparator()
    {
        return $this->separator;
    }

    /**
     * This method add routes to the initial application
     * Controller (in GET or POST method) should always be "$this" or must be defined before return
     */
    public function getRoutes():array {
        return [
            '[a-z]*/edit' => [
                'GET' => [
                    'controller' => $this,
                    # Access to edit method of the plugin
                    'action' => 'edit'
                ],
                'POST' => [
                    'controller' => $this,
                    # Recorder
                    'action' => 'saveEdit'
                ],
                # User must be logged in to see the page
                'login' => true,
                # and he have to have configuration permission
                'permissions' => \Core\Lib\User::EDIT,
                # Overwright the initial route
                'overwright' => true
            ],
            'editor/images/.*' => [
                'GET' => [
                    'controller' => $this,
                    'action' => 'displayImg'
                ],
                # User must be logged in to see the page
                'login' => true,
                # and he have to have configuration permission
                'permissions' => \Core\Lib\User::EDIT,
            ],
            'editor/styles/wysiwyg.css' => [
                'GET' => [
                    'controller' => $this,
                    'action' => 'displayCss'
                ],
                # User must be logged in to see the page
                'login' => true,
                # and he have to have configuration permission
                'permissions' => \Core\Lib\User::EDIT,
            ],
            'editor/addons.*' => [
                'GET' => [
                    'controller' => $this,
                    'action' => 'addons',
                ],
                'POST' => [
                    'controller' => $this,
                    'action' => 'uploadByAddon',
                ],
                # User must be logged in to see the page
                'login' => true,
                # and he have to have configuration permission
                'permissions' => \Core\Lib\User::EDIT,
            ],
            'editor/scripts' => [
                'GET' => [
                    'controller' => $this,
                    'action' => 'addons',
                ],
                # User must be logged in to see the page
                'login' => true,
                # and he have to have configuration permission
                'permissions' => \Core\Lib\User::EDIT,
            ],
            'editor/popups.*' => [
                'GET' => [
                    'controller' => $this,
                    'action' => 'addons',
                ],
                # User must be logged in to see the page
                'login' => true,
                # and he have to have configuration permission
                'permissions' => \Core\Lib\User::EDIT,
            ],
        ];
    }

    ######################################
    #                                    #
    # ██  ██  ████   ████  ██  ██  ████  #
    # ██  ██ ██  ██ ██  ██ ██  ██ ██  ██ #
    # ██  ██ ██  ██ ██  ██ ██ ██   ██    #
    # ██████ ██  ██ ██  ██ ████     ██   #
    # ██  ██ ██  ██ ██  ██ ██ ██     ██  #
    # ██  ██ ██  ██ ██  ██ ██  ██ ██  ██ #
    # ██  ██  ████   ████  ██  ██  ████  #
    #                                    #
    ######################################

    public function edit(array $originController, string $originMethod)
    {
        if (isset($originController['obj'])) {
            $template = $this->theme.DIRECTORY_SEPARATOR.$originController['obj']->$originMethod()['template'];

            $params = $originController['obj']->$originMethod();
            $content = file_get_contents($template);
            $params['javascript'][] = $this->dirSrc.'scripts'.DIRECTORY_SEPARATOR.'wysiwyg.js';                     
            $params['javascript'][] = $this->dirSrc.'scripts'.DIRECTORY_SEPARATOR.'wysiwyg-settings.js';
            $params['javascript'][] = $this->dirSrc.'scripts'.DIRECTORY_SEPARATOR.'wysiwyg-load.js';
            $params['noMinification'] = true;
            return $params;
        }
    }

    public function saveEdit(array $originController, string $originMethod)
    {
        if (isset($originController['obj'])) {
            return $originController['obj']->$originMethod();        
        }
    }

    public function addons()
    {
        $addon = str_replace('editor/','',$this->realRoute('editor/'));
        if (is_file($this->dirSrc.$addon)) {
            return [
                'template' => '..'.DIRECTORY_SEPARATOR.'openwysiwyg_'.$this->version.DIRECTORY_SEPARATOR.$addon,
                'title' => 'L_ADDON',
                'isIframeAllowed' => true,
                'break' => true,
                'token' => true
            ];
        }
    }

    public function javascript()
    {
        $route = str_replace('editor'.DIRECTORY_SEPARATOR,$this->dirSrc,$this->realRoute('editor/'));
        if (is_file($route) ) {
            $js = file_get_contents($route);
            echo $js;
        }   
    }

    public function getMediasDir()
    {
        return $this->mediasDir;
    }

    public function uploadByAddon()
    {   
        # Directory where to put the file
        if (!isset($_POST['baseurl'])) {
            return false;
        } else {
            $baseurl = trim(str_replace('.','',$_POST['baseurl'])).DIRECTORY_SEPARATOR;
            if (!is_dir($this->mediasDir.$baseurl)) {
                return false;
            }
        }

        # Visibility of the file (public or private)
        if (!isset($_POST['visibility'])) {
            return false;
        } else {
            $visibility = ($_POST['visibility'] == 'public' ? $_POST['visibility'] : 'private');
        }

        if (!isset($_GET['wysiwyg'])) {return [];}

        $allowuploads = (bool)$this->session->get('uid');

        $upload = $this->medias->uploadByFilesTools($this->mediasDir,$baseurl,($_POST['lead']??''),$visibility, $allowuploads,$this->browsedirs, $this->getSeparator());

        if ($baseurl == 'pict/') {
            $lib = 'image';
        } else {
            $lib = 'document';
        }
        if ($upload == false) {
            $msg = 'L_Error : L_Failed_to_move_uploaded_file';
        } else {
            $msg = 'L_Data_recorded_successfully';
        }

        return [
            'template' => $this->dirSrc.'addons'.DIRECTORY_SEPARATOR.$lib.'library'.DIRECTORY_SEPARATOR.'select_'.$lib.'.php',
            'title' => 'L_ADDON',
            'isIframeAllowed' => true,
            'break' => true,
            'token' => true,
            'up' => [
                'msg' => $msg,
                'leadon' => (trim($_POST['lead']) == '' ? '/' : $_POST['lead'])
            ]
        ];
        
    }

    public function displayCss()
    {
        echo file_get_contents($this->dirSrc.'styles'.DIRECTORY_SEPARATOR.'wysiwyg.css');
    }

    public function displayImg()
    {
        $img = basename($this->realRoute('editor/images'));
        $extensions = array('image/gif','image/jpeg','image/png','image/bmp');
        if (is_file($this->dirSrc.'images'.DIRECTORY_SEPARATOR.$img)) {
            $fileInfos = new \finfo(FILEINFO_MIME_TYPE);
            $info = $fileInfos->file($this->dirSrc.'images'.DIRECTORY_SEPARATOR.$img);
            if (in_array($info, $extensions)) {
                header($info);
                readfile($this->dirSrc.'images'.DIRECTORY_SEPARATOR.$img);
            }
        }
        exit();
    }

    private function realRoute($route)
    {
        foreach ($_GET as $key => $value) {
            if (strpos($key,$route) !== false) {
                $e = substr($key,strrpos($key,'_')+1);
                return str_replace('_'.$e,'.'.$e,$key);
            }
        }
    }
}