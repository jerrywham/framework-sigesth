<?php 

// get the identifier of the editor 
$wysiwyg = (isset($_GET['wysiwyg']) ? $_GET['wysiwyg'] : '');

// Because this file is included in EntryPoint->run method (via $page),
// so we can call : 
//*  all the variables defined before $page in EntryPoint->run method
//*  "$this" as EntryPoint object, 
//*  all EntryPoint private vars (via $this)
if($authentication->isLoggedIn() === false || $wysiwyg == '') {
	echo '<!DOCTYPE html><html><head><script type="text/javascript">window.opener.location.reload();window.self.close();</script></head><body></body></html>';
	exit();
}

$finfo = new \finfo(FILEINFO_MIME_TYPE);
$phpallowuploads = (bool) ini_get('file_uploads');// check if upload is enabled on server


define('L_IMG_SHOW_PASS','data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAEJGlDQ1BJQ0MgUHJvZmlsZQAAOBGFVd9v21QUPolvUqQWPyBYR4eKxa9VU1u5GxqtxgZJk6XtShal6dgqJOQ6N4mpGwfb6baqT3uBNwb8AUDZAw9IPCENBmJ72fbAtElThyqqSUh76MQPISbtBVXhu3ZiJ1PEXPX6yznfOec7517bRD1fabWaGVWIlquunc8klZOnFpSeTYrSs9RLA9Sr6U4tkcvNEi7BFffO6+EdigjL7ZHu/k72I796i9zRiSJPwG4VHX0Z+AxRzNRrtksUvwf7+Gm3BtzzHPDTNgQCqwKXfZwSeNHHJz1OIT8JjtAq6xWtCLwGPLzYZi+3YV8DGMiT4VVuG7oiZpGzrZJhcs/hL49xtzH/Dy6bdfTsXYNY+5yluWO4D4neK/ZUvok/17X0HPBLsF+vuUlhfwX4j/rSfAJ4H1H0qZJ9dN7nR19frRTeBt4Fe9FwpwtN+2p1MXscGLHR9SXrmMgjONd1ZxKzpBeA71b4tNhj6JGoyFNp4GHgwUp9qplfmnFW5oTdy7NamcwCI49kv6fN5IAHgD+0rbyoBc3SOjczohbyS1drbq6pQdqumllRC/0ymTtej8gpbbuVwpQfyw66dqEZyxZKxtHpJn+tZnpnEdrYBbueF9qQn93S7HQGGHnYP7w6L+YGHNtd1FJitqPAR+hERCNOFi1i1alKO6RQnjKUxL1GNjwlMsiEhcPLYTEiT9ISbN15OY/jx4SMshe9LaJRpTvHr3C/ybFYP1PZAfwfYrPsMBtnE6SwN9ib7AhLwTrBDgUKcm06FSrTfSj187xPdVQWOk5Q8vxAfSiIUc7Z7xr6zY/+hpqwSyv0I0/QMTRb7RMgBxNodTfSPqdraz/sDjzKBrv4zu2+a2t0/HHzjd2Lbcc2sG7GtsL42K+xLfxtUgI7YHqKlqHK8HbCCXgjHT1cAdMlDetv4FnQ2lLasaOl6vmB0CMmwT/IPszSueHQqv6i/qluqF+oF9TfO2qEGTumJH0qfSv9KH0nfS/9TIp0Wboi/SRdlb6RLgU5u++9nyXYe69fYRPdil1o1WufNSdTTsp75BfllPy8/LI8G7AUuV8ek6fkvfDsCfbNDP0dvRh0CrNqTbV7LfEEGDQPJQadBtfGVMWEq3QWWdufk6ZSNsjG2PQjp3ZcnOWWing6noonSInvi0/Ex+IzAreevPhe+CawpgP1/pMTMDo64G0sTCXIM+KdOnFWRfQKdJvQzV1+Bt8OokmrdtY2yhVX2a+qrykJfMq4Ml3VR4cVzTQVz+UoNne4vcKLoyS+gyKO6EHe+75Fdt0Mbe5bRIf/wjvrVmhbqBN97RD1vxrahvBOfOYzoosH9bq94uejSOQGkVM6sN/7HelL4t10t9F4gPdVzydEOx83Gv+uNxo7XyL/FtFl8z9ZAHF4bBsrEwAAAAlwSFlzAAALEwAACxMBAJqcGAAAAgpJREFUOBGFk01LG1EUhjOTxFUKgnWjUAhtJknJopDsijQBN61UIcTfoKvgvl0ILlwIXVRQ8A8IBbWS1GWC6wYKJZAPmkJBWzBxIZYgdjLxeaed0ETEAyfn633POXPvjdHv9313iYGoBuZOkDlKhuPPZDIBj+iRlVPtFt5rTtFtRux4oEgkMklstFqtttdoFOdOUncAtoixWGwefwl30o8oZ0UtJxqNtnF3qH1UTpxyuWwbyWQyWKlU/kAMU9xnwjj2vWmafsdxHmht4nOsg83Dvez1erlvSCqVCpoi032GQgM9rNfrYcAngDrEn1hil/gK/7TRaDzG3wsEAjUGZsQ1nyAUj9FFAKvxePwF/jQbVAHPoz9ouk0urkH4a2yygJaIYyYTCoBWKBwC0pU9xXxGF8i9rVarv5VHPlB7JodBRwxYxi3q5HXqg+sEZLJ+NxQKrZP3JRKJMVny7puQLxFOVj+vCd5ZlpVVAvlK95d8X1cBG1zLsmUO/SKfzxRni3jOwNHVPSdfQje09r9mOoeiCGDm0J/NZnOf716F+AadrdVqx4NrZNVHtm0fgJ8AvCki8vCv8Z3JQspTu8DNcg7fdY3uBt6jEIgJrwDqIU2hg++G+IuNdphaEM7juA2UgOQeCsChp6wab6ZD3v1D3cIp/7+C96fTaT3tobxyqo3mBxto0qgw7d6/8w1xE0QvGlqUMwAAAABJRU5ErkJggg==');


/*
 * Path to a directory which holds the images.
 */
// $basedir = '../../uploads';
$basedir = $this->pluginsEnabled['openwysiwyg']->getMediasDir();
