<?php

$basedir = $this->pluginsEnabled['openwysiwyg']->getMediasDir().$baseurl.(substr($baseurl,-1) == DIRECTORY_SEPARATOR ? '' : DIRECTORY_SEPARATOR);

if (!is_dir($basedir)) {
    mkdir($basedir);
}

/*
 * Allow your users to browse the subdir of the defined basedir.
 */
$browsedirs = true;

/*
 * If enabled users will be able to upload 
 * files to any viewable directory. You should really only enable
 * this if the area this script is in is already password protected.
 */
if ($authentication->isLoggedIn() === true) {
    $allowuploads = true;
}

/*
 * If a user uploads a file with the same
 * name as an existing file do you want the existing file
 * to be overwritten?
*/
$overwrite = false;

$supportedextentions = $this->pluginsEnabled['openwysiwyg']->supportedextentions;

$msg = null;
$dotdotdir = null;

// set file dir
$leadon = '';
if($leadon == '.') $leadon = '';
if((substr($leadon, -1) != '/') && $leadon != '') $leadon = $leadon . '/';

// validate the directory

if (isset($_GET['d']) && isset($_GET['dir']) && $authentication->isLoggedIn()) {
    $dir = $this->filesTools->nullbyteRemove(str_replace('.','',$_GET['dir']));
    $docToDel = $this->filesTools->nullbyteRemove(str_replace('/','',$_GET['d']));

    if (is_file($basedir.$dir.$docToDel) ) {
        if (false != $ext = array_search($finfo->file($basedir.$dir.$docToDel),$supportedextentions,true)) {
            unlink($basedir.$dir.$docToDel);
        } else {
            unset($_GET['d']);
        }
    } else {
        unset($_GET['d']);
    }
} else {
    unset($_GET['d']);
}

$_GET['dir'] = (isset($_POST['dir']) ? $this->filesTools->nullbyteRemove($_POST['dir']) : (!isset($_GET['dir']) ? '' : $this->filesTools->nullbyteRemove($_GET['dir']) ) );

if(substr($_GET['dir'], -1) != '/') {
    $_GET['dir'] = $_GET['dir'] . '/';
}

$dirok = true;
$dirnames = explode('/', $_GET['dir']);
$sizeofdirnames = sizeof($dirnames);
for($di = 0; $di < $sizeofdirnames; $di++) {
    if($di < ($sizeofdirnames-2) ) {
        $dotdotdir = $dotdotdir . $dirnames[$di] . '/';
    }
}
if(substr($_GET['dir'], 0, 1) == '/') {
    $dirok = false;
}

if($_GET['dir'] == $leadon) {
    $dirok = false;
}

if($dirok) {
    $leadon = $_GET['dir'];
}
$startdir = $basedir;

$actualDir = substr($leadon,0,strrpos(substr($leadon,0,-1),'/'));

if (isset($_FILES['file'])) {
    $leadon = $up['leadon'];
    $msg = $up['msg'];
    $actualDir = substr($leadon,0,strrpos(substr($leadon,0,-1),'/'));
    $dirok = true;
    $dirnames = explode('/', $leadon);
    for($di=0; $di<sizeof($dirnames); $di++) {
        if($di<(sizeof($dirnames)-2)) {
            $dotdotdir = $dotdotdir . $dirnames[$di] . '/';
        }
    }
}
if (!isset($up)) {
    $up = array('leadon' => '','msg' => '');
}

$opendir = $this->pluginsEnabled['openwysiwyg']->getMediasDir().$baseurl.DIRECTORY_SEPARATOR.$leadon;

if(!file_exists($opendir)) {
    $opendir = '.';
    $leadon = $startdir;
}
if ($leadon == '') {
    $leadon = $startdir;
}

clearstatcache();
if ($handle = opendir($opendir)) {
    $n=0;
    while (false !== ($file = readdir($handle))) { 
        //first see if this file is required in the listing
        if ($file == "." || $file == ".." || $file == ".DS_Store" || $file == ".thumbs")  continue;
        if (@filetype($opendir.$file) == "dir") {
            if(!$browsedirs) continue;
        
            $n++;
            if(isset($_GET['sort']) && $_GET['sort'] == "date") {
                $key = @filemtime($opendir.$file) . ".$n";
            }
            else {
                $key = $n;
            }
            if ($file != 'thumbs') {
                $dirs[$key] = $file . "/";
            }
        }
        else {
            $n++;
            if(isset($_GET['sort'])) {
                if($_GET['sort'] == "date") {
                    $key = @filemtime($opendir.$file) . ".$n";
                }
                elseif($_GET['sort'] == "size") {
                    $key = @filesize($opendir.$file) . ".$n";
                }
            }
            else {
                $key = $n;
            }
            $files[$key] = $file;
        }
    }
    closedir($handle); 
}
if ($leadon == $basedir) {
    $leadon = '';
}
//////////////

//sort our files
if (isset($_GET['sort'])) {
    if($_GET['sort'] == "date") {
        @ksort($dirs, SORT_NUMERIC);
        @ksort($files, SORT_NUMERIC);
    }
    elseif($_GET['sort'] == "size") {
        @natcasesort($dirs); 
        @ksort($files, SORT_NUMERIC);
    }
}
else {
    @natcasesort($dirs); 
    @natcasesort($files);
}

//order correctly
if (isset($_GET['order'])) {
    if($_GET['order'] == "desc" && $_GET['sort'] != "size") {$dirs = @array_reverse($dirs);}
    if($_GET['order'] == "desc") {$files = @array_reverse($files);}
}
$dirs = @array_values($dirs); $files = @array_values($files);
