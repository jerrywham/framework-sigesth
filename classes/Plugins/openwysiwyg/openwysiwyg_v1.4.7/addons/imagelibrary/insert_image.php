<?php
/********************************************************************
 * openImageLibrary addon Copyright (c) 2006 openWebWare.com
 * Contact us at devs@openwebware.com
 * This copyright notice MUST stay intact for use.
 ********************************************************************/
require __DIR__.DIRECTORY_SEPARATOR.'config.inc.php';
require dirname(__DIR__).DIRECTORY_SEPARATOR.'actions.inc.php';

?><!DOCTYPE html>
<html>
<head>
<title>openWYSIWYG | L_INSERT_IMAGE</title>
<meta http-equiv="refresh" content="3600; url=/editor/addons/imagelibrary/insert_image.php?wysiwyg=<?=$wysiwyg;?>">

<script type="text/javascript" src="/editor/scripts/wysiwyg-popup.js"></script>
<script type="text/javascript">

/* ---------------------------------------------------------------------- *\
  Function    : insertImage()
  Description : Inserts image into the WYSIWYG.
\* ---------------------------------------------------------------------- */
function insertImage() {
	var n = WYSIWYG_Popup.getParam('wysiwyg');
	
	// get values from form fields
	// var src = '<?=$this->getRoot().'/'; ?>' + document.getElementById('src').value;
	var src = document.getElementById('src').value;
	var alt = document.getElementById('alt').value == '' ? document.getElementById('src').value : document.getElementById('alt').value;
	var width = document.getElementById('width').value
	var height = document.getElementById('height').value
	var border = document.getElementById('border').value
	var align = document.getElementById('align').value
	var vspace = document.getElementById('vspace').value
	var hspace = document.getElementById('hspace').value

	// insert image
	WYSIWYG.insertImage(src, width, height, align, border, alt, hspace, vspace, n);
  	window.close();
}

/* ---------------------------------------------------------------------- *\
  Function    : loadImage()
  Description : load the settings of a selected image into the form fields
\* ---------------------------------------------------------------------- */
function loadImage() {
	var n = WYSIWYG_Popup.getParam('wysiwyg');
	
	// get selection and range
	var sel = WYSIWYG.getSelection(n);
	var range = WYSIWYG.getRange(sel);
	
	// the current tag of range
	var img = WYSIWYG.findParent("img", range);
		
	// if no image is defined then return
	if(img == null) return;

	if (img.parentNode.tagName == 'DIV') {
		selectItemByValue(document.getElementById('align'), img.parentNode.align);
	}
		
	// assign the values to the form elements
	for(var i = 0;i < img.attributes.length;i++) {
		var attr = img.attributes[i].name.toLowerCase();
		var value = img.attributes[i].value;
		// alert(attr + " = " + value);
		if(attr && value && value != "null") {
			switch(attr) {
				case "src": 
					// strip off urls on IE
					if(WYSIWYG_Core.isMSIE) value = WYSIWYG.stripURLPath(n, value, false);
					var posOfPict = value.indexOf(<?=$baseurl;?>+'/');
					document.getElementById('src').value = value.substr(posOfPict);
				break;
				case "alt":
					document.getElementById('alt').value = value;
				break;
				case "align":
					selectItemByValue(document.getElementById('align'), value);
				break;
				case "style": 
					parseStyle(value);
				break;
				case "hspace":
					document.getElementById('hspace').value = value;
				break;
				case "vspace":
					document.getElementById('vspace').value = value;
				break;				
			}
		}
	}
	
	// get width and height from style attribute in none IE browsers
	if(!WYSIWYG_Core.isMSIE && document.getElementById('width').value == "" && document.getElementById('width').value == "") {
		document.getElementById('width').value = img.style.width.replace(/px/, "");
		document.getElementById('height').value = img.style.height.replace(/px/, "");
	}
}

/* ---------------------------------------------------------------------- *\
  Function    : selectItem()
  Description : Select an item of an select box element by value.
\* ---------------------------------------------------------------------- */
function selectItemByValue(element, value) {
	if(element.options.length) {
		for(var i=0;i<element.options.length;i++) {
			if(element.options[i].value == value) {
				element.options[i].selected = true;
			}
		}
	}
}
/* ---------------------------------------------------------------------- *\
  Function    : parseStyle()
  Description : parse style and assign value to input field.
\* ---------------------------------------------------------------------- */
function parseStyle(value) {
	var styles = value.split(';');
	var nbStyles = styles.length;
	for (var i = 0; i < nbStyles; i++) {
		if (styles[i].indexOf('border') !== -1) {
			var bord = styles[i].replace('border','');
			bord = bord.replace(':','');
			bord = bord.replace('px','').trim().split(' ');
			document.getElementById('border').value = bord[0];
		}
		if (styles[i].indexOf('width') !== -1) {
			var W = styles[i].replace('width','');
			W = W.replace(':','');
			W = W.replace('%','');
			W = W.replace('px','').trim();
			document.getElementById('width').value = (W == '' ? '100' : W);
			if (styles[i].indexOf('px') !== -1) {
				selectItemByValue(document.getElementById('widthType'),'px');
			} else {
				selectItemByValue(document.getElementById('widthType'),'%');
			}
		}
		if (styles[i].indexOf('height') !== -1) {
			var H = styles[i].replace('height','');
			H = H.replace(':','').trim();
			var elm = document.getElementById('height');
		}
	};	
}

</script>
</head>
<body onLoad="loadImage();">
<table cellpadding="0" cellspacing="0" style="padding: 10px;border:1px solid #ccc;">
<tr>
	<td>
		<input type="hidden" id="dir" name="dir" value="">
		<span style="font-family: arial, verdana, helvetica; font-size: 11px; font-weight: bold;">L_INSERT_IMAGE:</span>
		<table cellpadding="0" cellspacing="0" style="width:100%;background-color: #F7F7F7; border: 2px solid #FFFFFF; padding: 5px;margin-bottom:50px">
			<tr>
				<td style="padding-bottom: 2px; padding-top: 0px; font-family: arial, verdana, helvetica; font-size: 11px;" width="80">L_IMAGE_URL :</td>
				<td style="padding-bottom: 2px; padding-top: 0px;" width="300"><input type="text" name="src" id="src" value=""  style="font-size: 10px; width: 100%;"></td>
				<td style="padding-top: 0px;padding-bottom: 2px;font-family: tahoma; font-size: 9px;">&nbsp;</td>
			</tr>
			<tr>
				<td style="padding-bottom: 2px; padding-top: 0px; font-family: arial, verdana, helvetica; font-size: 11px;">L_ALTERNATE_TEXT :</td>
				<td style="padding-bottom: 2px; padding-top: 0px;"><input type="text" name="alt" id="alt" value=""  style="font-size: 10px; width: 100%;"></td>		
				<td style="padding-top: 0px;padding-bottom: 2px;font-family: tahoma; font-size: 9px;">&nbsp;</td>

	
		<table cellpadding="0" cellspacing="0" style="border:0;width:100%">
			<tr>
				<td style="vertical-align:top;">
					<span style="font-family: arial, verdana, helvetica; font-size: 11px; font-weight: bold;">L_LAYOUT :</span>
					<table width="180" border="0" cellpadding="0" cellspacing="0" style="background-color: #F7F7F7; border: 2px solid #FFFFFF; padding: 5px;">
						<tr>
				  			<td style="padding-bottom: 2px; padding-top: 0px; font-family: arial, verdana, helvetica; font-size: 11px;">L_WIDTH :</td>
				  			<td style="width:60px;padding-bottom: 2px; padding-top: 0px;"><input type="text" name="width" id="width" value=""  style="font-size: 10px; width: 100%;"></td>
						</tr>
						<tr>
							<td style="padding-bottom: 2px; padding-top: 0px; font-family: arial, verdana, helvetica; font-size: 11px;">L_HEIGHT :</td>
							<td style="padding-bottom: 2px; padding-top: 0px;"><input type="text" name="height" id="height" value=""  style="font-size: 10px; width: 100%;"></td>
						</tr>
						<tr>
						 	<td style="padding-bottom: 2px; padding-top: 0px; font-family: arial, verdana, helvetica; font-size: 11px;">L_BORDER :</td>
							<td style="padding-bottom: 2px; padding-top: 0px;"><input type="text" name="border" id="border" value="0"  style="font-size: 10px; width: 100%;"></td>
						</tr>
					</table>	
				</td>
				<td width="10">&nbsp;</td>
				<td style="vertical-align:top;">
					<span style="font-family: arial, verdana, helvetica; font-size: 11px; font-weight: bold;">&nbsp;</span>
					<table width="200" border="0" cellpadding="0" cellspacing="0" style="background-color: #F7F7F7; border: 2px solid #FFFFFF; padding: 5px;">
						<tr>
							<td style="width: 115px;padding-bottom: 2px; padding-top: 0px; font-family: arial, verdana, helvetica; font-size: 11px;" width="100">L_ALIGNMENT :</td>
							<td style="width: 85px;padding-bottom: 2px; padding-top: 0px;">
								<select name="align" id="align" style="font-family: arial, verdana, helvetica; font-size: 11px; width: 100%;">
									<option value="">&nbsp;</option>
									<option value="left">L_LEFT</option>
									<option value="right">L_RIGHT</option>
									<option value="center">L_CENTER</option>
								</select>
							</td>
						</tr>
						<tr>
						 	<td style="padding-bottom: 2px; padding-top: 0px; font-family: arial, verdana, helvetica; font-size: 11px;">L_HORIZONTAL_SPACE :</td>
							<td style="padding-bottom: 2px; padding-top: 0px;"><input type="text" name="hspace" id="hspace" value=""  style="font-size: 10px; width: 100%;"></td>
						</tr>
						<tr>
							<td style="padding-bottom: 2px; padding-top: 0px; font-family: arial, verdana, helvetica; font-size: 11px;">L_VERTICAL_SPACE :</td>
							<td style="padding-bottom: 2px; padding-top: 0px;"><input type="text" name="vspace" id="vspace" value=""  style="font-size: 10px; width: 100%;"></td>
						</tr>
					</table>	
				</td>
			</tr>
		</table>
	</td>
	<td style="vertical-align: top;width: 100%; padding-left: 5px;">
		<iframe id="chooser" frameborder="0" style="height:380px;width: 100%;border: 2px solid #FFFFFF; padding: 5px;" src="/editor/addons/imagelibrary/select_image.php?wysiwyg=<?=$wysiwyg;?>&dir=<?=$leadon; ?>"></iframe>
	</td>
</tr>
<tr>
	<td colspan="2" style="padding-top: 5px;padding-right:20px;text-align: right;">
		<input type="submit" value="L_SUBMIT_BUTTON" onclick="insertImage();return false;" style="font-size: 12px;">
		<input type="button" value="L_CANCEL" onclick="window.close();" style="font-size: 12px;">	
	</td>
</tr>
</table>
</body>
</html>