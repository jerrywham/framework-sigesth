<?php
/********************************************************************
 * openImageLibrary addon v0.2.2 Copyright (c) 2006 openWebWare.com
 * Contact us at devs@openwebware.com
 * This copyright notice MUST stay intact for use.
 * 
 * $Id: config.inc.php,v 1.7 2006/12/17 21:34:28 xhaggi Exp $
 * 
 * An open source image library addon for the openWYSIWYG.
 * This library gives you the possibility to upload, browse and select 
 * images on your webserver.
 * 
 * Requirements: 
 * - PHP 4.1.x or later
 * - openWYSIWYG v1.4.6 or later
 ********************************************************************/

require_once dirname(__DIR__).DIRECTORY_SEPARATOR.'prepend.inc.php';

/*
 * An absolute or relative URL to the image folder.
 * This url is used to generate the source of the image.
 */
$baseurl = 'pict';