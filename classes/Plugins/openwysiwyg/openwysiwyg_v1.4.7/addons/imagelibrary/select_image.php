<?php
# ------------------ BEGIN LICENSE BLOCK ------------------
#
# This file is part of SIGesTH
#
# Copyright (c) 2009 - 2014 Cyril MAGUIRE, <contact@ecyseo.net>
# Licensed under the CeCILL v2.1 license.
# See http://www.cecill.info/licences.fr.html
#
# ------------------- END LICENSE BLOCK -------------------

require __DIR__.DIRECTORY_SEPARATOR.'config.inc.php';
require dirname(__DIR__).DIRECTORY_SEPARATOR.'actions.inc.php';

?><!DOCTYPE html>

<html>
<head>
<title>openWYSIWYG | SELECT_IMAGE</title>
<meta http-equiv="refresh" content="3600; url=/editor/addons/imagelibrary/select_image.php?wysiwyg=<?=$wysiwyg;?>">
<style type="text/css">
<?php include dirname(__DIR__).DIRECTORY_SEPARATOR.'filelibrary'.DIRECTORY_SEPARATOR.'style.css'; ?>

</style>
<script type="text/javascript">
	var selectImage = function (url) {
		if(parent) {
			parent.document.getElementById("src").value = url;
		}
	};
	
	if(parent) {
		parent.document.getElementById("dir").value = '<?php echo $leadon; ?>';
	} 

	var waiting = function(evt) {
		var anime = document.getElementById('animation');
		anime.style.visibility = 'visible';
	};
	
</script>
</head>
<body>
		<?php
		if($allowuploads) {?>

			<form method="post" action="/editor/addons/imagelibrary/select_image.php?wysiwyg=<?php echo $wysiwyg; ?>" enctype="multipart/form-data" id="send">
			<table cellpadding="0" cellspacing="0" style="width:100%;">
				<tr>
					<td>
						<input type="hidden" name="dir" value="">
						<span style="font-family: arial, verdana, helvetica; font-size: 11px; font-weight: bold;">L_UPLOAD_ON_SERVER :</span>
						<table cellpadding="0" cellspacing="0" style="width:100%;background-color: #F7F7F7; border: 2px solid #FFFFFF; padding: 5px;">
					<?php 
					if($phpallowuploads) {?>

							<tr>
								<td style="padding-top: 0px;padding-bottom: 0px;width:300px;">
									<input type="file" name="file" size="30" style="font-size: 10px; width: 87%;"/>
								</td>
								<td style="width:23%;"><input type="submit" value="L_UPLOAD" id="submitButton"  onclick="waiting();" style="display:inline-block;">&nbsp;<img src="/editor/images/loading.gif" alt="work in progress..." id="animation" style="visibility: hidden;display:inline-block;margin-bottom:-4px;"/></td>
							</tr>
							<tr>
								<td style="padding-top: 0px;padding-bottom: 20px;font-family: tahoma; font-size: 11px;" colspan="2"><?php echo $msg; ?>
									<input type="hidden" name="baseurl" value="<?php echo $baseurl ?>">
									<input type="hidden" name="lead" value="<?php echo $leadon ?> ">
									<input type="hidden" name="visibility" value="private">
        							<input type="hidden" name="token" value="<?=$token;?>">
								</td>
							</tr>
				<?php
					}
					else {
				?>

							<tr>
								<td style="padding-bottom: 2px; padding-top: 0px; font-family: arial, verdana, helvetica; font-size: 11px;" colspan="2">
									L_File_uploads_are_disabled_in_your_php.ini_file._Please_enable_them.
								</td>
							</tr>
				<?php
					}?>

						</table>
					</td>
				</tr>
			</table>
			</form>
		<?php 
		}?>

		<h2>
			<span style="font-family: arial, verdana, helvetica; font-size: 11px; font-weight: bold;">L_SELECT_IMAGE :</span>
		</h2>
		<p>
		<?php
			if (trim($leadon) == '') $leadon = '/';
			if (trim($actualDir) == '') $actualDir = '/';
		 	$breadcrumbs = explode('/', $leadon);
		 	$breadcrumbsDir = explode('/',$actualDir);
		 	$sofar = '';
		  	if(($bsize = sizeof($breadcrumbs)) > 0) {
		  		if(($bsize-1) > 0) {	
			  		echo '<div id="breadcrumbs"><a href="/editor/addons/imagelibrary/select_image.php?wysiwyg='.$wysiwyg.'&dir=">images</a>&nbsp;';
			  		for($bi=0;$bi<($bsize-1);$bi++) {
						$sofar = $sofar . $breadcrumbs[$bi] . '/';
						echo (isset($breadcrumbsDir[$bi]) && $breadcrumbsDir[$bi] == $breadcrumbs[$bi]) ? '<a href="/editor/addons/imagelibrary/select_image.php?wysiwyg='.$wysiwyg.'&dir='.urlencode($sofar).'">&raquo; '.$breadcrumbs[$bi].'</a>&nbsp;' : '&raquo; <strong>'.$breadcrumbs[$bi].'</strong>';
					}
					echo "</div>";
		  		}
		  	}
		 
		$class = 'b';
		if($dirok) {
		?>

		<a href="/editor/addons/imagelibrary/select_image.php?wysiwyg=<?=$wysiwyg;?>&dir=<?=urlencode($dotdotdir); ?>"><img src="/editor/addons/filelibrary/images/folder.png" alt="Folder" border="0" /> <strong>..</strong></a><br>
		<?php
			if($class=='b') $class='w';
			else $class = 'b';
		}
		$arsize = sizeof($dirs);
		for($i=0;$i<$arsize;$i++) {
			$dir = substr($dirs[$i], 0, strlen($dirs[$i]) - 1);
		?>

		<a href="/editor/addons/imagelibrary/select_image.php?wysiwyg=<?=$wysiwyg;?>&dir=<?=urlencode($leadon.$dirs[$i]); ?>"><img src="/editor/addons/filelibrary/images/folder.png" alt="<?php echo $dir; ?>" border="0" /> <strong><?php echo $dir; ?></strong></a><br>
		<?php
			if($class=='b') $class='w';
			else $class = 'b';	
		}
		
		$arsize = sizeof($files);
		for($i=0;$i<$arsize;$i++) {
			$icon = 'unknown.png';
			if (is_file($opendir.$files[$i]) && false != $ext = array_search($finfo->file($opendir.$files[$i]),$supportedextentions,true)) {
				$thumb = '';
				$pluginFileTypes = $this->pluginsEnabled['openwysiwyg']->filetypes;
				$pluginFileTypesPath = $this->pluginsEnabled['openwysiwyg']->filetypesPath;
				if(isset($pluginFileTypes[$ext]) ) {
					$icon = $pluginFileTypesPath.$pluginFileTypes[$ext];
				}
				
				$filename = substr($files[$i],0,strpos($files[$i], $this->pluginsEnabled['openwysiwyg']->getSeparator()));
				if ($filename == '') {
					$filename = $files[$i];
				}
				$lext = -strlen($ext);
				if (substr($filename, $lext) == $ext ) {
					$filename = substr($filename,0,$lext-1);
				}
				if (substr($filename, $lext) == '.jpg') {
					$filename = substr($filename,0,$lext);
				}
				if(strlen($filename)>35) {
					$filename = substr($files[$i], 0, 35) . '...';
				}
				$fileurl = $leadon . $files[$i];
				$filedir = str_replace($basedir, "", $leadon);
				$filepath = $baseurl.'/'.$filedir.$files[$i];
				$filepath = str_replace('//','/',$filepath);
				$filepath = $this->getRoot(false).'/'.$filepath;
		?>

		<a href="javascript:void(0)" onclick="selectImage('<?php echo $filepath; ?>');"><img src="<?php echo $icon; ?>" alt="<?php echo $files[$i]; ?>" border="0" width="18"/> <strong><?php echo $filename.'.'.$ext; ?></strong></a>
		<span class="wysiwyg-hover"><img src="<?=L_IMG_SHOW_PASS;?>"/>
			<span class="wysiwyg-tooltip">
				<img src="<?=$filepath; ?>">
			</span>
		</span>
		<?php if ($authentication->isLoggedIn() === true) :?>
		<a href="/editor/addons/imagelibrary/select_image.php?wysiwyg=<?=$wysiwyg;?>&dir=<?=urlencode($sofar).'&d='.urlencode($files[$i]); ?>" onclick="return confirm('L_CONFIRM_DELETE');" class="del">&cross;</a>
		 <?php endif;?>
		 <br/>
		<?php
				if($class=='b') $class='w';
				else $class = 'b';	
			}
		}	
		?>

		</p>
</body>
</html>