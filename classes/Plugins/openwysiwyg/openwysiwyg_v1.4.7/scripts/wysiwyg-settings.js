/********************************************************************
 * openWYSIWYG settings file Copyright (c) 2006 openWebWare.com
 * Contact us at devs@openwebware.com
 * This copyright notice MUST stay intact for use.
 *
 * $Id: wysiwyg-settings.js,v 1.4 2007/01/22 23:05:57 xhaggi Exp $
 ********************************************************************/

/*
 * Full featured setup used the openImageLibrary addon
 */
var dirRoot = window.location.pathname.substring(0,window.location.pathname.lastIndexOf('/')).substring(1,window.location.pathname.substring(0,window.location.pathname.lastIndexOf('/')).lastIndexOf('/')+1);
    
// var url = window.location.origin + dirRoot;
var url = window.location.origin + '/';
    
var imgExtLink = '&nbsp;&nbsp;<span title="Ouverture du lien dans un nouvel onglet ou une nouvelle fenêtre">&#8599;</span>';
var Trad = [];
Trad['bold'] = 'Gras';
Trad['italic'] = 'Italique';
Trad['underline'] = 'Souligner';
Trad['strikethrough'] = 'Barrer';
Trad['seperator'] = 'Séparateur';
Trad['subscript'] = 'Indice';
Trad['superscript'] = 'Exposant';
Trad['justifyleft'] = 'Aligner à gauche';
Trad['justifycenter'] = 'Centrer';
Trad['justifyright'] = 'Aligner à droite';
Trad['justifyfull'] = 'Justifier';
Trad['unorderedlist'] = 'Liste non ordonnée';
Trad['orderedlist'] = 'Liste ordonnée';
Trad['outdent'] = 'Diminuer le retrait';
Trad['indent'] = 'Augmenter le retrait';
Trad['cut'] = 'Couper';
Trad['copy'] = 'Copier';
Trad['paste'] = 'Coller';
Trad['forecolor'] = 'Couleur du texte';
Trad['backcolor'] = 'Couleur du fond';
Trad['undo'] = 'Annuler';
Trad['redo'] = 'Rétablir';
Trad['insertparagraph'] = 'Transformer la sélection en paragraphe';
Trad['inserttable'] = 'Insérer un tableau';
Trad['insertimage'] = 'Insérer une image';
Trad['insertdocument'] = 'Insérer un document';
Trad['createlink'] = 'Ajouter un lien';
Trad['viewSource'] = 'Voir le code source';
Trad['viewText'] = 'Voir le texte';
Trad['help'] = 'Aide';
Trad['fonts'] = 'Polices';
Trad['fontsizes'] = 'Taille de la police';
Trad['headings'] = 'Titres';
Trad['preview'] = 'Prévisualisation';
Trad['print'] = 'Imprimer';
Trad['removeformat'] = 'Supprimer le formatage issu de Microsoft Word';
Trad['delete'] = 'Supprimer';
Trad['save'] = 'Enregistrer uniquement le contenu du formulaire ci-dessous';
Trad['saveall'] = 'Enregistrer';
Trad['return'] = 'Retour';
Trad['maximize'] = 'Maximizer';
Trad['modifyTable'] = 'Modifier les propriétés du tableau&hellip;';
Trad['modifyImg'] = 'Modifier les propriétés de l\'image&hellip;';
Trad['modifyDoc'] = 'Modifier ou ajouter un document&hellip;';
Trad['modifyLink'] = 'Créer ou modifier un lien&hellip;';
Trad['removenode'] = 'Supprimer la mise en forme';
Trad['mergecells'] = 'Fusionner les cellules';
Trad['splitcellsH'] = 'Scinder les colonnes';
Trad['splitcellsV'] = 'Scinder les lignes';
Trad['RemoveFormatConfMessage'] = 'Nettoyer le HTML ajouté par MS Word';
Trad['NoValidBrowserMessage'] = 'openWYSIWYG n\'est pas compatible avec votre navigateur';
Trad['CommandDisabled'] = 'Vous êtes en mode TEXTE. Cette commande est désactivée.';
Trad['warningRightContentWillBeDeleted'] = 'Attention, le contenu des cellules de droite sera supprimé. Continuer ?';
Trad['warningContentWillBeDeleted'] = 'Attention, seul le contenu de la cellule du haut à gauche sera conservé.';
Trad['warningMergeNotAllowed'] = 'Attention, ce type de fusion n\'est pas possible'; 

var DefaultStyle = "\
	color: #444;\
	font-family: helvetica, arial, sans-serif;\
	font-size: 16px;\
	line-height: 1.4;\
";

var widthOfTextarea = '100%';
var heightOfTextarea = '650px';

var full = new WYSIWYG.Settings();
full.TradToolbar = Trad;
full.ImagesDir = url + dirRoot + "editor/images/";
full.PopupsDir = url + dirRoot + "editor/popups/";
full.DefaultStyle = DefaultStyle;
full.CustomPreviewStyle = url + dirRoot + 'css/style.css';
full.PreviewWidth = 950;
full.PreviewHeight = 600;
full.CSSFile = url + dirRoot + "editor/styles/wysiwyg.css";
full.Width = widthOfTextarea; 
full.Height = heightOfTextarea;
full.addToolbarElement("seperator", 1, 19); 
full.addToolbarElement("createlink", 1, 20); 
full.addToolbarElement("font", 3, 1); 
full.addToolbarElement("fontsize", 3, 2);
full.addToolbarElement("headings", 3, 3);
full.ImagePopupFile = url + dirRoot + "editor/addons/imagelibrary/insert_image.php";
full.DocumentPopupFile = url + dirRoot + "editor/addons/documentlibrary/insert_document.php";
full.ImagePopupWidth = 800;
full.ImagePopupHeight = 480;
full.DocumentPopupWidth = 800;
full.DocumentPopupHeight = 480;
full.Toolbar[1][7] = '';
full.Toolbar[1][8] = '';
full.Toolbar[1][15] = '';

full.RemoveFormatConfMessage = Trad['RemoveFormatConfMessage'];
full.NoValidBrowserMessage = Trad['NoValidBrowserMessage'];
full.CommandDisabled = Trad['CommandDisabled'];
full.warningRightContentWillBeDeleted = Trad['warningRightContentWillBeDeleted'];
full.warningContentWillBeDeleted = Trad['warningContentWillBeDeleted'];
full.warningMergeNotAllowed = Trad['warningMergeNotAllowed'];
 		
/*
 * Small Setup Example
 */
var small = new WYSIWYG.Settings();
small.ImagesDir = url + dirRoot + 'editor/images/';
small.PopupsDir = url + dirRoot + 'editor/popups/';
small.DefaultStyle = DefaultStyle;
small.DefaultPreviewStyle = url + dirRoot + 'css/plucss.css';
small.CustomPreviewStyle = url + dirRoot + 'css/style.css';
small.PreviewWidth = 950;
small.PreviewHeight = 600;
small.CSSFile = url + dirRoot + "editor/styles/wysiwyg.css";
small.Width = widthOfTextarea; 
small.Height = '250px';
// customize toolbar buttons
small.addToolbarElement("font", 3, 1); 
small.addToolbarElement("fontsize", 3, 2);
small.addToolbarElement("headings", 3, 3);
small.addToolbarElement("viewSource", 3, 4);
small.addToolbarElement("maximize", 3, 5);
small.TradToolbar = Trad;
small.RemoveFormatConfMessage = Trad['RemoveFormatConfMessage'];
small.NoValidBrowserMessage = Trad['NoValidBrowserMessage'];
small.CommandDisabled = Trad['CommandDisabled'];
small.warningRightContentWillBeDeleted = Trad['warningRightContentWillBeDeleted'];
small.warningContentWillBeDeleted = Trad['warningContentWillBeDeleted'];
small.warningMergeNotAllowed = Trad['warningMergeNotAllowed'];
small.Toolbar[0] = new Array("bold","italic","underline","strikethrough","inserttable","removeformat","seperator","undo","redo","seperator","justifyleft","justifycenter","justifyright","justifyfull","seperator","unorderedlist","orderedlist","outdent","indent","seperator","createlink" ); // small setup for toolbar 1
small.Toolbar[1] = ""; // disable toolbar 2
small.StatusBarEnabled = true;