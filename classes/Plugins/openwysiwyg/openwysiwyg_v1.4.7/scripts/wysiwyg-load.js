// Remove attribute name because, if name=submit, "form.submit() is not a function" error is triggered
var btns = document.querySelectorAll('input[type=submit]');
var nbBtns = btns.length;
for(var i=0;i<nbBtns;i++) {
    if (btns[i].name) {
        btns[i].removeAttribute('name');
    }
}
// Attach wysiwyg to all textarea by default
var txtarea = document.querySelectorAll('textarea');
var nbTxtarea = txtarea.length;
for(var i=0;i<nbTxtarea;i++) {
    WYSIWYG.attach(txtarea[i].id, full);
}