<?php 

require dirname(__DIR__).DIRECTORY_SEPARATOR.'addons'.DIRECTORY_SEPARATOR.'prepend.inc.php';

?><!DOCTYPE html>

<html>
<head>
<title>Preview</title>
<script src="/website/editor/scripts/wysiwyg-popup.js"></script>
<script>
function createCssLink(css) {
	var link = document.createElement("link");
		link.setAttribute("rel", "stylesheet");
		link.setAttribute("type", "text/css");
		link.setAttribute("href", css);
	return link;
}
function preview() {	
	// get params
	var n = WYSIWYG_Popup.getParam('wysiwyg');
	// set default styles
		
	if (WYSIWYG.config[n].DefaultPreviewStyle.lastIndexOf('.css') == -1) {
		WYSIWYG_Core.setAttribute(document.body, "style", WYSIWYG.config[n].DefaultStyle);
	} else {
		var linkDefault = createCssLink(WYSIWYG.config[n].DefaultPreviewStyle);
		var heads = document.getElementsByTagName("head");
		for(var i=0;i<heads.length;i++) {
			heads[i].appendChild(linkDefault);		
			if (WYSIWYG.config[n].CustomPreviewStyle.lastIndexOf('.css') != -1) {
				var linkCustom = createCssLink(WYSIWYG.config[n].CustomPreviewStyle);
				heads[i].appendChild(linkCustom);		
			}
			break;
		}
	}
	var prepend = '\
<!--[if IE 6]><div id="IE6" class="container grid"><![endif]-->\
<!--[if IE 7]><div id="IE7" class="container grid"><![endif]-->\
<!--[if (IE) & (!IE 6) & (!IE 7)]><div id="IE" class="container grid"><![endif]--> <!-- Pour les anciennes et prochaines versions d\'Internet Explorer (autres que 6 et 7) -->\
<!--[if !IE]>--><div id="NOTIE" class="container grid"><!--<![endif]--> <!-- Le commentaire est fermé, puis rouvert -->\
	<section role="main"><!-- Main -->\
		<div id="content" class="content nosidebar col">\
';
	var append = '\
		</div>\
	</section>\
	</div>\
';
	// get content
	WYSIWYG_Table.disableHighlighting(n);
	var content = WYSIWYG.getEditorWindow(n).document.body.innerHTML;
	WYSIWYG_Table.refreshHighlighting(n);
	content = content.replace(/src="([^/|^http|^https])/gi, "src=\"../$1"); // correct relative image path
	content = content.replace(/href="([^/|^http|^https|^ftp])/gi, "href=\"../$1"); // correct relative anchor path
	// set content
	document.body.innerHTML = prepend + content + append;
}
</script>
</head>
<body onLoad="preview();">
	
</body>
</html> 

