<?xml version="1.0"?> 
<!--AUTHOR: JULIE NABONG-->
<!--STYLESHEET THAT TRANSLATES SVG TO VML-->
<xsl:stylesheet version="1.0" 
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:v="urn:schemas-microsoft-com:vml"
 xmlns:msxsl="urn:schemas-microsoft-com:xslt"
 xmlns:translate="http://translate">
<xsl:output method="html" indent="no"/>

<msxsl:script language="JavaScript" implements-prefix="translate">
<![CDATA[

	var linearGradientTable = new Hashtable();
	var radialGradientTable = new Hashtable();
	var colorsTable = new Hashtable();
	var parentsTable = new Hashtable();
	var textboxfill = "";

function getFunctionName(scriptText)
{
	var arr = scriptText.split(" ");
	//ARR[2] CONTAINS FUNCTION NAME
	
	var closeParenIndex = arr[2].indexOf(')');
	var name = arr[2].substring(0,closeParenIndex + 1);
	return name;

}

function getNewScript(script)
{
	//REPLACE DOM2 EVENTS WITH BROWSER EVENTS
	var text = script.replace('getTarget().getOwnerDocument()','getXML()');
	var newText = text.replace('getDocumentElement()', 'documentElement');
	return newText;
}

function setTextboxfill(fill)
{
	textboxfill = fill;
	return textboxfill;
}

function getTextboxfill()
{	
	return textboxfill;
}

function storeLinearGradientInfo(id, y2)
{
	linearGradientTable.put(id, y2);
	return y2;
}

function getY(id)
{
	var y2 = linearGradientTable.get(id);
	return y2;
}

function storeParent(id, parentname)
{	
	var parent = parentname;
	parentsTable.put(id, parent);
	return parent;
}

function getParent(id)
{
	var parent = parentsTable.get(id);
	return parent;
}

//THIS FUNCTION IS CALLED MANY TIMES
function storeColors(id, color, info)
{
	var colorID = id + '-' + color;
	colorsTable.put(colorID, info);
	return colorID;
}

function storeColorsInTable(id)
{	
	//RETURNS AN ARRAY
	var colorsArray = colorsTable.values();
	var newColorID = id + '-colors';

	//CHECK PARENT
	var parent = getParent(id);	
	
	if(parent.match('radial'))
		radialGradientTable.put(newColorID, colorsArray);
	else
		linearGradientTable.put(newColorID, colorsArray);
	
	//CLEAR COLORSTABLE FOR NEXT SET OF COLORS
	colorsTable.clear();
	return newColorID;
}

function getColors(id)
{	
	//CHECK PARENT
	var parent = getParent(id);

	var newid = id + '-colors';
	
	//RETURNS ARRAY	
	if(parent.match('radial'))
	{
		var colorsArray = radialGradientTable.get(newid);
		var colors = colorsArray.join(',');
		return colors;
	}
	if(parent.match('linear'))
	{
		var array = linearGradientTable.get(newid);
		var newcolors = array.join(',');
		return newcolors;
	}
}

function getColor2(id)
{
	var colorsArray;
	
	//CHECK PARENT
	var parent = getParent(id);
	
	var newid = id + '-' + 'colors';
	
	if(parent.match('radialgradient'))
	{
		//RETRIEVE COLORS ARRAY
		colorsArray = radialGradientTable.get(newid);
	}
	else
		colorsArray = linearGradientTable.get(newid);
	
	//RETRIEVE THE LAST ELEMENT IN ARRAY
	var largestColor = colorsArray[colorsArray.length-1];
	
	//RETURNS ARRAY WITH TWO ELEMENTS
	var largestColorContent = largestColor.split(" ");
	return largestColorContent[1];
}

function getRadialfillcolor(id)
{
	var info = radialGradientTable.get(id);
	var arr = info.split(" ");
	var color = arr[8].substring(11, arr[8].length-1);
	return color;
}

//START HASHTABLE OBJECT
//AUTHOR: MICHAEL SYNOVIC
	/**
	    Contructor(s):
	     Hashtable()
		      Creates a new, empty hashtable

	    Method(s):
	     void clear() 
		      Clears this hashtable so that it contains no keys. 
	     boolean containsKey(String key) 
		      Tests if the specified object is a key in this hashtable. 
	     boolean containsValue(Object value) 
		      Returns true if this Hashtable maps one or more keys to this value. 
	     Object get(String key) 
		      Returns the value to which the specified key is mapped in this hashtable. 
	     boolean isEmpty() 
		      Tests if this hashtable maps no keys to values. 
	     Array keys() 
		      Returns an array of the keys in this hashtable. 
	     void put(String key, Object value) 
		      Maps the specified key to the specified value in this hashtable. A NullPointerException is thrown is the key or value is null.
	     Object remove(String key) 
		      Removes the key (and its corresponding value) from this hashtable. Returns the value of the key that was removed
	     int size() 
		      Returns the number of keys in this hashtable. 
	     String toString() 
		      Returns a string representation of this Hashtable object in the form of a set of entries, enclosed in braces and separated by the ASCII characters ", " (comma and space). 
	     Array values() 
		      Returns an array view of the values contained in this Hashtable. 

	*/
	function Hashtable(){
	    this.clear = hashtable_clear;
	    this.containsKey = hashtable_containsKey;
	    this.containsValue = hashtable_containsValue;
	    this.get = hashtable_get;
	    this.isEmpty = hashtable_isEmpty;
	    this.keys = hashtable_keys;
	    this.put = hashtable_put;
	    this.remove = hashtable_remove;
	    this.size = hashtable_size;
	    this.toString = hashtable_toString;
	    this.values = hashtable_values;
	    this.hashtable = new Array();
	}                
	/*=======Private methods for internal use only========*/

	function hashtable_clear(){
	    this.hashtable = new Array();
	}

	function hashtable_containsKey(key){
	    var exists = false;
	    for (var i in this.hashtable) {
		if (i == key && this.hashtable[i] != null) {
		    exists = true;
		    break;
		}
	    }
	    return exists;
	}

	function hashtable_containsValue(value){
	    var contains = false;
	    if (value != null) {
		for (var i in this.hashtable) {
		    if (this.hashtable[i] == value) {
			contains = true;
			break;
		    }
		}
	    }
	    return contains;
	}

	function hashtable_get(key){
	    return this.hashtable[key];
	}

	function hashtable_isEmpty(){
	    return (this.size == 0) ? true : false;
	}

	function hashtable_keys(){
	    var keys = new Array();
	    for (var i in this.hashtable) {
		if (this.hashtable[i] != null) 
		    keys.push(i);
	    }
	    return keys;
	}

	function hashtable_put(key, value){
	    if (key == null || value == null) {
		throw "NullPointerException {" + key + "},{" + value + "}";
	    }else{
		this.hashtable[key] = value;
	    }
	}

	function hashtable_remove(key){
	    var rtn = this.hashtable[key];
	    this.hashtable[key] = null;
	    return rtn;
	}

	function hashtable_size(){
	    var size = 0;
	    for (var i in this.hashtable) {
		if (this.hashtable[i] != null) 
		    size ++;
	    }
	    return size;
	}

	function hashtable_toString(){
	    var result = "";
	    for (var i in this.hashtable)
	    {      
		if (this.hashtable[i] != null) 
		    result += "{" + i + "},{" + this.hashtable[i] + "}\n";   
	    }
	    return result;
	}

	function hashtable_values(){
	    var values = new Array();
	    for (var i in this.hashtable) {
		if (this.hashtable[i] != null) 
		    values.push(this.hashtable[i]);
	    }
	    return values;
	}

//END HASHTABLE OBJECT

function getPoints(svgpoints)
{
	var arr = svgpoints.split(" ");
	var points = "";
	
	//SEPARATE X AND Y POINTS
   	for(var i=0; i < arr.length; i++)
   	{
   		var arrxy = arr[i].split(",");
   		var x = arrxy[0] - 10;
   		var y = arrxy[1] - 14;
   		points = points + x + "," + y + " ";
   	}
   	return points;
}

function getFill(style)
{
	var arr = style.split(" ");
	var fill = "";
	for(var i=0; i < arr.length; i++)
	{
		if(arr[i].match('fill'))
		{
			//CHECK IF FILL IS GRADIENT		
			if(arr[i].match('url'))
			{
				fill = arr[i].substring(10, arr[i].length-1);
				return fill;
			}
			else
			{
				fill = arr[i].substring(5, arr[i].length);
				return fill;
			}
		}
	}
}

function getStrokewidth(style)
{
	var arr = style.split(" ");
	var strokewidth = 0;
	for(var i=0; i < arr.length; i++)
	{
		if(arr[i].match('stroke-width'))
		{
			var temp = arr[i].substring(13, arr[i].length);
			if (temp.match(';'))
			{
				var index = temp.indexOf(';');
				strokewidth = temp.substring(0,index);
			}
			else
				strokewidth = temp;
		}
	}
	return strokewidth * 1;
}

function getStroke(style)
{
	var arr = style.split(" ");
	var stroke = "";
	for(var i=0; i < arr.length; i++)
	{
		if(arr[i].match('stroke:'))
			stroke = arr[i].substring(7, arr[i].length);
	}
	if(stroke == "")
		return 'NONE';
	else
		return stroke;
}

function getVMLcurvefrom(points)
{
	var arr = points.split(" ");
	var from = arr[0] + " " + arr[1];
	return from;
}

function getVMLcurvecontrol1(points)
{
	var arr = points.split(" ");
	var control1 = arr[2]  + " " + arr[3];
	return control1;
}

function getVMLcurvecontrol2(points)
{
	var arr = points.split(" ");
	var control2 = arr[4] + " " + arr[5];
	return control2;
}

function getVMLcurveto(points)
{
	var arr = points.split(" ");
	var to = arr[6] + " " + arr[7];
	return to;
}

function getPathpoints(points)
{
	var startpt = "";
	var nextpt = "";
	var midpts = "";
	var endpt = "";
	
	var arr = points.split(" ");
	
	//REMOVE M
	if(arr[0].match("M"))
	{
		var i = arr[0].indexOf("M");
		startpt = arr[0].substring(i+1, arr[0].length+1) + " " + arr[1];
	}
	
	//REMOVE L
	if(arr[2].match("L"))
	{
		var i = arr[2].indexOf("L");
		nextpt = arr[2].substring(i+1, arr[2].length+1) + " " + arr[3];
	}
	
	//REMOVE Z
	if(arr[arr.length-1].match("z"))
		endpt = startpt;
		
	//CONCATENATE REMAINING POINTS
	for(i=4; i<arr.length-1; i++)
		midpts = " " + midpts + " " + arr[i];
	
	var newpoints = startpt + " " + " " + nextpt + " " + " " + midpts 
		+ " " + endpt;
	return newpoints;
}

function getTypeface(style)
{
	var arr = style.split(";");
	var typeface = "";
	for(var i=0; i < arr.length; i++)
	{
		if(arr[i].match('font-family'))
		{
			typeface = arr[i].substring(13, arr[i].length-1);
			return typeface;
		}
	}
}

function getFontcolor(style)
{
	var arr = style.split(" ");
	var fontcolor = "";
	for(var i=0; i < arr.length; i++)
	{
		if(arr[i].match('fill'))
		{
			fontcolor = arr[i].substring(5,arr[i].length-1);
			return fontcolor;
		}
	}
}
		

function getFontsize(style)
{
	var arr = style.split(" ");
	var fontsize = 0;
	for(var i=0; i < arr.length; i++)
	{
		if(arr[i].match('font-size'))
		{
			fontsize = arr[i].substring(10, arr[i].length-1);
			return fontsize * 1;
		}
	}
}
]]>
</msxsl:script>

<xsl:template match="/">
  <html xmlns:v="urn:schemas-microsoft-com:vml">
    <head>
    	<style type="text/css">
      	v\:*{behavior:url(#default#VML);}
    	</style>
    </head>
    <body>
<!--CREATE AN XML DATA SOURCE OBJECT FOR THE SVG DOCUMENT-->
<OBJECT width="0" height="0"
    classid="clsid:550dda30-0541-11d2-9ca9-0060b0ec3d39"
    id="source">
 <xsl:copy-of select="/" />
 </OBJECT>    
    	<xsl:apply-templates />
  	</body>
  </html>
</xsl:template>

<!--TO SUPPRESS CDATA TEXT-->
<xsl:template match="*" />

<xsl:template match="g">
	<xsl:apply-templates />
</xsl:template>

<!--PROCESS SVG TAG-->
<xsl:template match="svg">

    <xsl:element name="v:rect">
      <xsl:attribute name="style">
            position:absolute;
            top:0;left:0
      </xsl:attribute>
	  
	  <xsl:if test="@width = '' ">
	  	<xsl:attribute name="width">100%</xsl:attribute>
	  	<xsl:attribute name="height">100%</xsl:attribute>
	  </xsl:if>
	  
	  <xsl:if test="not(@width = '')" >
	  	<xsl:attribute name="width">
	  		<xsl:value-of select="@width" /></xsl:attribute>
	  	<xsl:attribute name="height">
	  		<xsl:value-of select="@height" /></xsl:attribute>
	  </xsl:if>

      <xsl:attribute name="strokecolor">white</xsl:attribute>
      <xsl:attribute name="fillcolor">white</xsl:attribute>   
    </xsl:element>
    
    <xsl:apply-templates />
</xsl:template>

<!--PROCESS RECT TAG-->
<xsl:template match="rect">
 <xsl:variable name="left" select="@x" />
 <xsl:variable name="top" select="@y" />
 <xsl:variable name="rectWidth" select="@width" />
 <xsl:variable name="rectHeight" select="@height" />

<!--CHECK IF PARENT TAG IS 'G'-->
<xsl:variable name="style">
<xsl:if test="name(parent::*) = 'g' " />
	<xsl:value-of select="string((parent::*)/@style)" />
<xsl:if test="name(parent::*) != 'g' "/>
	<xsl:value-of select="string(@style)" />
</xsl:variable>

<!--CHECK IF NEXT NODE IS TEXT-->
<xsl:if test="name(following-sibling::*) = 'text' ">
	<xsl:variable name="textboxfill"
		select="translate:getFill(string($style))" />
	<xsl:variable name="textfill" select="translate:setTextboxfill(string($textboxfill))" />
</xsl:if>

<xsl:variable name="gradientID" select="translate:getFill(string($style))" />

<!--CHECK IF RADIALGRADIENT-->
<xsl:if test="contains($style, 'fill:url')" >
	<xsl:variable name="parent" select="translate:getParent(string($gradientID))" />
	<xsl:choose>
		<xsl:when test="contains($parent, 'radial')" >
			<xsl:call-template name="overlapRect">
				<xsl:with-param name="new-style" select="$style" />
				<xsl:with-param name="new-id" select="$gradientID" />
				<xsl:with-param name="new-left" select="$left" />
				<xsl:with-param name="new-top" select="$top" />
				<xsl:with-param name="new-width" select="$rectWidth" />
				<xsl:with-param name="new-height" select="$rectHeight" />
			</xsl:call-template>
		</xsl:when>
	
		<!--GRADIENT IS LINEAR-->
		<xsl:otherwise>
			<xsl:call-template name="drawRect">
				<xsl:with-param name="rect-style" select="$style" />
				<xsl:with-param name="rect-id" select="$gradientID" />
				<xsl:with-param name="rect-left" select="$left" />
				<xsl:with-param name="rect-top" select="$top" />
				<xsl:with-param name="rect-width" select="$rectWidth" />
				<xsl:with-param name="rect-height" select="$rectHeight" />
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
</xsl:if>

<!--THERE IS NO GRADIENT-->
<xsl:if test="not(contains($style, 'fill:url'))" >
		<xsl:call-template name="drawRect">
			<xsl:with-param name="rect-style" select="$style" />
			<xsl:with-param name="rect-id" select="$gradientID" />
			<xsl:with-param name="rect-left" select="$left" />
			<xsl:with-param name="rect-top" select="$top" />
			<xsl:with-param name="rect-width" select="$rectWidth" />
			<xsl:with-param name="rect-height" select="$rectHeight" />
		</xsl:call-template>

</xsl:if>
<!--END RECT TAG-->
</xsl:template>

<!--TEMPLATE TO DRAW OVAL ON TOP OF RECTANGLE-->
<!--OVERLAPRECT-->
<xsl:template name="overlapRect">
	<xsl:param name="new-style" />
	<xsl:param name="new-id" />
	<xsl:param name="new-left" />
	<xsl:param name="new-top" />
	<xsl:param name="new-width" />
	<xsl:param name="new-height" />
	<xsl:variable name="maincolor" select="translate:getColor2(string($new-id))" />

 <xsl:element name="v:rect">
	<xsl:attribute name="style">
  		position:absolute;
    	width:<xsl:value-of select="$new-width" />;
    	height:<xsl:value-of select="$new-height" />;
    	top:<xsl:value-of select="$new-top" />;
    	left:<xsl:value-of select="$new-left" />
 	</xsl:attribute>
	
	<xsl:attribute name="fillcolor">
		<xsl:value-of select="$maincolor" />
	</xsl:attribute>
	
	<xsl:attribute name="strokecolor">
		<xsl:value-of select="$maincolor" />
	</xsl:attribute>
</xsl:element>	

<xsl:element name="v:oval">
	<xsl:attribute name="style">
  		position:absolute;
    	width:<xsl:value-of select="$new-width" />;
    	height:<xsl:value-of select="$new-height" />;
    	top:<xsl:value-of select="$new-top" />;
    	left:<xsl:value-of select="$new-left" />
 	</xsl:attribute>

	<xsl:attribute name="strokecolor">
		<xsl:value-of select="$maincolor" />
	</xsl:attribute>
	
	<xsl:attribute name="strokeweight">
		<xsl:value-of select="translate:getStrokewidth(string(normalize-space($new-style)))" />
	</xsl:attribute>   

	<xsl:call-template name="processRadialGradient">
		<xsl:with-param name="id" select="string($new-id)" />
	</xsl:call-template>

</xsl:element>
</xsl:template>

<!--DRAWRECT-->
<xsl:template name="drawRect">
	<xsl:param name="rect-style" />
	<xsl:param name="rect-id" />
	<xsl:param name="rect-left" />
	<xsl:param name="rect-top" />
	<xsl:param name="rect-width" />
	<xsl:param name="rect-height" />
	
 <!--DRAW VML RECTANGLE-->
 <xsl:element name="v:rect">
	 <xsl:attribute name="style">
  		position:absolute;
    	width:<xsl:value-of select="$rect-width" />;
    	height:<xsl:value-of select="$rect-height" />;
    	top:<xsl:value-of select="$rect-top" />;
    	left:<xsl:value-of select="$rect-left" />
 	 </xsl:attribute>
 
<xsl:attribute name="id">
	<xsl:value-of select="@id" />
</xsl:attribute>

<!--CHECK FOR ONCLICK EVENT-->
<xsl:attribute name="onclick">
	<xsl:if test="contains(@onclick, 'alert') ">
		<xsl:value-of select="@onclick" />
	</xsl:if>
</xsl:attribute>

<!--CHECK FOR ONMOUSEOVER EVENT-->
<xsl:attribute name="onmouseover">
	<xsl:if test="contains(@onmouseover, 'alert') ">
		<xsl:value-of select="@onmouseover" />
	</xsl:if>
</xsl:attribute>

<!--CHECK FOR FUNCTION NOT EQUAL TO ALERT-->
<xsl:if test="not(contains(@onclick, 'alert')) ">
<xsl:variable name="onclickname" select="string(@onclick)" />
<xsl:variable name="tempName" select="substring-before($onclickname, '&#40;')" />
<xsl:variable name="event" select="'(event)'" />
<xsl:variable name="onclickEventName">
	<xsl:value-of select="concat('pre', $tempName, $event)" />
</xsl:variable>
<xsl:attribute name="onclick">
	<xsl:value-of select="$onclickEventName" />
</xsl:attribute>
<xsl:attribute name="id">
	<xsl:value-of select="string(@id)" />
</xsl:attribute>
</xsl:if>

	<xsl:call-template name="process-fill">
		<xsl:with-param name="svgstyle" select="$rect-style" />
		<xsl:with-param name="id" select="string($rect-id)" />
	</xsl:call-template>

 </xsl:element>	
<!--END DRAWRECT-->	
</xsl:template>

<!--PROCESSFILL-->
<xsl:template name="process-fill">
	<xsl:param name="svgstyle" />
	<xsl:param name="id" />
	<xsl:attribute name="strokeweight">
		<xsl:value-of select="translate:getStrokewidth(string(normalize-space($svgstyle)))" />
	</xsl:attribute>   
	<xsl:attribute name="strokecolor">
		<xsl:value-of select="translate:getStroke(string(normalize-space($svgstyle)))" />
	</xsl:attribute>      

	<!--CHECK FILLCOLOR-->
	<xsl:if test="not(contains($svgstyle, 'url'))">
		<xsl:attribute name="fillcolor">
			<xsl:value-of select="translate:getFill(string($svgstyle))" />
		</xsl:attribute>
	</xsl:if>

<!--CHECK GRADIENT IF LINEAR OR RADIAL-->
<xsl:if test="contains($svgstyle, 'fill:url')" >
	<xsl:variable name="parent" select="translate:getParent(string($id))" />
	<xsl:choose>
		<xsl:when test="contains($parent, 'radial')" >
			<xsl:call-template name="processRadialGradient">
				<xsl:with-param name="id" select="string($id)" />
			</xsl:call-template>
		</xsl:when>
	
		<xsl:otherwise>
			<xsl:call-template name="processLinearGradient">
				<xsl:with-param name="id" select="string($id)" />
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
</xsl:if>
      
<!--END PROCESS-FILL TEMPLATE-->
</xsl:template>

<!--PROCESS CIRCLE TAG-->
<xsl:template match="circle">

<!--CHECK IF PARENT TAG IS 'G'-->
<xsl:variable name="style">
<xsl:if test="name(parent::*) = 'g' " />
	<xsl:value-of select="string((parent::*)/@style)" />
<xsl:if test="name(parent::*) != 'g' "/>
	<xsl:value-of select="string(@style)" />
</xsl:variable>

<!--CHECK IF NEXT NODE IS TEXT-->
<xsl:if test="name(following-sibling::*) = 'text' ">
	<xsl:variable name="textboxfill"
		select="translate:getFill(string($style))" />
	<xsl:variable name="ok" select="translate:setTextboxfill(string($textboxfill))" />
</xsl:if>

<xsl:variable name="gradientID" select="translate:getFill(string($style))" />

<!--DRAW VML OVAL-->
<xsl:element name="v:oval">
<xsl:attribute name="style">position:absolute;
    left:<xsl:value-of select="@cx - @r" />;
    top:<xsl:value-of select="@cy - @r" />;
    width:<xsl:value-of select="@r * 2" />; 
    height:<xsl:value-of select="@r * 2" />
</xsl:attribute>
    
<!--CHECK IF THERE ONCLICK EVENT-->
<xsl:if test="contains(@onclick, 'alert') ">
	<xsl:attribute name="onclick">
		<xsl:value-of select="@onclick" />
	</xsl:attribute>
</xsl:if>

<!--CHECK FOR FUNCTION NOT EQUAL TO ALERT-->
<xsl:if test="not(contains(@onclick, 'alert')) ">
<xsl:variable name="onclickname" select="string(@onclick)" />
<xsl:variable name="tempName" select="substring-before($onclickname, '&#40;')" />
<xsl:variable name="event" select="'(event)'" />
<xsl:variable name="onclickEventName">
	<xsl:value-of select="concat('pre', $tempName, $event)" />
</xsl:variable>
<xsl:attribute name="onclick">
	<xsl:value-of select="$onclickEventName" />
</xsl:attribute>
<xsl:attribute name="id">
	<xsl:value-of select="string(@id)" />
</xsl:attribute>
</xsl:if>
   
<xsl:call-template name="process-fill">
	<xsl:with-param name="svgstyle" select="string($style)" />
	<xsl:with-param name="id" select="string($gradientID)" />
</xsl:call-template>

</xsl:element>

<!--END CIRCLE TAG-->
</xsl:template>


<!--PROCESS POLYLINE TAG-->
<xsl:template match="polyline">

<xsl:variable name="points"
	select="translate:getPoints(string(normalize-space(@points)))" />

<!--CHECK IF PARENT TAG IS 'G'-->
<xsl:variable name="style">
<xsl:if test="name(parent::*) = 'g' " />
	<xsl:value-of select="(parent::*)/@style" />
<xsl:if test="name(parent::*) != 'g' "/>
	<xsl:value-of select="@style" />
</xsl:variable>

<xsl:if test="contains($style, 'url')" />
<xsl:variable name="gradientID" select="translate:getFill(string($style))" />

  <!--DRAW VML POLYLINE-->
  
<xsl:element name="v:polyline">
<xsl:attribute name="style">position:absolute;</xsl:attribute>

<xsl:attribute name="id">
	<xsl:value-of select="@id" />
</xsl:attribute>

<xsl:attribute name="onclick">
	<xsl:if test="contains(@onclick, 'alert') ">
		<xsl:value-of select="@onclick" />
	</xsl:if>
</xsl:attribute>

<xsl:attribute name="onmouseover">
	<xsl:if test="contains(@onmouseover, 'alert') ">
		<xsl:value-of select="@onmouseover" />
	</xsl:if>
</xsl:attribute>

<!--CHECK FOR FUNCTION NOT EQUAL TO ALERT-->
<xsl:if test="not(contains(@onclick, 'alert')) ">
<xsl:variable name="onclickname" select="string(@onclick)" />
<xsl:variable name="tempName" select="substring-before($onclickname, '&#40;')" />
<xsl:variable name="event" select="'(event)'" />
<xsl:variable name="onclickEventName">
	<xsl:value-of select="concat('pre', $tempName, $event)" />
</xsl:variable>
<xsl:attribute name="onclick">
	<xsl:value-of select="$onclickEventName" />
</xsl:attribute>
<xsl:attribute name="id">
	<xsl:value-of select="string(@id)" />
</xsl:attribute>
</xsl:if>

<xsl:attribute name="points">
    <xsl:value-of select="$points" />
</xsl:attribute>

<xsl:call-template name="process-fill">
	<xsl:with-param name="svgstyle" select="string($style)" />
	<xsl:with-param name="id" select="string(gradientID)" />
</xsl:call-template>

</xsl:element>
</xsl:template>

<!--PROCESS LINE TAG-->
<xsl:template match="line">
	<xsl:variable name="x1" select="@x1" />
	<xsl:variable name="y1" select="@y1" />
	<xsl:variable name="x2" select="@x2" />
	<xsl:variable name="y2" select="@y2" />

<!--CHECK IF PARENT TAG IS 'G'-->
<xsl:variable name="style">
<xsl:if test="name(parent::*) = 'g' " />
	<xsl:value-of select="(parent::*)/@style" />
<xsl:if test="name(parent::*) != 'g' "/>
	<xsl:value-of select="@style" />
</xsl:variable>
  
<!--DRAW VML LINE-->

<xsl:element name="v:line">
<xsl:attribute name="from"><xsl:value-of select="$x1 - 10" />
<xsl:text>,</xsl:text>
<xsl:value-of select="$y1 - 14" />
</xsl:attribute>
<xsl:attribute name="to"><xsl:value-of select="$x2 - 10" />
<xsl:text>,</xsl:text>
<xsl:value-of select="$y2 - 14" />
</xsl:attribute>

<xsl:attribute name="id">
	<xsl:value-of select="@id" />
</xsl:attribute>

<xsl:attribute name="onclick">
	<xsl:if test="contains(@onclick, 'alert') ">
		<xsl:value-of select="@onclick" />
	</xsl:if>
</xsl:attribute>

<xsl:attribute name="onmouseover">
	<xsl:if test="contains(@onmouseover, 'alert') ">
		<xsl:value-of select="@onmouseover" />
	</xsl:if>
</xsl:attribute>

<!--CHECK FOR FUNCTION NOT EQUAL TO ALERT-->
<xsl:if test="not(contains(@onclick, 'alert')) ">
<xsl:variable name="onclickname" select="string(@onclick)" />
<xsl:variable name="tempName" select="substring-before($onclickname, '&#40;')" />
<xsl:variable name="event" select="'(event)'" />
<xsl:variable name="onclickEventName">
	<xsl:value-of select="concat('pre', $tempName, $event)" />
</xsl:variable>
<xsl:attribute name="onclick">
	<xsl:value-of select="$onclickEventName" />
</xsl:attribute>
<xsl:attribute name="id">
	<xsl:value-of select="string(@id)" />
</xsl:attribute>
</xsl:if>

<xsl:attribute name="strokeweight">
	<xsl:value-of select="translate:getStrokewidth(string(normalize-space($style)))" />
</xsl:attribute>   
<xsl:attribute name="strokecolor">
	<xsl:value-of select="translate:getStroke(string(normalize-space($style)))" />
</xsl:attribute>      
<!--NO FILLCOLOR SUPPORTED-->
</xsl:element>
<!--END LINE TAG-->
</xsl:template>

<!--PROCESS ELLIPSE TAG-->
<xsl:template match="ellipse">
<xsl:variable name="ellipseWidth" select="number(@rx * 2)" />
<xsl:variable name="ellipseHeight" select="number(@ry * 2)" />
<xsl:variable name="cx" select="@cx" />
<xsl:variable name="cy" select="@cy" />
<xsl:variable name="rx" select="@rx" />
<xsl:variable name="ry" select="@ry" />

<!--CHECK IF PARENT TAG IS 'G'-->
<xsl:variable name="style">
<xsl:if test="name(parent::*) = 'g' " />
	<xsl:value-of select="(parent::*)/@style" />
<xsl:if test="name(parent::*) != 'g' "/>
	<xsl:value-of select="@style" />
</xsl:variable>

<xsl:if test="name(following-sibling::*) = 'text' ">
	<xsl:variable name="textboxfill"
		select="translate:getFill(string($style))" />
	<xsl:variable name="setTextboxfill" select="translate:setTextboxfill(string($textboxfill))" />
</xsl:if>


<!--DRAW VML OVAL-->
<xsl:element name="v:oval">
<xsl:attribute name="style">position:absolute;
  left:<xsl:value-of select="number($cx - $rx)" />;
    top:<xsl:value-of select="number($cy - $ry)" />;
    width:<xsl:value-of select="$ellipseWidth" />; 
    height:<xsl:value-of select="$ellipseHeight" />
</xsl:attribute>
<xsl:if test="contains($style, 'url')" />
<xsl:variable name="gradientID" select="translate:getFill(string($style))" />

<xsl:attribute name="id">
	<xsl:value-of select="@id" />
</xsl:attribute>

<xsl:attribute name="onclick">
	<xsl:if test="contains(@onclick, 'alert') ">
		<xsl:value-of select="@onclick" />
	</xsl:if>
</xsl:attribute>

<xsl:attribute name="onmouseover">
	<xsl:if test="contains(@onmouseover, 'alert') ">
		<xsl:value-of select="@onmouseover" />
	</xsl:if>
</xsl:attribute>

<!--CHECK FOR FUNCTION NOT EQUAL TO ALERT-->
<xsl:if test="not(contains(@onclick, 'alert')) ">
<xsl:variable name="onclickname" select="string(@onclick)" />
<xsl:variable name="tempName" select="substring-before($onclickname, '&#40;')" />
<xsl:variable name="event" select="'(event)'" />
<xsl:variable name="onclickEventName">
	<xsl:value-of select="concat('pre', $tempName, $event)" />
</xsl:variable>
<xsl:attribute name="onclick">
	<xsl:value-of select="$onclickEventName" />
</xsl:attribute>
<xsl:attribute name="id">
	<xsl:value-of select="string(@id)" />
</xsl:attribute>
</xsl:if>

<xsl:call-template name="process-fill">
	<xsl:with-param name="svgstyle" select="string($style)" />
	<xsl:with-param name="id" select="string(gradientID)" />
</xsl:call-template>

</xsl:element>
</xsl:template>

<!--PROCESS POLYGON TAG-->
<xsl:template match="polygon">
	<xsl:variable name="points" select="@points" />

<!--CHECK IF PARENT TAG IS 'G'-->
<xsl:variable name="style">
<xsl:if test="name(parent::*) = 'g' " />
	<xsl:value-of select="(parent::*)/@style" />
<xsl:if test="name(parent::*) != 'g' "/>
	<xsl:value-of select="@style" />
</xsl:variable>

 <!--DRAW VML POLYLINE FOR POLYGON-->
  <xsl:element name="v:polyline">
<xsl:attribute name="style">position:absolute;</xsl:attribute>

<xsl:attribute name="id">
	<xsl:value-of select="@id" />
</xsl:attribute>

<xsl:attribute name="onclick">
	<xsl:if test="contains(@onclick, 'alert') ">
		<xsl:value-of select="@onclick" />
	</xsl:if>
</xsl:attribute>

<xsl:attribute name="onmouseover">
	<xsl:if test="contains(@onmouseover, 'alert') ">
		<xsl:value-of select="@onmouseover" />
	</xsl:if>
</xsl:attribute>

<!--CHECK FOR FUNCTION NOT EQUAL TO ALERT-->
<xsl:if test="not(contains(@onclick, 'alert')) ">
<xsl:variable name="onclickname" select="string(@onclick)" />
<xsl:variable name="tempName" select="substring-before($onclickname, '&#40;')" />
<xsl:variable name="event" select="'(event)'" />
<xsl:variable name="onclickEventName">
	<xsl:value-of select="concat('pre', $tempName, $event)" />
</xsl:variable>
<xsl:attribute name="onclick">
	<xsl:value-of select="$onclickEventName" />
</xsl:attribute>
<xsl:attribute name="id">
	<xsl:value-of select="string(@id)" />
</xsl:attribute>
</xsl:if>

<xsl:attribute name="points">
    <xsl:value-of select="translate:getPoints(string(normalize-space(@points)))" />
</xsl:attribute>

<xsl:if test="contains($style, 'url')" />
<xsl:variable name="gradientID" select="translate:getFill(string($style))" />

<xsl:call-template name="process-fill">
	<xsl:with-param name="svgstyle" select="$style" />
	<xsl:with-param name="id" select="$gradientID" />
</xsl:call-template>

</xsl:element>
<!--END POLYGON TAG-->
</xsl:template>

<!--PROCESS TEXT TAG-->
<xsl:template match="text">
<xsl:variable name="x" select="@x" />
<xsl:variable name="y" select="@y" />
<xsl:variable name="text" select="." />

<!--CHECK IF PARENT TAG IS 'G'-->
<xsl:variable name="style">
<xsl:if test="name(parent::*) = 'g' " />
	<xsl:value-of select="(parent::*)/@style" />
<xsl:if test="name(parent::*) != 'g' "/>
	<xsl:value-of select="string(@style)" />
</xsl:variable>

<xsl:variable name="fontsize"
	select="translate:getFontsize(string(normalize-space($style)))" />
	
<xsl:variable name="fontcolor"
	select="translate:getFontcolor(string(normalize-space($style)))" />
	
<xsl:variable name="typeface"
	select="translate:getTypeface(string(normalize-space($style)))" />

<!--DRAW VML RECTANGLE BEHIND TEXT-->
<xsl:element name="v:rect">
 	<xsl:attribute name="style">
  		position:absolute;
    	width:<xsl:value-of select="string-length($text) * 20" />;
    	height:<xsl:value-of select="$fontsize * 2" />;
    	top:<xsl:value-of select="$y - 20"/>;
    	left:<xsl:value-of select="$x - 10"/>
    </xsl:attribute>
         	
    <!--RECT IS DRAWN WITHOUT STROKECOLOR-->     	
   	<xsl:element name="v:stroke">
 		<xsl:attribute name="on">false</xsl:attribute>
 	</xsl:element>

 	<xsl:variable name="fillcolor"
 	 		select="translate:getTextboxfill()" />
	
	<xsl:if test="$fillcolor = '' ">
 		<xsl:attribute name="fillcolor">none</xsl:attribute>
 		<xsl:attribute name="strokecolor">none</xsl:attribute>
 	</xsl:if>
 	
 	<!--RECT IS DRAWN AS TRANSPARENT-->
 	<xsl:if test="not($fillcolor = '' )">
 		<xsl:element name="v:fill">
 			<xsl:attribute name="opacity">0.0</xsl:attribute>
 			<xsl:attribute name="color">
 				<xsl:value-of select="$fillcolor" /></xsl:attribute>
 		</xsl:element>
 	</xsl:if>
 		
 	
<!--DRAW VML TEXTBOX-->
 <xsl:element name="v:textbox">
  <xsl:element name="font">
  <xsl:attribute name="size">
  	<xsl:value-of select="$fontsize div 5" />pt
  </xsl:attribute>      
  
  <xsl:attribute name="id">
	<xsl:value-of select="@id" />
  </xsl:attribute>

  <xsl:attribute name="onclick">
	<xsl:if test="contains(@onclick, 'alert') ">
		<xsl:value-of select="@onclick" />
	</xsl:if>
  </xsl:attribute>
  
  <xsl:attribute name="onmouseover">
	<xsl:if test="contains(@onmouseover, 'alert') ">
		<xsl:value-of select="@onmouseover" />
	</xsl:if>
</xsl:attribute>

<!--CHECK FOR FUNCTION NOT EQUAL TO ALERT-->
<xsl:if test="not(contains(@onclick, 'alert')) ">
<xsl:variable name="onclickname" select="string(@onclick)" />
<xsl:variable name="tempName" select="substring-before($onclickname, '&#40;')" />
<xsl:variable name="event" select="'(event)'" />
<xsl:variable name="onclickEventName">
	<xsl:value-of select="concat('pre', $tempName, $event)" />
</xsl:variable>
<xsl:attribute name="onclick">
	<xsl:value-of select="$onclickEventName" />
</xsl:attribute>
<xsl:attribute name="id">
	<xsl:value-of select="string(@id)" />
</xsl:attribute>
</xsl:if>

  <xsl:attribute name="face"><xsl:value-of select="$typeface" /></xsl:attribute>   
  <xsl:attribute name="color"><xsl:value-of select="$fontcolor" /></xsl:attribute>
  <xsl:value-of select="$text"/>
  </xsl:element>
 </xsl:element>
</xsl:element>  
</xsl:template>


<!--PROCESS LINEAR GRADIENT TAG-->
<xsl:template name="processLinearGradient">
	<xsl:param name="id" />
		<!--GET COLORS-->			
		<xsl:variable name="colors"
			select="translate:getColors(string($id))" />		
		
		<!--GET COLOR2-->
		<xsl:variable name="color2"
			select="translate:getColor2(string($id))" />
		<!--WRITE VML FILL TAG-->
	<xsl:element name="v:fill">
		<xsl:attribute name="type">gradient</xsl:attribute>
		<xsl:attribute name="method">sigma</xsl:attribute>
		
		<!--CHECK IF HORIZONTAL OR VERTICAL-->
		<xsl:variable name="y2" select="translate:getY(string($id))" />
		
		<xsl:if test="contains($y2, '100')">
			<xsl:attribute name="angle">-180</xsl:attribute>
		</xsl:if>
		
		<xsl:if test="not(contains($y2, '100'))">
			<xsl:attribute name="angle">-90</xsl:attribute>
		</xsl:if>
		
		<xsl:attribute name="focussize">0,0</xsl:attribute>
		<xsl:attribute name="focus">100%</xsl:attribute>
		<xsl:attribute name="focusposition">50%,50%</xsl:attribute>
		<!--ADD COLORS-->
		<xsl:attribute name="color2">
			<xsl:value-of select="$color2" /></xsl:attribute>
		<xsl:attribute name="colors">
			<xsl:value-of select="$colors" /></xsl:attribute>		
		
	</xsl:element>		
	
<!--END LINEAR GRADIENT TAG-->
</xsl:template>

<!--PROCESS RADIAL GRADIENT TAG-->
<xsl:template name="processRadialGradient">
	<xsl:param name="id" />
		<!--GET COLORS-->			
		<xsl:variable name="colors"
			select="translate:getColors(string($id))" />		
		<!--GET COLOR2-->
		<xsl:variable name="color2"
			select="translate:getColor2(string($id))" />
		<!--WRITE VML FILL TAG-->
	<xsl:element name="v:fill">
		<xsl:attribute name="type">gradientradial</xsl:attribute>
		<xsl:attribute name="method">sigma</xsl:attribute>
		<xsl:attribute name="angle">-45</xsl:attribute>
		<xsl:attribute name="focussize">0,0</xsl:attribute>
		<xsl:attribute name="focus">100%</xsl:attribute>
		<xsl:attribute name="focusposition">50%,50%</xsl:attribute>
		<!--ADD COLORS-->

		<xsl:attribute name="color2">
			<xsl:value-of select="$color2" /></xsl:attribute>
		<xsl:attribute name="colors">
			<xsl:value-of select="$colors" /></xsl:attribute>		
		
	</xsl:element>		
<!--END PROCESSRADIALGRADIENT-->
</xsl:template>

<!--PROCESS DEFS/LINEARGRADIENT TAG-->
<xsl:template match="defs/linearGradient">

<xsl:variable name="id" select="string(@id)" />
<xsl:variable name="y2" select="string(@y2)" />

<xsl:variable name="storeParent" select="translate:storeParent($id, 'lineargradient')" />

<!--STORE Y2 IN JAVASCRIPT-->
<xsl:variable name="storeLinear" select="translate:storeLinearGradientInfo($id, $y2)" />	

<xsl:apply-templates />	
<!--STORE COLORS AFTER STOP TAGS HAVE BEEN PROCESSED-->
<xsl:variable name="storeColors" select="translate:storeColorsInTable($id)" />
<!--END DEFS/LINEARGRADIENT TAG-->
</xsl:template>

<!--PROCESS DEFS/RADIALGRADIENT TAG-->
<xsl:template match="defs">
	<xsl:apply-templates />
</xsl:template>

<!--RADIAL TEMPLATE-->
<xsl:template match="radialGradient">
	<xsl:variable name="id" select="string(@id)" />
	
	<xsl:variable name="storeParent" select="translate:storeParent($id, 'radialgradient')" />
	<xsl:apply-templates />	

<!--STORE IN JAVASCRIPT-->
<xsl:variable name="storeColors" select="translate:storeColorsInTable($id)" />
<!--END RADIAL TEMPLATE-->
</xsl:template>

<!--PROCESS STOP TAGS-->
<xsl:template match="stop">
	<xsl:variable name="id" select="string(parent::*/@id)" />
	<xsl:variable name="offset" select="string(@offset)" />
	<xsl:variable name="style" select="string(@style)" />
	<xsl:variable name="color" select="substring-after($style,'stop-color:')" />
	<xsl:variable name="colorinfo" select="string(concat($offset, ' ', $color))" />
	<xsl:variable name="storeColors" select="translate:storeColors($id, $color, $colorinfo)" />	
<!--END STOP TAGS-->
</xsl:template>

<!--PROCESS PATH TAG-->
<xsl:template match="path">
<xsl:variable name="d" select="string(@d)" />

<!--CHECK IF PARENT TAG IS 'G'-->
<xsl:variable name="style">
<xsl:if test="name(parent::*) = 'g' " />
	<xsl:value-of select="string((parent::*)/@style)" />
<xsl:if test="name(parent::*) != 'g' "/>
	<xsl:value-of select="string(@style)" />
</xsl:variable>

<xsl:if test="contains($style, 'url')" />
<xsl:variable name="gradientID" select="translate:getFill(string($style))" />

<!--CHECK IF CUBIC BEZIER CURVE-->
<xsl:if test="contains($d, 'C')" >
	<xsl:call-template name="writeVMLCurveTag">
		<xsl:with-param name="points" select="$d" />
		<xsl:with-param name="curvestyle" select="$style" />
		<xsl:with-param name="curveid" select="$gradientID" />
	</xsl:call-template>
</xsl:if>
<!--END PATH TAG-->
</xsl:template>

<!--WRITE VML CURVE TAG-->
<xsl:template name="writeVMLCurveTag">
	<xsl:param name="points" />
	<xsl:param name="curvestyle" />
	<xsl:param name="curveid" />
	
<xsl:element name="v:curve">
	<xsl:attribute name="from">
		<xsl:value-of select="translate:getVMLcurvefrom(string(normalize-space($points)))" />
	</xsl:attribute>
	
	<xsl:attribute name="control1">
		<xsl:value-of select="translate:getVMLcurvecontrol1(string(normalize-space($points)))" />
	</xsl:attribute>
	
	<xsl:attribute name="control2">
		<xsl:value-of select="translate:getVMLcurvecontrol2(string(normalize-space($points)))" />
	</xsl:attribute>
	
	<xsl:attribute name="to">
		<xsl:value-of select="translate:getVMLcurveto(string(normalize-space($points)))" />
	</xsl:attribute>
	
	<xsl:call-template name="process-fill">
		<xsl:with-param name="svgstyle" select="$curvestyle" />
		<xsl:with-param name="id" select="$curveid" />
	</xsl:call-template>

</xsl:element>
<!--END WRITEVMLCURVETAG-->
</xsl:template>

<!--PROCESS SCRIPT TAG-->
<xsl:template match="script">
<xsl:variable name="scriptText" select="." />
<xsl:variable name="newScript" select="translate:getNewScript(string($scriptText))" />

<!--SCRIPT ELEMENT AND TYPE ATTRIBUTE OK-->
<xsl:element name="script">
<xsl:attribute name="type">text&#47;javascript</xsl:attribute>
<xsl:text>
 var xsl = new ActiveXObject("Microsoft.XMLDOM");
 xsl.async = false;
 xsl.load("translator.xsl");
 var xml;
</xsl:text>
<!--LET JAVASCRIPT PARSE THE FUNCTION NAME-->
<xsl:variable name="functionName" 
	select="translate:getFunctionName(string($scriptText))" />

<xsl:variable name="preFunctionName"
	select="concat('pre', $functionName)" />
<xsl:variable name="postFunctionName"
	select="concat('post', $functionName)" />
	
<!--INSERT VML FUNCTIONS-->
<xsl:text>function </xsl:text>
<xsl:value-of select="$preFunctionName" />
<xsl:text>
{
	var my_evt = new My_Event();
	createShape(my_evt);
</xsl:text>	
<xsl:value-of select="$postFunctionName" />;
<xsl:text>
}
</xsl:text>
<!--CREATE CUSTOM EVENT FUNCTION-->
<xsl:text>function </xsl:text>
<xsl:value-of select="$postFunctionName" />
{
	document.close();
	document.open();
	document.write(xml.transformNode(xsl));
	document.close();
}
<!--INSERT ORIGINAL SVG SCRIPT-->
<xsl:value-of select="$newScript" />
<xsl:text>function My_Event()</xsl:text>
{
	xml = source.XMLDocument;
	xml.async = false;
	//LOAD XML DATA SOURCE OBJECT CONTAINING SVG
    xml.loadXML(source.altHtml); 
	this.getXML = event_getXML;
}

<xsl:text>function event_getXML()</xsl:text>
{
	return xml;
}
</xsl:element>
<!--END OF SCRIPT TAG-->
</xsl:template>
</xsl:stylesheet>