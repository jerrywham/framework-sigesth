<?php
/**
 * namespace must be Plugins\nameOfThePlugin
 */
namespace Plugins\php_graph;

class php_graph implements \Core\Interfaces\Plugins {

    # To enable configuration.
    // const configuration = true;

    # Variables must be private for immutability of object
    # and initiate by constructor
    # see Tom Butler's blog for more explanations: https://r.je

    /**
     * mandatory variables
     */
    private $lang;
    private $theme;
    private $hooks = [];
    private $authentication;
    private $session;
    private $pluginsTable;
    private $id;
    private $plugin_name;
    private $params_name;
    private $params_value;
    private $mediasDir;
    private $urlroot;
    private $graph;
    /**
     * if config is available
     * Respect structure of the array even if there is only one param
     */
    private $defaultParams = [
        
    ];

    public function __construct(string $default_lang,string $theme, array $hooks, \Core\Interfaces\Auth\Authentication $authentication, \Core\Interfaces\Auth\Session $session, string $urlroot, \App\Interfaces\Medias $medias = null, \Core\Interfaces\DatabaseTable $pluginsTable = null, string $mediasDir = '') {
        # lang file must be in pluginName/lang/lang.php (for example pluginName/fr/fr.php) directory
        # see sample/fr/fr.php for details
        $this->lang = $default_lang;
        $this->theme = $theme;
        $this->hooks = $hooks;
        $this->authentication = $authentication;
        $this->session = $session;
        $this->pluginsTable = $pluginsTable;
        $this->urlroot = $urlroot;

        $this->graph = new phpGraph();

        $this->mediasDir = $mediasDir;
        if (!is_dir($this->mediasDir)) {
            @mkdir($this->mediasDir, 0777, true);
        }
    }
    /**
     * To avoid $var to be changed because object should be immuable
     */
    public function __set($var,$value)
    {
        return null;
    }
    public function __call($name, $arguments)
    {
        return null;
    }
    public function __isset($name)
    {
        return null;
    }
    public function __unset($name)
    {
        return $name;
    }
    /**
     * Exception for __get to allow pluginsConfiguration to access private properties in read mode
     * @param  [mixed] $var [var to access to]
     * @return [mixed]      [var]
     */
    public function __get($var)
    {
        return $this->$var;
    }

    /**
     * This method is called on plugin activation
     * 
     */
    public function onActivate() {
        
    }

     /**
     * This method is called on plugin deactivation
     * 
     */
    public function onDeactivate(){
        
    }
    /**
     * This method add routes to the initial application
     * Controller (in GET or POST method) should always be "$this" or must be defined before return
     */
    public function getRoutes():array {
        return [
            'downloadsvg/.*' => [
                'POST' => [
                    'controller' => $this,
                    'action' => 'downloadSvg'
                ],
                'GET' => [
                    'controller' => $this,
                    'action' => 'downloadSvg'
                ]
            ],
        ];
    }

    public function temporaryFile($name, $content)
    {
        $file = DIRECTORY_SEPARATOR .
                trim(sys_get_temp_dir(), DIRECTORY_SEPARATOR) .
                DIRECTORY_SEPARATOR .
                ltrim($name, DIRECTORY_SEPARATOR);

        file_put_contents($file, $content);

        register_shutdown_function(function() use($file) {
            unlink($file);
        });
        return $file;
    }

    public function downloadSvg()
    {
        $id = str_replace('downloadsvg/','',strip_tags(ltrim(str_replace(DIR_ROOT,'',strtok($_SERVER['REQUEST_URI'], '?')), '/')));
        $name = date('Ym-').$id;
        $svgToDownload = strip_tags($_POST['svgToDownload']) ?? null;
        $f = base64_decode(str_replace('data:image/png;base64,','',$svgToDownload));
        header('Content-Description: File Transfer');
        header('Content-Type: application/png');
        header("Content-Disposition: attachment; filename=\"".$name.".png\"");
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: no-cache');
        header('Content-Length: '.strlen($f));
        echo $f;
        exit();
        
    }

    ######################################
    #                                    #
    # ██  ██  ████   ████  ██  ██  ████  #
    # ██  ██ ██  ██ ██  ██ ██  ██ ██  ██ #
    # ██  ██ ██  ██ ██  ██ ██ ██   ██    #
    # ██████ ██  ██ ██  ██ ████     ██   #
    # ██  ██ ██  ██ ██  ██ ██ ██     ██  #
    # ██  ██ ██  ██ ██  ██ ██  ██ ██  ██ #
    # ██  ██  ████   ████  ██  ██  ████  #
    #                                    #
    ######################################
    /**
     * This method is called only if its name is declared on hooks.php file of the plugin
     * add "echo $hooks->callHook('\Plugins\php_graph\php_graph', 'addJavascript', ['CSP_RAND' => CSP_RAND, 'theme' => $theme]);" in your template to use it
     * Not mandatory because of javascript method
     */
    public function addJavascript(array $vars = []):string
    {
        $CSP_RAND = $vars['CSP_RAND'] ?? '';
        $theme = $vars['theme'] ?? '';
            return '
            <!-- javascript for phpGraph -->
            <script src="/assets/javascript/'.(new \Core\Lib\Js([], 'Vendors\JavaScriptPacker', $theme))->recordJs($this->graph->javascriptFunctions(),false).'"'.($CSP_RAND == '' ? '' : ' nonce="'.$CSP_RAND.'"').'></script>';
    }

    public function javascript()
    {
        echo $this->graph->javascriptFunctions();
    }


    public function addGraph(array $vars)
    {
        $data = $vars['data'] ?? [];
        $options = $vars['options'] ?? [];
        $options['token'] = $vars['token'] ?? null;
        $putInCache = $vars['putInCache'] ?? '';
        $id = $vars['id'] ?? '';
        $link = $vars['link'] ?? '/downloadsvg/';
        $txt = $vars['txt'] ?? 'Download';

        echo $this->graph->draw(
                $data,
                $options,
                $putInCache,
                $id,
                $link,
                $txt
            );
    }
}