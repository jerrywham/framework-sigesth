<?php 
namespace Plugins\php_graph;

# ------------------ BEGIN LICENSE BLOCK ------------------
#
##################################################################
#                                                                #
# ▓▓▓▓▓  ▓▓  ▓▓ ▓▓▓▓▓          ▓▓▓▓  ▓▓▓▓▓   ▓▓▓▓  ▓▓▓▓▓  ▓▓  ▓▓ #
# ▓▓  ▓▓ ▓▓  ▓▓ ▓▓  ▓▓        ▓▓  ▓▓ ▓▓  ▓▓ ▓▓  ▓▓ ▓▓  ▓▓ ▓▓  ▓▓ #
# ▓▓  ▓▓ ▓▓  ▓▓ ▓▓  ▓▓        ▓▓     ▓▓  ▓▓ ▓▓  ▓▓ ▓▓  ▓▓ ▓▓  ▓▓ #
# ▓▓▓▓▓  ▓▓▓▓▓▓ ▓▓▓▓▓         ▓▓ ▓▓▓ ▓▓▓▓▓  ▓▓▓▓▓▓ ▓▓▓▓▓  ▓▓▓▓▓▓ #
# ▓▓     ▓▓  ▓▓ ▓▓            ▓▓  ▓▓ ▓▓  ▓▓ ▓▓  ▓▓ ▓▓     ▓▓  ▓▓ #
# ▓▓     ▓▓  ▓▓ ▓▓            ▓▓  ▓▓ ▓▓  ▓▓ ▓▓  ▓▓ ▓▓     ▓▓  ▓▓ #
# ▓▓     ▓▓  ▓▓ ▓▓             ▓▓▓▓  ▓▓  ▓▓ ▓▓  ▓▓ ▓▓     ▓▓  ▓▓ #
#                                                                #
##################################################################
# @update     2019-01-02
# @copyright  2013-2019 Cyril MAGUIRE
# @licence    http://www.cecill.info/licences/Licence_CeCILL_V2.1-fr.txt CONTRAT DE LICENCE DE LOGICIEL LIBRE CeCILL version 2.1
# @link       http://jerrywham.github.io/phpGraph/
# @version    1.5
#
# ------------------- END LICENSE BLOCK -------------------
/**
 * @package    SIGesTH
 * @author     MAGUIRE Cyril <contact@ecyseo.net>
 * @copyright  2009-2019 Cyril MAGUIRE, <contact@ecyseo.net>
 * @license    Licensed under the CeCILL v2.1 license. http://www.cecill.info/licences.fr.html
 */
final class phpGraph
{

    # Basic css style
    private $css = '
        .draw {
            width:70%;/*Adjust this value to resize svg automatically*/
            margin:auto;
        }
        svg/**/ {/*width and height of svg is 100% of dimension of its parent (draw class here)*/
            display: block;
            margin:auto;
            margin-bottom: 50px;
        }
        .graph-title {
            stroke-width:4;
            stroke:transparent;
            fill:#000033;
            font-size: 1.2em;
        }
        .graph-grid {
            stroke-width:1;
            stroke:#c4c4c4;
        }
        .graph-stroke {
            stroke-width:2;
            stroke:#424242;
        }
        .graph-legends {}
        .graph-x text,
        .graph-y text {
            font-size:0.5em;
        }
        .graph-active .graph-legend {
            font-size:0.6em;
        }
        .graph-legend-stroke {}
        .graph-line {
            stroke-linejoin:round;
            stroke-width:2;
        }
        .graph-fill {
            stroke-width:0;
        }
        .graph-bar {}
        .graph-point {
            stroke-width:1;
            fill:#fff;
            fill-opacity:1;
            stroke-opacity:1;
        }
        .graph-point-active:hover {
            stroke-width:5;
            transition-duration:.9s;
            cursor: pointer;
        }
        title.graph-tooltip {
            background-color:#d6d6d6;
        }
        .graph-pie {
            cursor: pointer;
            stroke-width:1;
            stroke:#fff;
        }
        text {
            fill:#000;
        }
    ';

    private $options = array(
        'width' => 600,// (int) width of grid
        'height' => 300,// (int) height of grid
        'paddingTop' => 10,// (int)
        'type' => 'line',// (string) line, bar, pie, ring, stock or h-stock (todo curve)
        'steps' => null,// (int) 2 graduations on y-axis are separated by $steps units. "steps" is automatically calculated but we can set the value with integer. No effect on stock and h-stock charts
        'filled' => true,// (bool) to fill lines/histograms/disks
        'tooltips' => true,// (bool) to show tooltips
        'circles' => true,// (bool) to show circles on graph (lines or histograms). No effect on stock and h-stock charts
        'stroke' => '#3cc5f1',// (string) color of lines by default. Use an array to personalize each line
        'background' => "#ffffff",// (string) color of grid background. Don't use short notation (#fff) because of $this->__genColor();
        'opacity' => '0.5',// (float) between 0 and 1. No effect on stock and h-stock charts
        'gradient' => null,// (array) 2 colors from left to right
        'titleHeight' => 0,// (int) Height of main title
        'tooltipLegend' => null,// (string or array) Text display in tooltip with y value. Each text can be personalized using an array. No effect on stock and h-stock charts
        'legends' => null,// (string or array or bool) General legend for each line/histogram/disk displaying under diagram
        'title' => null,// (string) Main title. Title wil be displaying in a tooltip too.
        'radius' => 100,// (int) Radius of pie
        'diskLegends' => false,// (bool) to display legends around a pie
        'diskLegendsType' => 'label',// (string) data, pourcent or label to display around a pie as legend
        'diskLegendsLineColor' => 'darkgrey',// (string) color of lines which join pie to legends
        'responsive' => true,// (bool) to avoid svg to be responsive (dimensions fixed)
        'paddingLegendX' => 10,//We add 10 units in viewbox to display x legend correctly
        'paddingLegendY' => 0,//Padding to display y legend correctly, if needed, for pie and ring
        'transform' => null,
    );
    # authorized types
    private $types = array('line','line','bar','pie','ring','stock','h-stock');
    private $periodOfCache = 1;//A REGLER ET A VERIFIER
    private $colors = [];

    //protected $colors = array();

    /**
     * Constructor
     *
     * @param   $width integer Width of grid
     * @param   $height integer Height of grid
     * @param   $options array Options
     * @return  stdio
     *
     * @author  Cyril MAGUIRE
     **/
    public function __construct() {}

    /**
     * To add your own style
     * @param $css string your css
     * @return string css
     *
     * @author Cyril MAGUIRE
     */
    public function setCss($css)
    {
        if (is_string($css) && !empty($css)) {
            $this->css .= $css;
        }
    }

    /**
     * Main function
     * @param $data array Uni or bidimensionnal array
     * @param $options array Array of options
     * @param $putInCache string path of directory where cache will be recorded
     * @param $id string index of svg tag
     * @param $txt string text of dowload link
     * @return string SVG 
     *
     * @author Cyril MAGUIRE
     */
    public function draw(array $data, array $options = array(), string $putInCache = '', string $id = '', string $link= '/downloadSvg/', string $txt = 'Download')
    {
        if ($id != '') {
            $this->css = str_replace('/**/', '#'.$id.' ', $this->css);
        }

        $stat = null;

        $token = $options['token'] ?? null;

        $downloadLink = '<p class="downloadSvg"><a href="'.$link.$id.'" data-exportsvg="'.$id.'"'.($token !== null ? ' data-token="'.$token.'"' : '').' class="phpGraph">'.$txt.'</a></p>';

        # Cache
        $nameOfFiles = glob($putInCache.'*.svg');

        if ($putInCache != '' && isset($nameOfFiles[0])) {
            $stat = stat($nameOfFiles[0]);
            if ($stat['mtime'] > (time() - $this->periodOfCache)) {
                return file_get_contents($nameOfFiles[0]).($id != '' ? $downloadLink : '');
            }
        }
        $return = '';

        $options = array_merge($this->options, $options);
        $this->options = $options;

        extract($options);

        if ($title) {
            $options['titleHeight'] = $titleHeight = 40;
        }
        if ($opacity < 0 || $opacity > 1) {
            $options['opacity'] = 0.5;
        }
        if (!is_string($diskLegendsLineColor)) {
            $diskLegendsLineColor = 'darkgrey';
        }

        $HEIGHT = $height+$titleHeight+$paddingTop;

        $heightLegends = 0;
        if (isset($legends) && !empty($legends)) {
            $heightLegends = count($legends)*30+2*$paddingTop;
        }

        $pie = '';

        if ($type != 'pie' && $type != 'ring') {
            # looking for min and max
            extract($this->__minMax($data, $type));
            $options['type'] = $type;
            
            extract($this->__xAxisConfig($type, $width, $max, $Xmax, $Xmin, $lenght, $options));

            $options['steps'] = $steps;

            $unitY = ($height/abs(($max+$steps)-$min));
            $gridV = $gridH = '';
            $x = $y = '';

            $headerDimensions = $this->__headerDimensions($widthViewBox, $HEIGHT, $heightLegends, $titleHeight, $paddingTop, $paddingLegendX, $lenght, $stepX);

            # Size of canevas will be bigger than grid size to display legends
            $return = $this->__header($headerDimensions, $responsive, $id);

            if ($type == 'stock' || (is_array($type) && in_array('stock', $type))) {
                $plotLimit = $this->__stockDef();
            }
            if ($type == 'h-stock' || (is_array($type) && in_array('h-stock', $type))) {
                $plotLimit = $this->__hstockDef();
            }
            # we draw the grid
            $return .= $this->__svgGrid($gradient, $width, $height, ($paddingTop+$titleHeight));

            if ($title) {
                $return .= $this->__titleDef($title, $width, $titleHeight);
            }
            # Legends x axis and vertical grid
            extract($this->__XAxisDef($type, $Xmin, $Xmax, $XM, $stepX, $unitX, $HEIGHT, $paddingTop, $titleHeight, $labels, $lenght,$transform));

            # Legendes y axis and horizontal grid
            extract($this->__YAxisDef($type, $width, $min, $max, $steps, $HEIGHT, $titleHeight, $paddingTop, $paddingLegendX, $unitY, $labels));
            //Grid
            $return .= "\t".'<g class="graph-grid">'."\n";
            $return .= $gridH."\n";
            $return .= $gridV;
            $return .= "\t".'</g>'."\n";

            $return .= $x;
            $return .= $y;
            if (!$multi) {
                if (is_array($type) && count($type) == 1) {
                    $type = $type[0];
                    $options['type'] = $type;
                }
                $options['stroke'] = is_array($stroke) ? $stroke[0] : $stroke;
                switch ($type) {
                    case 'line':
                        $return .= $this->__drawLine($data, $height, $HEIGHT, $stepX, $unitY, $lenght, $min, $max, $options);
                        break;
                    case 'bar':
                        $return .= $this->__drawBar($data, $height, $HEIGHT, $stepX, $unitY, $lenght, $min, $max, $options);
                        break;
                    default:
                        $return .= $this->__drawLine($data, $height, $HEIGHT, $stepX, $unitY, $lenght, $min, $max, $options);
                        break;
                }
            } else {
                $i = 1;
                foreach ($data as $line => $datas) {
                    if (!isset($type[$line]) && !is_string($type) && is_numeric($line)) {
                        $type[$line] = 'line';
                    }
                    if (!isset($type[$line]) && !is_string($type) && !is_numeric($line)) {
                        $type[$line] = 'stock';
                    }
                    if (is_string($options['type'])) {
                        $type = array();
                        $type[$line] = $options['type'];
                    }
                    if (!isset($tooltipLegend[$line])) {
                        $options['tooltipLegend'] = '';
                    } else {
                        $options['tooltipLegend'] = $tooltipLegend[$line];
                    }
                    if (!isset($stroke[$line])) {
                        $stroke[$line] = $this->__genColor();
                    }
                    $options['stroke'] = $STROKE = $stroke[$line];
                    $options['fill'] = $stroke[$line];
                    switch ($type[$line]) {
                        case 'line':
                            $return .= $this->__drawLine($datas, $height, $HEIGHT, $stepX, $unitY, $lenght, $min, $max, $options);
                            break;
                        case 'bar':
                            $return .= $this->__drawBar($datas, $height, $HEIGHT, $stepX, $unitY, $lenght, $min, $max, $options);
                            break;
                        case 'stock':
                            $id = rand();
                            $return .= str_replace(array('id="plotLimit"', 'stroke=""'), array('id="plotLimit'.$id.'"', 'stroke="'.$stroke[$line].'"'), $plotLimit);
                            $return .= $this->__drawStock($data, $height, $HEIGHT, $stepX, $unitY, $lenght, $min, $max, $options, $i, $labels, $id);
                            $i++;
                            break;
                        case 'h-stock':
                            $id = rand();
                            $return .= str_replace(array('id="plotLimit"', 'stroke=""'), array('id="plotLimit'.$id.'"', 'stroke="'.$stroke[$line].'"'), $plotLimit);
                            $return .= $this->__drawHstock($data, $HEIGHT, $stepX, $unitX, $unitY, $lenght, $Xmin, $Xmax, $options, $i, $labels, $id);
                            $i++;
                            break;
                        case 'ring':
                            $options['subtype'] = 'ring';
                        case 'pie':
                            $options['multi'] = $multi;
                            if (is_array($stroke)) {
                                $options['stroke'] = $stroke[$line];
                                $options['fill'] = $stroke;
                            }
                            if (is_array($legends)) {
                                $options['legends'] = $legends[$line];
                            }
                            $pie .= $this->__drawDisk($datas, $options, $id);
                            $pie .= "\n".'</svg>'."\n";
                            break;
                        default:
                            $return .= $this->__drawLine($datas, $height, $HEIGHT, $stepX, $unitY, $lenght, $min, $max, $options);
                            break;
                    }
                }
            }
            # legends
            $return .= $this->__legendsDef($legends, $type, $stroke, $HEIGHT, $paddingTop);
            $return .= "\n".'</svg>'."\n";
            $return .= $pie;
        } else {
            $options['tooltipLegend'] = array();
            if ($tooltipLegend && !is_array($tooltipLegend)) {
                foreach ($data as $key => $value) {
                    $options['tooltipLegend'][] = $tooltipLegend;
                }
            }
            if ($tooltipLegend && is_array($tooltipLegend)) {
                $options['tooltipLegend'] = $tooltipLegend;
            }
            $options['stroke'] = array();
            if (isset($stroke) && !is_array($stroke)) {
                foreach ($data as $key => $value) {
                    $options['stroke'][] = $stroke;
                }
            }
            if (isset($stroke) && is_array($stroke)) {
                $options['stroke'] = $stroke;
            }
            foreach ($data as $line => $datas) {
                if (is_array($datas)) {
                    if (is_array($stroke)) {
                        $options['stroke'] = $stroke[$line];
                        $options['fill'] = $stroke;
                    }
                    if (is_array($legends)) {
                        $options['legends'] = $legends[$line];
                    }
                    $return .= $this->__drawDisk($datas, $options, $id);
                    $return .= "\n".'</svg>'."\n";
                    $multi = true;
                } else {
                    $multi = false;
                }
            }
            if (!$multi) {
                if (is_array($stroke)) {
                    $options['stroke'] = $stroke;
                    $options['fill'] = $stroke;
                }
                if (is_array($legends)) {
                    $options['legends'] = $legends;
                }
                $return .= $this->__drawDisk($data, $options, $id);
                $return .= "\n".'</svg>'."\n";
            }
        }

        $this->colors = array();
        if ($putInCache && $stat['mtime'] > (time() - $this->periodOfCache)) {
            $this->putInCache(trim($return), md5(implode('', $nameOfFile)), $putInCache);
            //file_put_contents($putInCache, trim($return));
        }
        return $return.($id != '' ? $downloadLink : '');
    }

    /**
     * To draw lines
     * @param $data array Unidimensionnal array
     * @param $height integer Height of grid
     * @param $HEIGHT integer Height of grid + title + padding top
     * @param $stepX integer Unit of x-axis
     * @param $unitY integer Unit of y-axis
     * @param $lenght integer Size of data array
     * @param $min integer Minimum value of data
     * @param $max integer Maximum value of data
     * @param $options array Options
     * @return string Path of lines (with options)
     *
     * @author Cyril MAGUIRE
     */
    protected function __drawLine($data, $height, $HEIGHT, $stepX, $unitY, $lenght, $min, $max, $options)
    {
        $return = '';

        extract($options);

        $this->colors[] = $options['stroke'];
    
        //Ligne
        $i = 0;
        $c = '';
        $t = '';
        $path = "\t\t".'<path d="';
        foreach ($data as $label => $value) {

            if ($min < 0) {
                $V = ($value-$min);
            } else {
                $V = $value;
            }

            $coordonneesCircle1 = 'cx="'.($i * $stepX + 50).'" cy="'.($HEIGHT - $unitY*$V).'"';
            //$min == $value
            $coordonneesCircle2 = 'cx="'.($i * $stepX + 50).'" cy="'.($HEIGHT - $unitY*$V - $value*$unitY).'"';
            
            $coordonnees1 = ($i * $stepX + 50).' '.($HEIGHT - $unitY*$V);
            //$min == $value
            $coordonnees2 = ($i * $stepX + 50).' '.($HEIGHT - $unitY*$V - $value*$unitY);

            # Tooltips
            if($tooltips == true) {
                $c .= "\n\t\t".'<g class="graph-active">';
            }
            # Line
            $c1 = $this->__c($coordonneesCircle1,$stroke);
            $c2 = $this->__c($coordonneesCircle2,$stroke);
            if ($value != $max) {
                if ($value == $min) {
                    if ($i == 0) {
                        // if ($min<=0) {
                        //  $path .= 'M '.$coordonnees1.' L';
                        //  //Tooltips and circles
                        //  $c .= $c1;
                        // } else {
                            $path .= 'M '.$coordonnees1.' L';
                            //Tooltips and circles
                            $c .= $c1;
                        // }
                    } else {
                        $path .= "\n\t\t\t\t".$coordonnees1;
                        //Tooltips and circles
                        $c .= $c1;
                    }
                } else {
                    if ($i == 0) {
                        $path .= 'M '.$coordonnees1.' L';
                        //Tooltips and circles
                        $c .= $c1;
                    } else {
                        $path .= "\n\t\t\t\t".$coordonnees1;
                        //Tooltips and circles
                        $c .= $c1;
                    }
                }
            } else {
                //Line
                if ($i == 0) {
                    $path .= 'M '.($i * $stepX + 50).' '.($V == 0 ? $HEIGHT : ($titleHeight + ($HEIGHT - $unitY*$V))).' L';
                    //Tooltips and circles
                    $c .= "\n\t\t\t".'<circle cx="'.($i * $stepX + 50).'" cy="'.($V == 0 ? $HEIGHT : ($titleHeight + ($HEIGHT - $unitY*$V))).'" r="3" stroke="'.$stroke.'" class="graph-point-active"/>';
                } else {
                    $path .= "\n\t\t\t\t".$coordonnees1;
                    //Tooltips and circles
                    $c .= "\n\t\t\t".'<circle '.$coordonneesCircle1.' r="3" stroke="'.$stroke.'" class="graph-point-active"/>';
                }
                
            }
            $i++;
            //End tooltips
            if($tooltips == true) {
                $c .= "\n\t\t\t".'<title class="graph-tooltip">'.(is_array($tooltipLegend) ? $tooltipLegend[$i] : $tooltipLegend).$value.'</title>'."\n\t\t".'</g>';
            }
        }
        if ($opacity > 0.8 && $filled === true) {
            $tmp = $stroke;
            $stroke = '#a1a1a1';
        }
        //End of line
        $pathLine = '" class="graph-line" stroke="'.$stroke.'" fill="#fff" fill-opacity="0"/>'."\n";
        //Filling
        if ($filled === true) {
            if ($min<=0) {
                $path .= "\n\t\t\t\t".(($i - 1) * $stepX + 50).' '.($HEIGHT + ($unitY)*($min-$value) + ($unitY * $value)).' 50 '.($HEIGHT + ($unitY)*($min-$value) + ($unitY * $value))."\n\t\t\t\t";
            } else {
                $path .= "\n\t\t\t\t".(($i - 1) * $stepX + 50).' '.$HEIGHT.' 50 '.$HEIGHT."\n\t\t\t\t";
            }
            if ($opacity > 0.8) {
                $stroke = $tmp;
            }
            $return .= $path.'" class="graph-fill" fill="'.$stroke.'" fill-opacity="'.$opacity.'"/>'."\n";
        }
        //Display line
        $return .= $path.$pathLine;
        
        if($circles == true) {
            $return .= "\t".'<g class="graph-point">';
            $return .= $c;
            $return .= "\n\t".'</g>'."\n";
        }
        return $return;
    }
    
    /**
     * To draw histograms
     * @param $data array Unidimensionnal array
     * @param $height integer Height of grid
     * @param $HEIGHT integer Height of grid + title + padding top
     * @param $stepX integer Unit of x-axis
     * @param $unitY integer Unit of y-axis
     * @param $lenght integer Size of data array
     * @param $min integer Minimum value of data
     * @param $max integer Maximum value of data
     * @param $options array Options
     * @return string Path of lines (with options)
     *
     * @author Cyril MAGUIRE
     */
    protected function __drawBar($data,$height,$HEIGHT,$stepX,$unitY,$lenght,$min,$max,$options) {
        $return = '';

        extract($options);
        
        $this->colors[] = $options['stroke'];

        //Bar
        $bar = '';
        $i = 0;
        $c = '';
        $t = '';
        foreach ($data as $label => $value) {

            if ($min <=0) {
                $V = ($value-$min);
            } else {
                $V = $value;
            }

            //Tooltips and circles
            if($tooltips == true) {
                $c .= "\n\t\t".'<g class="graph-active">';
            }

            $stepY = $value*$unitY;

            //$min>=0
            $coordonnees1 = 'x="'.($i * $stepX + 50).'" y="'.($HEIGHT - $unitY*$V).'"';
            //On recule d'un demi pas pour que la valeur de x soit au milieu de la barre de diagramme
            $coordonnees2 = 'x="'.($i * $stepX + 50 - $stepX/2).'" y="'.($HEIGHT - $stepY).'"';
            //$min<0
            $coordonnees3 = 'x="'.($i * $stepX + 50 - $stepX/2).'" y="'.($HEIGHT - $unitY*$V).'"';
            //$min<0 et $value<0
            $coordonnees4 = 'x="'.($i * $stepX + 50 - $stepX/2).'" y="'.($HEIGHT - $unitY*$V + $stepY).'"';
            $coordonnees5 = 'x="'.($i * $stepX + 50).'" y="'.($HEIGHT - $unitY*$V + $stepY).'"';
            //$min>=0 et $value == $max
            $coordonnees6 = 'x="'.($i * $stepX + 50).'" y="'.($paddingTop + $titleHeight + ($height - $stepY)).'"';
            //$value == 0
            $coordonnees7 = 'x="50" y="'.($HEIGHT + $unitY*$min).'"';
            if ($value == 0) {
                $stepY = 1;
            }
            //Diagramme
            //On est sur la première valeur, on divise la largeur de la barre en deux
            if ($i == 0) {
                if ($value == $max) {
                    $bar .= "\n\t".'<rect '.$coordonnees6.' width="'.($stepX/2).'" height="'.$stepY.'" class="graph-bar" stroke="'.$stroke.'" fill="#fff" fill-opacity="0"/>';
                    
                    $c .= "\n\t\t\t".'<circle c'.str_replace('y="', 'cy="', $coordonnees6).' r="3" stroke="'.$stroke.'" class="graph-point-active"/>';
                    
                } else {
                    if ($min>=0) {
                        $bar .= "\n\t".'<rect '.$coordonnees1.' width="'.($stepX/2).'" height="'.$stepY.'" class="graph-bar" stroke="'.$stroke.'" fill="#fff" fill-opacity="0"/>';
                        
                        $c .= "\n\t\t\t".'<circle c'.str_replace('y="', 'cy="', $coordonnees1).' r="3" stroke="'.$stroke.'" class="graph-point-active"/>';
                        
                    } else {
                        if ($value == $min) {
                            $bar .= "\n\t".'<rect '.$coordonnees5.' width="'.($stepX/2).'" height="'.-$stepY.'" class="graph-bar" stroke="'.$stroke.'" fill="#fff" fill-opacity="0"/>';
                        } else {
                            if ($value == 0) {
                                $bar .= "\n\t".'<rect '.$coordonnees7.' width="'.($stepX/2).'" height="1" class="graph-bar" stroke="'.$stroke.'" fill="#fff" fill-opacity="0"/>';
                            } else {
                                $bar .= "\n\t".'<rect '.$coordonnees1.' width="'.($stepX/2).'" height="'.$stepY.'" class="graph-bar" stroke="'.$stroke.'" fill="#fff" fill-opacity="0"/>';
                            }
                        }
                        
                        $c .= "\n\t\t\t".'<circle c'.str_replace('y="', 'cy="', $coordonnees1).' r="3" stroke="'.$stroke.'" class="graph-point-active"/>';
                        
                    }
                    
                }
            } else {
                if ($value == $max) {
                    if ($min>=0) {
                        //Si on n'est pas sur la dernière valeur
                        if ($i != $lenght-1) {
                            $bar .= "\n\t".'<rect '.$coordonnees2.' width="'.$stepX.'" height="'.$stepY.'" class="graph-bar" stroke="'.$stroke.'" fill="#fff" fill-opacity="0"/>';
                            
                        } else {
                            $bar .= "\n\t".'<rect '.$coordonnees2.' width="'.($stepX/2).'" height="'.$height.'" class="graph-bar" stroke="'.$stroke.'" fill="#fff" fill-opacity="0"/>';
                        }
                        $c .= "\n\t\t\t".'<circle c'.str_replace('y="', 'cy="', $coordonnees1).' r="3" stroke="'.$stroke.'" class="graph-point-active"/>';
                    } else {
                        if ($value >= 0) {
                            //Si on n'est pas sur la dernière valeur
                            if ($i != $lenght-1) {
                                $bar .= "\n\t".'<rect '.$coordonnees3.' width="'.$stepX.'" height="'.$stepY.'" class="graph-bar" stroke="'.$stroke.'" fill="#fff" fill-opacity="0"/>';
                            } else {
                                $bar .= "\n\t".'<rect '.$coordonnees3.' width="'.($stepX/2).'" height="'.$stepY.'" class="graph-bar" stroke="'.$stroke.'" fill="#fff" fill-opacity="0"/>';
                            }
                        } else {
                            //Si on n'est pas sur la dernière valeur
                            if ($i != $lenght-1) {
                                $bar .= "\n\t".'<rect '.$coordonnees4.' width="'.$stepX.'" height="'.-$stepY.'" class="graph-bar" stroke="'.$stroke.'" fill="#fff" fill-opacity="0"/>';
                            } else {
                                $bar .= "\n\t".'<rect '.$coordonnees4.' width="'.($stepX/2).'" height="'.-$stepY.'" class="graph-bar" stroke="'.$stroke.'" fill="#fff" fill-opacity="0"/>';
                            }
                        }
                        
                        $c .= "\n\t\t\t".'<circle c'.str_replace('y="', 'cy="', $coordonnees1).' r="3" stroke="'.$stroke.'" class="graph-point-active"/>';
                        
                    }
                }else {
                    if ($min>=0) {
                        //Si on n'est pas sur la dernière valeur
                        if ($i != $lenght-1) {
                            $bar .= "\n\t".'<rect '.$coordonnees2.' width="'.$stepX.'" height="'.$stepY.'" class="graph-bar" stroke="'.$stroke.'" fill="#fff" fill-opacity="0"/>';
                        } else {
                            $bar .= "\n\t".'<rect '.$coordonnees2.' width="'.($stepX/2).'" height="'.$stepY.'" class="graph-bar" stroke="'.$stroke.'" fill="#fff" fill-opacity="0"/>';
                        }
                        
                        $c .= "\n\t\t\t".'<circle c'.str_replace('y="', 'cy="', $coordonnees1).' r="3" stroke="'.$stroke.'" class="graph-point-active"/>';
                        
                    } else {
                        if ($value >= 0) {
                            //Si on n'est pas sur la dernière valeur
                            if ($i != $lenght-1) {
                                $bar .= "\n\t".'<rect '.$coordonnees3.' width="'.$stepX.'" height="'.($stepY).'" class="graph-bar" stroke="'.$stroke.'" fill="#fff" fill-opacity="0"/>';
                            } else {
                                $bar .= "\n\t".'<rect '.$coordonnees3.' width="'.($stepX/2).'" height="'.$stepY.'" class="graph-bar" stroke="'.$stroke.'" fill="#fff" fill-opacity="0"/>';
                            }
                        } else {
                            //Si on n'est pas sur la dernière valeur
                            if ($i != $lenght-1) {
                                $bar .= "\n\t".'<rect '.$coordonnees4.' width="'.$stepX.'" height="'.-$stepY.'" class="graph-bar" stroke="'.$stroke.'" fill="#fff" fill-opacity="0"/>';
                            } else {
                                $bar .= "\n\t".'<rect '.$coordonnees4.' width="'.($stepX/2).'" height="'.-$stepY.'" class="graph-bar" stroke="'.$stroke.'" fill="#fff" fill-opacity="0"/>';
                            }
                        }
                        
                        $c .= "\n\t\t\t".'<circle c'.str_replace('y="', 'cy="', $coordonnees1).' r="3" stroke="'.$stroke.'" class="graph-point-active"/>';
                        
                    }
                }
            }
            $i++;
            //End of tooltips
            if($tooltips == true) {
                $c .= '<title class="graph-tooltip">'.(is_array($tooltipLegend) ? $tooltipLegend[$i] : $tooltipLegend).$value.'</title>'."\n\t\t".'</g>';
            }
        }

        //Filling
        if ($filled === true) {
            if ($opacity == 1) {
                $opacity = '1" stroke="#424242';
            }
            $barFilled = str_replace(' class="graph-bar" stroke="'.$stroke.'" fill="#fff" fill-opacity="0"/>', ' class="graph-bar" fill="'.$stroke.'" fill-opacity="'.$opacity.'"/>',$bar);
            $return .= $barFilled;
        }

        $return .= $bar;

        if($circles == true) {
            $return .= "\n\t".'<g class="graph-point">';
            $return .= $c;
            $return .= "\n\t".'</g>'."\n";
        }
        return $return;
    }

    /**
     * To draw pie diagrams
     * @param $data array Unidimensionnal array
     * @param $options array Options
     * @return string Path of lines (with options)
     *
     * @author Cyril MAGUIRE
     */
    protected function __drawDisk($data, $options=array(), $id=false)
    {
        $options = array_merge($this->options, $options);

        extract($options);

        if (is_string($legends)) {
            $mainLegend = $legends;
        } else {
            $mainLegend = null;
        }
        if (is_string($stroke)) {
            $mainStroke = $stroke;
        } else {
            $mainStroke = $this->__genColor();
        }
        
        $lenght = count($data);
        $max = max($data);

        $total = 0;
        foreach ($data as $label => $value) {
            if ($value < 0) {
                $value = 0;
            }
            $total += $value;
        }
        $deg = array();
        $i = 0;
        foreach ($data as $label => $value) {
            if ($value < 0) {
                $value = 0;
            }
            if ($total == 0) {
                $deg[] = array(
                    'pourcent' => 0,
                    'val' => $value,
                    'label' => $label,
                    'tooltipLegend' => (is_array($tooltipLegend) && isset($tooltipLegend[$i])) ? $tooltipLegend[$i] : (isset($tooltipLegend) && is_string($tooltipLegend) ? $tooltipLegend : ''),
                    'stroke' => (is_array($stroke) && isset($stroke[$i]))? $stroke[$i] : $this->__genColor(),
                );
            } else {
                $deg[] = array(
                    'pourcent' => round(((($value * 100)/$total)/100), 2),
                    'val' => $value,
                    'label' => $label,
                    'tooltipLegend' => (is_array($tooltipLegend) && isset($tooltipLegend[$i])) ? $tooltipLegend[$i] : (isset($tooltipLegend) && is_string($tooltipLegend) ? $tooltipLegend : ''),
                    'stroke' => (is_array($stroke) && isset($stroke[$i])) ? $stroke[$i] : $this->__genColor(),
                );
            }
            $i++;
        }
        if (isset($legends)) {
            if (!is_array($legends) && !empty($legends) && !is_bool($legends)) {
                $Legends = array();
                for ($l=0;$l<$lenght;$l++) {
                    $Legends[$l] = array(
                        'label' => $deg[$l]['label'].' : '.$deg[$l]['val'],
                        'stroke' => $deg[$l]['stroke']
                    );
                }
                $legends = $Legends;
                unset($Legends);
            } elseif (empty($legends)) {
                $notDisplayLegends = true;
            } elseif (is_bool($legends)) {
                $legends = array();
            }
            foreach ($deg as $k => $v) {
                if (!isset($legends[$k]) || !is_array($legends[$k])) {
                    $legends[$k] = array(
                        'label' => $v['label'].' : '.$v['val'],
                        'stroke' => $v['stroke']
                    );
                }
            }
        }
        $deg = array_reverse($deg);

        $heightLegends = 0;
        if (isset($legends) && !empty($legends)) {
            $heightLegends = count($legends)*30+2*$paddingTop+80;
        } else {
            $heightLegends = 2*$paddingTop+100;
        }

        $this->colors[] = $options['stroke'];

        $originX = (2*$radius+400)/2;
        $originY = 10+$titleHeight+2*$paddingTop;


        //Size of canevas will be bigger than grid size to display legends
        $return = "\n".'<svg width="100%" height="100%" viewBox="0 0 '.ceil(2*$radius+400).' '.ceil(2*$radius+100+$titleHeight+$paddingTop+$heightLegends).'" preserveAspectRatio="xMidYMid meet" xmlns="http://www.w3.org/2000/svg" version="1.1"'.($id ? ' id="'.$id.'"':'').'>'."\n";
        $return .= "\n\t".'<defs>
        <style type="text/css"><![CDATA[
          '.$this->css.'
        ]]></style>'."\n";
        // $return .= "\n\t\t".'<marker id="Triangle"';
        // $return .= "\n\t\t\t".'viewBox="0 0 10 10" refX="0" refY="5"';
        // $return .= "\n\t\t\t".'markerUnits="strokeWidth"';
        // $return .= "\n\t\t\t".'markerWidth="10" markerHeight="3"';
        // $return .= "\n\t\t\t".'fill="#a1a1a1" fill-opacity="0.7"';
        // $return .= "\n\t\t\t".'orient="auto">';
        // $return .= "\n\t\t\t".'<path d="M 0 0 L 10 5 L 0 10 z" />';
        // $return .= "\n\t\t".'</marker>';
        if (is_array($gradient)) {
            $id = 'BackgroundGradient'.rand();
            $return .= "\n\t\t".'<linearGradient id="'.$id.'">';
            $return .= "\n\t\t\t".'<stop offset="5%" stop-color="'.$gradient[0].'" />';
            $return .= "\n\t\t\t".'<stop offset="95%" stop-color="'.$gradient[1].'" />';
            $return .= "\n\t\t".'</linearGradient>';
            $return .= "\n\t".'</defs>'."\n";
            $background = 'url(#'.$id.')';
            $return .= "\t".'<rect x="0" y="0" width="'.(2*$radius+400).'" height="'.(2*$radius+100+$titleHeight+$paddingTop+$heightLegends).'" class="graph-stroke" fill="'.$background.'" fill-opacity="1"/>'."\n";
        } else {
            $return .= "\n\t".'</defs>'."\n";
        }
        
        if (isset($title)) {
            $return .= "\t".'<text x="'.($originX).'" y="'.$titleHeight.'" text-anchor="middle" class="graph-title">'.$title.'</text>'."\n";
        }

        $ox = $prevOriginX = $originX;
        $oy = $prevOriginY = $originY;
        $total = 1;

        $i = 0;
        while ($i <= $lenght-1) {
            if ($deg[0]['val'] != 0) {
                $t = (2-$deg[0]['pourcent'])/2;
                $cosOrigine = cos((-90 + 360 * $t) * M_PI / 180)*$radius;
                $sinOrigine = sin((-90 + 360 * $t) * M_PI / 180)*$radius;
                $cosLegOrigine = cos((-90 + 360 * $t) * M_PI / 180)*(2*$radius);
                $sinLegOrigine = sin((-90 + 360 * $t) * M_PI / 180)*(2*$radius);
            } else {
                $cosOrigine = 0;
                $sinOrigine = 0;
                $cosLegOrigine = 0;
                $sinLegOrigine = 0;
            }

            if ($deg[$i]['val'] != 0) {
                //Tooltips
                if ($tooltips == true) {
                    $return .= "\n\t\t".'<g class="graph-active">';
                }
                $color = $deg[0]['stroke'];
                $return .= "\n\t\t\t".'<circle cx="'.$originX.'" cy="'.($originY+2*$radius).'" r="'.$radius.'" fill="'.$color.'" class="graph-pie"/>'."\n\t\t\t";

                if ($diskLegends == true && $deg[0]['val'] != 0) {
                    if ($deg[0]['pourcent'] >= 0 && $deg[0]['pourcent'] <= 0.5 || $deg[0]['pourcent'] == 1) {
                        $gapx = $gapy = 0;
                        $pathGap = 2;
                    }
                    if ($deg[0]['pourcent'] > 0.5 && $deg[0]['pourcent'] < 1) {
                        $gapx = $gapy = 1;
                        $pathGap = 1;
                    }

                    $LABEL = ($diskLegendsType == 'label' ? $deg[$i]['label'] : ($diskLegendsType == 'pourcent' ? ($deg[$i]['pourcent']*100).'%' : $deg[$i]['val']));

                    if ($gapx == -1) {
                        $gapx = strlen($LABEL)*$gapx*12;
                        $gapy = 5;
                    }

                    $return .= "\n\t\t\t".'<path d=" M '.($originX+$cosOrigine).' '.($originY+2*$radius+$sinOrigine).' L '.($originX+$cosLegOrigine).' '.($originY + 2*$radius + $sinLegOrigine).' L '.($originX+$cosLegOrigine-30).' '.($originY + 2*$radius + $sinLegOrigine).'" class="graph-line" stroke="'.$diskLegendsLineColor.'" stroke-opacity="0.5" stroke-dasharray="2,2,2" fill="none"/>';

                    $return .= "\n\t\t\t".'<text x="'.($originX+$cosLegOrigine+$gapx-30*$pathGap).'" y="'.($originY+2*$radius+$sinLegOrigine+$gapy-5).'" class="graph-legend" stroke="darkgrey" stroke-opacity="0.5">'.$LABEL.'</text>'."\n\t\t\t";
                }
                
                //End tooltips
                if ($tooltips == true) {
                    $return .= '<title class="graph-tooltip">'.$deg[$i]['tooltipLegend']. $deg[$i]['label'].' : '.$deg[$i]['val'].'</title>';
                    $return .= "\n\t\t".'</g>';
                }
                //$i = $deg[$i]['label'];
                break;
            }
            $i++;
        }
        // $tmp = array(); 
        // if (is_array($legends)) {
        //  foreach($legends as &$ma) {
        //      $tmp[] = &$ma['label'];
        //  }
        //  array_multisort($tmp, $legends); 
        // }

        foreach ($deg as $key => $value) {
            $total -= $value['pourcent'];
            $total2 = $total;

            $cos = cos((-90 + 360 * $total) * M_PI / 180)*$radius;
            $sin = sin((-90 + 360 * $total) * M_PI / 180)*$radius;

            $cosLeg = cos((-90 + 360 * $total) * M_PI / 180)*(2*$radius);
            $sinLeg = sin((-90 + 360 * $total) * M_PI / 180)*(2*$radius);

            if (isset($deg[$key+1])) {
                $total2 -= $deg[$key+1]['pourcent'];
                $t = (($total - $total2)/2) + $total2;
                $cos2 = cos((-90 + 360 * $t) * M_PI / 180)*$radius;
                $sin2 = sin((-90 + 360 * $t) * M_PI / 180)*$radius;
                $cosLeg2 = cos((-90 + 360 * $t) * M_PI / 180)*(2*$radius);
                $sinLeg2 = sin((-90 + 360 * $t) * M_PI / 180)*(2*$radius);
            } else {
                $cos2 = 0;
                $sin2 = 0;
                $cosLeg2 = 0;
                $sinLeg2 = 0;
            }

                //Tooltips
                if ($tooltips == true && $key < ($lenght-1)) {
                    $return .= "\n\t\t".'<g class="graph-active">';
                }
                
            if ($total >= 0 && $total <= 0.5 || $total == 1) {
                $arc = 0;
                $gapx = $gapy = 0;
                $signe = 1;
                $pathGap = 1;
            }
            if ($total > 0.5 && $total < 1) {
                $arc = 1;
                $signe = -1;
                $pathGap = 2.5;
            }
            $index = ($key == $lenght-1 ? 0 : $key+1);

            if ($key != $lenght-1 && $deg[$index]['val'] != 0) {
                $return .= "\n\t\t\t".'<path d="M '.$originX.' '.($originY + $radius).'  A '.$radius.' '.$radius.'  0 '.$arc.' 1 '.($originX + $cos).' '.($originY + 2*$radius + $sin).' L '.$originX.' '.($originY+2*$radius).' z" fill="'.$deg[$index]['stroke'].'" class="graph-pie"/>'."\n\t\t\t";
            }

            if ($key < ($lenght-1) && $deg[$key+1]['val'] != 0 && $diskLegends == true) {
                $LABEL = ($diskLegendsType == 'label' ?  $deg[$key+1]['label'] : ($diskLegendsType == 'pourcent' ? ($deg[$key+1]['pourcent']*100).'%' : $deg[$key+1]['val']));

                if ($arc == 1) {
                    $gapx = strlen($LABEL)*$gapx*12;
                    $gapy = 5;
                }

                $return .= "\n\t\t\t".'<path d=" M '.($originX+$cos2).' '.($originY+2*$radius+$sin2).' L '.($originX + $cosLeg2).' '.($originY + 2*$radius + $sinLeg2).' L '.($originX+$cosLeg2+$signe*30).' '.($originY + 2*$radius + $sinLeg2).'" fill="none" class="graph-line" stroke="'.$diskLegendsLineColor.'" stroke-opacity="0.5"  stroke-dasharray="2,2,2"/>';

                $return .= "\n\t\t\t".'<text x="'.($originX + $cosLeg2 + $gapx + $signe*30*$pathGap).'" y="'.($originY + 2*$radius + $sinLeg2 + $gapy).'" class="graph-legend" stroke="darkgrey" stroke-opacity="0.5">&nbsp;&nbsp;'.$LABEL.'</text>'."\n\t\t\t";
            }
                //End tooltips
                if ($tooltips == true && $key < ($lenght-1)) {
                    $return .= '<title class="graph-tooltip">'.$deg[$key+1]['tooltipLegend'].$deg[$key+1]['label'].' : '.$deg[$key+1]['val'].'</title>'."\n\t\t".'</g>';
                }
        }

        if ($mainLegend) {
            $return .= '<rect x="50" y="'.(4*$radius+$titleHeight+$paddingTop+(2*$paddingTop)+30).'" width="10" height="10" fill="'.$mainStroke.'" class="graph-legend-stroke"/>
            <text x="70" y="'.(4*$radius+$titleHeight+$paddingTop+(2*$paddingTop)+40).'" class="graph-legend">'.$mainLegend.'</text>';
            $paddingLegendY += 70;
        }
        if (isset($legends) && !empty($legends) && !isset($notDisplayLegends)) {
            $leg = "\t".'<g class="graph-legends">';
            foreach ($legends as $key => $value) {
                $colorToFillWith = ($key == 0 ? $value['stroke'] : $deg[$lenght-$key-1]['stroke']);
                $leg .= "\n\t\t".'<rect x="70" y="'.(4*$radius+$titleHeight+$paddingTop+$paddingLegendY+$key*(2*$paddingTop)+intval($this->options['paddingLegendY'])).'" width="10" height="10" fill="'.$colorToFillWith.'" class="graph-legend-stroke"/>';
                $leg .= "\n\t\t".'<text x="90" y="'.(4*$radius+$titleHeight+$paddingTop+$paddingLegendY+10+$key*(2*$paddingTop)).'" text-anchor="start" class="graph-legend">'.$value['label'].'</text>';
            }
            $leg .= "\n\t".'</g>';

            $return .= $leg;
        }
        if ($type == 'ring' || isset($subtype)) {
            $return .= '<circle cx="'.$originX.'" cy="'.($originY+2*$radius).'" r="'.($radius/2).'" fill="'.$background.'" class="graph-pie"/>';
        }

        return $return;
    }
    
    /**
     * To draw vertical stock chart
     * @param $data array Array with structure equal to array('index'=> array('open'=>val,'close'=>val,'min'=>val,'max'=>val))
     * @param $height integer Height of grid
     * @param $HEIGHT integer Height of grid + title + padding top
     * @param $stepX integer Distance between two graduations on x-axis
     * @param $unitY integer Unit of y-axis
     * @param $lenght integer Number of graduations on x-axis
     * @param $min integer Minimum value of data
     * @param $max integer Maximum value of data
     * @param $options array Options
     * @param $i integer index of current data
     * @param $labels array labels of x-axis
     * @param $id integer index of plotLimit
     * @return string Path of lines (with options)
     *
     * @author Cyril MAGUIRE
     */
    protected function __drawStock($data, $height, $HEIGHT, $stepX, $unitY, $lenght, $min, $max, $options, $i, $labels, $id)
    {
        $error = null;
        if (!isset($data[$labels[$i]]['open'])) {
            $data[$labels[$i]]['open'] = 0;
            //$error[] = 'open';
        }
        if (!isset($data[$labels[$i]]['close'])) {
            $data[$labels[$i]]['close'] = 0;
            //$error[] = 'close';
        }
        if (!isset($data[$labels[$i]]['max'])) {
            $data[$labels[$i]]['max'] = 0;
            //$error[] = 'max';
        }
        if (!isset($data[$labels[$i]]['min'])) {
            $data[$labels[$i]]['min'] = 0;
            //$error[] = 'min';
        }
        if ($error) {
            $return = "\t\t".'<path id="chemin" d="M '.($i * $stepX + 50).' '.($HEIGHT-$height+10).' V '.$height.'" class="graph-line" stroke="transparent" fill="#fff" fill-opacity="0"/>'."\n";
            $return .= "\t\t".'<text><textPath xlink:href="#chemin">Error : "';
            foreach ($error as $key => $value) {
                $return .= $value.(count($error)>1? ' ' : '');
            }
            $return .= '" missing</textPath></text>'."\n";
            return $return;
        }
        $options = array_merge($this->options, $options);

        extract($options);

        $return = '';
        if ($data[$labels[$i]]['close'] < $data[$labels[$i]]['open']) {
            $return .= "\n\t".'<rect x="'.($i * $stepX + 50 - $stepX/4).'" y="'.($HEIGHT - $unitY*$data[$labels[$i]]['open']).'" width="'.($stepX/2).'" height="'.($unitY*$data[$labels[$i]]['open'] - ($unitY*$data[$labels[$i]]['close'])).'" class="graph-bar" fill="'.$stroke.'" fill-opacity="1"/>';
        }
        if ($data[$labels[$i]]['close'] == $data[$labels[$i]]['open']) {
            $return .= "\n\t".'<path d="M'.($i * $stepX + 50 + 5).' '.($HEIGHT - $unitY*$data[$labels[$i]]['open']).' l -5 -5, -5 5, 5 5 z" class="graph-line" stroke="'.$stroke.'" fill="'.$stroke.'" fill-opacity="1"/>';
        }
        //Limit Up
        $return .= "\n\t".'<path d="M'.($i * $stepX + 50).' '.($HEIGHT - $unitY*$data[$labels[$i]]['close']).'  L'.($i * $stepX + 50).' '.($HEIGHT-$unitY*$data[$labels[$i]]['max']).' " class="graph-line" stroke="'.$stroke.'" fill="#fff" fill-opacity="0"/>';
        $return .= '<use xlink:href="#plotLimit'.$id.'" transform="translate('.($i * $stepX + 50 - 5).','.($HEIGHT-$unitY*$data[$labels[$i]]['max']).')"/>';
        //Limit Down
        $return .= "\n\t".'<path d="M'.($i * $stepX + 50).' '.($HEIGHT - $unitY*$data[$labels[$i]]['open']).'  L'.($i * $stepX + 50).' '.($HEIGHT-$unitY*$data[$labels[$i]]['min']).' " class="graph-line" stroke="'.$stroke.'" fill="#fff" fill-opacity="0"/>';
        $return .= '<use xlink:href="#plotLimit'.$id.'" transform="translate('.($i * $stepX + 50 - 5).','.($HEIGHT-$unitY*$data[$labels[$i]]['min']).')"/>';
        if ($tooltips == true) {
            //Open
            $return .= $this->__circle($i, $stepX, ($HEIGHT - $unitY*$data[$labels[$i]]['open']), $data[$labels[$i]]['open'], $stroke);
            //Close
            $return .= $this->__circle($i, $stepX, ($HEIGHT - $unitY*$data[$labels[$i]]['close']), $data[$labels[$i]]['close'], $stroke);
            //Max
            $return .= $this->__circle($i, $stepX, ($HEIGHT - $unitY*$data[$labels[$i]]['max']), $data[$labels[$i]]['max'], $stroke);
            //Min
            $return .= $this->__circle($i, $stepX, ($HEIGHT - $unitY*$data[$labels[$i]]['min']), $data[$labels[$i]]['min'], $stroke);
        }
        return $return;
    }
    
    /**
     * To draw horizontal stock chart
     * @param $data array Array with structure equal to array('index'=> array('open'=>val,'close'=>val,'min'=>val,'max'=>val))
     * @param $HEIGHT integer Height of grid + title + padding top
     * @param $stepX integer Distance between two graduations on x-axis
     * @param $unitX integer Unit of x-axis
     * @param $unitY integer Unit of y-axis
     * @param $lenght integer Number of graduations on y-axis
     * @param $Xmin integer Minimum value of data
     * @param $Xmax integer Maximum value of data
     * @param $options array Options
     * @param $i integer index of current data
     * @param $labels array labels of y-axis
     * @param $id integer index of plotLimit
     * @return string Path of lines (with options)
     *
     * @author Cyril MAGUIRE
     */
    protected function __drawHstock($data, $HEIGHT, $stepX, $unitX, $unitY, $lenght, $Xmin, $Xmax, $options, $i, $labels, $id)
    {
        if ($i>0) {
            $i--;
        }

        $stepY = $HEIGHT - ($unitY*($i+1));

        $error = null;
        if (!isset($data[$labels[$i]]['open'])) {
            $error[] = 'open';
        }
        if (!isset($data[$labels[$i]]['close'])) {
            $error[] = 'close';
        }
        if (!isset($data[$labels[$i]]['max'])) {
            $error[] = 'max';
        }
        if (!isset($data[$labels[$i]]['min'])) {
            $error[] = 'min';
        }
        if ($error) {
            $return = "\t\t".'<path id="chemin" d="M '.(2*$unitX + 50).' '.$stepY.' H '.(($Xmax-$Xmin)*$unitX).'" class="graph-line" stroke="transparent" fill="#fff" fill-opacity="0"/>'."\n";
            $return .= "\t\t".'<text><textPath xlink:href="#chemin">Error : "';
            foreach ($error as $key => $value) {
                $return .= $value.(count($error)>1? ' ' : '');
            }
            $return .= '" missing</textPath></text>'."\n";
            return $return;
        }
        $options = array_merge($this->options, $options);

        extract($options);

        $return = '';
        if ($data[$labels[$i]]['close'] > $data[$labels[$i]]['open']) {
            $return .= "\n\t".'<rect x="'.($unitX*$data[$labels[$i]]['open']+50).'" y="'.($stepY-10).'" width="'.(($unitX*$data[$labels[$i]]['close']) - ($unitX*$data[$labels[$i]]['open'])).'" height="20" class="graph-bar" fill="'.$stroke.'" fill-opacity="1"/>';
        }
        if ($data[$labels[$i]]['close'] == $data[$labels[$i]]['open']) {
            $return .= "\n\t".'<path d="M'.($unitX*$data[$labels[$i]]['open']+50+5).' '.($stepY).' l -5 -5, -5 5, 5 5 z" class="graph-line" stroke="'.$stroke.'" fill="'.$stroke.'" fill-opacity="1"/>';
        }
        // //Limit Up
        $return .= "\n\t".'<path d="M'.($unitX*$data[$labels[$i]]['max']+50).' '.($stepY).'  L'.($unitX*$data[$labels[$i]]['close']+50).' '.($stepY).' " class="graph-line" stroke="'.$stroke.'" fill="#fff" fill-opacity="0"/>';
        $return .= '<use xlink:href="#plotLimit'.$id.'" transform="translate('.($unitX*$data[$labels[$i]]['max']+50).','.($stepY-5).')"/>';
        // //Limit Down
        $return .= "\n\t".'<path d="M'.($unitX*$data[$labels[$i]]['min']+50).' '.($stepY).'  L'.($unitX*$data[$labels[$i]]['open']+50).' '.($stepY).' " class="graph-line" stroke="'.$stroke.'" fill="#fff" fill-opacity="0"/>';
        $return .= '<use xlink:href="#plotLimit'.$id.'" transform="translate('.($unitX*$data[$labels[$i]]['min']+50).','.($stepY-5).')"/>';
        if ($tooltips == true) {
            //Open
            $return .= $this->__circle($unitX, $data[$labels[$i]]['open'], $stepY, $data[$labels[$i]]['open'], $stroke);
            //Close
            $return .= $this->__circle($unitX, $data[$labels[$i]]['close'], $stepY, $data[$labels[$i]]['close'], $stroke);
            //Max
            $return .= $this->__circle($unitX, $data[$labels[$i]]['max'], $stepY, $data[$labels[$i]]['max'], $stroke);
            //Min
            $return .= $this->__circle($unitX, $data[$labels[$i]]['min'], $stepY, $data[$labels[$i]]['min'], $stroke);
        }
        return $return;
    }

    /**
     * To add circles on histogram
     * @param $unitX integer index of current data
     * @param $cx integer Distance between two graduations on x-axis
     * @param $cy integer Distance between two graduations on y-axis
     * @param $label string label of tooltip
     * @param $stroke string color of circle
     * @return string path of circle
     *
     * @author Cyril MAGUIRE
     */
    protected function __circle($unitX, $cx, $cy, $label, $stroke)
    {
        $return = "\n\t\t".'<g class="graph-active">';
        $return .= "\n\t\t\t".'<circle cx="'.($unitX*$cx+50).'" cy="'.$cy.'" r="1" stroke="'.$stroke.'" opacity="0" class="graph-point-active"/>';
        $return .= "\n\t".'<title class="graph-tooltip">'.$label.'</title>'."\n\t\t".'</g>';
        return $return;
    }

    /**
     * To research min and max values of data depends on type of graph
     * @param $data array data
     * @param $type string line, bar, pie, ring, stock or h-stock
     * @return array of variables needed for draw method
     *
     * @author Cyril MAGUIRE
     */
    protected function __minMax($data, $type)
    {
        $arrayOfMin = $arrayOfMax = $arrayOfLenght = $labels = $tmp['type'] = array();
        $valuesMax = $valuesMin = '';
        $Xmin = $Xmax = null;
        $multi = false;
        //For each diagrams with several lines/histograms
        foreach ($data as $line => $datas) {
            if ($type == 'stock' || (is_array($type) && in_array('stock', $type))|| $type == 'h-stock' || (is_array($type) && in_array('h-stock', $type))) {
                $arrayOfMin[] = isset($datas['min']) ? floor($datas['min']):0;
                $arrayOfMax[] = isset($datas['max']) ?  ceil($datas['max']) : 0;
                $arrayOfLenght[] = count($data);
                $labels = array_merge(array_keys($data), $labels);
                if (is_string($type)) {
                    $tmp['type'][$line] = $type;
                }
                $multi = true;
            } else {
                if (is_array($datas)) {
                    $valuesMax = array_map('ceil', $datas);
                    $valuesMin = array_map('ceil', $datas);
                    $arrayOfMin[] = min($valuesMin);
                    $arrayOfMax[] = max($valuesMax);
                    $arrayOfLenght[] = count($datas);
                    $labels = array_merge(array_keys($datas), $labels);
                    if (is_string($type)) {
                        $tmp['type'][] = $type;
                    }
                    $multi = true;
                } else {
                    $multi = false;
                }
            }
        }

        if ($multi == true) {
            if (!empty($tmp['type'])) {
                $type = $tmp['type'];
            }
            unset($tmp);

            $labels = array_unique($labels);

            if ($type == 'h-stock' || (is_array($type) && in_array('h-stock', $type))) {
                $min = 0;
                $max = count($labels);
                $Xmax = max($arrayOfMax);
                $Xmin = min($arrayOfMin);
                $lenght = $Xmax - $Xmin;
            } else {
                $min = min($arrayOfMin);
                $max = max($arrayOfMax);
                $lenght = max($arrayOfLenght);
            }
            if ($type == 'stock' || (is_array($type) && in_array('stock', $type))) {
                array_unshift($labels, '');
                $labels[] = '';
                $lenght += 2;
            }
        } else {
            $labels = array_keys($data);
            $lenght = count($data);
            $min = min($data);
            $max = max($data);
        }
        return array('min'=>$min, 'max'=>$max, 'lenght'=>$lenght, 'labels'=>$labels, 'multi'=>$multi, 'valuesMax'=>$valuesMax, 'valuesMin'=>$valuesMin, 'Xmin'=>$Xmin, 'Xmax'=>$Xmax, 'type' => $type);
    }

    /**
     * Calcul of variables needed for x-axis configuration
     * @param $type string type of graph
     * @param $width integer width of grid
     * @param $max integer max of values in data
     * @param $Xmax integer max of values in x-axis
     * @param $Xmin integer min of values in x-axis
     * @param $lenght integer distance between min and max values
     * @param $options array options
     * @return array of variables needed for draw method
     *
     * @author Cyril MAGUIRE
     */
    protected function __xAxisConfig($type, $width, $max, $Xmax, $Xmin, $lenght, $options=false)
    {
        $XM = $stepX = $unitX = null;
        if ($type == 'h-stock' || (is_array($type) && in_array('h-stock', $type))) {
            $l = strlen(abs($Xmax))-1;
            if ($l == 0) {
                $l = 1;
                $XM = ceil($Xmax); # Max value to display in x axis
                $stepX = 1;
                $M = $lenght+1; # temporary max
                $steps = 1;
                if ($XM == 0) {
                    $XM = 1;
                }
                $unitX = $width/$XM;
                $widthViewBox = ceil($width+$XM+50);
            } else {
                $XM =  ceil($Xmax/($l*10))*($l*10);
                $stepX = $l*10;
                $M = $lenght+1;
                $steps = 1;
                if ($Xmin>0 || ($Xmin<0 && $Xmax<0)) {
                    $Xmin = 0;
                }
                if ($XM == 0) {
                    $XM = 1;
                }
                $unitX = ($width/$XM);
                $widthViewBox = ceil($width + ($XM/$stepX)*$unitX);
            }
        } else {
            $l = strlen(abs($max))-1;
            if ($l == 0) {
                $l = 1;
                $M =  ceil($max);
                $steps = 1;
            } else {
                $M =  ceil($max/($l*10))*($l*10);
                $steps = $l*10;
            }
            
            $max = $M;
            if (isset($options['steps']) && is_int($steps)) {
                $steps = $options['steps'];
            }

            $stepX = $width / ($lenght - 1);
            $widthViewBox = ceil($lenght*$stepX+$stepX);
        }
        return array('XM'=>$XM,'stepX'=>$stepX,'steps'=>$steps,'unitX'=>$unitX,'widthViewBox'=>$widthViewBox,'Xmin'=>$Xmin);
    }

    /**
     * To draw x-axis of the grid
     * @param $type string type of graph
     * @param $Xmin integer min of values in x-axis
     * @param $Xmax integer max of values in x-axis
     * @param $XM integer Max value to display in x axis
     * @param $stepX integer Distance between two graduations on x-axis
     * @param $unitX integer index of current data
     * @param $HEIGHT integer height of the entire svg tag
     * @param $paddingTop integer padding on top 
     * @param $titleHeight integer height of title
     * @param $labels array Array of labels
     * @param $lenght integer distance between min and max values
     * @return array of variables needed for draw method
     *
     * @author Cyril MAGUIRE
     */
    protected function __XAxisDef($type,$Xmin,$Xmax,$XM,$stepX,$unitX,$HEIGHT,$paddingTop,$titleHeight,$labels,$lenght,$transform) {
        $gridV = $x = '';
        $x .= "\t".'<g class="graph-x">'."\n";
        if (is_array($type) && in_array('h-stock', $type) ) {
            for ($i=$Xmin; $i <= $XM; $i+=$stepX) {
                //1 graduation every $steps units
                $step = $unitX*$i;

                $x .= "\t\t".'<text x="'.(($transform !== null ? 25 : 50)+$step).'" y="'.($HEIGHT+2*$paddingTop).'"'.($transform !== null ? ' transform="translate(30,'.(3*$paddingTop).')rotate('.$transform.','.(($transform !== null ? 25 : 50)+$step).','.($HEIGHT+2*$paddingTop).')"' : '').' text-anchor="end" baseline-shift="-1ex" dominant-baseline="middle">'.$i.'</text>'."\n";
                //Vertical grid
                if ($i != $Xmax) {
                    $gridV .= "\t\t".'<path d="M '.(50+$step).' '.($paddingTop+$titleHeight).' V '.($HEIGHT).'"/>'."\n" ;
                }
            }
        } else {
            $i=0;
            foreach ($labels as $key => $label) {
                //We add a gap of 50 units 
                $x .= "\t\t".'<text x="'.($i*$stepX+($transform !== null ? 0 : 50)).'" y="'.($HEIGHT+2*$paddingTop).'"'.($transform !== null ? ' transform="translate(30,'.(3*$paddingTop).')rotate('.$transform.','.($i*$stepX+($transform !== null ? 25 : 50)).','.($HEIGHT+2*$paddingTop).')"' : '').' text-anchor="'.($transform == 90 || $transform == -90 ? 'top' : 'middle').'">'.$label.'</text>'."\n";
                //Vertical grid
                if ($i != 0 && $i != $lenght) {
                    $gridV .= "\t\t".'<path d="M '.($i*$stepX+50).' '.($paddingTop+$titleHeight).' V '.($HEIGHT).'"/>'."\n" ;
                }
                $i++;
            }
        }
        $x .= "\t".'</g>'."\n";
        return array('x'=>$x, 'gridV'=>$gridV);
    }

    /**
     * To draw y-axis of the grid
     * @param $type string type of graph
     * @param $width integer width of grid
     * @param $min integer min of values in data
     * @param $max integer max of values in data
     * @param $steps integer Distance between two graduations
     * @param $HEIGHT integer height of the entire svg tag
     * @param $titleHeight integer height of title
     * @param $paddingTop integer padding on top 
     * @param $paddingLegendX integer padding between legend and x axis
     * @param $unitY integer unit of y graduations
     * @param $labels array Array of labels
     * @return array of variables needed for draw method
     *
     * @author Cyril MAGUIRE
     */
    protected function __YAxisDef($type, $width, $min, $max, $steps, $HEIGHT, $titleHeight, $paddingTop, $paddingLegendX, $unitY, $labels)
    {
        $gridH = $y = '';
        $y .= "\t".'<g class="graph-y">'."\n";
        if ($min>0 || ($min<0 && $max<0)) {
            $min = 0;
        }
        for ($i=$min; $i <= ($max+$steps); $i+=$steps) {
            //1 graduation every $steps units
            if ($min<0) {
                $stepY = $HEIGHT + $unitY*($min-$i);
            } else {
                $stepY = $HEIGHT - ($unitY*$i);
            }
        
            if ($stepY >= ($titleHeight+$paddingTop+$paddingLegendX)) {
                if (is_array($type) && in_array('h-stock', $type) && isset($labels[$i-1])) {
                    $y .= "\t\t".'<g class="graph-active"><text x="40" y="'.$stepY.'" text-anchor="end" baseline-shift="-1ex" dominant-baseline="middle" >'.($i > 0 ? (strlen($labels[$i-1]) > 3 ? substr($labels[$i-1], 0, 3).'.</text><title>'.$labels[$i-1].'</title>' : $labels[$i-1].'</text>') : '</text>')."</g>\n";
                } else {
                    $y .= "\t\t".'<text x="40" y="'.$stepY.'" text-anchor="end" baseline-shift="-1ex" dominant-baseline="middle" >'.$i.'</text>';
                }
                //Horizontal grid
                $gridH .= "\t\t".'<path d="M 50 '.$stepY.' H '.($width+50).'"/>'."\n" ;
            }
        }
        $y .= "\t".'</g>'."\n";
        return array('y'=>$y,'gridH'=>$gridH);
    }

    /**
     * To draw legends under the grid
     * @param $legends array Array of legends
     * @param $type string type of graph
     * @param $stroke string color of legends
     * @param $HEIGHT integer height of the entire svg tag
     * @param $paddingTop integer padding on top
     * @return string path of legends
     *
     * @author Cyril MAGUIRE
     */
    protected function __legendsDef($legends, $type, $stroke, $HEIGHT, $paddingTop)
    {
        if (isset($legends) && !empty($legends)) {
            $leg = "\n\t".'<g class="graph-legends">';
            if (!is_array($legends)) {
                $legends = array(0 => $legends);
            }
            foreach ($legends as $key => $value) {
                if (is_array($type) && isset($type[$key]) && $type[$key] != 'pie' && $type[$key] != 'ring') {
                    if (is_array($stroke) && isset($stroke[$key])) {
                        $leg .= "\n\t\t".'<rect x="50" y="'.($HEIGHT+30+$key*(2*$paddingTop)+intval($this->options['paddingLegendY'])).'" width="10" height="10" fill="'.$stroke[$key].'" class="graph-legend-stroke"/>';
                    } else {
                        $leg .= "\n\t\t".'<rect x="50" y="'.($HEIGHT+30+$key*(2*$paddingTop)+intval($this->options['paddingLegendY'])).'" width="10" height="10" fill="'.$stroke.'" class="graph-legend-stroke"/>';
                    }
                    $leg .= "\n\t\t".'<text x="70" y="'.($HEIGHT+40+$key*(2*$paddingTop)+intval($this->options['paddingLegendY'])).'" text-anchor="start" class="graph-legend">'.$value.'</text>';
                }
                if (!is_array($type) && $type != 'pie' && $type != 'ring') {
                    if (is_array($stroke) && isset($stroke[$key])) {
                        $leg .= "\n\t\t".'<rect x="50" y="'.($HEIGHT+30+$key*(2*$paddingTop)+intval($this->options['paddingLegendY'])).'" width="10" height="10" fill="'.$stroke[$key].'" class="graph-legend-stroke"/>';
                    } else {
                        $leg .= "\n\t\t".'<rect x="50" y="'.($HEIGHT+30+$key*(2*$paddingTop)+intval($this->options['paddingLegendY'])).'" width="10" height="10" fill="'.$stroke.'" class="graph-legend-stroke"/>';
                    }
                    $leg .= "\n\t\t".'<text x="70" y="'.($HEIGHT+40+$key*(2*$paddingTop)+intval($this->options['paddingLegendY'])).'" text-anchor="start" class="graph-legend">'.$value.'</text>';
                }
                if (is_array($type) && (in_array('stock', $type) || in_array('h-stock', $type))) {
                    if (is_array($stroke)) {
                        $stroke = array_values($stroke);
                        if (isset($stroke[$key+1])) {
                            $leg .= "\n\t\t".'<rect x="50" y="'.($HEIGHT+30+$key*(2*$paddingTop)+intval($this->options['paddingLegendY'])).'" width="10" height="10" fill="'.$stroke[$key+1].'" class="graph-legend-stroke"/>';
                        }
                    }
                    $leg .= "\n\t\t".'<text x="70" y="'.($HEIGHT+40+$key*(2*$paddingTop)+intval($this->options['paddingLegendY'])).'" text-anchor="start" class="graph-legend">'.$value.'</text>';
                }
            }
            $leg .= "\n\t".'</g>';
        } else {
            $leg = '';
        }
        return $leg;
    }

    /**
     * To generate hexadecimal code for color
     * @param null
     * @return string hexadecimal code
     *
     * @author Cyril MAGUIRE
     */
    protected function __genColor()
    {
        $val = array("0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F");
        shuffle($val);
        $rand = array_rand($val, 6);
        $hexa = '';
        foreach ($rand as $key => $keyOfVal) {
            $hexa .= $val[$keyOfVal];
        }
        if ('#'.$hexa == $this->options['background']) {
            return $this->__genColor();
        }
        if (!in_array($hexa, $this->colors)) {
            $this->colors[] = $hexa;
            return '#'.$hexa;
        } else {
            return $this->__genColor();
        }
    }

    protected function __stockDef()
    {
        $return = "\n\t".'<defs>';
        $return .= "\n\t\t".'<g id="plotLimit">';
        $return .= "\n\t\t\t".'<path d="M 0 0 L 10 0" class="graph-line" stroke="" stroke-opacity="1"/>';
        $return .= "\n\t\t".'</g>';
        $return .= "\n\t".'</defs>'."\n";
        return $return;
    }

    protected function __hstockDef()
    {
        $return = "\n\t".'<defs>';
        $return .= "\n\t\t".'<g id="plotLimit">';
        $return .= "\n\t\t\t".'<path d="M 0 0 V 0 10" class="graph-line" stroke="" stroke-opacity="1"/>';
        $return .= "\n\t\t".'</g>';
        $return .= "\n\t".'</defs>'."\n";
        return $return;
    }

    protected function __gradientDef($gradient, $id)
    {
        $return ="\n\t".'<defs>';
        $return .= "\n\t\t".'<linearGradient id="'.$id.'">';
        $return .= "\n\t\t\t".'<stop offset="5%" stop-color="'.$gradient[0].'" />';
        $return .= "\n\t\t\t".'<stop offset="95%" stop-color="'.$gradient[1].'" />';
        $return .= "\n\t\t".'</linearGradient>';
        $return .= "\n\t".'</defs>'."\n";
        return $return;
    }

    protected function __header($dimensions=array('width'=>'100%', 'height'=>'100%', 'widthViewBox'=>685, 'heightViewBox'=>420), $responsive=true, $id=false)
    {
        if ($responsive == true) {
            $return = "\n".'<svg xmlns="http://www.w3.org/2000/svg" version="1.1" xml:lang="fr" xmlns:xlink="http://www.w3/org/1999/xlink" class="graph" width="100%" height="100%" viewBox="0 0 '.($dimensions['widthViewBox']).' '.($dimensions['heightViewBox']).'" preserveAspectRatio="xMidYMid meet"'.($id ? ' id="'.$id.'"':'').'>'."\n";
        } else {
            $return = "\n".'<svg xmlns="http://www.w3.org/2000/svg" version="1.1" xml:lang="fr" xmlns:xlink="http://www.w3/org/1999/xlink" class="graph" width="'.$dimensions['width'].'" height="'.$dimensions['height'].'" viewBox="0 0 '.$dimensions['widthViewBox'].' '.($dimensions['heightViewBox']).'" preserveAspectRatio="xMidYMid meet"'.($id ? ' id="'.$id.'"':'').'>'."\n";
        }
        $return .= '<defs>
        <style type="text/css"><![CDATA[
          '.$this->css.'
        ]]></style>'."\n".'</defs>'."\n";
        return $return;
    }

    protected function __svgGrid($gradient, $width, $height, $yHeight)
    {
        $return = '';
        $background = '#ffffff';
        if (is_array($gradient) && count($gradient) == 2) {
            $id = 'BackgroundGradient'.rand();
            $return .= $this->__gradientDef($gradient, $id);
            $background = 'url(#'.$id.')';
        }
        //Grid is beginning at 50 units from the left
        return $return .= "\t".'<rect x="50" y="'.$yHeight.'" width="'.$width.'" height="'.$height.'" class="graph-stroke" fill="'.$background.'" fill-opacity="1"/>'."\n";
    }

    protected function __titleDef($title, $width, $titleHeight)
    {
        $return = "\t".'<title class="graph-tooltip">'.$title.'</title>'."\n";
        $return .= "\t".'<text x="'.(($width/2)+50).'" y="'.$titleHeight.'" text-anchor="middle" class="graph-title">'.$title.'</text>'."\n";
        return $return;
    }

    protected function __headerDimensions($widthViewBox, $HEIGHT, $heightLegends, $titleHeight, $paddingTop, $paddingLegendX, $lenght, $stepX)
    {
        return array(
            'widthViewBox' => $widthViewBox,
            'heightViewBox' => $HEIGHT+$heightLegends+$titleHeight+2*$paddingTop+$paddingLegendX+intval($this->options['paddingLegendY']),
            'width' => $lenght*$stepX+$stepX,
            'height' => $HEIGHT+$heightLegends+$titleHeight+2*$paddingTop,
        );
    }

    protected function __c($coordonnees, $stroke)
    {
        return "\n\t\t\t".'<circle '.$coordonnees.' r="3" stroke="'.$stroke.'" class="graph-point-active"/>';
    }

    /**
    * Méthode permettant de trier un tableau multidimensionnel sur un ou deux index
    * @param $array array le tableau à trier
    * @param $keys mixed (array ou string) l'index ou le tableau des deux index sur le(s)quel(s) va(vont) se faire le tri
    * 
    * @return array
    * 
    * @author Cyril MAGUIRE
    */
    public function arraySort(&$array, $keys)
    {
        if (!is_array($keys)) {
            $keys = array(0=>$keys);
        }
        $c = count($keys);
        $cmp = function ($a, $b) use ($keys, $c) {
            $i = 0;
            if ($c>1) {
                if (strcasecmp($a[$keys[$i]], $b[$keys[$i]]) == 0) {
                    if (isset($keys[$i+1]) && isset($a[$keys[$i+1]])) {
                        return strcasecmp($a[$keys[$i+1]], $b[$keys[$i+1]]);
                    } else {
                        return strcasecmp($a[$keys[$c-1]], $b[$keys[$c-1]]);
                    }
                } else {
                    return strcasecmp($a[$keys[$i]], $b[$keys[$i]]);
                }
            } else {
                return strcasecmp($a[$keys[0]], $b[$keys[0]]);
            }
        };
        return usort($array, $cmp);
    }

    /**
     * Transform svg file into vml file for internet explorer
     *
     * @param $svg string svg to transform
     * @param $vml string path to vml file
     * @param $root string path of root of app
     * @param $url string url of site
     * @return vml string
     *
     * @author Cyril MAGUIRE
     */
    public function svg2vml($svg, $vml, $root, $xsl='svg2vml/svg2vml.xsl', $xslpath='/svg2vml/')
    {
        if (is_string($svg)) {
            $xsl = str_replace('include href="XSL2', 'include href="'.$xslpath.'XSL2', file_get_contents($xsl));
            # for $xsl, see http://vectorconverter.sourceforge.net/index.html
            $xml_contents=$svg;
            $from="/(<meta[^>]*[^\/]?)>/i";
            $xml_contents=preg_replace($from, "$1/>", $xml_contents);
            $from="/\/(\/>)/i";
            $xml_contents=preg_replace($from, "$1", $xml_contents);
            $xml_contents=preg_replace("/<\!DOCTYPE[^>]+\>/i", "", $xml_contents);
            $xml_contents=preg_replace("/<\?xml-stylesheet[^>]+\>/i", "", $xml_contents);
            $xml_contents=preg_replace("/(\r\n|\n|\r)/s", '', $xml_contents);
            $xml_contents=str_replace(array("\r\n", "\n", "\r", CHR(10), CHR(13)), '', trim($xml_contents));
            $xml_contents=preg_replace("/\<defs\>(\s*)\<style(.*)\<\/style\>(\s*)\<\/defs\>/", "", $xml_contents);
            
            $xh=xslt_create();
            xslt_set_base($xh, SIG_ROOT);
            $arguments=array('/_xml' =>$xml_contents,'/_xsl' => $xsl);
            $result=xslt_process($xh, 'arg:/_xml', 'arg:/_xsl', null, $arguments);
            xslt_free($xh);
            if ($result !== false) {
                $result = str_replace('<?xml version="1.0"?>'."\n", '', $result);
                $result = str_replace('><', ">\n<", $result);
                file_put_contents($root.$vml, $result);
                $output = "<div class=\"object\"><object type=\"text/html\" data=\"".Router::url($vml)."\" >";
                $output .= "</object></div>\n";
                $output = $this->wmodeTransparent($output);
                return $output;
            } else {
                if (Config::$DEBUG == 1) {
                    libxml_use_internal_errors(true);
                    echo '<pre style="border: 1px solid #e3af43; background-color: #f8edd5; padding: 10px;color:#111;white-space: pre;white-space: pre-wrap;word-wrap: break-word;">';
                    print_r(libxml_get_last_error());
                    print_r(libxml_get_errors());
                    echo('</pre>');
                } else {
                    return false;
                }
            }
        } else {
            return L_ERROR_FILE_NOT_FOUND;
        }
    }

    /**
     * Méthode pour prendre en compte le mode transparent des iframes
     *
     * @parm    html    chaine de caractères à scanner
     * @return  string  chaine de caractères modifiée
     * @author  Stephane F
     **/
    public function wmodeTransparent($html)
    {
        if (strpos($html, "<embed src=") !== false) {
            return str_replace('</param><embed', '</param><param name="wmode" value="transparent"></param><embed wmode="transparent" ', $html);
        } elseif (strpos($html, 'feature=oembed') !== false) {
            return str_replace('feature=oembed', 'feature=oembed&amp;wmode=transparent', $html);
        } else {
            return $html;
        }
    }


    /**
     * Put svg file in cache
     *
     * @param $svg string svg to record
     * @param $outputName string name of svg file
     * @param $outputDir string path of cache directory
     * @return bool
     *
     * @author Cyril MAGUIRE
     */
    protected function putInCache($svg, $outputName='svg', $outputDir='data/img/')
    {

        if (!is_dir($outputDir)) {
            mkdir($outputDir);
        }
        // To avoid multiple file in a directory, we delete all (I know it's brutal)
        $files = glob($outputDir.'*.svg');
        foreach ($files as $key => $file) {
            unlink($file);
        }
        // Then, we record only one file
        file_put_contents($outputDir.$outputName.'.svg', $svg);
    }

    # Work in progress......
    public function svgToPng($svg, $outputName='svg', $outputDir='data/img/', $width=800, $height=600)
    {
        // exit();
        // exec("convert -version", $out);//On teste la présence d'imagick sur le serveur
        if (class_exists('Imagick')) {
            $im = new Imagick();
            $imagick->setBackgroundColor(new ImagickPixel('transparent'));
            $im->readImageBlob($svg);
            /*png settings*/
            $im->setImageFormat("png24");
            $im->resizeImage($width, $height, imagick::FILTER_LANCZOS, 1);  /*Optional, if you need to resize*/

            /*jpeg*/
            $im->setImageFormat("jpeg");
            $im->adaptiveResizeImage($width, $height); /*Optional, if you need to resize*/

            $im->writeImage(SIG_ROOT.$outputDir.$outputName.'.png');
            $im->writeImage(SIG_ROOT.$outputDir.$outputName.'.jpg');
            $im->clear();
            $im->destroy();
            echo '<img src="'.$outputDir.$outputName.'.png" alt="'.$outputName.'.png" />';
        } else {
            $return = array();
            // exec(escapeshellcmd('python '.SIG_CORE.'vendors/convertPython/svgtopng --'.$width.' --'.$height.' --o '.SIG_ROOT.$outputDir.$outputName.'.png '.$svg));
        }
    }

    public function javascriptFunctions($CSP_RAND = '', string $token = '',string $convertFile = 'conversion/convert.php'):string
    {
        $javascript = ($CSP_RAND != '' ? '<script nonce="'.$CSP_RAND.'">' : '')."
        
        var links = document.querySelectorAll('a.phpGraph');
        for(var i= 0, nb = links.length; i < nb; i++) {
            var id = links[i].getAttribute('data-exportsvg');
            var token = links[i].getAttribute('data-token');
            if (id != undefined) {
                links[i].addEventListener('click', function(e) {
                    e.preventDefault(); 
                    e.returnValue = false;
                    exportToPng(links[i].href,id,token);
                },false);
            }
        }
        ";
        ob_start();?>
        /**
         * A class to parse color values
         * @author Stoyan Stefanov <sstoo@gmail.com>
         * @link   http://www.phpied.com/rgb-color-parser-in-javascript/
         * @license Use it if you like it
         */
        function RGBColor(color_string)
        {
            this.ok = false;

            // strip any leading #
            if (color_string.charAt(0) == '#') { // remove # if any
                color_string = color_string.substr(1,6);
            }

            color_string = color_string.replace(/ /g,'');
            color_string = color_string.toLowerCase();

            // before getting into regexps, try simple matches
            // and overwrite the input
            var simple_colors = {
                aliceblue: 'f0f8ff',
                antiquewhite: 'faebd7',
                aqua: '00ffff',
                aquamarine: '7fffd4',
                azure: 'f0ffff',
                beige: 'f5f5dc',
                bisque: 'ffe4c4',
                black: '000000',
                blanchedalmond: 'ffebcd',
                blue: '0000ff',
                blueviolet: '8a2be2',
                brown: 'a52a2a',
                burlywood: 'deb887',
                cadetblue: '5f9ea0',
                chartreuse: '7fff00',
                chocolate: 'd2691e',
                coral: 'ff7f50',
                cornflowerblue: '6495ed',
                cornsilk: 'fff8dc',
                crimson: 'dc143c',
                cyan: '00ffff',
                darkblue: '00008b',
                darkcyan: '008b8b',
                darkgoldenrod: 'b8860b',
                darkgray: 'a9a9a9',
                darkgreen: '006400',
                darkkhaki: 'bdb76b',
                darkmagenta: '8b008b',
                darkolivegreen: '556b2f',
                darkorange: 'ff8c00',
                darkorchid: '9932cc',
                darkred: '8b0000',
                darksalmon: 'e9967a',
                darkseagreen: '8fbc8f',
                darkslateblue: '483d8b',
                darkslategray: '2f4f4f',
                darkturquoise: '00ced1',
                darkviolet: '9400d3',
                deeppink: 'ff1493',
                deepskyblue: '00bfff',
                dimgray: '696969',
                dodgerblue: '1e90ff',
                feldspar: 'd19275',
                firebrick: 'b22222',
                floralwhite: 'fffaf0',
                forestgreen: '228b22',
                fuchsia: 'ff00ff',
                gainsboro: 'dcdcdc',
                ghostwhite: 'f8f8ff',
                gold: 'ffd700',
                goldenrod: 'daa520',
                gray: '808080',
                green: '008000',
                greenyellow: 'adff2f',
                honeydew: 'f0fff0',
                hotpink: 'ff69b4',
                indianred : 'cd5c5c',
                indigo : '4b0082',
                ivory: 'fffff0',
                khaki: 'f0e68c',
                lavender: 'e6e6fa',
                lavenderblush: 'fff0f5',
                lawngreen: '7cfc00',
                lemonchiffon: 'fffacd',
                lightblue: 'add8e6',
                lightcoral: 'f08080',
                lightcyan: 'e0ffff',
                lightgoldenrodyellow: 'fafad2',
                lightgrey: 'd3d3d3',
                lightgreen: '90ee90',
                lightpink: 'ffb6c1',
                lightsalmon: 'ffa07a',
                lightseagreen: '20b2aa',
                lightskyblue: '87cefa',
                lightslateblue: '8470ff',
                lightslategray: '778899',
                lightsteelblue: 'b0c4de',
                lightyellow: 'ffffe0',
                lime: '00ff00',
                limegreen: '32cd32',
                linen: 'faf0e6',
                magenta: 'ff00ff',
                maroon: '800000',
                mediumaquamarine: '66cdaa',
                mediumblue: '0000cd',
                mediumorchid: 'ba55d3',
                mediumpurple: '9370d8',
                mediumseagreen: '3cb371',
                mediumslateblue: '7b68ee',
                mediumspringgreen: '00fa9a',
                mediumturquoise: '48d1cc',
                mediumvioletred: 'c71585',
                midnightblue: '191970',
                mintcream: 'f5fffa',
                mistyrose: 'ffe4e1',
                moccasin: 'ffe4b5',
                navajowhite: 'ffdead',
                navy: '000080',
                oldlace: 'fdf5e6',
                olive: '808000',
                olivedrab: '6b8e23',
                orange: 'ffa500',
                orangered: 'ff4500',
                orchid: 'da70d6',
                palegoldenrod: 'eee8aa',
                palegreen: '98fb98',
                paleturquoise: 'afeeee',
                palevioletred: 'd87093',
                papayawhip: 'ffefd5',
                peachpuff: 'ffdab9',
                peru: 'cd853f',
                pink: 'ffc0cb',
                plum: 'dda0dd',
                powderblue: 'b0e0e6',
                purple: '800080',
                red: 'ff0000',
                rosybrown: 'bc8f8f',
                royalblue: '4169e1',
                saddlebrown: '8b4513',
                salmon: 'fa8072',
                sandybrown: 'f4a460',
                seagreen: '2e8b57',
                seashell: 'fff5ee',
                sienna: 'a0522d',
                silver: 'c0c0c0',
                skyblue: '87ceeb',
                slateblue: '6a5acd',
                slategray: '708090',
                snow: 'fffafa',
                springgreen: '00ff7f',
                steelblue: '4682b4',
                tan: 'd2b48c',
                teal: '008080',
                thistle: 'd8bfd8',
                tomato: 'ff6347',
                turquoise: '40e0d0',
                violet: 'ee82ee',
                violetred: 'd02090',
                wheat: 'f5deb3',
                white: 'ffffff',
                whitesmoke: 'f5f5f5',
                yellow: 'ffff00',
                yellowgreen: '9acd32'
            };
            for (var key in simple_colors) {
                if (color_string == key) {
                    color_string = simple_colors[key];
                }
            }
            // emd of simple type-in colors

            // array of color definition objects
            var color_defs = [
                {
                    re: /^rgb\((\d{1,3}),\s*(\d{1,3}),\s*(\d{1,3})\)$/,
                    example: ['rgb(123, 234, 45)', 'rgb(255,234,245)'],
                    process: function (bits){
                        return [
                            parseInt(bits[1]),
                            parseInt(bits[2]),
                            parseInt(bits[3])
                        ];
                    }
                },
                {
                    re: /^(\w{2})(\w{2})(\w{2})$/,
                    example: ['#00ff00', '336699'],
                    process: function (bits){
                        return [
                            parseInt(bits[1], 16),
                            parseInt(bits[2], 16),
                            parseInt(bits[3], 16)
                        ];
                    }
                },
                {
                    re: /^(\w{1})(\w{1})(\w{1})$/,
                    example: ['#fb0', 'f0f'],
                    process: function (bits){
                        return [
                            parseInt(bits[1] + bits[1], 16),
                            parseInt(bits[2] + bits[2], 16),
                            parseInt(bits[3] + bits[3], 16)
                        ];
                    }
                }
            ];

            // search through the definitions to find a match
            for (var i = 0; i < color_defs.length; i++) {
                var re = color_defs[i].re;
                var processor = color_defs[i].process;
                var bits = re.exec(color_string);
                if (bits) {
                    channels = processor(bits);
                    this.r = channels[0];
                    this.g = channels[1];
                    this.b = channels[2];
                    this.ok = true;
                }

            }

            // validate/cleanup values
            this.r = (this.r < 0 || isNaN(this.r)) ? 0 : ((this.r > 255) ? 255 : this.r);
            this.g = (this.g < 0 || isNaN(this.g)) ? 0 : ((this.g > 255) ? 255 : this.g);
            this.b = (this.b < 0 || isNaN(this.b)) ? 0 : ((this.b > 255) ? 255 : this.b);

            // some getters
            this.toRGB = function () {
                return 'rgb(' + this.r + ', ' + this.g + ', ' + this.b + ')';
            };
            this.toHex = function () {
                var r = this.r.toString(16);
                var g = this.g.toString(16);
                var b = this.b.toString(16);
                if (r.length == 1) r = '0' + r;
                if (g.length == 1) g = '0' + g;
                if (b.length == 1) b = '0' + b;
                return '#' + r + g + b;
            };

            // help
            this.getHelpXML = function () {

                var examples = new Array();
                // add regexps
                for (var i = 0; i < color_defs.length; i++) {
                    var example = color_defs[i].example;
                    for (var j = 0; j < example.length; j++) {
                        examples[examples.length] = example[j];
                    }
                }
                // add type-in colors
                for (var sc in simple_colors) {
                    examples[examples.length] = sc;
                }

                var xml = document.createElement('ul');
                xml.setAttribute('id', 'rgbcolor-examples');
                for (var i = 0; i < examples.length; i++) {
                    try {
                        var list_item = document.createElement('li');
                        var list_color = new RGBColor(examples[i]);
                        var example_div = document.createElement('div');
                        example_div.style.cssText =
                                'margin: 3px; '
                                + 'border: 1px solid black; '
                                + 'background:' + list_color.toHex() + '; '
                                + 'color:' + list_color.toHex()
                        ;
                        example_div.appendChild(document.createTextNode('test'));
                        var list_item_value = document.createTextNode(
                            ' ' + examples[i] + ' -> ' + list_color.toRGB() + ' -> ' + list_color.toHex()
                        );
                        list_item.appendChild(example_div);
                        list_item.appendChild(list_item_value);
                        xml.appendChild(list_item);

                    } catch(e){}
                }
                return xml;

            }

        }

        /*
         * canvg.js - Javascript SVG parser and renderer on Canvas
         * MIT Licensed 
         * Gabe Lerner (gabelerner@gmail.com)
         * http://code.google.com/p/canvg/
         *
         * Requires: rgbcolor.js - http://www.phpied.com/rgb-color-parser-in-javascript/
         */
        if(!window.console) {
            window.console = {};
            window.console.log = function(str) {};
            window.console.dir = function(str) {};
        }

        // <3 IE
        if(!Array.indexOf){
            Array.prototype.indexOf = function(obj){
                for(var i=0; i < this.length; i++){
                    if(this[i]==obj){
                        return i;
                    }
                }
                return -1;
            }
        }

        (function(){
            // canvg(target, s)
            // target: canvas element or the id of a canvas element
            // s: svg string or url to svg file
            // opts: optional hash of options
            //       ignoreMouse: true => ignore mouse events
            //       ignoreAnimation: true => ignore animations
            //       renderCallback: function => will call the function after the first render is completed
            //       forceRedraw: function => will call the function on every frame, if it returns true, will redraw
            this.canvg = function (target, s, opts) {
                if (typeof target == 'string') {
                    target = document.getElementById(target);
                }
                
                // reuse class per canvas
                var svg;
                if (target.svg == null) {
                    svg = build();
                    target.svg = svg;
                }
                else {
                    svg = target.svg;
                    svg.stop();
                }
                svg.opts = opts;
                
                var ctx = target.getContext('2d');
                if (s.substr(0,1) == '<') {
                    // load from xml string
                    svg.loadXml(ctx, s);
                }
                else {
                    // load from url
                    svg.load(ctx, s);
                }
            };

            function build() {
                var svg = { };
                
                svg.FRAMERATE = 30;
                
                // globals
                svg.init = function(ctx) {
                    svg.Definitions = {};
                    svg.Styles = {};
                    svg.Animations = [];
                    svg.Images = [];
                    svg.ctx = ctx;
                    svg.ViewPort = new (function () {
                        this.viewPorts = [];
                        this.SetCurrent = function(width, height) { this.viewPorts.push({ width: width, height: height }); };
                        this.RemoveCurrent = function() { this.viewPorts.pop(); };
                        this.Current = function() { return this.viewPorts[this.viewPorts.length - 1]; };
                        this.width = function() { return this.Current().width; };
                        this.height = function() { return this.Current().height; };
                        this.ComputeSize = function(d) {
                            if (d != null && typeof(d) == 'number') return d;
                            if (d == 'x') return this.width();
                            if (d == 'y') return this.height();
                            return Math.sqrt(Math.pow(this.width(), 2) + Math.pow(this.height(), 2)) / Math.sqrt(2);            
                        }
                    });
                };
                svg.init();
                
                // images loaded
                svg.ImagesLoaded = function() { 
                    for (var i=0; i < svg.Images.length; i++) {
                        if (!svg.Images[i].loaded) return false;
                    }
                    return true;
                };

                // trim
                svg.trim = function(s) { return s.replace(/^\s+|\s+$/g, ''); };
                
                // compress spaces
                svg.compressSpaces = function(s) { return s.replace(/[\s\r\t\n]+/gm,' '); };
                
                // ajax
                svg.ajax = function(url) {
                    var AJAX;
                    if(window.XMLHttpRequest){AJAX=new XMLHttpRequest();}
                    else{AJAX=new ActiveXObject('Microsoft.XMLHTTP');}
                    if(AJAX){
                       AJAX.open('GET',url,false);
                       AJAX.send(null);
                       return AJAX.responseText;
                    }
                    return null;
                }; 
                
                // parse xml
                svg.parseXml = function(xml) {
                    if (window.DOMParser)
                    {
                        var parser = new DOMParser();
                        return parser.parseFromString(xml, 'text/xml');
                    }
                    else 
                    {
                        xml = xml.replace(/<!DOCTYPE svg[^>]*>/, '');
                        var xmlDoc = new ActiveXObject('Microsoft.XMLDOM');
                        xmlDoc.async = 'false';
                        xmlDoc.loadXML(xml); 
                        return xmlDoc;
                    }       
                };
                
                svg.Property = function(name, value) {
                    this.name = name;
                    this.value = value;
                    
                    this.hasValue = function() {
                        return (this.value != null && this.value != '');
                    };
                                    
                    // return the numerical value of the property
                    this.numValue = function() {
                        if (!this.hasValue()) return 0;
                        
                        var n = parseFloat(this.value);
                        if ((this.value + '').match(/%$/)) {
                            n = n / 100.0;
                        }
                        return n;
                    };
                    
                    this.valueOrDefault = function(def) {
                        if (this.hasValue()) return this.value;
                        return def;
                    };
                    
                    this.numValueOrDefault = function(def) {
                        if (this.hasValue()) return this.numValue();
                        return def;
                    };
                    
                    /* EXTENSIONS */
                    var that = this;
                    
                    // color extensions
                    this.Color = {
                        // augment the current color value with the opacity
                        addOpacity: function(opacity) {
                            var newValue = that.value;
                            if (opacity != null && opacity != '') {
                                var color = new RGBColor(that.value);
                                if (color.ok) {
                                    newValue = 'rgba(' + color.r + ', ' + color.g + ', ' + color.b + ', ' + opacity + ')';
                                }
                            }
                            return new svg.Property(that.name, newValue);
                        }
                    };
                    
                    // definition extensions
                    this.Definition = {
                        // get the definition from the definitions table
                        getDefinition: function() {
                            var name = that.value.replace(/^(url\()?#([^\)]+)\)?$/, '$2');
                            return svg.Definitions[name];
                        },
                        
                        isUrl: function() {
                            return that.value.indexOf('url(') == 0
                        },
                        
                        getFillStyle: function(e) {
                            var def = this.getDefinition();
                            
                            // gradient
                            if (def != null && def.createGradient) {
                                return def.createGradient(svg.ctx, e);
                            }
                            
                            // pattern
                            if (def != null && def.createPattern) {
                                return def.createPattern(svg.ctx, e);
                            }
                            
                            return null;
                        }
                    };
                    
                    // length extensions
                    this.Length = {
                        DPI: function(viewPort) {
                            return 96.0; // TODO: compute?
                        },
                        
                        EM: function(viewPort) {
                            var em = 12;
                            
                            var fontSize = new svg.Property('fontSize', svg.Font.Parse(svg.ctx.font).fontSize);
                            if (fontSize.hasValue()) em = fontSize.Length.toPixels(viewPort);
                            
                            return em;
                        },
                    
                        // get the length as pixels
                        toPixels: function(viewPort) {
                            if (!that.hasValue()) return 0;
                            var s = that.value+'';
                            if (s.match(/em$/)) return that.numValue() * this.EM(viewPort);
                            if (s.match(/ex$/)) return that.numValue() * this.EM(viewPort) / 2.0;
                            if (s.match(/px$/)) return that.numValue();
                            if (s.match(/pt$/)) return that.numValue() * 1.25;
                            if (s.match(/pc$/)) return that.numValue() * 15;
                            if (s.match(/cm$/)) return that.numValue() * this.DPI(viewPort) / 2.54;
                            if (s.match(/mm$/)) return that.numValue() * this.DPI(viewPort) / 25.4;
                            if (s.match(/in$/)) return that.numValue() * this.DPI(viewPort);
                            if (s.match(/%$/)) return that.numValue() * svg.ViewPort.ComputeSize(viewPort);
                            return that.numValue();
                        }
                    };
                    
                    // time extensions
                    this.Time = {
                        // get the time as milliseconds
                        toMilliseconds: function() {
                            if (!that.hasValue()) return 0;
                            var s = that.value+'';
                            if (s.match(/s$/)) return that.numValue() * 1000;
                            if (s.match(/ms$/)) return that.numValue();
                            return that.numValue();
                        }
                    };
                    
                    // angle extensions
                    this.Angle = {
                        // get the angle as radians
                        toRadians: function() {
                            if (!that.hasValue()) return 0;
                            var s = that.value+'';
                            if (s.match(/deg$/)) return that.numValue() * (Math.PI / 180.0);
                            if (s.match(/grad$/)) return that.numValue() * (Math.PI / 200.0);
                            if (s.match(/rad$/)) return that.numValue();
                            return that.numValue() * (Math.PI / 180.0);
                        }
                    }
                };
                
                // fonts
                svg.Font = new (function() {
                    this.Styles = ['normal','italic','oblique','inherit'];
                    this.Variants = ['normal','small-caps','inherit'];
                    this.Weights = ['normal','bold','bolder','lighter','100','200','300','400','500','600','700','800','900','inherit'];
                    
                    this.CreateFont = function(fontStyle, fontVariant, fontWeight, fontSize, fontFamily, inherit) { 
                        var f = inherit != null ? this.Parse(inherit) : this.CreateFont('', '', '', '', '', svg.ctx.font);
                        return { 
                            fontFamily: fontFamily || f.fontFamily, 
                            fontSize: fontSize || f.fontSize, 
                            fontStyle: fontStyle || f.fontStyle, 
                            fontWeight: fontWeight || f.fontWeight, 
                            fontVariant: fontVariant || f.fontVariant,
                            toString: function () { return [this.fontStyle, this.fontVariant, this.fontWeight, this.fontSize, this.fontFamily].join(' ') } 
                        } 
                    };
                    
                    var that = this;
                    this.Parse = function(s) {
                        var f = {};
                        var d = svg.trim(svg.compressSpaces(s || '')).split(' ');
                        var set = { fontSize: false, fontStyle: false, fontWeight: false, fontVariant: false };
                        var ff = '';
                        for (var i=0; i < d.length; i++) {
                            if (!set.fontStyle && that.Styles.indexOf(d[i]) != -1) { if (d[i] != 'inherit') f.fontStyle = d[i]; set.fontStyle = true; }
                            else if (!set.fontVariant && that.Variants.indexOf(d[i]) != -1) { if (d[i] != 'inherit') f.fontVariant = d[i]; set.fontStyle = set.fontVariant = true;  }
                            else if (!set.fontWeight && that.Weights.indexOf(d[i]) != -1) { if (d[i] != 'inherit') f.fontWeight = d[i]; set.fontStyle = set.fontVariant = set.fontWeight = true; }
                            else if (!set.fontSize) { if (d[i] != 'inherit') f.fontSize = d[i].split('/')[0]; set.fontStyle = set.fontVariant = set.fontWeight = set.fontSize = true; }
                            else { if (d[i] != 'inherit') ff += d[i]; }
                        } if (ff != '') f.fontFamily = ff;
                        return f;
                    }
                });
                
                // points and paths
                svg.ToNumberArray = function(s) {
                    var a = svg.trim(svg.compressSpaces((s || '').replace(/,/g, ' '))).split(' ');
                    for (var i=0; i < a.length; i++) {
                        a[i] = parseFloat(a[i]);
                    }
                    return a;
                };      
                svg.Point = function(x, y) {
                    this.x = x;
                    this.y = y;
                    
                    this.angleTo = function(p) {
                        return Math.atan2(p.y - this.y, p.x - this.x);
                    };
                    
                    this.applyTransform = function(v) {
                        var xp = this.x * v[0] + this.y * v[2] + v[4];
                        var yp = this.x * v[1] + this.y * v[3] + v[5];
                        this.x = xp;
                        this.y = yp;
                    }
                };
                svg.CreatePoint = function(s) {
                    var a = svg.ToNumberArray(s);
                    return new svg.Point(a[0], a[1]);
                };
                svg.CreatePath = function(s) {
                    var a = svg.ToNumberArray(s);
                    var path = [];
                    for (var i=0; i < a.length; i+=2) {
                        path.push(new svg.Point(a[i], a[i+1]));
                    }
                    return path;
                };
                
                // bounding box
                svg.BoundingBox = function(x1, y1, x2, y2) { // pass in initial points if you want
                    this.x1 = Number.NaN;
                    this.y1 = Number.NaN;
                    this.x2 = Number.NaN;
                    this.y2 = Number.NaN;
                    
                    this.x = function() { return this.x1; };
                    this.y = function() { return this.y1; };
                    this.width = function() { return this.x2 - this.x1; };
                    this.height = function() { return this.y2 - this.y1; };
                    
                    this.addPoint = function(x, y) {    
                        if (x != null) {
                            if (isNaN(this.x1) || isNaN(this.x2)) {
                                this.x1 = x;
                                this.x2 = x;
                            }
                            if (x < this.x1) this.x1 = x;
                            if (x > this.x2) this.x2 = x;
                        }
                    
                        if (y != null) {
                            if (isNaN(this.y1) || isNaN(this.y2)) {
                                this.y1 = y;
                                this.y2 = y;
                            }
                            if (y < this.y1) this.y1 = y;
                            if (y > this.y2) this.y2 = y;
                        }
                    };          
                    this.addX = function(x) { this.addPoint(x, null); };
                    this.addY = function(y) { this.addPoint(null, y); };
                    
                    this.addQuadraticCurve = function(p0x, p0y, p1x, p1y, p2x, p2y) {
                        var cp1x = p0x + 2/3 * (p1x - p0x); // CP1 = QP0 + 2/3 *(QP1-QP0)
                        var cp1y = p0y + 2/3 * (p1y - p0y); // CP1 = QP0 + 2/3 *(QP1-QP0)
                        var cp2x = cp1x + 1/3 * (p2x - p0x); // CP2 = CP1 + 1/3 *(QP2-QP0)
                        var cp2y = cp1y + 1/3 * (p2y - p0y); // CP2 = CP1 + 1/3 *(QP2-QP0)
                        this.addBezierCurve(p0x, p0y, cp1x, cp2x, cp1y, cp2y, p2x, p2y);
                    };
                    
                    this.addBezierCurve = function(p0x, p0y, p1x, p1y, p2x, p2y, p3x, p3y) {
                        // from http://blog.hackers-cafe.net/2009/06/how-to-calculate-bezier-curves-bounding.html
                        var p0 = [p0x, p0y], p1 = [p1x, p1y], p2 = [p2x, p2y], p3 = [p3x, p3y];
                        this.addPoint(p0[0], p0[1]);
                        this.addPoint(p3[0], p3[1]);
                        
                        for (i=0; i <= 1; i++) {
                            var f = function(t) { 
                                return Math.pow(1-t, 3) * p0[i]
                                + 3 * Math.pow(1-t, 2) * t * p1[i]
                                + 3 * (1-t) * Math.pow(t, 2) * p2[i]
                                + Math.pow(t, 3) * p3[i];
                            };
                            
                            var b = 6 * p0[i] - 12 * p1[i] + 6 * p2[i];
                            var a = -3 * p0[i] + 9 * p1[i] - 9 * p2[i] + 3 * p3[i];
                            var c = 3 * p1[i] - 3 * p0[i];
                            
                            if (a == 0) {
                                if (b == 0) continue;
                                var t = -c / b;
                                if (0 < t && t < 1) {
                                    if (i == 0) this.addX(f(t));
                                    if (i == 1) this.addY(f(t));
                                }
                                continue;
                            }
                            
                            var b2ac = Math.pow(b, 2) - 4 * c * a;
                            if (b2ac < 0) continue;
                            var t1 = (-b + Math.sqrt(b2ac)) / (2 * a);
                            if (0 < t1 && t1 < 1) {
                                if (i == 0) this.addX(f(t1));
                                if (i == 1) this.addY(f(t1));
                            }
                            var t2 = (-b - Math.sqrt(b2ac)) / (2 * a);
                            if (0 < t2 && t2 < 1) {
                                if (i == 0) this.addX(f(t2));
                                if (i == 1) this.addY(f(t2));
                            }
                        }
                    };
                    
                    this.isPointInBox = function(x, y) {
                        return (this.x1 <= x && x <= this.x2 && this.y1 <= y && y <= this.y2);
                    };
                    
                    this.addPoint(x1, y1);
                    this.addPoint(x2, y2);
                };
                
                // transforms
                svg.Transform = function(v) {   
                    var that = this;
                    this.Type = {};
                
                    // translate
                    this.Type.translate = function(s) {
                        this.p = svg.CreatePoint(s);            
                        this.apply = function(ctx) {
                            ctx.translate(this.p.x || 0.0, this.p.y || 0.0);
                        };
                        this.applyToPoint = function(p) {
                            p.applyTransform([1, 0, 0, 1, this.p.x || 0.0, this.p.y || 0.0]);
                        }
                    };
                    
                    // rotate
                    this.Type.rotate = function(s) {
                        var a = svg.ToNumberArray(s);
                        this.angle = new svg.Property('angle', a[0]);
                        this.cx = a[1] || 0;
                        this.cy = a[2] || 0;
                        this.apply = function(ctx) {
                            ctx.translate(this.cx, this.cy);
                            ctx.rotate(this.angle.Angle.toRadians());
                            ctx.translate(-this.cx, -this.cy);
                        };
                        this.applyToPoint = function(p) {
                            var a = this.angle.Angle.toRadians();
                            p.applyTransform([1, 0, 0, 1, this.p.x || 0.0, this.p.y || 0.0]);
                            p.applyTransform([Math.cos(a), Math.sin(a), -Math.sin(a), Math.cos(a), 0, 0]);
                            p.applyTransform([1, 0, 0, 1, -this.p.x || 0.0, -this.p.y || 0.0]);
                        }           
                    };
                    
                    this.Type.scale = function(s) {
                        this.p = svg.CreatePoint(s);
                        this.apply = function(ctx) {
                            ctx.scale(this.p.x || 1.0, this.p.y || this.p.x || 1.0);
                        };
                        this.applyToPoint = function(p) {
                            p.applyTransform([this.p.x || 0.0, 0, 0, this.p.y || 0.0, 0, 0]);
                        }               
                    };
                    
                    this.Type.matrix = function(s) {
                        this.m = svg.ToNumberArray(s);
                        this.apply = function(ctx) {
                            ctx.transform(this.m[0], this.m[1], this.m[2], this.m[3], this.m[4], this.m[5]);
                        };
                        this.applyToPoint = function(p) {
                            p.applyTransform(this.m);
                        }                   
                    };
                    
                    this.Type.SkewBase = function(s) {
                        this.base = that.Type.matrix;
                        this.base(s);
                        this.angle = new svg.Property('angle', s);
                    };
                    this.Type.SkewBase.prototype = new this.Type.matrix;
                    
                    this.Type.skewX = function(s) {
                        this.base = that.Type.SkewBase;
                        this.base(s);
                        this.m = [1, 0, Math.tan(this.angle.Angle.toRadians()), 1, 0, 0];
                    };
                    this.Type.skewX.prototype = new this.Type.SkewBase;
                    
                    this.Type.skewY = function(s) {
                        this.base = that.Type.SkewBase;
                        this.base(s);
                        this.m = [1, Math.tan(this.angle.Angle.toRadians()), 0, 1, 0, 0];
                    };
                    this.Type.skewY.prototype = new this.Type.SkewBase;
                
                    this.transforms = [];
                    
                    this.apply = function(ctx) {
                        for (var i=0; i < this.transforms.length; i++) {
                            this.transforms[i].apply(ctx);
                        }
                    };
                    
                    this.applyToPoint = function(p) {
                        for (var i=0; i < this.transforms.length; i++) {
                            this.transforms[i].applyToPoint(p);
                        }
                    };
                    
                    var data = v.split(/\s(?=[a-z])/);
                    for (var i=0; i < data.length; i++) {
                        var type = data[i].split('(')[0];
                        var s = data[i].split('(')[1].replace(')','');
                        var transform = eval('new this.Type.' + type + '(s)');
                        this.transforms.push(transform);
                    }
                };
                
                // aspect ratio
                svg.AspectRatio = function(ctx, aspectRatio, width, desiredWidth, height, desiredHeight, minX, minY, refX, refY) {
                    // aspect ratio - http://www.w3.org/TR/SVG/coords.html#PreserveAspectRatioAttribute
                    aspectRatio = svg.compressSpaces(aspectRatio);
                    aspectRatio = aspectRatio.replace(/^defer\s/,''); // ignore defer
                    var align = aspectRatio.split(' ')[0] || 'xMidYMid';
                    var meetOrSlice = aspectRatio.split(' ')[1] || 'meet';                  
            
                    // calculate scale
                    var scaleX = width / desiredWidth;
                    var scaleY = height / desiredHeight;
                    var scaleMin = Math.min(scaleX, scaleY);
                    var scaleMax = Math.max(scaleX, scaleY);
                    if (meetOrSlice == 'meet') { desiredWidth *= scaleMin; desiredHeight *= scaleMin; }
                    if (meetOrSlice == 'slice') { desiredWidth *= scaleMax; desiredHeight *= scaleMax; }    
                    
                    refX = new svg.Property('refX', refX);
                    refY = new svg.Property('refY', refY);
                    if (refX.hasValue() && refY.hasValue()) {               
                        ctx.translate(-scaleMin * refX.Length.toPixels('x'), -scaleMin * refY.Length.toPixels('y'));
                    } 
                    else {                  
                        // align
                        if (align.match(/^xMid/) && ((meetOrSlice == 'meet' && scaleMin == scaleY) || (meetOrSlice == 'slice' && scaleMax == scaleY))) ctx.translate(width / 2.0 - desiredWidth / 2.0, 0); 
                        if (align.match(/YMid$/) && ((meetOrSlice == 'meet' && scaleMin == scaleX) || (meetOrSlice == 'slice' && scaleMax == scaleX))) ctx.translate(0, height / 2.0 - desiredHeight / 2.0); 
                        if (align.match(/^xMax/) && ((meetOrSlice == 'meet' && scaleMin == scaleY) || (meetOrSlice == 'slice' && scaleMax == scaleY))) ctx.translate(width - desiredWidth, 0); 
                        if (align.match(/YMax$/) && ((meetOrSlice == 'meet' && scaleMin == scaleX) || (meetOrSlice == 'slice' && scaleMax == scaleX))) ctx.translate(0, height - desiredHeight); 
                    }
                    
                    // scale
                    if (meetOrSlice == 'meet') ctx.scale(scaleMin, scaleMin); 
                    if (meetOrSlice == 'slice') ctx.scale(scaleMax, scaleMax);  
                    
                    // translate
                    ctx.translate(minX == null ? 0 : -minX, minY == null ? 0 : -minY);          
                };
                
                // elements
                svg.Element = {};
                
                svg.Element.ElementBase = function(node) {  
                    this.attributes = {};
                    this.styles = {};
                    this.children = [];
                    
                    // get or create attribute
                    this.attribute = function(name, createIfNotExists) {
                        var a = this.attributes[name];
                        if (a != null) return a;
                                    
                        a = new svg.Property(name, '');
                        if (createIfNotExists == true) this.attributes[name] = a;
                        return a;
                    };
                    
                    // get or create style
                    this.style = function(name, createIfNotExists) {
                        var s = this.styles[name];
                        if (s != null) return s;
                        
                        var a = this.attribute(name);
                        if (a != null && a.hasValue()) {
                            return a;
                        }
                            
                        s = new svg.Property(name, '');
                        if (createIfNotExists == true) this.styles[name] = s;
                        return s;
                    };
                    
                    // base render
                    this.render = function(ctx) {
                        // don't render display=none
                        if (this.attribute('display').value == 'none') return;
                    
                        ctx.save();
                        this.setContext(ctx);
                        this.renderChildren(ctx);
                        this.clearContext(ctx);
                        ctx.restore();
                    };
                    
                    // base set context
                    this.setContext = function(ctx) {
                        // OVERRIDE ME!
                    };
                    
                    // base clear context
                    this.clearContext = function(ctx) {
                        // OVERRIDE ME!
                    };          
                    
                    // base render children
                    this.renderChildren = function(ctx) {
                        for (var i=0; i < this.children.length; i++) {
                            this.children[i].render(ctx);
                        }
                    };
                    
                    this.addChild = function(childNode, create) {
                        var child = childNode;
                        if (create) child = svg.CreateElement(childNode);
                        child.parent = this;
                        this.children.push(child);          
                    };
                        
                    if (node != null && node.nodeType == 1) { //ELEMENT_NODE
                        // add children
                        for (var i=0; i < node.childNodes.length; i++) {
                            var childNode = node.childNodes[i];
                            if (childNode.nodeType == 1) this.addChild(childNode, true); //ELEMENT_NODE
                        }
                        
                        // add attributes
                        for (var i=0; i < node.attributes.length; i++) {
                            var attribute = node.attributes[i];
                            this.attributes[attribute.nodeName] = new svg.Property(attribute.nodeName, attribute.nodeValue);
                        }
                                                
                        // add tag styles
                        var styles = svg.Styles[this.type];
                        if (styles != null) {
                            for (var name in styles) {
                                this.styles[name] = styles[name];
                            }
                        }                   
                        
                        // add class styles
                        if (this.attribute('class').hasValue()) {
                            var classes = svg.compressSpaces(this.attribute('class').value).split(' ');
                            for (var j=0; j < classes.length; j++) {
                                styles = svg.Styles['.'+classes[j]];
                                if (styles != null) {
                                    for (var name in styles) {
                                        this.styles[name] = styles[name];
                                    }
                                }
                            }
                        }
                        
                        // add inline styles
                        if (this.attribute('style').hasValue()) {
                            var styles = this.attribute('style').value.split(';');
                            for (var i=0; i < styles.length; i++) {
                                if (svg.trim(styles[i]) != '') {
                                    var style = styles[i].split(':');
                                    var name = svg.trim(style[0]);
                                    var value = svg.trim(style[1]);
                                    this.styles[name] = new svg.Property(name, value);
                                }
                            }
                        }   

                        // add id
                        if (this.attribute('id').hasValue()) {
                            if (svg.Definitions[this.attribute('id').value] == null) {
                                svg.Definitions[this.attribute('id').value] = this;
                            }
                        }
                    }
                };
                
                svg.Element.RenderedElementBase = function(node) {
                    this.base = svg.Element.ElementBase;
                    this.base(node);
                    
                    this.setContext = function(ctx) {
                        // fill
                        if (this.style('fill').Definition.isUrl()) {
                            var fs = this.style('fill').Definition.getFillStyle(this);
                            if (fs != null) ctx.fillStyle = fs;
                        }
                        else if (this.style('fill').hasValue()) {
                            var fillStyle = this.style('fill');
                            if (this.style('fill-opacity').hasValue()) fillStyle = fillStyle.Color.addOpacity(this.style('fill-opacity').value);
                            ctx.fillStyle = (fillStyle.value == 'none' ? 'rgba(0,0,0,0)' : fillStyle.value);
                        }
                                            
                        // stroke
                        if (this.style('stroke').Definition.isUrl()) {
                            var fs = this.style('stroke').Definition.getFillStyle(this);
                            if (fs != null) ctx.strokeStyle = fs;
                        }
                        else if (this.style('stroke').hasValue()) {
                            var strokeStyle = this.style('stroke');
                            if (this.style('stroke-opacity').hasValue()) strokeStyle = strokeStyle.Color.addOpacity(this.style('stroke-opacity').value);
                            ctx.strokeStyle = (strokeStyle.value == 'none' ? 'rgba(0,0,0,0)' : strokeStyle.value);
                        }
                        if (this.style('stroke-width').hasValue()) ctx.lineWidth = this.style('stroke-width').Length.toPixels();
                        if (this.style('stroke-linecap').hasValue()) ctx.lineCap = this.style('stroke-linecap').value;
                        if (this.style('stroke-linejoin').hasValue()) ctx.lineJoin = this.style('stroke-linejoin').value;
                        if (this.style('stroke-miterlimit').hasValue()) ctx.miterLimit = this.style('stroke-miterlimit').value;

                        // font
                        if (typeof(ctx.font) != 'undefined') {
                            ctx.font = svg.Font.CreateFont( 
                                this.style('font-style').value, 
                                this.style('font-variant').value, 
                                this.style('font-weight').value, 
                                this.style('font-size').hasValue() ? this.style('font-size').Length.toPixels() + 'px' : '', 
                                this.style('font-family').value).toString();
                        }
                        
                        // transform
                        if (this.attribute('transform').hasValue()) { 
                            var transform = new svg.Transform(this.attribute('transform').value);
                            transform.apply(ctx);
                        }
                        
                        // clip
                        if (this.attribute('clip-path').hasValue()) {
                            var clip = this.attribute('clip-path').Definition.getDefinition();
                            if (clip != null) clip.apply(ctx);
                        }
                        
                        // opacity
                        if (this.style('opacity').hasValue()) {
                            ctx.globalAlpha = this.style('opacity').numValue();
                        }
                    }       
                };
                svg.Element.RenderedElementBase.prototype = new svg.Element.ElementBase;
                
                svg.Element.PathElementBase = function(node) {
                    this.base = svg.Element.RenderedElementBase;
                    this.base(node);
                    
                    this.path = function(ctx) {
                        if (ctx != null) ctx.beginPath();
                        return new svg.BoundingBox();
                    };
                    
                    this.renderChildren = function(ctx) {
                        this.path(ctx);
                        svg.Mouse.checkPath(this, ctx);
                        if (ctx.fillStyle != '') ctx.fill();
                        if (ctx.strokeStyle != '') ctx.stroke();
                        
                        var markers = this.getMarkers();
                        if (markers != null) {
                            if (this.attribute('marker-start').Definition.isUrl()) {
                                var marker = this.attribute('marker-start').Definition.getDefinition();
                                marker.render(ctx, markers[0][0], markers[0][1]);
                            }
                            if (this.attribute('marker-mid').Definition.isUrl()) {
                                var marker = this.attribute('marker-mid').Definition.getDefinition();
                                for (var i=1;i < markers.length-1;i++) {
                                    marker.render(ctx, markers[i][0], markers[i][1]);
                                }
                            }
                            if (this.attribute('marker-end').Definition.isUrl()) {
                                var marker = this.attribute('marker-end').Definition.getDefinition();
                                marker.render(ctx, markers[markers.length-1][0], markers[markers.length-1][1]);
                            }
                        }                   
                    };
                    
                    this.getBoundingBox = function() {
                        return this.path();
                    };
                    
                    this.getMarkers = function() {
                        return null;
                    }
                };
                svg.Element.PathElementBase.prototype = new svg.Element.RenderedElementBase;
                
                // svg element
                svg.Element.svg = function(node) {
                    this.base = svg.Element.RenderedElementBase;
                    this.base(node);
                    
                    this.baseClearContext = this.clearContext;
                    this.clearContext = function(ctx) {
                        this.baseClearContext(ctx);
                        svg.ViewPort.RemoveCurrent();
                    };
                    
                    this.baseSetContext = this.setContext;
                    this.setContext = function(ctx) {
                        this.baseSetContext(ctx);
                        
                        // create new view port
                        if (this.attribute('x').hasValue() && this.attribute('y').hasValue()) {
                            ctx.translate(this.attribute('x').Length.toPixels('x'), this.attribute('y').Length.toPixels('y'));
                        }
                        
                        var width = svg.ViewPort.width();
                        var height = svg.ViewPort.height();
                        if (this.attribute('width').hasValue() && this.attribute('height').hasValue()) {
                            width = this.attribute('width').Length.toPixels('x');
                            height = this.attribute('height').Length.toPixels('y');
                            
                            var x = 0;
                            var y = 0;
                            if (this.attribute('refX').hasValue() && this.attribute('refY').hasValue()) {
                                x = -this.attribute('refX').Length.toPixels('x');
                                y = -this.attribute('refY').Length.toPixels('y');
                            }
                            
                            ctx.beginPath();
                            ctx.moveTo(x, y);
                            ctx.lineTo(width, y);
                            ctx.lineTo(width, height);
                            ctx.lineTo(x, height);
                            ctx.closePath();
                            ctx.clip();
                        }
                        svg.ViewPort.SetCurrent(width, height); 
                                
                        // viewbox
                        if (this.attribute('viewBox').hasValue()) {             
                            var viewBox = svg.ToNumberArray(this.attribute('viewBox').value);
                            var minX = viewBox[0];
                            var minY = viewBox[1];
                            width = viewBox[2];
                            height = viewBox[3];
                            
                            svg.AspectRatio(ctx,
                                            this.attribute('preserveAspectRatio').value, 
                                            svg.ViewPort.width(), 
                                            width,
                                            svg.ViewPort.height(),
                                            height,
                                            minX,
                                            minY,
                                            this.attribute('refX').value,
                                            this.attribute('refY').value);
                                                
                            svg.ViewPort.RemoveCurrent();   
                            svg.ViewPort.SetCurrent(viewBox[2], viewBox[3]);                        
                        }               
                        
                        // initial values
                        ctx.strokeStyle = 'rgba(0,0,0,0)';
                        ctx.lineCap = 'butt';
                        ctx.lineJoin = 'miter';
                        ctx.miterLimit = 4;
                    }
                };
                svg.Element.svg.prototype = new svg.Element.RenderedElementBase;

                // rect element
                svg.Element.rect = function(node) {
                    this.base = svg.Element.PathElementBase;
                    this.base(node);
                    
                    this.path = function(ctx) {
                        var x = this.attribute('x').Length.toPixels('x');
                        var y = this.attribute('y').Length.toPixels('y');
                        var width = this.attribute('width').Length.toPixels('x');
                        var height = this.attribute('height').Length.toPixels('y');
                        var rx = this.attribute('rx').Length.toPixels('x');
                        var ry = this.attribute('ry').Length.toPixels('y');
                        if (this.attribute('rx').hasValue() && !this.attribute('ry').hasValue()) ry = rx;
                        if (this.attribute('ry').hasValue() && !this.attribute('rx').hasValue()) rx = ry;
                        
                        if (ctx != null) {
                            ctx.beginPath();
                            ctx.moveTo(x + rx, y);
                            ctx.lineTo(x + width - rx, y);
                            ctx.quadraticCurveTo(x + width, y, x + width, y + ry);
                            ctx.lineTo(x + width, y + height - ry);
                            ctx.quadraticCurveTo(x + width, y + height, x + width - rx, y + height);
                            ctx.lineTo(x + rx, y + height);
                            ctx.quadraticCurveTo(x, y + height, x, y + height - ry);
                            ctx.lineTo(x, y + ry);
                            ctx.quadraticCurveTo(x, y, x + rx, y);
                            ctx.closePath();
                        }
                        
                        return new svg.BoundingBox(x, y, x + width, y + height);
                    }
                };
                svg.Element.rect.prototype = new svg.Element.PathElementBase;
                
                // circle element
                svg.Element.circle = function(node) {
                    this.base = svg.Element.PathElementBase;
                    this.base(node);
                    
                    this.path = function(ctx) {
                        var cx = this.attribute('cx').Length.toPixels('x');
                        var cy = this.attribute('cy').Length.toPixels('y');
                        var r = this.attribute('r').Length.toPixels();
                    
                        if (ctx != null) {
                            ctx.beginPath();
                            ctx.arc(cx, cy, r, 0, Math.PI * 2, true); 
                            ctx.closePath();
                        }
                        
                        return new svg.BoundingBox(cx - r, cy - r, cx + r, cy + r);
                    }
                };
                svg.Element.circle.prototype = new svg.Element.PathElementBase; 

                // ellipse element
                svg.Element.ellipse = function(node) {
                    this.base = svg.Element.PathElementBase;
                    this.base(node);
                    
                    this.path = function(ctx) {
                        var KAPPA = 4 * ((Math.sqrt(2) - 1) / 3);
                        var rx = this.attribute('rx').Length.toPixels('x');
                        var ry = this.attribute('ry').Length.toPixels('y');
                        var cx = this.attribute('cx').Length.toPixels('x');
                        var cy = this.attribute('cy').Length.toPixels('y');
                        
                        if (ctx != null) {
                            ctx.beginPath();
                            ctx.moveTo(cx, cy - ry);
                            ctx.bezierCurveTo(cx + (KAPPA * rx), cy - ry,  cx + rx, cy - (KAPPA * ry), cx + rx, cy);
                            ctx.bezierCurveTo(cx + rx, cy + (KAPPA * ry), cx + (KAPPA * rx), cy + ry, cx, cy + ry);
                            ctx.bezierCurveTo(cx - (KAPPA * rx), cy + ry, cx - rx, cy + (KAPPA * ry), cx - rx, cy);
                            ctx.bezierCurveTo(cx - rx, cy - (KAPPA * ry), cx - (KAPPA * rx), cy - ry, cx, cy - ry);
                            ctx.closePath();
                        }
                        
                        return new svg.BoundingBox(cx - rx, cy - ry, cx + rx, cy + ry);
                    }
                };
                svg.Element.ellipse.prototype = new svg.Element.PathElementBase;            
                
                // line element
                svg.Element.line = function(node) {
                    this.base = svg.Element.PathElementBase;
                    this.base(node);
                    
                    this.getPoints = function() {
                        return [
                            new svg.Point(this.attribute('x1').Length.toPixels('x'), this.attribute('y1').Length.toPixels('y')),
                            new svg.Point(this.attribute('x2').Length.toPixels('x'), this.attribute('y2').Length.toPixels('y'))];
                    };
                                        
                    this.path = function(ctx) {
                        var points = this.getPoints();
                        
                        if (ctx != null) {
                            ctx.beginPath();
                            ctx.moveTo(points[0].x, points[0].y);
                            ctx.lineTo(points[1].x, points[1].y);
                        }
                        
                        return new svg.BoundingBox(points[0].x, points[0].y, points[1].x, points[1].y);
                    };
                    
                    this.getMarkers = function() {
                        var points = this.getPoints();  
                        var a = points[0].angleTo(points[1]);
                        return [[points[0], a], [points[1], a]];
                    }
                };
                svg.Element.line.prototype = new svg.Element.PathElementBase;       
                        
                // polyline element
                svg.Element.polyline = function(node) {
                    this.base = svg.Element.PathElementBase;
                    this.base(node);
                    
                    this.points = svg.CreatePath(this.attribute('points').value);
                    this.path = function(ctx) {
                        var bb = new svg.BoundingBox(this.points[0].x, this.points[0].y);
                        if (ctx != null) {
                            ctx.beginPath();
                            ctx.moveTo(this.points[0].x, this.points[0].y);
                        }
                        for (var i=1; i < this.points.length; i++) {
                            bb.addPoint(this.points[i].x, this.points[i].y);
                            if (ctx != null) ctx.lineTo(this.points[i].x, this.points[i].y);
                        }
                        return bb;
                    };
                    
                    this.getMarkers = function() {
                        var markers = [];
                        for (var i=0; i < this.points.length - 1; i++) {
                            markers.push([this.points[i], this.points[i].angleTo(this.points[i+1])]);
                        }
                        markers.push([this.points[this.points.length-1], markers[markers.length-1][1]]);
                        return markers;
                    }           
                };
                svg.Element.polyline.prototype = new svg.Element.PathElementBase;               
                        
                // polygon element
                svg.Element.polygon = function(node) {
                    this.base = svg.Element.polyline;
                    this.base(node);
                    
                    this.basePath = this.path;
                    this.path = function(ctx) {
                        var bb = this.basePath(ctx);
                        if (ctx != null) {
                            ctx.lineTo(this.points[0].x, this.points[0].y);
                            ctx.closePath();
                        }
                        return bb;
                    }
                };
                svg.Element.polygon.prototype = new svg.Element.polyline;

                // path element
                svg.Element.path = function(node) {
                    this.base = svg.Element.PathElementBase;
                    this.base(node);
                            
                    var d = this.attribute('d').value;
                    // TODO: floating points, convert to real lexer based on http://www.w3.org/TR/SVG11/paths.html#PathDataBNF
                    d = d.replace(/,/gm,' '); // get rid of all commas
                    d = d.replace(/([A-Za-z])([A-Za-z])/gm,'$1 $2'); // separate commands from commands
                    d = d.replace(/([A-Za-z])([A-Za-z])/gm,'$1 $2'); // separate commands from commands
                    d = d.replace(/([A-Za-z])([^\s])/gm,'$1 $2'); // separate commands from points
                    d = d.replace(/([^\s])([A-Za-z])/gm,'$1 $2'); // separate commands from points
                    d = d.replace(/([0-9])([+\-])/gm,'$1 $2'); // separate digits when no comma
                    d = d.replace(/(\.[0-9]*)(\.)/gm,'$1 $2'); // separate digits when no comma
                    d = d.replace(/([Aa](\s+[0-9]+){3})\s+([01])\s*([01])/gm,'$1 $3 $4 '); // shorthand elliptical arc path syntax
                    d = svg.compressSpaces(d); // compress multiple spaces
                    d = svg.trim(d);
                    this.PathParser = new (function(d) {
                        this.tokens = d.split(' ');
                        
                        this.reset = function() {
                            this.i = -1;
                            this.command = '';
                            this.previousCommand = '';
                            this.control = new svg.Point(0, 0);
                            this.current = new svg.Point(0, 0);
                            this.points = [];
                            this.angles = [];
                        };
                                        
                        this.isEnd = function() {
                            return this.i == this.tokens.length - 1;
                        };
                        
                        this.isCommandOrEnd = function() {
                            if (this.isEnd()) return true;
                            return this.tokens[this.i + 1].match(/[A-Za-z]/) != null;
                        };
                        
                        this.isRelativeCommand = function() {
                            return this.command == this.command.toLowerCase();
                        };
                                    
                        this.getToken = function() {
                            this.i = this.i + 1;
                            return this.tokens[this.i];
                        };
                        
                        this.getScalar = function() {
                            return parseFloat(this.getToken());
                        };
                        
                        this.nextCommand = function() {
                            this.previousCommand = this.command;
                            this.command = this.getToken();
                        };              
                        
                        this.getPoint = function() {
                            var p = new svg.Point(this.getScalar(), this.getScalar());
                            return this.makeAbsolute(p);
                        };
                        
                        this.getAsControlPoint = function() {
                            var p = this.getPoint();
                            this.control = p;
                            return p;
                        };
                        
                        this.getAsCurrentPoint = function() {
                            var p = this.getPoint();
                            this.current = p;
                            return p;   
                        };
                        
                        this.getReflectedControlPoint = function() {
                            if (this.previousCommand.toLowerCase() != 'c' && this.previousCommand.toLowerCase() != 's') {
                                return this.current;
                            }
                            
                            // reflect point
                            var p = new svg.Point(2 * this.current.x - this.control.x, 2 * this.current.y - this.control.y);                    
                            return p;
                        };
                        
                        this.makeAbsolute = function(p) {
                            if (this.isRelativeCommand()) {
                                p.x = this.current.x + p.x;
                                p.y = this.current.y + p.y;
                            }
                            return p;
                        };
                        
                        this.addMarker = function(p, from) {
                            this.addMarkerAngle(p, from == null ? null : from.angleTo(p));
                        };
                        
                        this.addMarkerAngle = function(p, a) {
                            this.points.push(p);
                            this.angles.push(a);
                        };              
                        
                        this.getMarkerPoints = function() { return this.points; };
                        this.getMarkerAngles = function() {
                            for (var i=0; i < this.angles.length; i++) {
                                if (this.angles[i] == null) {
                                    for (var j=i+1; j < this.angles.length; j++) {
                                        if (this.angles[j] != null) {
                                            this.angles[i] = this.angles[j];
                                            break;
                                        }
                                    }
                                }
                            }
                            return this.angles;
                        }
                    })(d);
                    
                    this.path = function(ctx) {     
                        var pp = this.PathParser;
                        pp.reset();
                        
                        var bb = new svg.BoundingBox();
                        if (ctx != null) ctx.beginPath();
                        while (!pp.isEnd()) {
                            pp.nextCommand();
                            if (pp.command.toUpperCase() == 'M') {
                                var p = pp.getAsCurrentPoint();
                                pp.addMarker(p);
                                bb.addPoint(p.x, p.y);
                                if (ctx != null) ctx.moveTo(p.x, p.y);
                                while (!pp.isCommandOrEnd()) {
                                    var p = pp.getAsCurrentPoint();
                                    pp.addMarker(p);
                                    bb.addPoint(p.x, p.y);
                                    if (ctx != null) ctx.lineTo(p.x, p.y);
                                }
                            }
                            else if (pp.command.toUpperCase() == 'L') {
                                while (!pp.isCommandOrEnd()) {
                                    var c = pp.current;
                                    var p = pp.getAsCurrentPoint();
                                    pp.addMarker(p, c);
                                    bb.addPoint(p.x, p.y);
                                    if (ctx != null) ctx.lineTo(p.x, p.y);
                                }
                            }
                            else if (pp.command.toUpperCase() == 'H') {
                                while (!pp.isCommandOrEnd()) {
                                    var newP = new svg.Point((pp.isRelativeCommand() ? pp.current.x : 0) + pp.getScalar(), pp.current.y);
                                    pp.addMarker(newP, pp.current);
                                    pp.current = newP;
                                    bb.addPoint(pp.current.x, pp.current.y);
                                    if (ctx != null) ctx.lineTo(pp.current.x, pp.current.y);
                                }
                            }
                            else if (pp.command.toUpperCase() == 'V') {
                                while (!pp.isCommandOrEnd()) {
                                    var newP = new svg.Point(pp.current.x, (pp.isRelativeCommand() ? pp.current.y : 0) + pp.getScalar());
                                    pp.addMarker(newP, pp.current);
                                    pp.current = newP;
                                    bb.addPoint(pp.current.x, pp.current.y);
                                    if (ctx != null) ctx.lineTo(pp.current.x, pp.current.y);
                                }
                            }
                            else if (pp.command.toUpperCase() == 'C') {
                                while (!pp.isCommandOrEnd()) {
                                    var curr = pp.current;
                                    var p1 = pp.getPoint();
                                    var cntrl = pp.getAsControlPoint();
                                    var cp = pp.getAsCurrentPoint();
                                    pp.addMarker(cp, cntrl);
                                    bb.addBezierCurve(curr.x, curr.y, p1.x, p1.y, cntrl.x, cntrl.y, cp.x, cp.y);
                                    if (ctx != null) ctx.bezierCurveTo(p1.x, p1.y, cntrl.x, cntrl.y, cp.x, cp.y);
                                }
                            }
                            else if (pp.command.toUpperCase() == 'S') {
                                while (!pp.isCommandOrEnd()) {
                                    var curr = pp.current;
                                    var p1 = pp.getReflectedControlPoint();
                                    var cntrl = pp.getAsControlPoint();
                                    var cp = pp.getAsCurrentPoint();
                                    pp.addMarker(cp, cntrl);
                                    bb.addBezierCurve(curr.x, curr.y, p1.x, p1.y, cntrl.x, cntrl.y, cp.x, cp.y);
                                    if (ctx != null) ctx.bezierCurveTo(p1.x, p1.y, cntrl.x, cntrl.y, cp.x, cp.y);
                                }               
                            }                   
                            else if (pp.command.toUpperCase() == 'Q') {
                                while (!pp.isCommandOrEnd()) {
                                    var curr = pp.current;
                                    var cntrl = pp.getAsControlPoint();
                                    var cp = pp.getAsCurrentPoint();
                                    pp.addMarker(cp, cntrl);
                                    bb.addQuadraticCurve(curr.x, curr.y, cntrl.x, cntrl.y, cp.x, cp.y);
                                    if (ctx != null) ctx.quadraticCurveTo(cntrl.x, cntrl.y, cp.x, cp.y);
                                }
                            }                   
                            else if (pp.command.toUpperCase() == 'T') {
                                while (!pp.isCommandOrEnd()) {
                                    var curr = pp.current;
                                    var cntrl = pp.getReflectedControlPoint();
                                    pp.control = cntrl;
                                    var cp = pp.getAsCurrentPoint();
                                    pp.addMarker(cp, cntrl);
                                    bb.addQuadraticCurve(curr.x, curr.y, cntrl.x, cntrl.y, cp.x, cp.y);
                                    if (ctx != null) ctx.quadraticCurveTo(cntrl.x, cntrl.y, cp.x, cp.y);
                                }                   
                            }
                            else if (pp.command.toUpperCase() == 'A') {
                                while (!pp.isCommandOrEnd()) {
                                    var curr = pp.current;
                                    var rx = pp.getScalar();
                                    var ry = pp.getScalar();
                                    var xAxisRotation = pp.getScalar() * (Math.PI / 180.0);
                                    var largeArcFlag = pp.getScalar();
                                    var sweepFlag = pp.getScalar();
                                    var cp = pp.getAsCurrentPoint();
                                    
                                    // Conversion from endpoint to center parameterization
                                    // http://www.w3.org/TR/SVG11/implnote.html#ArcImplementationNotes
                                    // x1', y1'
                                    var currp = new svg.Point(
                                        Math.cos(xAxisRotation) * (curr.x - cp.x) / 2.0 + Math.sin(xAxisRotation) * (curr.y - cp.y) / 2.0,
                                        -Math.sin(xAxisRotation) * (curr.x - cp.x) / 2.0 + Math.cos(xAxisRotation) * (curr.y - cp.y) / 2.0
                                    );
                                    // adjust radii
                                    var l = Math.pow(currp.x,2)/Math.pow(rx,2)+Math.pow(currp.y,2)/Math.pow(ry,2);
                                    if (l > 1) {
                                        rx *= Math.sqrt(l);
                                        ry *= Math.sqrt(l);
                                    }
                                    // cx', cy'
                                    var s = (largeArcFlag == sweepFlag ? -1 : 1) * Math.sqrt(
                                        ((Math.pow(rx,2)*Math.pow(ry,2))-(Math.pow(rx,2)*Math.pow(currp.y,2))-(Math.pow(ry,2)*Math.pow(currp.x,2))) /
                                        (Math.pow(rx,2)*Math.pow(currp.y,2)+Math.pow(ry,2)*Math.pow(currp.x,2))
                                    );
                                    if (isNaN(s)) s = 0;
                                    var cpp = new svg.Point(s * rx * currp.y / ry, s * -ry * currp.x / rx);
                                    // cx, cy
                                    var centp = new svg.Point(
                                        (curr.x + cp.x) / 2.0 + Math.cos(xAxisRotation) * cpp.x - Math.sin(xAxisRotation) * cpp.y,
                                        (curr.y + cp.y) / 2.0 + Math.sin(xAxisRotation) * cpp.x + Math.cos(xAxisRotation) * cpp.y
                                    );
                                    // vector magnitude
                                    var m = function(v) { return Math.sqrt(Math.pow(v[0],2) + Math.pow(v[1],2)); };
                                    // ratio between two vectors
                                    var r = function(u, v) { return (u[0]*v[0]+u[1]*v[1]) / (m(u)*m(v)) };
                                    // angle between two vectors
                                    var a = function(u, v) { return (u[0]*v[1] < u[1]*v[0] ? -1 : 1) * Math.acos(r(u,v)); };
                                    // initial angle
                                    var a1 = a([1,0], [(currp.x-cpp.x)/rx,(currp.y-cpp.y)/ry]);
                                    // angle delta
                                    var u = [(currp.x-cpp.x)/rx,(currp.y-cpp.y)/ry];
                                    var v = [(-currp.x-cpp.x)/rx,(-currp.y-cpp.y)/ry];
                                    var ad = a(u, v);
                                    if (r(u,v) <= -1) ad = Math.PI;
                                    if (r(u,v) >= 1) ad = 0;
                                    
                                    if (sweepFlag == 0 && ad > 0) ad = ad - 2 * Math.PI;
                                    if (sweepFlag == 1 && ad < 0) ad = ad + 2 * Math.PI;
                                    
                                    // for markers
                                    var halfWay = new svg.Point(
                                        centp.x - rx * Math.cos((a1 + ad) / 2),
                                        centp.y - ry * Math.sin((a1 + ad) / 2)
                                    );
                                    pp.addMarkerAngle(halfWay, (a1 + ad) / 2 + (sweepFlag == 0 ? 1 : -1) * Math.PI / 2);
                                    pp.addMarkerAngle(cp, ad + (sweepFlag == 0 ? 1 : -1) * Math.PI / 2);
                                                                
                                    bb.addPoint(cp.x, cp.y); // TODO: this is too naive, make it better
                                    if (ctx != null) {
                                        var r = rx > ry ? rx : ry;
                                        var sx = rx > ry ? 1 : rx / ry;
                                        var sy = rx > ry ? ry / rx : 1;
                                    
                                        ctx.translate(centp.x, centp.y);
                                        ctx.rotate(xAxisRotation);
                                        ctx.scale(sx, sy);
                                        ctx.arc(0, 0, r, a1, a1 + ad, 1 - sweepFlag);
                                        ctx.scale(1/sx, 1/sy);
                                        ctx.rotate(-xAxisRotation);
                                        ctx.translate(-centp.x, -centp.y);
                                    }
                                }
                            }
                            else if (pp.command.toUpperCase() == 'Z') {
                                if (ctx != null) ctx.closePath();
                            }
                        }
                                    
                        return bb;
                    };
                    
                    this.getMarkers = function() {
                        var points = this.PathParser.getMarkerPoints();
                        var angles = this.PathParser.getMarkerAngles();
                        
                        var markers = [];
                        for (var i=0; i < points.length; i++) {
                            markers.push([points[i], angles[i]]);
                        }
                        return markers;
                    }
                };
                svg.Element.path.prototype = new svg.Element.PathElementBase;
                
                // pattern element
                svg.Element.pattern = function(node) {
                    this.base = svg.Element.ElementBase;
                    this.base(node);
                    
                    this.createPattern = function(ctx, element) {
                        // render me using a temporary svg element
                        var tempSvg = new svg.Element.svg();
                        tempSvg.attributes['viewBox'] = new svg.Property('viewBox', this.attribute('viewBox').value);
                        tempSvg.attributes['x'] = new svg.Property('x', this.attribute('x').value);
                        tempSvg.attributes['y'] = new svg.Property('y', this.attribute('y').value);
                        tempSvg.attributes['width'] = new svg.Property('width', this.attribute('width').value);
                        tempSvg.attributes['height'] = new svg.Property('height', this.attribute('height').value);
                        tempSvg.children = this.children;
                        
                        var c = document.createElement('canvas');
                        c.width = this.attribute('width').Length.toPixels();
                        c.height = this.attribute('height').Length.toPixels();
                        tempSvg.render(c.getContext('2d'));     
                        return ctx.createPattern(c, 'repeat');
                    }
                };
                svg.Element.pattern.prototype = new svg.Element.ElementBase;
                
                // marker element
                svg.Element.marker = function(node) {
                    this.base = svg.Element.ElementBase;
                    this.base(node);
                    
                    this.baseRender = this.render;
                    this.render = function(ctx, point, angle) {
                        ctx.translate(point.x, point.y);
                        if (this.attribute('orient').valueOrDefault('auto') == 'auto') ctx.rotate(angle);
                        if (this.attribute('markerUnits').valueOrDefault('strokeWidth') == 'strokeWidth') ctx.scale(ctx.lineWidth, ctx.lineWidth);
                        ctx.save();
                                    
                        // render me using a temporary svg element
                        var tempSvg = new svg.Element.svg();
                        tempSvg.attributes['viewBox'] = new svg.Property('viewBox', this.attribute('viewBox').value);
                        tempSvg.attributes['refX'] = new svg.Property('refX', this.attribute('refX').value);
                        tempSvg.attributes['refY'] = new svg.Property('refY', this.attribute('refY').value);
                        tempSvg.attributes['width'] = new svg.Property('width', this.attribute('markerWidth').value);
                        tempSvg.attributes['height'] = new svg.Property('height', this.attribute('markerHeight').value);
                        tempSvg.attributes['fill'] = new svg.Property('fill', this.attribute('fill').valueOrDefault('black'));
                        tempSvg.attributes['stroke'] = new svg.Property('stroke', this.attribute('stroke').valueOrDefault('none'));
                        tempSvg.children = this.children;
                        tempSvg.render(ctx);
                        
                        ctx.restore();
                        if (this.attribute('markerUnits').valueOrDefault('strokeWidth') == 'strokeWidth') ctx.scale(1/ctx.lineWidth, 1/ctx.lineWidth);
                        if (this.attribute('orient').valueOrDefault('auto') == 'auto') ctx.rotate(-angle);
                        ctx.translate(-point.x, -point.y);
                    }
                };
                svg.Element.marker.prototype = new svg.Element.ElementBase;
                
                // definitions element
                svg.Element.defs = function(node) {
                    this.base = svg.Element.ElementBase;
                    this.base(node);    
                    
                    this.render = function(ctx) {
                        // NOOP
                    }
                };
                svg.Element.defs.prototype = new svg.Element.ElementBase;
                
                // base for gradients
                svg.Element.GradientBase = function(node) {
                    this.base = svg.Element.ElementBase;
                    this.base(node);
                    
                    this.gradientUnits = this.attribute('gradientUnits').valueOrDefault('objectBoundingBox');
                    
                    this.stops = [];            
                    for (var i=0; i < this.children.length; i++) {
                        var child = this.children[i];
                        this.stops.push(child);
                    }   
                    
                    this.getGradient = function() {
                        // OVERRIDE ME!
                    };          

                    this.createGradient = function(ctx, element) {
                        var stopsContainer = this;
                        if (this.attribute('xlink:href').hasValue()) {
                            stopsContainer = this.attribute('xlink:href').Definition.getDefinition();
                        }
                    
                        var g = this.getGradient(ctx, element);
                        for (var i=0; i < stopsContainer.stops.length; i++) {
                            g.addColorStop(stopsContainer.stops[i].offset, stopsContainer.stops[i].color);
                        }
                        return g;               
                    }
                };
                svg.Element.GradientBase.prototype = new svg.Element.ElementBase;
                
                // linear gradient element
                svg.Element.linearGradient = function(node) {
                    this.base = svg.Element.GradientBase;
                    this.base(node);
                    
                    this.getGradient = function(ctx, element) {
                        var bb = element.getBoundingBox();
                        
                        var x1 = (this.gradientUnits == 'objectBoundingBox' 
                            ? bb.x() + bb.width() * this.attribute('x1').numValue() 
                            : this.attribute('x1').Length.toPixels('x'));
                        var y1 = (this.gradientUnits == 'objectBoundingBox' 
                            ? bb.y() + bb.height() * this.attribute('y1').numValue()
                            : this.attribute('y1').Length.toPixels('y'));
                        var x2 = (this.gradientUnits == 'objectBoundingBox' 
                            ? bb.x() + bb.width() * this.attribute('x2').numValue()
                            : this.attribute('x2').Length.toPixels('x'));
                        var y2 = (this.gradientUnits == 'objectBoundingBox' 
                            ? bb.y() + bb.height() * this.attribute('y2').numValue()
                            : this.attribute('y2').Length.toPixels('y'));
                        
                        var p1 = new svg.Point(x1, y1);
                        var p2 = new svg.Point(x2, y2);
                        if (this.attribute('gradientTransform').hasValue()) { 
                            var transform = new svg.Transform(this.attribute('gradientTransform').value);
                            transform.applyToPoint(p1);
                            transform.applyToPoint(p2);
                        }
                        
                        return ctx.createLinearGradient(p1.x, p1.y, p2.x, p2.y);
                    }
                };
                svg.Element.linearGradient.prototype = new svg.Element.GradientBase;
                
                // radial gradient element
                svg.Element.radialGradient = function(node) {
                    this.base = svg.Element.GradientBase;
                    this.base(node);
                    
                    this.getGradient = function(ctx, element) {
                        var bb = element.getBoundingBox();
                        
                        var cx = (this.gradientUnits == 'objectBoundingBox' 
                            ? bb.x() + bb.width() * this.attribute('cx').numValue() 
                            : this.attribute('cx').Length.toPixels('x'));
                        var cy = (this.gradientUnits == 'objectBoundingBox' 
                            ? bb.y() + bb.height() * this.attribute('cy').numValue() 
                            : this.attribute('cy').Length.toPixels('y'));
                        
                        var fx = cx;
                        var fy = cy;
                        if (this.attribute('fx').hasValue()) {
                            fx = (this.gradientUnits == 'objectBoundingBox' 
                            ? bb.x() + bb.width() * this.attribute('fx').numValue() 
                            : this.attribute('fx').Length.toPixels('x'));
                        }
                        if (this.attribute('fy').hasValue()) {
                            fy = (this.gradientUnits == 'objectBoundingBox' 
                            ? bb.y() + bb.height() * this.attribute('fy').numValue() 
                            : this.attribute('fy').Length.toPixels('y'));
                        }
                        
                        var r = (this.gradientUnits == 'objectBoundingBox' 
                            ? (bb.width() + bb.height()) / 2.0 * this.attribute('r').numValue()
                            : this.attribute('r').Length.toPixels());
                        
                        var c = new svg.Point(cx, cy);
                        var f = new svg.Point(fx, fy);
                        if (this.attribute('gradientTransform').hasValue()) { 
                            var transform = new svg.Transform(this.attribute('gradientTransform').value);
                            transform.applyToPoint(c);
                            transform.applyToPoint(f);
                        }               
                        
                        return ctx.createRadialGradient(f.x, f.y, 0, c.x, c.y, r);
                    }
                };
                svg.Element.radialGradient.prototype = new svg.Element.GradientBase;
                
                // gradient stop element
                svg.Element.stop = function(node) {
                    this.base = svg.Element.ElementBase;
                    this.base(node);
                    
                    this.offset = this.attribute('offset').numValue();
                    
                    var stopColor = this.style('stop-color');
                    if (this.style('stop-opacity').hasValue()) stopColor = stopColor.Color.addOpacity(this.style('stop-opacity').value);
                    this.color = stopColor.value;
                };
                svg.Element.stop.prototype = new svg.Element.ElementBase;
                
                // animation base element
                svg.Element.AnimateBase = function(node) {
                    this.base = svg.Element.ElementBase;
                    this.base(node);
                    
                    svg.Animations.push(this);
                    
                    this.duration = 0.0;
                    this.begin = this.attribute('begin').Time.toMilliseconds();
                    this.maxDuration = this.begin + this.attribute('dur').Time.toMilliseconds();

                    this.calcValue = function() {
                        // OVERRIDE ME!
                        return '';
                    };
                    
                    this.update = function(delta) {         
                        // if we're past the end time
                        if (this.duration > this.maxDuration) {
                            // loop for indefinitely repeating animations
                            if (this.attribute('repeatCount').value == 'indefinite') {
                                this.duration = 0.0
                            }
                            else {
                                return false; // no updates made
                            }
                        }           
                        this.duration = this.duration + delta;
                    
                        // if we're past the begin time
                        var updated = false;
                        if (this.begin < this.duration) {
                            var newValue = this.calcValue(); // tween
                            var attributeType = this.attribute('attributeType').value;
                            var attributeName = this.attribute('attributeName').value;
                            
                            if (this.parent != null) {
                                if (attributeType == 'CSS') {
                                    this.parent.style(attributeName, true).value = newValue;
                                }
                                else { // default or XML
                                    if (this.attribute('type').hasValue()) {
                                        // for transform, etc.
                                        var type = this.attribute('type').value;
                                        this.parent.attribute(attributeName, true).value = type + '(' + newValue + ')';
                                    }
                                    else {
                                        this.parent.attribute(attributeName, true).value = newValue;
                                    }
                                }
                                updated = true;
                            }
                        }
                        
                        return updated;
                    };
                    
                    // fraction of duration we've covered
                    this.progress = function() {
                        return ((this.duration - this.begin) / (this.maxDuration - this.begin));
                    }           
                };
                svg.Element.AnimateBase.prototype = new svg.Element.ElementBase;
                
                // animate element
                svg.Element.animate = function(node) {
                    this.base = svg.Element.AnimateBase;
                    this.base(node);
                    
                    this.calcValue = function() {
                        var from = this.attribute('from').numValue();
                        var to = this.attribute('to').numValue();
                        
                        // tween value linearly
                        return from + (to - from) * this.progress(); 
                    };
                };
                svg.Element.animate.prototype = new svg.Element.AnimateBase;
                    
                // animate color element
                svg.Element.animateColor = function(node) {
                    this.base = svg.Element.AnimateBase;
                    this.base(node);

                    this.calcValue = function() {
                        var from = new RGBColor(this.attribute('from').value);
                        var to = new RGBColor(this.attribute('to').value);
                        
                        if (from.ok && to.ok) {
                            // tween color linearly
                            var r = from.r + (to.r - from.r) * this.progress();
                            var g = from.g + (to.g - from.g) * this.progress();
                            var b = from.b + (to.b - from.b) * this.progress();
                            return 'rgb('+parseInt(r,10)+','+parseInt(g,10)+','+parseInt(b,10)+')';
                        }
                        return this.attribute('from').value;
                    };
                };
                svg.Element.animateColor.prototype = new svg.Element.AnimateBase;
                
                // animate transform element
                svg.Element.animateTransform = function(node) {
                    this.base = svg.Element.animate;
                    this.base(node);
                };
                svg.Element.animateTransform.prototype = new svg.Element.animate;
                
                // text element
                svg.Element.text = function(node) {
                    this.base = svg.Element.RenderedElementBase;
                    this.base(node);
                    
                    if (node != null) {
                        // add children
                        this.children = [];
                        for (var i=0; i < node.childNodes.length; i++) {
                            var childNode = node.childNodes[i];
                            if (childNode.nodeType == 1) { // capture tspan and tref nodes
                                this.addChild(childNode, true);
                            }
                            else if (childNode.nodeType == 3) { // capture text
                                this.addChild(new svg.Element.tspan(childNode), false);
                            }
                        }
                    }
                    
                    this.baseSetContext = this.setContext;
                    this.setContext = function(ctx) {
                        this.baseSetContext(ctx);
                        if (this.attribute('text-anchor').hasValue()) {
                            var textAnchor = this.attribute('text-anchor').value;
                            ctx.textAlign = textAnchor == 'middle' ? 'center' : textAnchor;
                        }
                        if (this.attribute('alignment-baseline').hasValue()) ctx.textBaseline = this.attribute('alignment-baseline').value;
                    };
                    
                    this.renderChildren = function(ctx) {
                        var x = this.attribute('x').Length.toPixels('x');
                        var y = this.attribute('y').Length.toPixels('y');
                        for (var i=0; i < this.children.length; i++) {
                            var child = this.children[i];
                        
                            if (child.attribute('x').hasValue()) {
                                child.x = child.attribute('x').Length.toPixels('x');
                            }
                            else {
                                if (child.attribute('dx').hasValue()) x += child.attribute('dx').Length.toPixels('x');
                                child.x = x;
                                x += child.measureText(ctx);
                            }
                            
                            if (child.attribute('y').hasValue()) {
                                child.y = child.attribute('y').Length.toPixels('y');
                            }
                            else {
                                if (child.attribute('dy').hasValue()) y += child.attribute('dy').Length.toPixels('y');
                                child.y = y;
                            }   
                            
                            child.render(ctx);
                        }
                    }
                };
                svg.Element.text.prototype = new svg.Element.RenderedElementBase;
                
                // text base
                svg.Element.TextElementBase = function(node) {
                    this.base = svg.Element.RenderedElementBase;
                    this.base(node);
                    
                    this.renderChildren = function(ctx) {
                        ctx.fillText(svg.compressSpaces(this.getText()), this.x, this.y);
                    };
                    
                    this.getText = function() {
                        // OVERRIDE ME
                    };
                    
                    this.measureText = function(ctx) {
                        var textToMeasure = svg.compressSpaces(this.getText());
                        if (!ctx.measureText) return textToMeasure.length * 10;
                        return ctx.measureText(textToMeasure).width;
                    }
                };
                svg.Element.TextElementBase.prototype = new svg.Element.RenderedElementBase;
                
                // tspan 
                svg.Element.tspan = function(node) {
                    this.base = svg.Element.TextElementBase;
                    this.base(node);
                    
                    //                               TEXT             ELEMENT
                    this.text = node.nodeType == 3 ? node.nodeValue : node.childNodes[0].nodeValue;
                    this.getText = function() {
                        return this.text;
                    }
                };
                svg.Element.tspan.prototype = new svg.Element.TextElementBase;
                
                // tref
                svg.Element.tref = function(node) {
                    this.base = svg.Element.TextElementBase;
                    this.base(node);
                    
                    this.getText = function() {
                        var element = this.attribute('xlink:href').Definition.getDefinition();
                        if (element != null) return element.children[0].getText();
                    }
                };
                svg.Element.tref.prototype = new svg.Element.TextElementBase;       
                
                // a element
                svg.Element.a = function(node) {
                    this.base = svg.Element.TextElementBase;
                    this.base(node);
                    
                    this.hasText = true;
                    for (var i=0; i < node.childNodes.length; i++) {
                        if (node.childNodes[i].nodeType != 3) this.hasText = false;
                    }
                    
                    // this might contain text
                    this.text = this.hasText ? node.childNodes[0].nodeValue : '';
                    this.getText = function() {
                        return this.text;
                    };      

                    this.baseRenderChildren = this.renderChildren;
                    this.renderChildren = function(ctx) {
                        if (this.hasText) {
                            // render as text element
                            this.baseRenderChildren(ctx);
                            var fontSize = new svg.Property('fontSize', svg.Font.Parse(svg.ctx.font).fontSize);
                            svg.Mouse.checkBoundingBox(this, new svg.BoundingBox(this.x, this.y - fontSize.Length.toPixels('y'), this.x + this.measureText(ctx), this.y));                  
                        }
                        else {
                            // render as temporary group
                            var g = new svg.Element.g();
                            g.children = this.children;
                            g.parent = this;
                            g.render(ctx);
                        }
                    };
                    
                    this.onclick = function() {
                        window.open(this.attribute('xlink:href').value);
                    };
                    
                    this.onmousemove = function() {
                        svg.ctx.canvas.style.cursor = 'pointer';
                    }
                };
                svg.Element.a.prototype = new svg.Element.TextElementBase;      
                
                // image element
                svg.Element.image = function(node) {
                    this.base = svg.Element.RenderedElementBase;
                    this.base(node);
                    
                    svg.Images.push(this);
                    this.img = document.createElement('img');
                    this.loaded = false;
                    var that = this;
                    this.img.onload = function() { that.loaded = true; };
                    this.img.src = this.attribute('xlink:href').value;
                    
                    this.renderChildren = function(ctx) {
                        var x = this.attribute('x').Length.toPixels('x');
                        var y = this.attribute('y').Length.toPixels('y');
                        
                        var width = this.attribute('width').Length.toPixels('x');
                        var height = this.attribute('height').Length.toPixels('y');         
                        if (width == 0 || height == 0) return;
                    
                        ctx.save();
                        ctx.translate(x, y);
                        svg.AspectRatio(ctx,
                                        this.attribute('preserveAspectRatio').value,
                                        width,
                                        this.img.width,
                                        height,
                                        this.img.height,
                                        0,
                                        0); 
                        ctx.drawImage(this.img, 0, 0);          
                        ctx.restore();
                    }
                };
                svg.Element.image.prototype = new svg.Element.RenderedElementBase;
                
                // group element
                svg.Element.g = function(node) {
                    this.base = svg.Element.RenderedElementBase;
                    this.base(node);
                };
                svg.Element.g.prototype = new svg.Element.RenderedElementBase;

                // symbol element
                svg.Element.symbol = function(node) {
                    this.base = svg.Element.RenderedElementBase;
                    this.base(node);
                };
                svg.Element.symbol.prototype = new svg.Element.RenderedElementBase;     
                    
                // style element
                svg.Element.style = function(node) { 
                    this.base = svg.Element.ElementBase;
                    this.base(node);
                    
                    var css = node.childNodes[0].nodeValue;
                    css = css.replace(/(\/\*([^*]|[\r\n]|(\*+([^*\/]|[\r\n])))*\*+\/)|(\/\/.*)/gm, ''); // remove comments
                    css = svg.compressSpaces(css); // replace whitespace
                    var cssDefs = css.split('}');
                    for (var i=0; i < cssDefs.length; i++) {
                        if (svg.trim(cssDefs[i]) != '') {
                            var cssDef = cssDefs[i].split('{');
                            var cssClasses = cssDef[0].split(',');
                            var cssProps = cssDef[1].split(';');
                            for (var j=0; j < cssClasses.length; j++) {
                                var cssClass = svg.trim(cssClasses[j]);
                                if (cssClass != '') {
                                    var props = {};
                                    for (var k=0; k < cssProps.length; k++) {
                                        var prop = cssProps[k].split(':');
                                        var name = prop[0];
                                        var value = prop[1];
                                        if (name != null && value != null) {
                                            props[svg.trim(prop[0])] = new svg.Property(svg.trim(prop[0]), svg.trim(prop[1]));
                                        }
                                    }
                                    svg.Styles[cssClass] = props;
                                }
                            }
                        }
                    }
                };
                svg.Element.style.prototype = new svg.Element.ElementBase;
                
                // use element 
                svg.Element.use = function(node) {
                    this.base = svg.Element.RenderedElementBase;
                    this.base(node);
                    
                    this.baseSetContext = this.setContext;
                    this.setContext = function(ctx) {
                        this.baseSetContext(ctx);
                        if (this.attribute('x').hasValue()) ctx.translate(this.attribute('x').Length.toPixels('x'), 0);
                        if (this.attribute('y').hasValue()) ctx.translate(0, this.attribute('y').Length.toPixels('y'));
                    };
                    
                    this.getDefinition = function() {
                        return this.attribute('xlink:href').Definition.getDefinition();
                    };
                    
                    this.path = function(ctx) {
                        var element = this.getDefinition();
                        if (element != null) element.path(ctx);
                    };
                    
                    this.renderChildren = function(ctx) {
                        var element = this.getDefinition();
                        if (element != null) element.render(ctx);
                    }
                };
                svg.Element.use.prototype = new svg.Element.RenderedElementBase;
                
                // clip element
                svg.Element.clipPath = function(node) {
                    this.base = svg.Element.ElementBase;
                    this.base(node);
                    
                    this.apply = function(ctx) {
                        for (var i=0; i < this.children.length; i++) {
                            if (this.children[i].path) {
                                this.children[i].path(ctx);
                                ctx.clip();
                            }
                        }
                    }
                };
                svg.Element.clipPath.prototype = new svg.Element.ElementBase;

                // title element, do nothing
                svg.Element.title = function(node) {
                };
                svg.Element.title.prototype = new svg.Element.ElementBase;

                // desc element, do nothing
                svg.Element.desc = function(node) {
                };
                svg.Element.desc.prototype = new svg.Element.ElementBase;       
                
                svg.Element.MISSING = function(node) {
                    console.log('ERROR: Element \'' + node.nodeName + '\' not yet implemented.');
                };
                svg.Element.MISSING.prototype = new svg.Element.ElementBase;
                
                // element factory
                svg.CreateElement = function(node) {
                    var className = 'svg.Element.' + node.nodeName.replace(/^[^:]+:/,'');
                    if (!eval(className)) className = 'svg.Element.MISSING';
                
                    var e = eval('new ' + className + '(node)');
                    e.type = node.nodeName;
                    return e;
                };
                        
                // load from url
                svg.load = function(ctx, url) {
                    svg.loadXml(ctx, svg.ajax(url));
                };
                
                // load from xml
                svg.loadXml = function(ctx, xml) {
                    svg.init(ctx);
                    
                    var mapXY = function(p) {
                        var e = ctx.canvas;
                        while (e) {
                            p.x -= e.offsetLeft;
                            p.y -= e.offsetTop;
                            e = e.offsetParent;
                        }
                        if (window.scrollX) p.x += window.scrollX;
                        if (window.scrollY) p.y += window.scrollY;
                        return p;
                    };
                    
                    // bind mouse
                    if (svg.opts == null || svg.opts['ignoreMouse'] != true) {
                        ctx.canvas.onclick = function(e) {
                            var p = mapXY(new svg.Point(e != null ? e.clientX : event.clientX, e != null ? e.clientY : event.clientY));
                            svg.Mouse.onclick(p.x, p.y);
                        };
                        ctx.canvas.onmousemove = function(e) {
                            var p = mapXY(new svg.Point(e != null ? e.clientX : event.clientX, e != null ? e.clientY : event.clientY));
                            svg.Mouse.onmousemove(p.x, p.y);
                        };
                    }
                
                    var dom = svg.parseXml(xml);
                    var e = svg.CreateElement(dom.documentElement);
                            
                    // render loop
                    var isFirstRender = true;
                    var draw = function() {
                        // set canvas size
                        // COMMENT-OUT-HACK 1/2 TO KEEP ORIGINAL CANVAS DRAWINGS
                        // if (e.style('width').hasValue()) {
                        //  ctx.canvas.width = e.style('width').Length.toPixels(ctx.canvas.parentNode.clientWidth);
                        // }
                        // if (e.style('height').hasValue()) {
                        //  ctx.canvas.height = e.style('height').Length.toPixels(ctx.canvas.parentNode.clientHeight);
                        // }
                        svg.ViewPort.SetCurrent(ctx.canvas.clientWidth, ctx.canvas.clientHeight);           
                    
                        // clear and render
                        // COMMENT-OUT-HACK 2/2 TO KEEP ORIGINAL CANVAS DRAWINGS
                        // ctx.clearRect(0, 0, ctx.canvas.clientWidth, ctx.canvas.clientHeight);
                        e.render(ctx);
                        if (isFirstRender) {
                            isFirstRender = false;
                            if (svg.opts != null && typeof(svg.opts['renderCallback']) == 'function') svg.opts['renderCallback']();
                        }           
                    };
                    
                    var waitingForImages = true;
                    if (svg.ImagesLoaded()) {
                        waitingForImages = false;
                        draw();
                    }
                    svg.intervalID = setInterval(function() { 
                        var needUpdate = false;
                        
                        if (waitingForImages && svg.ImagesLoaded()) {
                            waitingForImages = false;
                            needUpdate = true;
                        }
                    
                        // need update from mouse events?
                        if (svg.opts == null || svg.opts['ignoreMouse'] != true) {
                            needUpdate = needUpdate | svg.Mouse.hasEvents();
                        }
                    
                        // need update from animations?
                        if (svg.opts == null || svg.opts['ignoreAnimation'] != true) {
                            for (var i=0; i < svg.Animations.length; i++) {
                                needUpdate = needUpdate | svg.Animations[i].update(1000 / svg.FRAMERATE);
                            }
                        }
                        
                        // need update from redraw?
                        if (svg.opts != null && typeof(svg.opts['forceRedraw']) == 'function') {
                            if (svg.opts['forceRedraw']() == true) needUpdate = true;
                        }
                        
                        // render if needed
                        if (needUpdate) {
                            draw();             
                            svg.Mouse.runEvents(); // run and clear our events
                        }
                    }, 1000 / svg.FRAMERATE);
                };
                
                svg.stop = function() {
                    if (svg.intervalID) {
                        clearInterval(svg.intervalID);
                    }
                };
                
                svg.Mouse = new (function() {
                    this.events = [];
                    this.hasEvents = function() { return this.events.length != 0; };
                
                    this.onclick = function(x, y) {
                        this.events.push({ type: 'onclick', x: x, y: y, 
                            run: function(e) { if (e.onclick) e.onclick(); }
                        });
                    };
                    
                    this.onmousemove = function(x, y) {
                        this.events.push({ type: 'onmousemove', x: x, y: y,
                            run: function(e) { if (e.onmousemove) e.onmousemove(); }
                        });
                    };          
                    
                    this.eventElements = [];
                    
                    this.checkPath = function(element, ctx) {
                        for (var i=0; i < this.events.length; i++) {
                            var e = this.events[i];
                            if (ctx.isPointInPath && ctx.isPointInPath(e.x, e.y)) this.eventElements[i] = element;
                        }
                    };
                    
                    this.checkBoundingBox = function(element, bb) {
                        for (var i=0; i < this.events.length; i++) {
                            var e = this.events[i];
                            if (bb.isPointInBox(e.x, e.y)) this.eventElements[i] = element;
                        }           
                    };
                    
                    this.runEvents = function() {
                        svg.ctx.canvas.style.cursor = '';
                        
                        for (var i=0; i < this.events.length; i++) {
                            var e = this.events[i];
                            var element = this.eventElements[i];
                            while (element) {
                                e.run(element);
                                element = element.parent;
                            }
                        }       
                    
                        // done running, clear
                        this.events = []; 
                        this.eventElements = [];
                    }
                });
                
                return svg;
            }
        })();

        var SVGToCanvas = {
            endpoint: "conversion/convert.php",
            
            base64dataURLencode: function(s) {
                var b64 = "data:image/svg+xml;base64,";

                if (window.btoa) b64 += btoa(s);
                else b64 += Base64.encode(s);
                
                return b64;
            },
            
            xmlSerialize: function(SVGdom) {
                if (window.XMLSerializer) {
                    return (new XMLSerializer()).serializeToString(SVGdom);
                } else {
                    return SVGToCanvas.XMLSerializerForIE(SVGdom);
                }
            },
            
            // quick-n-serialize an SVG dom, needed for IE9 where there's no XMLSerializer nor SVG.xml
            // s: SVG dom, which is the <svg> elemennt
            XMLSerializerForIE: function(s) {
                var out = "";

                out += "<" + s.nodeName;
                for (var n = 0; n < s.attributes.length; n++) {
                    out += " " + s.attributes[n].name + "=" + "'" + s.attributes[n].value + "'";
                }
                
                if (s.hasChildNodes()) {
                    out += ">\n";

                    for (var n = 0; n < s.childNodes.length; n++) {
                        out += SVGToCanvas.XMLSerializerForIE(s.childNodes[n]);
                    }

                    out += "</" + s.nodeName + ">" + "\n";

                } else out += " />\n";

                return out;
            },


            // works in webkit
            convert: function (sourceSVG, targetCanvas, x,y) {
                var svg_xml = this.xmlSerialize(sourceSVG);
                var ctx = targetCanvas.getContext('2d');
                var img = new Image();
                // ff fails here, http://en.wikipedia.org/wiki/SVG#Native_support
                img.src = this.base64dataURLencode(svg_xml);

                img.onload = function() {
                    console.log("Exported image size " + img.width + "x" + img.height);
                    // ie and opera fail here for svg input, maybe it's a plan to prevent origin-dirtying?
                    ctx.drawImage(img, (x | 0), (y | 0));
                };
                
                img.onerror = function() {
                    // TODO: if export fails, don't set Canvas dirty in GUI
                    alert("Can't export! Maybe your browser doesn't support " +
                        "SVG in img element or SVG input for Canvas drawImage?\n" +
                        "http://en.wikipedia.org/wiki/SVG#Native_support")
                }
                
            },

            // needs (jquery and) a same-origin server?
            convertServer: function (sourceSVG, targetCanvas, x,y) {
                var svg_xml = this.xmlSerialize(sourceSVG);
            
                $.post(this.endpoint, { svg_xml: svg_xml }, function(data) {
                    var ctx = targetCanvas.getContext('2d');
                    var img = new Image();
                    img.src = data;
                    
                    img.onload = function() {
                        ctx.drawImage(img, x | 0, y | 0);
                    }
                });
            },

            // works in ff, opera and ie9
            convertCanvg: function (sourceSVG, targetCanvas) {
                // TODO: what's canvg's proposed method for getting svg string value?
                var svg_xml = this.xmlSerialize(sourceSVG);
                canvg(targetCanvas, svg_xml, { ignoreMouse: true, ignoreAnimation: true });
            },


            // WON'T WORK; canvas' origin-clean is dirty
            exportPNG: function (svg, callback) {
                var canvas = document.createElement("canvas");
                var ctx = canvas.getContext('2d');
                var img = new Image();
                // ff fails here, http://en.wikipedia.org/wiki/SVG#Native_support
                var svg_xml = this.xmlSerialize(svg);
                img.src = this.base64dataURLencode(svg_xml);

                img.onload = function() {
                    console.log("Exported image size " + img.width + "x" + img.height);
                    // ie and opera fail here for svg input, maybe it's a plan to prevent origin-dirtying?
                    ctx.drawImage(img, 0, 0);
                    // SECURITY_ERR
                    callback(canvas.toDataURL());
                };
                
                img.onerror = function() {
                    console.log(
                        "Can't export! Maybe your browser doesn't support " +
                        "SVG in img element or SVG input for Canvas drawImage?\n" +
                        "http://en.wikipedia.org/wiki/SVG#Native_support"
                    );
                }
            },

            // works nicely
            exportPNGcanvg: function (sourceSVG, callback) {
                var svg_xml = this.xmlSerialize(sourceSVG);
                
                var exportCanvas = document.createElement("canvas");    
                exportCanvas.setAttribute("style", "display: none;");
                document.body.appendChild(exportCanvas);
                
                canvg(exportCanvas, svg_xml, { ignoreMouse: true, ignoreAnimation: true });
                png_dataurl = exportCanvas.toDataURL();
                document.body.removeChild(exportCanvas);
                
                callback(png_dataurl);
            },

            exportPNGserver: function(sourceSVG, callback) {
                var svg_xml = this.xmlSerialize(sourceSVG);
                $.post(this.endpoint, { svg_xml: svg_xml }, callback);      
            },

            exportSVG: function (sourceSVG, callback) {
                var svg_xml = this.xmlSerialize(sourceSVG);
                svg_dataurl = SVGToCanvas.base64dataURLencode(svg_xml);
                callback(svg_dataurl);
            },
        };

        /**
            The missing SVG.toDataURL library for your SVG elements
            
            SVG.toDataURL( [type], [keepNonSafe=false], [keepOutsideViewport=false] )

            type    MIME type of the exported data.
                    Default: image/svg+xml.
                    Must support: image/png.
            
            [the rest of the parameters only apply when exportin image/png (or other non-svg)]

            keepNonSafe
                Export non-safe (image and foreignObject) elements.
                This will set the Canvas origin-clean property to false, if this data is transferred to Canvas.
                Default: false (to keep origin-clean true).

            keepOutsideViewport
                Export all drawn content, even if not visible.
                Default: false, export only visible viewport, similar to Canvas toDataURL().
                
            IMPLEMENTATION NOTES
            
            keepNonSafe and keepOutsideViewport are not supported at all and will be ignored.
            
            if you don't have canvg, a client-side hack¹ is attempted, 
            but this will fail on all current browsers (as of 2010-08)
            
            ¹ http://svgopen.org/2010/papers/62-From_SVG_to_Canvas_and_Back/#svg_to_canvas
        */

        SVGElement.prototype.toDataURL = function(type, keepNonSafe, keepOutsideViewport) {
            var _svg = this;
            
            function debug(s) {
                console.log("SVG.toDataURL: " + s);
            };

            function exportSVG() {
                var svg_xml = XMLSerialize(_svg);
                var svg_dataurl = base64dataURLencode(svg_xml);
                return svg_dataurl;
            };

            function XMLSerialize(svg) {

                // quick-n-serialize an SVG dom, needed for IE9 where there's no XMLSerializer nor SVG.xml
                // s: SVG dom, which is the <svg> elemennt
                function XMLSerializerForIE(s) {
                    var out = "";
                    
                    out += "<" + s.nodeName;
                    for (var n = 0; n < s.attributes.length; n++) {
                        out += " " + s.attributes[n].name + "=" + "'" + s.attributes[n].value + "'";
                    }
                    
                    if (s.hasChildNodes()) {
                        out += ">\n";

                        for (var n = 0; n < s.childNodes.length; n++) {
                            out += XMLSerializerForIE(s.childNodes[n]);
                        }

                        out += "</" + s.nodeName + ">" + "\n";

                    } else out += " />\n";

                    return out;
                };

                
                if (window.XMLSerializer) {
                    debug("using standard XMLSerializer.serializeToString");
                    return (new XMLSerializer()).serializeToString(svg);
                } else {
                    debug("using custom XMLSerializerForIE");
                    return XMLSerializerForIE(svg);
                }
            
            };


            var Base64 = {

                // private property
                _keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

                // public method for encoding
                encode : function (input) {
                    var output = "";
                    var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
                    var i = 0;

                    input = Base64._utf8_encode(input);

                    while (i < input.length) {

                        chr1 = input.charCodeAt(i++);
                        chr2 = input.charCodeAt(i++);
                        chr3 = input.charCodeAt(i++);

                        enc1 = chr1 >> 2;
                        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                        enc4 = chr3 & 63;

                        if (isNaN(chr2)) {
                            enc3 = enc4 = 64;
                        } else if (isNaN(chr3)) {
                            enc4 = 64;
                        }

                        output = output +
                        this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
                        this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

                    }

                    return output;
                },

                // public method for decoding
                decode : function (input) {
                    var output = "";
                    var chr1, chr2, chr3;
                    var enc1, enc2, enc3, enc4;
                    var i = 0;

                    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

                    while (i < input.length) {

                        enc1 = this._keyStr.indexOf(input.charAt(i++));
                        enc2 = this._keyStr.indexOf(input.charAt(i++));
                        enc3 = this._keyStr.indexOf(input.charAt(i++));
                        enc4 = this._keyStr.indexOf(input.charAt(i++));

                        chr1 = (enc1 << 2) | (enc2 >> 4);
                        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                        chr3 = ((enc3 & 3) << 6) | enc4;

                        output = output + String.fromCharCode(chr1);

                        if (enc3 != 64) {
                            output = output + String.fromCharCode(chr2);
                        }
                        if (enc4 != 64) {
                            output = output + String.fromCharCode(chr3);
                        }

                    }

                    output = Base64._utf8_decode(output);

                    return output;

                },

                // private method for UTF-8 encoding
                _utf8_encode : function (string) {
                    string = string.replace(/\r\n/g,"\n");
                    var utftext = "";

                    for (var n = 0; n < string.length; n++) {

                        var c = string.charCodeAt(n);

                        if (c < 128) {
                            utftext += String.fromCharCode(c);
                        }
                        else if((c > 127) && (c < 2048)) {
                            utftext += String.fromCharCode((c >> 6) | 192);
                            utftext += String.fromCharCode((c & 63) | 128);
                        }
                        else {
                            utftext += String.fromCharCode((c >> 12) | 224);
                            utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                            utftext += String.fromCharCode((c & 63) | 128);
                        }

                    }

                    return utftext;
                },

                // private method for UTF-8 decoding
                _utf8_decode : function (utftext) {
                    var string = "";
                    var i = 0;
                    var c = c1 = c2 = 0;

                    while ( i < utftext.length ) {

                        c = utftext.charCodeAt(i);

                        if (c < 128) {
                            string += String.fromCharCode(c);
                            i++;
                        }
                        else if((c > 191) && (c < 224)) {
                            c2 = utftext.charCodeAt(i+1);
                            string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                            i += 2;
                        }
                        else {
                            c2 = utftext.charCodeAt(i+1);
                            c3 = utftext.charCodeAt(i+2);
                            string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                            i += 3;
                        }

                    }

                    return string;
                }

            };
            
            function base64dataURLencode(s) {
                var b64 = "data:image/svg+xml;base64,";

                if (window.btoa) {
                    debug("using btoa for base64 encoding");
                    b64 += btoa(s);
                } else {
                    debug("using custom base64 encoding");
                    b64 += Base64.encode(s);
                }
                
                return b64;
            };
            
            // WON'T WORK; canvas' origin-clean is dirty
            function exportPNG() {
                var canvas = document.createElement("canvas");
                var ctx = canvas.getContext('2d');
                var img = new Image();
                var svg_xml = XMLSerialize(_svg);
                img.src = base64dataURLencode(svg_xml);

                img.onload = function() {
                    ctx.drawImage(img, 0, 0);
                    // SECURITY_ERR WILL HAPPEN NOW
                    var png_dataurl = canvas.toDataURL();
                    callback(png_dataurl);
                };
                
                img.onerror = function() {
                    console.log(
                        "Can't export! Maybe your browser doesn't support " +
                        "SVG in img element or SVG input for Canvas drawImage?\n" +
                        "http://en.wikipedia.org/wiki/SVG#Native_support"
                    );
                }

                // TODO: will not return anything
            };

            function exportPNGcanvg() {
                var canvas = document.createElement("canvas");
                var ctx = canvas.getContext('2d');

                // TODO: canvg issue don't require parentNode
                canvas.setAttribute("style", "display: none;");
                document.body.appendChild(canvas);

                var svg_xml = XMLSerialize(_svg);

                canvg(canvas, svg_xml, { ignoreMouse: true, ignoreAnimation: true });
                var png_dataurl = canvas.toDataURL();
                document.body.removeChild(canvas);

                return png_dataurl;
            };

            // BEGIN MAIN

            if (!type) type = "image/svg+xml";

            if (keepOutsideViewport) debug("keepOutsideViewport NOT supported and will be ignored!");
            if (keepNonSafe) debug("keepNonSafe is NOT supported and will be ignored!");
            
            switch (type) {
                case "image/svg+xml":
                    return exportSVG(this);
                    break;

                case "image/png":

                    if (window.canvg) {
                        debug("Using canvg for png exporting");
                        return exportPNGcanvg(this);
                    } else {
                        debug("Sorry! You don't have canvg. Using native hack for png exporting, THIS WILL FAIL!");
                        return exportPNG(this);
                    }

                    break;

                default:
                    debug("Sorry! Exporting as \"" + type + "\" is not supported!");
            }
        };
        function Remove(idOfParent,idToRemove) {
            var nav = navigator.userAgent.toLowerCase();
            var isIE = (nav.indexOf('msie') != -1) ? parseInt(nav.split('msie')[1]) : false;

            if (isIE) {
                document.getElementById(idToRemove).removeNode(true);
            } else {
                var Node1 = document.getElementById(idOfParent); 
                if (Node1 != null && Node1.childNodes != undefined) {
                    var len = Node1.childNodes.length;
                    
                    for(var i = 0; i < len; i++){           
                        if (Node1.childNodes[i] != undefined && Node1.childNodes[i].id != undefined && Node1.childNodes[i].id == idToRemove){
                            Node1.removeChild(Node1.childNodes[i]);
                        }
                    }
                } else {
                    document.body.removeChild(document.getElementById(idToRemove));
                }
            }   
        };
        function exportToPng(data_url,id,token) {

            var canvs = document.createElement('canvas');
                canvs.setAttribute('id','canvas');
                canvs.setAttribute('width',1800);
                canvs.setAttribute('height',800);
                document.body.appendChild(canvs);

            SVGToCanvas.convertCanvg(document.getElementById(id), canvas);

            var form = document.createElement('form');
                form.setAttribute('action', data_url);
                form.setAttribute('method', 'post');
                form.setAttribute('id', 'canvasToExport');
            var inputvar = document.createElement('input');
                inputvar.setAttribute('type', 'hidden');
                inputvar.setAttribute('name', 'svgToDownload');
                inputvar.setAttribute('value', canvas.toDataURL());
                form.appendChild(inputvar);
            var inputtoken = document.createElement('input');
                inputtoken.setAttribute('type', 'hidden');
                inputtoken.setAttribute('name', 'token');
                inputtoken.setAttribute('value', token);
                form.appendChild(inputtoken);
                document.body.appendChild(form);
                form.submit();
            Remove('body','canvas');
            Remove('body','canvasToExport');
        };
    <?php
        $javascript .= ob_get_clean();
        $javascript .= ($CSP_RAND != '' ? "
        </script>" : '');

        return $javascript;
    }
}
