<?php 
define('ROOT_PATH', __DIR__.DIRECTORY_SEPARATOR);
define('FILE_SEPARATOR', '-sig-');
define('DIR_ROOT', ltrim(str_replace(realpath($_SERVER['DOCUMENT_ROOT']),'',ROOT_PATH),DIRECTORY_SEPARATOR));

$includes = ROOT_PATH.'includes'.DIRECTORY_SEPARATOR;

include $includes.'functions.php';

# Sanitize Global var
$_POST = sanitize($_POST,['tags'], false);//We keep tags for textarea
$_GET  = sanitize($_GET);
$_FILES  = sanitize($_FILES);

# Initialisation du timer d'execution
define('MICROTIME', getMicrotime());

# Directory of configuration
$dirConfig = $includes.'config'.DIRECTORY_SEPARATOR;

# Security configuration
ini_set('open_basedir', ROOT_PATH.(is_dir('/tmp/') ? ':/tmp/' : '').(is_dir('/usr/lib/php/') ? ':/usr/lib/php/' : '').':/usr/sbin/sendmail' );
set_include_path(ROOT_PATH);

# Generating key for crypt and decrypt
if (!is_file($dirConfig.'sodium')) {
    if (function_exists('sodium_crypto_secretbox_keygen')) {
        $string_key = sodium_crypto_secretbox_keygen();
        $file_key = sodium_crypto_aead_chacha20poly1305_keygen();
    } else {
        throw new Exception("Update the server. PHP v 7.0 or + required", 1);
    }
    file_put_contents(
        $dirConfig.'sodium',
        base64_encode(gzdeflate(serialize($string_key.'=SIG='.$file_key))),
        LOCK_EX
    );
} else {
    $keys = explode('=SIG=',unserialize(gzinflate(base64_decode(file_get_contents($dirConfig.'sodium')))));
    $string_key = $keys[0];
    $file_key = $keys[1];
}
define('SODIUM_STRING_KEY',$string_key);
define('SODIUM_FILE_KEY', $file_key);

# Config and classes
# Because file include by "include" is not update
# To be improved when I find how to do better !!!
$config = md5(file_get_contents($dirConfig.'config.php'));
if (is_file($dirConfig.$config.'.php')) {
    include $dirConfig.$config.'.php';
} else {
    include $dirConfig.'config.php';
}

include $includes.'autoload.php';

define('APP_NAME', ($appName ?? 'Sigesth'));

$Theme = $theme;
$theme = __DIR__.DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.$theme;

# Generating key for encoding files
$J2OUuid = new \Vendors\J20Uuid();

if (!is_file($dirConfig.'uuid')) {
    $uuid = $J2OUuid->v4();
    file_put_contents($dirConfig.'uuid',$uuid,LOCK_EX);
} else {
    $uuid = file_get_contents($dirConfig.'uuid');
}
define('APP_UUID', $uuid);

# Clean up
$filesTools = new \Core\Lib\FilesTools($J2OUuid, FILE_SEPARATOR);
$filesTools->cronDel($dirConfig,['config.php','sodium','uuid','.htaccess','index.html','mysql.ini','sqlite.ini'],1);
$filesTools->cronDel($theme. '/javascript/packed',['.gitkeep'],2);
$filesTools->cronDel(__DIR__.DIRECTORY_SEPARATOR.'.ipbans', ['.htaccess','ipbans.php']);

# routing
$urlroot = (new \Core\Lib\Request())->getSchemeAndHttpHost().'/'.DIR_ROOT;
$session = new \Core\Lib\Session(new \Core\Lib\Request(), APP_NAME, $lang);

$route = ltrim(str_replace(DIR_ROOT,'',strtok($_SERVER['REQUEST_URI'], '?')), '/');
$params = strstr($_SERVER['REQUEST_URI'], '?');

$javascriptFiles = [];
$js = explode(';',$defaultJs);
foreach ($js as $key => $file) {
    $javascriptFiles[] = $theme.'/javascript/'.$file.'.js';
}
$token = new \Core\Lib\Token($session, $route, 3600);
$config = [
    'urlroot' => $urlroot, 
    'appName' => $appName,
    'theme' => $Theme,
    'lang' => $lang,
    'mediasDir' => $mediasDir,
    'dirForIcons' => $dirForIcons,
    'defaultJs' => $defaultJs,
    'emailParams' => $emailParams
];

$configuration = new \Core\Lib\Configuration($dirConfig.'config.php', $session, new \Core\Lib\CryptoTools(), $config);

$medias = new \App\Controllers\Medias($mediasDir, $Theme, $dirForIcons, $urlroot, $session, [], [], $filesTools);