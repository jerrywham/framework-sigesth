
<h2>L_Categories</h2>
<?php if ($authentication->isLoggedIn() && $authentication->getUser()->hasPermission(\Core\Lib\User::ADMIN)): ?>

<p class="txtright"><a href="/category/edit">L_Add_a_new_category</a></p>
<?php endif;?>

<?php foreach($categories as $category): ?>
<blockquote>
  <p>
  <a href="/article/list?category=<?=$category->id;?>"><?=htmlspecialchars($category->name, ENT_QUOTES, 'UTF-8')?></a>
<?php if ($authentication->isLoggedIn() && $authentication->getUser()->hasPermission(\Core\Lib\User::ADMIN)): ?>

  <a href="/category/edit?id=<?=$category->id?>">L_Edit</a>
  <form action="/category/delete" method="post">
    <input type="hidden" name="id" value="<?=$category->id?>">
    <input type="submit" value="L_Delete" class="btn">
    <input type="hidden" name="token" value="<?=$token;?>">
  </form>
<?php endif;?>

  </p>
</blockquote>

<?php endforeach; ?>
