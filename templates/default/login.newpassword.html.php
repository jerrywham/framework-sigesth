<?php if (isset($error)):?>
    <div class="errors"><?=$error;?></div>
<?php endif; ?>
<div class="grid-4 textcenter">
    <form method="post" action="">
        <p>
            <label for="email">L_Your_email_address</label>
            <input type="text" id="email" name="email" class="focus">
        </p>
        <p>
            <input type="submit" name="login" value="L_Log_in" class="btn">
            <input type="hidden" name="token" value="<?=$token;?>">
        </p>
    </form>
</div>