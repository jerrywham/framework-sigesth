<h2><?=$stringTools->t([$user->name],$LANG['L_EDIT_%s\'S_PERMISSIONS'],'');?></h2>

<form action="" method="post">
	
	<?php foreach ($permissions as $name => $value): ?>
	<p>
		<input name="permissions[]" type="checkbox" value="<?=$value?>" <?php if ($user->hasPermission($value)) echo 'checked'; ?> id="<?=$value?>"/>
		<label for="<?=$value?>" class="inbl">L_<?=ucwords(strtolower($name))?>
	</p>
	<?php endforeach; ?>

	<input type="submit" value="L_Submit" class="btn"/>
    <input type="hidden" name="token" value="<?=$token;?>">
</form>