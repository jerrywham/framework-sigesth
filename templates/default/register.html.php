<?php if (!empty($errors)): ?>
	<div class="errors">
		<p>L_Your_account_could_not_be_<?=isset($_GET['id']) ? 'updated' : 'created';?>,_please_check_the_following:</p>
		<ul>
		<?php foreach ($errors as $error): ?>
			<li><?= $error ?></li>
		<?php endforeach; 	?>
		</ul>
	</div>
<?php endif; ?>
<div class="textcenter">
<form action="" method="post">
    <p>
        <label for="email">L_Login<span class="mandatory">*</span> <small>(L_Email_address)</small></label>
        <input name="user[email]" id="email" type="text" value="<?=$userToEdit->email ?? ''?>" required="required">
        <?php if(isset($_GET['id'])) :?>

        <input name="user[old-email]" type="hidden" value="<?=$userToEdit->email ?? ''?>">
        <?php endif; ?>
        
    </p>
    <p>
        <label for="name">L_Name<span class="mandatory">*</span></label>
        <input name="user[name]" id="name" type="text" value="<?=$userToEdit->name ?? ''?>" required="required">
    </p>

    <p>
        <label for="password">L_Password<span class="mandatory">*</span><span id="password_strenght"></span></label>
        <input name="user[password]" id="password" type="password" value="" placeholder="L_12_char_minimum" title="L_char_can_be_used" required="required">
    </p>
    <?php if(isset($_GET['id'])) :?>

    <p>
        <label for="password_confirmation">L_Confirm_password<span class="mandatory">*</span></label>
        <input name="user[password_confirmation]" id="password_confirmation" type="password" value="" required="required">
    </p>
    <input type="hidden" name="user[id]" value="<?=$userToEdit->id;?>">
    <?php else: ?>
        
    <input type="hidden" name="user[id]" value="">
    <?php endif; ?>

    <input type="submit" name="submit" value="<?=(isset($_GET['id']) ? 'L_Edit' : 'L_Register_an_account');?>" class="btn">
    <input type="hidden" name="token" value="<?=$token;?>">
</form>
    <p class="mandatory"><small><i>* : L_Mandatory</i></small></p>
</div>