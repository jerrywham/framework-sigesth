<h2>L_Edit_Informations</h2>
<form action="" method="post">
    <p class="flex-container--column">
        <label for="filename"><?=($ftoken != '' ? 'L_Enter_filename' : 'L_Enter_dirname');?>:</label>
        <input type="text" id="filename" name="new" value="<?=$filename ?? ''?>" />
        <input type="hidden" name="filename" value="<?=$filename ?? ''?>">
    </p>
    <?php if ($ftoken != ''): ?>

    <p>
        <label for="visibility">L_visibility</label>
        <select name="visibility" id="visibility">
            <option value="public"<?=$fvisibility == 0 ? ' selected="selected"' : '';?>>L_public</option>
            <option value="private"<?=$fvisibility == 1 ? ' selected="selected"' : '';?>>L_private</option>
        </select>
    </p>
    <?php endif; ?>

    <p>
        <input type="submit" name="submit" value="L_Save" class="btn">
        <input type="hidden" name="token" value="<?=$token;?>">
        <input type="hidden" name="dir" value="<?=$dir;?>">
        <input type="hidden" name="action" value="<?=$action;?>">
        <input type="hidden" name="ftoken" value="<?=$ftoken;?>">
        <input type="hidden" name="fvisibility" value="<?=$fvisibility;?>">
    </p>
</form>
