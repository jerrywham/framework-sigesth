
AJOUTER SUPPRESSION EN MASSE
<div class="tabs js-tabs clearfix">
    <nav class="tabs-menu" data-tab-id="tabMedias">
        <a href="#pict" class="tabs-menu-link is-active" data-tab-id="pict">L_Pictures</a>
        <a href="#files" class="tabs-menu-link" data-tab-id="files">L_Files</a>
    </nav>

    <div class="tabs-content" data-tab-content="tabMedias">
        <div data-tab-content="pict" class="tabs-content-item">
            <div class="fr add-media">
                <a href="/medias/add<?=$pictroot;?>">L_Add_media</a>
            </div>
            <p class="fr badge--info showBubble" id="show-info1">&nbsp;?&nbsp;</p>
            <div class="bubble bubble--left" id="bubble-info1">
                <!-- <span class="close-bubble" id="close-info1">&cross;</span> -->
                <h2>L_Rename</h2>
                <p>L_To_rename_a_file_double_click_on_its_name.</p>
                <p>L_A_form_will_be_displayed.</p>
                <p>L_To_come_back_press_escape.</p>
                <h2>L_visibility</h2>
                <p>L_visibility_status_private_is_made_to_display_file_only_for_connected_users.</p>
                <span class="btn--info fr" id="close-info1">Close</span>
            </div>
            <p class="breadcrumbs"><?=$filesTools->ariane($pictroot,'medias','','pict');?></p>
            <section>
                <p><input type="search" class="light-table-filter fr" data-table="order-table-pict" placeholder="L_Filter"></p>
                <form action="/medias/add/folder" method="post">
                    <input type="text" name="newfolder" value="" placeholder="L_New_Folder">
                    <input type="hidden" name="dir" value="<?=$pictroot;?>">
                    <input type="hidden" name="token" value="<?=$token;?>">
                    <input type="submit" name="submit" value="L_Ok" class="btn--primary">
                </form>
                <table class="table table--zebra order-table-pict sortMe" id="tablePict">
                    <caption>L_Pictures</caption>
                    <thead>
                        <tr>
                            <th class="flex-container">
                                <form action="/medias/delete" method="post" class="confirmAction item-center" data-msg="L_Are_you_sure_to_delete_all_this_data_?">
                                    <select>
                                        <option value="">L_For_selection</option>
                                        <option value="">--------</option>
                                        <option value="delete">L_Delete</option>
                                    </select>
                                    <input type="submit" value="L_Ok" class="btn--warning">
                                    <input type="hidden" name="token" value="<?=$token;?>">
                                    <input type="hidden" name="idPict[]" value="">
                                    <input type="checkbox" class="checkbox" onclick="var allForms = prepareCheck(document.querySelector('#tablePict').tBodies[0].querySelectorAll('form'));checkAll(allForms, 'idPict[]', true)" />
                                </form>
                            </th>
                            <th>L_Thumb</th>
                            <th data-do-sort="1" data-sort-id="2" data-sort-direction="1">L_Name</th>
                            <th data-do-sort="1" data-sort-id="3" data-sort-direction="1">L_Mime-type</th>
                            <th data-do-sort="1" data-sort-id="4" data-sort-direction="1">L_Size</th>
                            <th>L_Dimensions</th>
                            <th data-do-sort="1" data-sort-id="6" data-sort-direction="1">L_visibility</th>
                            <th>L_Action</th>
                        </tr>
                    </thead>
                    <tbody>

                <?=$filesTools->displayFiles($mediasDir,$pictroot,$pict,'pict',$filesIcons,$dirForIcons,$token,$theme.DIRECTORY_SEPARATOR.'filesIcons'.DIRECTORY_SEPARATOR.'folder.png','png','targetBlank', 
                '<tr>                        
                        <td><form action="javascript:void(0)" method="post"><input type="checkbox" class="checkbox" name="idPict[]" value="#dir#name" /></form></td>
                        <td><a href="#path" class="#class"><img src="#thumb" alt="#name"/></a></td>
                        <td>
                            <small class="toogleRename" title="L_Double_click_for_edit">#name</small>
                            <form action="" method="post" class="rename">
                                <input type="text" name="new" value="#name" />
                                <input type="hidden" name="filename" value="#name">
                                <div class="txtleft">
                                    L_visibility :<br/>
                                    <input type="radio" idfora class="radio" name="visibility" value="1"#cvisibilityTrue>&nbsp;<label class="small" forida>L_public</label>
                                    <input type="radio" idforb class="radio" name="visibility" value="0"#cvisibilityFalse>&nbsp;<label class="small" foridb>L_private</label>
                                </div>
                                <div class="hide">
                                    <input type="hidden" name="token" value="#token">
                                    <input type="hidden" name="dir" value="#dir">
                                    <input type="hidden" name="action" value="rename">
                                    <input type="hidden" name="ftoken" value="#ftoken">
                                    <input type="hidden" name="fvisibility" value="#fvisibility">
                                </div> 
                                    <span class="escape">Annuler</span>&nbsp;<input type="submit" value="L_Ok" class="btn--primary">
                            </form>
                        </td>
                        <td>#mime-type</td>
                        <td>#size</td>
                        <td>#dimensions</td>
                        <td>#visibility</td>
                        <td class="td-form"><form action="/medias/delete" method="post" class="confirmAction" data-msg="L_Are_you_sure_to_delete «#name» ?"><input type="hidden" name="dir" value="#dir"><input type="hidden" name="file" value="#file"><input type="submit" value="L_Delete" class="btn--danger"><input type="hidden" name="token" value="#token"></form></td>
                    
                        </tr>');?>
                        
                    </tbody>
                </table>

            </section>
        </div>
        <div data-tab-content="files" class="tabs-content-item">
            <div class="fr add-media">
                <a href="/medias/add<?=$filesroot;?>">L_Add_media</a>
            </div>
            <p class="breadcrumbs"><?=$filesTools->ariane($filesroot,'medias','','files');?></p>
            <section>
                <p><input type="search" class="light-table-filter fr" data-table="order-table-files" placeholder="L_Filter"></p>
                <form action="/medias/add/folder" method="post">
                    <input type="text" name="newfolder" value="" placeholder="L_New_Folder">
                    <input type="hidden" name="dir" value="<?=$filesroot;?>/">
                    <input type="hidden" name="token" value="<?=$token;?>">
                    <input type="submit" name="submit" value="L_Save" class="btn">
                </form>
                <table class="table table--zebra order-table-files sortMe" >
                    <caption>L_Files</caption>
                    <thead>
                        <tr>
                            <th>L_Thumb</th>
                            <th data-do-sort="1" data-sort-id="1" data-sort-direction="1">L_Name</th>
                            <th>L_Mime-type</th>
                            <th>L_Size</th>
                            <th>L_visibility</th>
                            <th>L_Action</th>
                        </tr>
                    </thead>
                    <tbody>
                <?=$filesTools->displayFiles($mediasDir,$filesroot.'/', $files, 'files',$filesIcons,$dirForIcons,$token,$theme.DIRECTORY_SEPARATOR.'filesIcons'.DIRECTORY_SEPARATOR.'folder.png','png','targetBlank',
                '<tr>
                        <td><a href="#path" class="#class">#img</a></td>
                        <td>
                            <small class="toogleRename" title="L_Double_click_for_edit">#name</small>
                            <form action="" method="post" class="rename">
                                <input type="text" name="new" value="#name" />
                                <input type="hidden" name="filename" value="#name">
                                <div class="txtleft">
                                    L_visibility :<br/>
                                    <input type="radio" idforc class="radio" name="visibility" value="1"#cvisibilityTrue>&nbsp;<label class="small" foridc>L_public</label>
                                    <input type="radio" idford class="radio" name="visibility" value="0"#cvisibilityFalse>&nbsp;<label class="small" foridd>L_private</label>
                                </div>
                                <div class="hide">
                                    <input type="hidden" name="token" value="#token">
                                    <input type="hidden" name="dir" value="#dir">
                                    <input type="hidden" name="action" value="rename">
                                    <input type="hidden" name="ftoken" value="#ftoken">
                                    <input type="hidden" name="fvisibility" value="#fvisibility">
                                </div> 
                                    <span class="escape">Annuler</span>&nbsp;<input type="submit" value="L_Ok" class="btn--primary">
                            </form>
                        </td>
                        <td>#mime-type</td>
                        <td>#size</td>
                        <td>#visibility</td>
                        <td class="td-form"><form action="/medias/delete" method="post" class="confirmAction" data-msg="L_Are_you_sure_to_delete «#name» ?"><input type="hidden" name="dir" value="#dir"><input type="hidden" name="file" value="#file"><input type="submit" value="L_Delete" class="btn"><input type="hidden" name="token" value="#token"></form></td>
                    </tr>');?>
                        
                    </tbody>
                </table>

            </section>
        </div>
    </div>
</div>
