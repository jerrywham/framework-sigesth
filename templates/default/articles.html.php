<div class="articlelist">
        <ul class="categories">
        <?php foreach($categories as $category): ?>

            <li><a href="/article/list?category=<?=$category->id?>"><?=$category->name?></a><li>
        <?php endforeach; ?>

        </ul>
        
        <div class="articles">

            <p><?=$stringTools->t(['total' => $total],'L_article_have_been_submitted_to_the','L_articles_have_been_submitted_to_the');?> L_Application<?=(($categoryName)? ', L_in_category "'.$categoryName.'"' : '');?>.</p>


            <?php foreach($articles as $article): ?>
                <h2><?= $article->articletitle ?></h2>
                <?php if ($user): ?>
                    <?php if ($user->id == $article->authorId || $user->hasPermission(\Core\Lib\User::EDIT)): ?>

                        <a href="/article/edit?id=<?=$article->id?>">L_Edit</a></p>
                    <?php endif; ?>
                <?php endif; ?>
            <blockquote>
                <?=(new \Core\Lib\Markdown($article->articletext))->toHtml(true)?>

                <p>(L_by <a href="mailto:<?=(is_object($article->getAuthor()) ? htmlspecialchars($article->getAuthor()->email, ENT_QUOTES,
                'UTF-8') : ''); ?>">
                <?=(is_object($article->getAuthor()) ? htmlspecialchars($article->getAuthor()->name, ENT_QUOTES,
                'UTF-8') : ''); ?></a> L_on 
                <?=$article->articledate;?>)

                <?php if ($user): ?>
                    <?php if ($user->id == $article->authorId || $user->hasPermission(\Core\Lib\User::DELETE)): ?>

                        <form action="/article/delete" method="post" class="confirmAction" data-msg="L_Are_you_sure_?">
                            <input type="hidden" name="id" value="<?=$article->id?>">
                            <input type="submit" value="L_Delete" class="btn">
                            <input type="hidden" name="token" value="<?=$token;?>">
                        </form>
                    <?php endif; ?>
                <?php else: ?>

                </p>
                <?php endif; ?>
                <?php foreach ($article->getCategories() as $key => $cat) :?>

                    <?=$key==0 ? 'L_Record_in : ' : '';?><a href="/article/list?category=<?=$cat->id;?>" class="tag"><?=$cat->name;?></a>
                    
                <?php endforeach; ?>

            </blockquote>
            <?php endforeach; ?>


            <p class="pagination">L_Select_page: <?=$paginator->make(new $pagination($total, $currentPage, $elementsByPage, array('category' => $categoryId)))->displayPagination('/article/list','badge--info','badge--primary');?></p>
            
            <?php $hooks->callHook('\Plugins\php_graph\php_graph', 'addGraph', array(
                'data' => [
                    '2000' => 7,
                    '2001' => 15,
                    '2002' => 39,
                    '2003' => 26,
                    '2004' => 36,
                    '2005' => 18,
                    '2006' => 32,
                    '2007' => 56,
                    '2008' => 38,
                    '2009' => 103,
                    '2010' => 104,
                    '2011' => 126,
                    '2012' => 123,
                    '2013' => 75,
                    '2014' => 47,
                    '2015' => 62,
                    '2016' => 49,
                    '2017' => 16,
                ],
                'options' => [
                    'type' => 'line',
                    'filled' => true,
                    // 'transform' => -90,
                    // 'legends' => [
                    //     '2000',
                    //     '2001',
                    //     '2002',
                    //     '2003',
                    //     '2004',
                    //     '2005',
                    //     '2006',
                    //     '2007',
                    //     '2008',
                    //     '2009',
                    //     '2010',
                    //     '2011',
                    //     '2012',
                    //     '2013',
                    //     '2014',
                    //     '2015',
                    //     '2016',
                    //     '2017'
                    // ],
                    // 'paddingLegendY' => 60
                ],
                'id' => md5(time()),
                'token' => $token ?? null
            )); ?>

        </div>
    </div>
