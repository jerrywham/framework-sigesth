<h2>L_Configuration_page </h2>
<?php if (!empty($errors)): ?>
    <div class="errors">
        <p>L_Error , L_please_check_the_following:</p>
        <ul>
        <?php foreach ($errors as $error): ?>
            <li><?= $error ?></li>
        <?php endforeach;   ?>
        </ul>
    </div>
<?php endif; ?>
<h3>L_General_configuration</h3>
<form action="" method="post" class="large">
    
    <p>
        <label for="appName">L_AppName (<span class="mandatory">*</span>)</label>
        <input type="text" name="appName" id="appName" value="<?=$appName;?>" required="required">
    </p>
    <p>
        <label for="ctheme">L_Theme (<span class="mandatory">*</span>)</label>
        <input type="text" name="ctheme" id="ctheme" value="<?=$ctheme;?>" required="required">
    </p>
    <p>
        <label for="lang">L_Lang (<span class="mandatory">*</span>)</label>
        <select name="lang" id="lang" required="required">
            <?php foreach ($languages as $key => $value):?>

            <option value="<?=$value;?>"<?=($value == $language ? ' selected="selected"' : '');?>><?=$value;?></option>
            <?php endforeach; ?>

        </select>
    </p>
    <p>
        <label for="mediasDir">L_Medias_directory (<span class="mandatory">*</span>)</label>
        <input type="text" name="mediasDir" id="mediasDir" value="<?=$mediasDir;?>" required="required">
    </p>
    <p>
        <label for="dirForIcons">L_Icons_directory_in_theme (<span class="mandatory">*</span>)</label>
        <input type="text" name="dirForIcons" id="dirForIcons" value="<?=$dirForIcons;?>" required="required">
    </p>
    <p>
        <label for="defaultJs">L_default_javascript (<span class="mandatory">*</span>)</label>
        <input type="text" name="defaultJs" id="defaultJs" value="<?=$defaultJs;?>" required="required">
    </p>
    <h3>L_Database</h3>

    <p>
        <label for="dbType">L_Type (<span class="mandatory">*</span>)</label>
        <select name="dbType" id="dbType" required="required">
            <option value=""></option>
            <option value="mysql"<?=($dbType=='mysql' ? ' selected="selected"' : '');?>>mysql</option>
            <option value="sqlite"<?=($dbType=='sqlite' ? ' selected="selected"' : '');?>>sqlite</option>
        </select>
    </p>

    <h3>L_Email_params</h3>
    <?php foreach ($emailParams as $key => $value):?>

    <p>
        <label for="emailParams[<?=$key;?>]">L_<?=$key;?> (<span class="mandatory">*</span>)</label>
        <input type="text" name="emailParams[<?=$key;?>]" id="emailParams[<?=$key;?>]" value="<?=$value;?>" required="required">
    </p>
    <?php endforeach; ?>

    <p>
        <input type="submit" name="submit" value="L_Save" class="btn">
        <!-- token is mandatory to post data -->
        <input type="hidden" name="token" value="<?=$token;?>">
    </p>
    <p class="mandatory"><small><i>* : L_Mandatory</i></small></p>
</form>