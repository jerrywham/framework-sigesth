<h2>L_Manage_plugins</h2>
<form action="" method="post">
    <p><input type="submit" value="L_Submit" class="btn"/></p>
    <table class="table table--zebra" summary="L_All_informations_about_the_plugins" id="plugins-table">
        <caption>L_List_of_plugins</caption>
        <colgroup>
            <col class="w1">
            <col class="w39">
            <col class="w60">
        </colgroup>
        <thead>
            <tr>
                <th><input type="checkbox" class="switch" id="checkAll"/></th>
                <th><input type="text" id="plugins-search" placeholder="L_Search..." title="L_Search" /></th>
                <th scope="col">L_Description</th>
                <th scope="col">L_Configuration</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($plugins as $key => $plugin) :?>
                
                <tr>
                    <td>
                        <input type="hidden" name="plugName[]" value="<?=$plugin['title'];?>" />
                        <input type="checkbox" name="chkAction[]" value="<?=$plugin['title'];?>" class="switch" <?=($plugin['enabled'] ? ' checked="checked"' : '');?>>
                    </td>
                    <td data-label="<?=$plugin['title'];?>">
                        <img src="<?=$plugin['icon'];?>" alt="<?=$stringTools->t([$plugin['title']],'L_%s\'S_ICON','');?>" width="64"/>
                        <br/><br/><strong class="uppercase"><?=$plugin['title'];?></strong>
                        <div class="txtleft">
                            <br/><small>L_Version : <?=$plugin['version'];?></small>
                            <br/><small>L_UpdateOn : <?=$plugin['updateOn'];?></small>
                            <br/><small>L_Author : <?=($plugin['contact'] != '' ? '<a href="mailto:'.$plugin['contact'].'">'.$plugin['author'].'</a>' : $plugin['author']);?></small>
                            <br/><small>L_Compatibility : <?=$plugin['compatibility'];?></small>
                            <br/><small><?=($plugin['depot'] != '' ? '<a href="'.$plugin['depot'].'" title="'.$plugin['depot'].'" class="targetBlank">L_Depot</a>' : 'L_Depot');?></small>
                        </div>
                    </td>
                    <td><?=$plugin['description'];?></td>
                    <td><?=($plugin['configuration'] && $plugin['enabled'] ? '<a href="/plugin/configuration/'.$plugin['title'].'">L_Params</a>' : '');?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <input type="submit" value="L_Submit" class="btn"/>
    <input type="hidden" name="token" value="<?=$token;?>">
</form>
<p></p>