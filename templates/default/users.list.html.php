<div class="clearfix">
	<p class="fr"><a href="/users/register">L_Add_user</a></p>
</div>
<h2>L_User_List</h2>
<div class="clearfix">
	<p>
		<input type="search" class="light-table-filter fr" data-table="order-table" placeholder="L_Filter">
	</p>
</div>
<p>&nbsp;</p>
<table class="table table--zebra order-table">
	<thead>
		<tr>
			<th>L_Name</th>
			<th>L_Email</th>
			<th colspan="2">L_Edit</th>
		</tr>
	</thead>

	<tbody>
		<?php foreach ($users as $user): ?>
		<tr>
			<td><?=$user->name;?></td>		
			<td><?=$user->email;?></td>
			<td><a href="/users/permissions?id=<?=$user->id;?>">L_Edit_Permissions</a></td>
			<td><a href="/users/edit?id=<?=$user->id;?>">L_Edit_Informations</a></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>