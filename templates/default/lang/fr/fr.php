<?php
# Specific of this App
$LANG = array(
    ## Classes
    # Register
    'L_Author_List' => 'Liste des auteurs',
    # Article
    'L_Edit_article' => 'Modifier un article',
    'L_Edit_articles' => 'Modifier les articles',
    # Category
    'L_Edit_Category' => 'Modifier une catégorie',
    'L_Edit_Categories' => 'Modifier les catégories',
    'L_Article_Categories' => 'Categories des articles',
    # AppPagination
    'L_first' => 'première',
    'L_last' => 'dernière',
    'L_previous' => 'précédente',
    'L_next' => 'suivante',
    ##############
    ## Views
    # users.list.html.php
    # categories.html.php
    'L_Categories' => 'Catégories',
    'L_Add_a_new_category' => 'Ajouter une nouvelle categorie',
    # category.edit.html.php
    'L_Enter_category_name' => 'Nom de la catégorie ',
    # article.edit.html.php
    'L_Type_your_article_here' => 'Texte de l&#039;article ',
    'L_Select_categories_for_this_article' => 'Sélectionner la catégorie pour cet article',
    'L_You_may_only_edit_articles_that_you_posted.' => 'Vous ne devriez modifier que les articles que vous avez postés.',
    # home.html.php
    'L_Welcome_to_the' => 'Bienvenue',
    'L_Application' => 'Framework SiGesTH',
    # articles.html.php
    'L_article_have_been_submitted_to_the' => '%d article a été enregistré dans le',
    'L_articles_have_been_submitted_to_the' => '%d articles ont été enregistrés dans le',
    'L_in_category' => 'dans la catégorie',
    'L_Select_page' => 'Pagination ',
    'L_id' => 'Index',
    'L_Date' => 'Date',
    'L_Category' => 'Catégorie',
    'L_Comments' => 'Commentaires',
    'L_For_selection' => 'Pour la sélection',
    # layout.menu.html.php
    'L_Articles_list' => 'Liste des articles',
    'L_Add_a_new_article' => 'Ajouter un nouvel article',
    # login.html.php
    'L_Your_email_address' => 'Votre adresse email',
    'L_Your_password' => 'Votre mot de passe',
    # permissions.html.php
    'L_EDIT_%s\'S_PERMISSIONS' => 'Permissions de "%s"',
    # register.success.html.php
    'L_You_are_now_registered_on_the_framework' => 'Vous êtes maintenant enregistré dans la base de données du Framework',
    # medias.html.php
    'L_Double_click_for_edit' => 'Double-clic pour modifier la visibilité',
);