<?php if (!empty($errors)): ?>
    <div class="errors">
        <p>L_Your_account_could_not_be_updated,_please_check_the_following:</p>
        <ul>
        <?php foreach ($errors as $error): ?>
            <li><?= $error ?></li>
        <?php endforeach;   ?>
        </ul>
    </div>
<?php endif; ?>
<div class="textcenter">
<form action="" method="post">
    <p>
        <label for="email">L_Login<span class="mandatory">*</span> <small>(L_Email_address)</small></label>
        <input name="user[email]" id="email" type="text" value="<?=$user->email ?? ''?>" required="required">
    </p>
    <p>
        <label for="password">L_Password<span class="mandatory">*</span><span id="password_strenght"></span></label>
        <input name="user[password]" id="password" type="password" value="<?=($user->password ?? '')?>" placeholder="L_12_char_minimum" required="required">
    </p>
    <input type="submit" name="submit" value="L_Edit" class="btn">
    <input type="hidden" name="token" value="<?=$token;?>">
</form>
    <p class="mandatory"><small><i>* : L_Mandatory</i></small></p>
</div>