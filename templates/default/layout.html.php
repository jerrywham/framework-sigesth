<!doctype html>
<html lang="<?=$lang;?>">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1" />
		<link rel="stylesheet" href="/assets/css/style.css">
		<title><?=$title?></title>
		<!-- FAVICONS -->
		<link href="/assets/favicons/favicon.png" type="image/x-icon" rel="icon" />
		<link href="/assets/favicons/favicon.png" type="image/x-icon" rel="shortcut icon" />
		<link href="/assets/favicons/favicon.png" type="image/apple-touch-icon" rel="apple-touch-icon" />
		<!-- !FAVICONS -->
	</head>
	<body>
		<?=$alert;?>

		<header>
			<h1><?=$mainTitle;?></h1>
		</header>
		<div id="ligne"></div>
		<aside class="nav-wrapper">
			<?=$menu;?>

		</aside>		
		<!-- /sidebar -->

		<div class="wrapper<?=(isset($loginPage) ? 'login' : '');?>">
			<main>

			<?=$output?>

			</main>

		</div>
		<!-- /wrapper -->
			<footer class="clearfix">
				<div class="fl"><a href="https://framagit.org/jerrywham/framework-sigesth">framework-sigesth</a>&copy; !Pragmagiciels 2018 in <?php echo round(getMicrotime()-MICROTIME,3).'s'; ?>.</div> 
				<div class="fr"><?=$authentication->msg();?></div>
			</footer>

		<?php if($javascript): ?>
			<?php $hooks->callHook('\Plugins\jquery\jquery','addJquery'); ?>

			<script src="/assets/javascript/<?=(new \Core\Lib\Js($javascript, 'Vendors\JavaScriptPacker', $theme))->pack((isset($noMinification) ? false : true));?>" nonce="<?=CSP_RAND?>"></script>
		<?php endif; ?>

		<?php if($javascriptInline): ?>

			<script type="text/javascript" nonce="<?=CSP_RAND?>">
			<?=$javascriptInline;?>

			</script>
		<?php endif; ?>

		<?php $hooks->callHook('\Plugins\sample\sample','sampleHook',[$_SESSION]); ?>
		<?php $hooks->callHook('\Plugins\sample\sample','sampleHook',['L_var_defined_in_"layout.html.php"_template_of_default_theme,_called_by_hook_"sampleHook"_method._The_result_is_displayed_on_every_pages']); ?>

		<?= file_get_contents(__DIR__.DIRECTORY_SEPARATOR.'svg'.DIRECTORY_SEPARATOR.'sprite.svg'); ?>
	
	</body>
</html>