;(function(window,undefined){

    'use strict'; 

    /*
     * inpt = input
     * l = min length (12 by default)
     * s = array of msg (4 msg)
     * c = base color    
     */
    function pwdStrength(inpt, l, s, colors, bgOrigin, weak) {
        if (isNaN(l) || l == 0) l = 12;
        // colors: white = empty, red = very weak, orange = weak, yellow = good, green = strong
        if (colors==undefined || colors.length != 5) colors = ['#ffff', '#ff0000', '#ff9900', '#ffcc00', '#33cc33'];
        if (weak == undefined) weak = false;
        var val = inpt.value;
        var no=0;
        var regex = /.[!,@,#,$,%,^,&,*,?,_,~,-,(,)+,/,°,`,£,€,',§,ç,é,è,ù,à,=,:,;, ]/;
        // If the password length is less than l
        if(val.length>0 && val.length<l) no=1;
        // If the password length is greater than l and contain any lowercase alphabet or any number or any special character
        if(val.length>=l && (val.match(/[a-z]/) || val.match(/[A-Z]/) || val.match(/\d+/) || val.match(regex))) no=2;
        // If the password length is greater than l and contain any lowercase alphabet or any uppercase alphabet or any number or any special character
        if(val.length>=l && (val.match(/[a-z]/) && val.match(/[A-Z]/) || val.match(/\d+/) || val.match(regex))) no=2;
        // If the password length is greater than l and contain alphabet,number,special character respectively
        if((val.length>=l && ((val.match(/[a-z]/) && val.match(/[A-Z]/) && val.match(/\d+/)) || (val.match(/\d+/) && val.match(regex)) || (val.match(/[a-z]/) && val.match(/[A-Z]/) && val.match(regex))))) {
            if (weak === true) {no=4;}
            else {
                no=3;
                // If the password length is greater than l and must contain alphabets,numbers and special characters
                if(val.length>=l && val.match(/[a-z]/) && val.match(/[A-Z]/) && val.match(/\d+/) && val.match(regex)) no=4;
            }
        }
        // Change password background color
        inpt.style.backgroundColor=colors[no];
        if (val==''){inpt.style.backgroundColor=bgOrigin;}
        // Change label strenght password
        var id = inpt.id;
        var pwdstr=document.getElementById(id+'_strenght');
        if (pwdstr != null) {
            pwdstr.innerHTML='';
            if(no>0){pwdstr.innerHTML=s[no-1]};
        }
    }

    window.pwdStrength = pwdStrength;

})(window);

var pwdInputs = document.querySelectorAll('input[type=password]');
var nbPwd = pwdInputs.length;
for (var i = 0; i < nbPwd; i++) {
    pwdInputs[i].addEventListener('keyup',function() {
        pwdStrength(this,12,['L_Too_short','L_Too_weak','L_Weak','L_Good','L_Strong'],['rgba(255, 255, 255, 0.1)','rgba(255, 0, 0, 0.4)','rgba(255, 153, 0, 0.4)','rgba(255, 204, 0, 0.4)','rgba(51, 204, 51, 0.4)'])}, false);
}