/*
# ------------------ BEGIN LICENSE BLOCK ------------------
#
# This file is part of SIGesTH
#
# Copyright (c) 2009 - 2014 Cyril MAGUIRE, <contact@ecyseo.net>
# Licensed under the CeCILL v2.1 license.
# See http://www.cecill.info/licences.fr.html
#
# ------------------- END LICENSE BLOCK -------------------
*/
;(function(window,undefined){
    
    'use strict';

    var isIE = isBrowserIE();
    function isBrowserIE() {
        var nav = navigator.userAgent.toLowerCase();
        return (nav.indexOf('msie') != -1) ? parseInt(nav.split('msie')[1]) : false;
    }

    /* addEvent — cette fonction écrite par John Resig , à l’origine de jQuery , a gagné le concours addEvent() recoding contest . Elle permet tout simplement d’attacher une fonction à un événement (onload, onclick, onmouseover, etc) :*/
    function addEvent( obj, type, fn ) {
        if ( obj.attachEvent ) {
            obj['e'+type+fn] = fn;
            obj[type+fn] = function(){obj['e'+type+fn]( window.event );};
            obj.attachEvent( 'on'+type, obj[type+fn] );
        } else {
            obj.addEventListener( type, fn, false );
        }
    }

    /* En prime, voici la fonction inverse, au cas où :*/
    function removeEvent( obj, type, fn ) {
        if ( obj.detachEvent ) {
            obj.detachEvent( 'on'+type, obj[type+fn] );
            obj[type+fn] = null;
        } else {
            obj.removeEventListener( type, fn, false );
        }
    }

    /* Visual Effects */
    function setOpacity(obj, opacity) {
        obj.style.minHeight = obj.style.minHeight; // hack IE
        opacity = (opacity == 100)?99.999:opacity;
        obj.style.filter = "alpha(opacity="+opacity+")"; // IE/Win
        obj.style.KHTMLOpacity = opacity/100; // Safari<1.2, Konqueror
        obj.style.MozOpacity = opacity/100; // Older Mozilla and Firefox
        obj.style.opacity = opacity/100; // Safari 1.2, newer Firefox and Mozilla, CSS3
    }
    function fadeOut(objId,opacity) {
        var obj = document.getElementById(objId);
        var stop = document.getElementById('noToogle');
        if(obj && !stop) {
            if(opacity==undefined) {
                window.setTimeout("fadeOut('"+objId+"',"+100+")", 2000);
            } else {
                if (opacity >=0) {
                    setOpacity(obj, opacity);
                    opacity -= 10;
                    window.setTimeout("fadeOut('"+objId+"',"+opacity+")", 100);
                } else {
                    obj.style.display = 'none';
                }
            }
        }
    }
    function setMsg(msg) {
        if(document.getElementById(msg)) {

            objDiv = document.getElementById(msg);
            objSidebar = document.getElementById('sidebar');
            if (typeof window.innerWidth != 'undefined') {
                wndWidth = window.innerWidth;
            }
            else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth !='undefined' && document.documentElement.clientWidth != 0) {
                wndWidth = document.documentElement.clientWidth;
            }
            else {
                wndWidth = document.body.clientWidth;
            }
            xpos = Math.round((wndWidth-objDiv.offsetWidth)/2);
            objDiv.style.left=xpos+'px';
            fadeOut(msg);
        }
    }

    function fadeIn(el) {
        var opacity = 0;

        el.style.opacity = 0;
        el.style.filter = '';

        var last = +new Date();
        var tick = function() {
            opacity += (new Date() - last) / 400;
            el.style.opacity = opacity;
            el.style.filter = 'alpha(opacity=' + (100 * opacity)|0 + ')';

            last = +new Date();

            if (opacity < 1) {
                (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16);
            }
        };

        tick();
    }

    function when_ready(func){
        document.addEventListener("DOMContentLoaded",func,false);
    }

    function hide(elem) {
        var Elem = document.getElementById(elem);
        if (Elem != null || Elem != undefined) {
            Elem.style.display = 'none';
        }
    }
    function show(elem) {
        var Elem = document.getElementById(elem);
        if (Elem != null || Elem != undefined) {
            Elem.style.display = 'block';
            fadeIn(Elem);
        }
    }
    function toogle(elem,obj) {
        if (obj == undefined) {
            var Elem = elem.target.parentElement;
            if (Elem.children[1].style.display == 'none') {
                if (elem.target.innerHTML == '+') {
                    elem.target.innerHTML = '-';
                }
                Elem.children[1].style.display = 'block';
            } else {
                if (elem.target.innerHTML == '-') {
                    elem.target.innerHTML = '+';
                }
                Elem.children[1].style.display = 'none';
            }
                
        } else {
            var marqueur = obj.innerHTML;   
            var Elem = document.getElementById(elem);
            if (Elem != null || Elem != undefined) {
                if (Elem.style.display == 'none') {
                    if (marqueur == '+') {
                        obj.innerHTML = '-';
                    }
                    show(elem);
                } else {
                    if (marqueur == '-') {
                        obj.innerHTML = '+';
                    }
                    hide(elem);
                }
            }
        }
    }

    function displayMenu(mainId) {
        var menu = document.getElementById(mainId);
        var subMenu = [];
        for (var i = 0; i < menu.childElementCount; i++) {
            if (menu.children[i].className != undefined && menu.children[i].className == 'toogleSubMenu') {
                for (var j = 0; j < menu.children[i].childElementCount; j++) {
                    menu.children[i].children[1].style.display = 'none';
                    menu.children[i].children[0].style.cursor = 'pointer';
                    menu.children[i].children[0].addEventListener('click', toogle,false);
                }
                    
            }
        }
    }

    function resetRequire(elm) {
        var Elm = document.getElementById(elm);
        if (Elm.getAttribute('required') == 'required') {
            Elm.removeAttribute('required');
        }
        if (elm.indexOf('date') != -1) {
            Elm.value = '00/00/0000';
        } else {
            Elm.value = 'NA';
        }
    }
    function activeRequire(elm) {
        var Elm = document.getElementById(elm);
        if (Elm.getAttribute('required') == null) {
            Elm.setAttribute('required','required');
        }
        //Elm.value = '';
    }

    var animations = {
            init: function(elem) {
                if (elem != null) {
                    var list = document.getElementById(elem);
                    if (list !== null ) {
                        this.addEventListener(list,"change", this);
                    }
                }
            },
            addEventListener: function(el, eventName, handler) {
                if (el.addEventListener) {
                    el.addEventListener(eventName, handler, false);
                } else {
                    el.attachEvent('on' + eventName, animations.handleEvent);
                }
            },
            handleEvent: function(e) {
                if (isIE) {
                    var obj = null;
                    obj = animations;
                } else {
                    var obj = Object.create(null);
                    obj = this;
                }
                var evt = e ? e:event;
                obj.action(e);
            },
            action: function(a) {
                if (isIE) {
                    var obj = null;
                    obj = object;
                } else {
                    var obj = Object.create(null);
                    obj = this;
                }
                var elementSelected = a.target;
                var visite = document.getElementById('id_visite');

                if (elementSelected.type == 'checkbox') {
                    var etiquette = document.getElementById('id_date_reetiquetage');
                    if (elementSelected.checked === false) {
                        hide('ifRelabeling');
                        etiquette.value = '00/00/0000';
                    } else {
                        show('ifRelabeling');
                        etiquette.value = '';
                    }
                } else {
                    if (elementSelected.value == 0) {
                        displayForm('ifNewLot');
                    }
                    if (elementSelected.value == 1) {                                  
                        visite.value = '';  
                        displayForm('ifDelivery');
                    }
                    if (elementSelected.value == 2) {
                        displayForm('ifReturn');
                    }
                        
                    // if (elementSelected.value == 0) {      
                    //  document.getElementById('id_nom').value = '';                
                    //  document.getElementById('id_dosage').value = '';              
                    //  document.getElementById('id_temperature_conservation').value = '';              
                    //  document.getElementById('id_date_reetiquetage').value = '';              
                    //  document.getElementById('id_date_expiration').value = '';
                    // } else {    
                    //  document.getElementById('id_nom').value = 'NA';                
                    //  document.getElementById('id_numero_conditionnement').value = 'NA';              
                    //  document.getElementById('id_numero_lot').value = 'NA';              
                    //  document.getElementById('id_unite').value = 'NA';              
                    //  document.getElementById('id_dosage').value = 'NA';              
                    //  document.getElementById('id_temperature_conservation').value = 'NA';              
                    //  document.getElementById('id_date_reetiquetage').value = '00/00/0000';              
                    //  document.getElementById('id_date_expiration').value = '00/00/0000';  
                    // }
                }
                return true;
            }
        };

    window.addEvent = addEvent;
    window.removeEvent = removeEvent;
    window.setOpacity = setOpacity;
    window.fadeOut = fadeOut;
    window.setMsg = setMsg;
    window.hide = hide;
    window.show = show;
    window.toogle = toogle;
    window.animations = animations;
    window.when_ready = when_ready;
    window.displayMenu = displayMenu;

})(window);