let matches = document.body.querySelectorAll('form.confirmAction');
let nbConfirm = matches.length;
for (var i = 0; i < nbConfirm; i++) {
    var inpt = matches[i].querySelectorAll('input[type="submit"]');
    for (var j = 0, nb = inpt.length; j < nb; j++) {
        inpt[j].addEventListener('click', function(e) {
            e.preventDefault(); 
            e.returnValue = false;
            var msg = this.form.dataset.msg;
            if(confirm(msg)) {
                this.form.submit();
            }
        });
    }
}