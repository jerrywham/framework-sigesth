;(function(window,undefined){
    
    'use strict';

    function focusOnLogin() {
        var login = document.querySelectorAll('.focus');
        if (login[0] != undefined) {
            login[0].focus();
        }
    }

    window.focusOnLogin = focusOnLogin;

})(window);

addEvent( window, 'load', focusOnLogin );
