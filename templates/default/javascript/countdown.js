function zeroLeft(number) {
    return (number < 10 ? '0' : '') + number;
}
// 1000 ms = 1000 = 1 s
// 1000 * 60 ms = 60000 = 1 m
// 1000 * 60 * 60 ms = 3600000 = 1 h
// 1000 * 60 * 60 * 24 ms = 86400000 = 1 d
var currenturl = window.location.href;
var rep = currenturl.substring(0, currenturl.lastIndexOf('/'));
var countdown = (1000 * 60 * 60);
(function(  o, //milliseconds to countdown
            i,  //interval to check and fire callback
            f) //callback function.  Number of milliseconds left will be passed through
{
    var g='getTime',                //cache getTime method name     
        z=o=o+new Date()[g](),      //record the time to "stop" the countdown
        b = function(){ 
            z=o-new Date()[g]();    //set remaining milliseconds 
            if (z+i>=0){            //prevent recursion if z is greater than 0... but allow it to fire one more time
                f(z);               //fire callback
                setTimeout(b, i);   //recurse
            } else {
                f('Time out');
            }
        };
    b();                            //start countdown
})(countdown , 500, function(t){
    if (t == 'Time out') {
        document.getElementById('countdown').innerHTML = 'Time out';
        window.location.href = rep + '/logout';
    } else {
        var future = (new Date().getMilliseconds()) + t;
        var h = parseInt((new Date(future).getMinutes())/60);
        var m = new Date(future).getMinutes();
        var s = new Date(future).getSeconds();
        var countdown = zeroLeft(h) + ':' + zeroLeft(m) + ':' + zeroLeft(s);   
        document.getElementById('countdown').innerHTML = countdown;
    }
});