/*
# ------------------ BEGIN LICENSE BLOCK ------------------
#
# This file is part of SIGesTH
#
# Copyright (c) 2009 - 2018 Cyril MAGUIRE, <contact@ecyseo.net>
# Licensed under the CeCILL v2.1 license.
# See http://www.cecill.info/licences.fr.html
#
# ------------------- END LICENSE BLOCK -------------------
*/
/*
HTML structure
***********************************************************
    <nav class="tabs-menu" data-tab-id="tabId">
        <a href="#tabA" class="tabs-menu-link is-active" data-tab-id="tabA">TAB A</a>
        <a href="#tabB" class="tabs-menu-link" data-tab-id="tabB">TAB B</a>
    </nav>

    <div class="tabs-content" data-tab-content="tabId">
        <div data-tab-content="tabA" class="tabs-content-item">
        </div>
        <div data-tab-content="tabB" class="tabs-content-item">
        </div>
    </div>
*/
;(function(window,undefined){
    
    'use strict';
    
    // class helper functions from bonzo https://github.com/ded/bonzo
    var regExp = function(name) {
        return new RegExp("(^|\\s+)" + name + "(\\s+|$)");
    };

    // classList support for class management
    // altho to be fair, the api sucks because it won't accept multiple classes at once
    var hasClass, addClass, removeClass;

    if ( 'classList' in document.documentElement ) {
        hasClass = function( elem, c ) {
            return elem.classList.contains( c );
        };
        addClass = function( elem, c ) {
            elem.classList.add( c );
        };
        removeClass = function( elem, c ) {
            elem.classList.remove( c );
        };
    }
    else {
        hasClass = function( elem, c ) {
            return regExp( c ).test( elem.className );
        };
        addClass = function( elem, c ) {
            if ( !hasClass( elem, c ) ) {
                elem.className = elem.className + ' ' + c;
            }
        };
        removeClass = function( elem, c ) {
            elem.className = elem.className.replace( regExp( c ), ' ' );
        };
    }

    function toggleClass( elem, c ) {
        var fn = hasClass( elem, c ) ? removeClass : addClass;
        fn( elem, c );
    }

    var isIE = isBrowserIE();
    function isBrowserIE() {
        var nav = navigator.userAgent.toLowerCase();
        return (nav.indexOf('msie') != -1) ? parseInt(nav.split('msie')[1]) : false;
    }

    function getScrollPosition() {
        return Array(
            (document.documentElement && document.documentElement.scrollLeft) || window.pageXOffset || self.pageXOffset || document.body.scrollLeft,
            (document.documentElement && document.documentElement.scrollTop) || window.pageYOffset || self.pageYOffset || document.body.scrollTop
        );
    }

    function tab(name,navParentId){                       
        if (navParentId != null) {

            var tabs = navParentId.querySelectorAll('.tabs-menu-link');
            var contentTab = document.querySelector('div[data-tab-content="'+navParentId.dataset.tabId+'"]');        
            var contentItems = contentTab.querySelectorAll('.tabs-content-item');
            
            for (var i = 0,c = tabs.length; i < c; i++) {
                if(tabs[i].dataset.tabId == name) {
                    if (! hasClass(tabs[i], 'is-active')) {
                        addClass(tabs[i], 'is-active');
                    }
                } else {
                    removeClass(tabs[i], 'is-active');
                }
            }
            for (var i = 0,c = contentItems.length; i < c; i++) {
                if(contentItems[i].dataset.tabContent != name) {
                    contentItems[i].style.display = 'none';
                }
                if(contentItems[i].dataset.tabContent == name) {
                    contentItems[i].style.display = 'block';
                }
            }
            backToAnchor(name);
        }
    }

    function backToAnchor(onglet) {
        var anchor = document.querySelector('div[data-tab-content="'+onglet+'"]');
        var pos = findXY(anchor);
        window.scrollTo(0,0);
        
    }

    function findXY(obj){
        var x=0,y=0;
        while (obj!=null){
            x+=obj.offsetLeft-obj.scrollLeft;
            y+=obj.offsetTop-obj.scrollTop;
            obj=obj.offsetParent;
        }
        return {x:x,y:y};
    }

    function isOnTop() {
        var pos = getScrollPosition();
        if (pos[1] > 50) {
            window.scrollTo(0,0);
        }
    }

    var idSelected = null;
    var parentNav = null;

    var mainNav = document.querySelectorAll('nav.tabs-menu');
    for (var I = 0,c = mainNav.length; I < c; I++) {
        var onglet = mainNav[I].querySelector('.is-active');
        var idTab = null;
        // on récupère l'ancre dans l'URL
        var anchor = window.location.hash;
        anchor = anchor.substring(1,anchor.length); // enlève le #
        if (anchor == '' && onglet != undefined) {
            idTab = onglet.dataset.tabId;
        } else {
            idTab = anchor;
        }
        if (idSelected == null) {
            idSelected = idTab;
        }
        var ongletId = onglet.dataset.tabId;
        if( ongletId != null) {
            // On masque le contenu des onglets sauf celui du premier
            var tabContent = document.querySelectorAll('.tabs-content-item');
            for (var i = 0, nb = tabContent.length; i < nb; i++) {
                if (tabContent[i].dataset.tabContent == idSelected) {
                    tabContent[i].style.display = 'block';
                }else {
                    tabContent[i].style.display = 'none';
                }
            }
        }
        
        parentNav = mainNav[I];
        var tabs = parentNav.querySelectorAll('.tabs-menu-link');
        for (var j = 0, nbTabs = tabs.length; j < nbTabs; j++) {
            tabs[j].addEventListener("click", function(e){ 
                idSelected = e.target.dataset.tabId;
                tab(idSelected,parentNav)}, false);
        }
        tab(idSelected,parentNav);
    } 

    window.onload = isOnTop;
    window.backToAnchor = backToAnchor;

})(window);
