function previewFiles() {

    var preview = document.querySelector('#preview');
    var files   = document.querySelector('#browse').files;
    preview.innerHTML = '';
    preview.style.width = 'auto';

    function readAndPreview(file) {

        // Veillez à ce que `file.name` corresponde à nos critères d’extension
        if ( /\.(jpe?g|png|gif)$/i.test(file.name) ) {
            var reader = new FileReader();

            reader.addEventListener("load", function () {
                var image = new Image();
                image.height = 100;
                image.title = file.name;
                image.src = this.result;
                preview.appendChild( image );
            }, false);

            reader.readAsDataURL(file);
        }

    }

    if (files) {
        [].forEach.call(files, readAndPreview);
    }
}

document.querySelector('#browse').addEventListener('change',previewFiles,false);