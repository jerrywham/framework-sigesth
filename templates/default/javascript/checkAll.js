function checkAll(inputs, field, populate = false) {
    if (populate === true) {
        var all = document.querySelector('input[name="'+field+'"]');
        all.val = {
            length: 0,
            addElem: function addElem(elem) {
                // obj.length is automatically incremented 
                // every time an element is added.
                [].push.call(this, elem);
            }
        };
    }
    for(var i = 0; i < inputs.elements.length; i++) {
        if(inputs[i].type == "checkbox" && inputs[i].name==field) {
            inputs[i].checked = !inputs[i].checked ;
            if (all != undefined && inputs[i].checked == 1) {
                all.val.addElem(inputs[i].value);
            }
        }
    }
    if (all != undefined) {
        all.value = all.val;
    }
}

function prepareCheck(Elem) {
    var domElement =  {
        length: 0,
        elements: {},
        addElem: function addElem(elem) {
            // obj.length is automatically incremented 
            // every time an element is added.
            [].push.call(this, elem);
            [].push.call(this.elements, elem);
        }
    };
    var nbElements = Elem.length;
    for (var i = 0; i < nbElements; i++) {
        for (var j = 0; j < Elem[i].length; j++) {
            domElement.addElem(Elem[i][j]);
        }
       
    }
    return domElement;   
}