// Open a new tab more safely than target="__blank"
var targetBlank = document.querySelectorAll('.targetBlank');
if (targetBlank !== null) {
    var nb = targetBlank.length;
    for (var i = 0; i < nb; i++) {
        targetBlank[i].addEventListener('click', function(evt) { 
            evt.preventDefault(); 
            evt.returnValue = false; 
            openNew(this.href);
        });
    }
}

function openNew(url) {
    var otherWindow = window.open();
    otherWindow.opener = null;
    otherWindow.location = url;
}



///////////////////////////////////////////////
// Open a new window as a pop-up

var wopen = document.querySelectorAll('.Wopen');
// var paramsOfUrl = window.location.href.substring(window.location.href.toLowerCase().indexOf('sigesth4.0')+11);
// var url = window.location.href.replace(paramsOfUrl,'');

var dirRoot = window.location.pathname.substring(0,window.location.pathname.lastIndexOf('/')).substring(1,window.location.pathname.substring(0,window.location.pathname.lastIndexOf('/')).lastIndexOf('/')+1);
    
// var url = window.location.origin + dirRoot;
var url = window.location.origin + '/' + dirRoot;

if (wopen != null) {
    var l = wopen.length;
    for (var i = 0; i < l; i++) {
        var newElement = document.createElement('img');
        newElement.setAttribute('src',url+'/vendors/WillCreateOrRecycleNewWindow.gif');
        newElement.setAttribute('title','Ouvre une nouvelle fenêtre');
        wopen[i].setAttribute('title','Ouvre une nouvelle fenêtre');
        wopen[i].appendChild(newElement);
        wopen[i].addEventListener('click', function(evt) { 
            evt.preventDefault(); 
            evt.returnValue = false; 
            if (this.href != undefined) {
                Wopen(this.href,null,'1215','920');
            } else {
                Wopen(this.dataset.set,null,'1215','920');
            }
        });
    }
}

var windowObjectReference;
var strWindowFeatures = "toolbar=no,menubar=no,resizable=no,scrollbars=yes,status=no,dialog=no,fullscreen=no";

function Wopen(url,title,w,h) {
    //Fixes dual-screen position                          Most Browsers       Firefox
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : window.screenX;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : window.screenY;

    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    if(windowObjectReference == null || windowObjectReference.closed) {
        windowObjectReference = window.open(url, title, "width="+w+",height="+h+",top="+top+",left="+left+","+strWindowFeatures);
    } else {
        //Puts focus on the new window
        windowObjectReference.focus();
        
        //Close new window when click on parent
        var wrapper = document.querySelector('input.btn');
        if (wrapper.type == 'submit') {
            wrapper.addEventListener('click', function(evt) { 
                windowObjectReference.close();
            });
        }
    }
}