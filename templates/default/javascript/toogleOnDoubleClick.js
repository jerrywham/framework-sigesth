var toogleBtn = document.querySelectorAll('.toogleRename');
var renameForm = document.querySelectorAll('.rename');
var nb = toogleBtn.length;
if (nb != renameForm.length) {
    alert('Please check your html code. Number of toogle buttons does not match number of forms.');
} else {
    for (var i = 0; i < nb; i++) {
        renameForm[i].className = 'hide';
        toogleBtn[i].renameForm = renameForm[i];
        toogleBtn[i].addEventListener('dblclick', function(evt) { 
            evt.preventDefault(); 
            evt.returnValue = false; 
            if (this.className == 'toogleRename') {
                this.renameForm.className = 'formOpened';
                this.className = 'hide name';
            } else {
                this.renameForm.className = 'hide';
                this.className = 'toogleRename';
            }
        });
    }
}

function closeForm() {
    var renameFormOpen = document.querySelectorAll('.formOpened');
    var nb = renameFormOpen.length;
    for (var i = 0; i < nb; i++) {
        renameFormOpen[i].className = 'hide';
        toogleBtn[i].className = 'toogleRename';
    }
    var nameBtn = document.querySelectorAll('.name');
    var nb = nameBtn.length;
    for (var i = 0; i < nb; i++) {
        nameBtn[i].className = 'toogleRename';
    }
}

document.addEventListener('keydown', (event) => {
    const keyName = event.key;
    if (keyName == 'Escape') {
        closeForm();
    }

});
var escapeBtn = document.querySelectorAll('.escape');
var nb = escapeBtn.length;
for (var i = 0; i < nb; i++) {
    escapeBtn[i].addEventListener('click', (e) => {
        closeForm();
    });
}