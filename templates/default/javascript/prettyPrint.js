function prettyPrint() {
    var txtar = document.querySelectorAll('textarea');
    if (txtar != undefined) {
        var nb = txtar.length;
        for(i=0;i<nb;i++) {
            var ugly = txtar[i].value;
            try {
                var obj = JSON.parse(ugly);
                var pretty = JSON.stringify(obj, undefined, 4);
                txtar[i].value = pretty;
            } catch {
                return false;
            }
        }
    }
}
addEvent( window, 'load', prettyPrint );