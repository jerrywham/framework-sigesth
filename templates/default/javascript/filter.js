;(function(window,undefined){

    'use strict'; 
    
    function doFilter(idSearch, idTable) {
        var input, pos, filter, table, tr, td, i;
        pos = document.getElementById(idSearch).parentNode.cellIndex;
        filter = document.getElementById(idSearch).value;
        table = document.getElementById(idTable);
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[pos];
            if (td != undefined) {
                if (td.getAttribute('data-label').toLowerCase().indexOf(filter.toLowerCase()) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
        if (typeof(Storage) !== "undefined" && filter !== "undefined") {
            localStorage.setItem(idSearch, filter);
        }
    }
    
    window.doFilter = doFilter;

})(window);