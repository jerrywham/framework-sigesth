/*
# ------------------ BEGIN LICENSE BLOCK ------------------
#
# This file is part of SIGesTH
#
# Copyright (c) 2009 - 2014 Cyril MAGUIRE, <contact@ecyseo.net>
# Licensed under the CeCILL v2.1 license.
# See http://www.cecill.info/licences.fr.html
#
# ------------------- END LICENSE BLOCK -------------------
*/

// if (blockToShow == undefined || blockToShow == null) {
//     // bubble-id
//     var blockToShow = "coordonnees";
// }  
// if (idForClosing == undefined || idForClosing == null) {
//     //close-id
//     var idForClosing = "close";
// }  
// if (myId == undefined || myId == null) {  
//     var myId;
//     alert('Message de bulle.js : Vous devez définir l\'index permettant l\'ouverture de la pop-up !');
// }
//infobulle coordonnées arc
;(function(window,undefined) {
    
    'use strict';

    function fadeIn(el) {
        var opacity = 0;

        el.style.opacity = 0;
        el.style.filter = '';

        var last = +new Date();
        var tick = function() {
            opacity += (new Date() - last) / 400;
            el.style.opacity = opacity;
            el.style.filter = 'alpha(opacity=' + (100 * opacity)|0 + ')';

            last = +new Date();

            if (opacity < 1) {
                (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16);
            }
        };

        tick();
    }

    function showBubble(el) {
        var id = el.getAttribute('id').replace('show-','');
        var bubble = document.getElementById('bubble-'+id);
        bubble.style.display='none';
        if (el != null) {
            el.addEventListener('click',toogleBubble);
        }
    }

    function toogleBubble(el){
        var id = el.target.getAttribute('id').replace('show-','');
        id = id.replace('img-','');
        var bulle = document.getElementById('bubble-'+id);
        var isTop = (bulle.className.lastIndexOf('bubble--top') != -1 ? true : false);
        var isRight = (bulle.className.lastIndexOf('bubble--right') != -1 ? true : false);
        var isBottom = (bulle.className.lastIndexOf('bubble--bottom') != -1 ? true : false);
        var isLeft = (bulle.className.lastIndexOf('bubble--left') != -1 ? true : false);
            
        var close = document.getElementById('close-'+id);
        if (bulle.style.display == 'none') {
            bulle.style.display = 'block';
            bulle.style.position = 'absolute';
            var bubbleWidth = bulle.offsetWidth;
            var bubbleHeight = bulle.offsetHeight;
            var posLeft = this.offsetLeft+(isLeft ? -bubbleWidth - el.target.offsetWidth : (isRight ? (el.target.offsetWidth*2) : -(bubbleWidth/2) ));
            var posTopOfTarget = el.target.offsetTop + (el.target.offsetHeight/2);
            var posTop = (isTop ? -(bubbleHeight - (posTopOfTarget - (el.target.offsetHeight*2))) : (isBottom ? this.offsetTop + el.target.offsetTop + (el.target.offsetHeight/2) : (posTopOfTarget - (bubbleHeight/2) )));
            bulle.style.top =  (posTop) + 'px';
            bulle.style.left =  posLeft + 'px';
            bulle.style.opacity =  0;
            fadeIn(bulle);
        } else {
            bulle.style.display = 'none';
        }
        close.addEventListener('click',closeBubble);
    };

    function closeBubble(el){
        var id = el.target.getAttribute('id').replace('close-','');
        var bulle = document.getElementById('bubble-'+id);
        bulle.style.display='none';
    }

    window.showBubble = showBubble;

})(window);


var allBubbles = document.querySelectorAll('.showBubble');
var nbBubbles = allBubbles.length;

for (var i = 0; i < nbBubbles; i++) {  
    showBubble(allBubbles[i]);
}
