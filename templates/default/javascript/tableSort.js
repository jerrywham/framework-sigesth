/**
 * https://lehollandaisvolant.net/table-sort November 2018
 *@author: Timo Van Neerden
 *
 *@contributor: Cyril Maguire
 */

;(function(window,undefined) {
    
    'use strict';

    function doSort(table) {
        var tableBody = table.tBodies[0];
        var tableHead = table.tHead;

        var thCells = (tableHead).querySelectorAll('tr > th[data-do-sort="1"]');
        var len = thCells.length;
        for (var j=0; j < len; j++) {
            thCells[j].dataset.sortDirection = 1;
            thCells[j].addEventListener('click', function() {sortForColumn(this);});
        }

        function sortForColumn(columnTh) {
            // put class on active column
            for (var th of thCells) { th.classList.remove('active-sorting');}
            columnTh.classList.add('active-sorting');

            // get col number + sorting direction
            var col = parseInt(columnTh.dataset.sortId); 
            var sortDirection = (columnTh.dataset.sortDirection ^= 1);

            // create table with values of selected column
            var valuesForCol = new Array();
            var rows = tableBody.querySelectorAll('tr');
            for (var i=0, len = rows.length ; i<len ; i++) {
                valuesForCol.push({'val': (rows[i].querySelector('td:nth-child(' + (col+1) + ')')).textContent, 'index': i});
            }

            // sorting (if elements are numeric, a numeric test is done)
            valuesForCol.sort(function (a,b) {        
                var areNumbers = (a['val'] == parseFloat(a['val']) && b['val'] == parseFloat(b['val'])) ? true : false;
                if (sortDirection == 0) {
                    if (areNumbers) return a['val'] - b['val'];
                    return new Intl.Collator("fr", {sensitivity: "variant",ignorePunctuation: true,caseFirst:"upper"}).compare(a['val'],b['val']);
                } else {
                    if (areNumbers) return b['val'] - a['val'];
                    return new Intl.Collator("fr", {sensitivity: "variant",ignorePunctuation: true,caseFirst:"upper"}).compare(b['val'],a['val']);
                }
            });

            // reorder the rows in the document
            for (var i=0, len = valuesForCol.length ; i<len ; i++) {
                tableBody.appendChild(rows[valuesForCol[i]['index']])
            }
        }
    }

    window.doSort = doSort;

})(window);


var tables = document.querySelectorAll('.sortMe');
var nbTables = tables.length;
for (var i = 0; i < nbTables; i++) {
    var table = tables[i];
    doSort(table);
}
