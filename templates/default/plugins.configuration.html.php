<h2>L_Plugin_configuration_page <?=$plugin; ?></h2>
<?php if (!empty($errors)): ?>
    <div class="errors">
        <p>L_Error , L_please_check_the_following:</p>
        <ul>
        <?php foreach ($errors as $error): ?>
            <li><?= $error ?></li>
        <?php endforeach;   ?>
        </ul>
    </div>
<?php endif; ?>
<form action="" method="post">
    
    <?php foreach ($config as $key => $param): 
        $pv = json_decode($param['params_value']);?>
        
    <p>
        <label for="<?=$param['params_name'];?>"><?=$param['params_name'];?><?=($param['params_mandatory']==1 ? ' (<span class="mandatory">*</span>)' : '');?></label>
        <?php if(isset($param['id'])): ?>

        <input type="hidden" name="config[<?=$param['params_name'];?>][id]" value="<?=$param['id'];?>">
        <?php endif; ?>

        <input type="hidden" name="config[<?=$param['params_name'];?>][params_name]" value="<?=$param['params_name'];?>">
        <?php if (!is_array($pv) && !is_object($pv)) : ?>
            <?php if (is_bool($this->pluginsEnabled[$plugin]->defaultParams[$key]['params_value'] ) ) : ?>

        <select name="config[<?=$param['params_name'];?>][params_value]">
            <option value="1"<?=$pv==1? ' selected="selected"' : '';?>>TRUE</option>
            <option value="0"<?=$pv==0? ' selected="selected"' : '';?>>FALSE</option>
        </select>
            <?php else: ?>

        <input type="text" name="config[<?=$param['params_name'];?>][params_value]" id="<?=$param['params_name'];?>" value="<?=$pv;?>"<?=($param['params_mandatory']==1 ? ' required="required"' : '');?>>
            <?php endif; ?>
        <?php else: ?>

        <textarea name="config[<?=$param['params_name'];?>][params_value]" id="<?=$param['params_name'];?>" rows="10" cols="80"<?=($param['params_mandatory']==1 ? ' required="required"' : '');?>><?=$param['params_value'];?></textarea>
        <?php endif; ?>

        <input type="hidden" name="config[<?=$param['params_name'];?>][params_mandatory]" value="<?=$param['params_mandatory'];?>">
    </p>
    <?php endforeach ?>

    <p>
        <input type="submit" name="submit" value="L_Save" class="btn">
        <!-- token is mandatory to post data -->
        <input type="hidden" name="token" value="<?=$token;?>">
    </p>
    <p class="mandatory"><small><i>* : L_Mandatory</i></small></p>
</form>