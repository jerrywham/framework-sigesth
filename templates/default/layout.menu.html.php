            
            <input type="checkbox" id="menu-checkbox" class="menu-checkbox" />
            <label for="menu-checkbox" class="menu-toggle">&#9776; Menu</label>
            <nav id="navigation" role="navigation" class="clearfix">
                <?php if ($authentication->isLoggedIn()): ?>

                <li class="submenu">
                    <nav role="administration" id="nav-left">
                        <li><a href="/article/list">L_Articles_list</a></li>
                        <li><a href="/article/edit">L_Add_a_new_article</a></li>
                    </nav>
                    <a href="#" class="close-menu" aria-label="Close Navigation">
                        <svg class="icons icons--cross" aria-hidden="true"><use href="#iconCross"></use></svg>
                    </a>
                    <a href="#nav-left" class="open-menu" aria-label="administration" title="L_Administration">
                        <svg class="icons icons--admin" aria-hidden="true"><use href="#iconAdmin"></use></svg>
                        <span>L_Administration</span>
                    </a>
                </li>
                <?php endif; ?>

                <li>
                    <a href="/home" aria-label="home" title="L_Home">
                        <svg class="icons icons--home" aria-hidden="true"><use href="#iconHome"></use></svg>
                        <span>L_Home</span>
                    </a>
                </li>
                <li>
                    <a href="/category/list">L_Categories</a>
                </li>
                <!-- Float right  -->
                <div class="fr">
                <?php if ($authentication->isLoggedIn()): ?>
                <li>
                    <a href="/logout" class="important" aria-label="login" title="L_Log_out">
                        <svg class="icons icons--cadenas-ouvert" aria-hidden="true"><use href="#iconLogout"></use></svg>
                        <span>L_Log_out</span>
                    </a>
                </li>
                <li class="submenu">
                    <nav role="administration" id="nav-right">
                        <?php if ($authentication->getUser()->hasPermission(\Core\Lib\User::EDIT_USER_ACCESS)): ?>

                        <li><a href="/users/list">L_User_List</a></li>
                        <?php endif; ?>
                        <?php if ($authentication->getUser()->hasPermission(\Core\Lib\User::ADMIN)): ?>

                        <li><a href="/medias">L_Medias</a></li>
                        <li><a href="/configuration">L_Configuration</a></li>
                        <li><a href="/plugins/list">L_Manage_plugins</a></li>
                        <?php endif; ?>

                    </nav>
                    <a href="#" class="close-menu-right" aria-label="Close Navigation">
                        <svg class="icons icons--cross" aria-hidden="true"><use href="#iconCross"></use></svg>
                    </a>
                    <a href="#nav-right" class="open-menu-right" aria-label="administration" title="L_Administration">
                        <svg class="icons icons--admin" aria-hidden="true"><use href="#iconAdmin"></use></svg>
                        <span>L_Administration</span>
                    </a>
                </li>
                    <?php else: ?>

                <li>
                    <a href="/login" aria-label="login" title="L_Log_in">
                        <svg class="icons icons--cadenas" aria-hidden="true"><use href="#iconLogin"></use></svg>
                        <span>L_Log_in</span>
                    </a>
                </li>
                <?php endif; ?>
                </div>
            </nav>