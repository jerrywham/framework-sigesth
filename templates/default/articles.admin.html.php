
    <div class="articlelist">
        <div class="articles">

            <p><?=$stringTools->t(['total' => $total],'L_article_have_been_submitted_to_the','L_articles_have_been_submitted_to_the');?> L_Application<?=(($categoryName)? ', L_in_category "'.$categoryName.'"' : '');?>.</p>
            <form action="/article/delete" method="post" class="confirmAction" data-msg="L_Are_you_sure_to_delete_all_this_data_?">
                <select>
                    <option value="">L_For_selection</option>
                    <option value="">--------</option>
                    <option value="delete">L_Delete</option>
                </select>
                <input type="submit" value="L_Ok" class="btn--warning">
                <input type="hidden" name="token" value="<?=$token;?>">
                <table class="table table--zebra order-table-pict sortMe">
                    <caption>L_Articles_list</caption>
                    <thead>
                        <tr>
                            <th><input type="checkbox" class="checkbox" onclick="checkAll(this.form, 'idArt[]')" /></th>
                            <th data-do-sort="1" data-sort-id="1" data-sort-direction="1">L_id</th>
                            <th data-do-sort="1" data-sort-id="2" data-sort-direction="1">L_Date</th>
                            <th data-do-sort="1" data-sort-id="3" data-sort-direction="1">L_Title</th>
                            <th data-do-sort="1" data-sort-id="4" data-sort-direction="1">L_Category</th>
                            <th>L_Comments</th>
                            <th data-do-sort="1" data-sort-id="6" data-sort-direction="1">L_Author</th>
                            <th>L_Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        

                <?php foreach($articles as $article): ?>
                    <tr>
                        <td><input type="checkbox" class="checkbox" name="idArt[]" value="<?=$article->id?>" /></td>
                        <td><?=$article->id?></td>
                        <td><?=$article->articledate?></td>
                        <td><a href="/article/edit?id=<?=$article->id?>" title="L_Edit"><?=$article->articletitle;?></a></td>
                        <td><?php foreach ($article->getCategories() as $key => $cat) :?>

                        <span class="tag"><a href="/article/list?category=<?=$cat->id;?>"><?=$cat->name;?></a><?=($categoryName)? '<a href="/article/list">&cross;</a>' : '';?></span>
                        
                        <?php endforeach; ?></td>
                        <td></td>
                        <td><?=(is_object($article->getAuthor()) ? htmlspecialchars($article->getAuthor()->name, ENT_QUOTES,
                    'UTF-8') : ''); ?></td>

                        <td>
                            <?php if ($user->id == $article->authorId || $user->hasPermission(\Core\Lib\User::DELETE)): ?>

                                <input type="hidden" name="id" value="<?=$article->id?>">
                                <input type="submit" value="L_Delete" class="btn--danger">
                                <input type="hidden" name="token" value="<?=$token;?>">

                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>

                    </tbody>
                </table>
            </form>


            <p class="pagination">L_Select_page: <?=$paginator->make(new $pagination($total, $currentPage, $elementsByPage, array('category' => $categoryId)))->displayPagination('/article/list','badge--info','badge--primary');?></p>

        </div>
    </div>
