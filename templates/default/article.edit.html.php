<?php if (empty($article->id) || $user->id == $article->authorId || $user->hasPermission(\Core\Lib\User::EDIT_JOKES)): ?>
<h2>L_Edit_article</h2>
<form action="" method="post" id="">
	<input type="hidden" name="article[id]" value="<?=$article->id ?? ''?>">
    <label for="articletitle">L_Title:</label>
    <input id="articletitle" type="text" name="article[articletitle]" value="<?=$article->articletitle ?? ''?>">
    <label for="articledate">L_Date:</label>
    <input id="articledate" type="date" name="article[articledate]" value="<?=$article->articledate ?? ''?>">
    <label for="articletext">L_Type_your_article_here:</label>
    <textarea id="articletext" name="article[articletext]" rows="3" cols="40"><?=$article->articletext ?? ''?></textarea>

    <p>L_Select_categories_for_this_article:</p>
    <?php foreach ($categories as $category): ?>

    <p>
        <input type="checkbox"<?=($article && $article->hasCategory($category->id) ? ' checked':'');?> name="category[]" value="<?=$category->id?>" id="cat<?=$category->id?>" /> 
        <label for="cat<?=$category->id?>" class="inbl"><?=$category->name?></label>
    </p>
    <?php endforeach; ?>

    <p>
        <input type="submit" name="submit" value="L_Save" class="btn">
        <input type="hidden" name="token" value="<?=$token;?>">
    </p>
</form>
<?php else: ?>

<p>L_You_may_only_edit_articles_that_you_posted.</p>

<?php endif; ?>