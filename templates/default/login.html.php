<?php if (isset($error)):?>
	<div class="errors"><?=$error;?></div>
<?php endif; ?>
<div class="txtcenter login">
    <form method="post" action="">
    	<p>
            <span class="hover-underline-animation">   
                <label for="login">L_Your_login</label><i class="user icon"></i>
                <input type="text" id="login" name="login" class="focus" placeholder="L_Your_login">
            </span>
        </p>
        <p>
            <span class="hover-underline-animation">   
        	   <label for="password">L_Your_password</label><i class="lock icon"></i>
        	   <input type="password" id="password" name="password" placeholder="L_Your_password">
            </span>
        </p>
        <p>
        	<input type="submit" name="dologin" value="L_Log_in" class="btn">
            <input type="hidden" name="token" value="<?=$token;?>">
        </p>
        <p><small><a href="/login/newpassword">L_New_password</a></small></p>
    </form>
</div>