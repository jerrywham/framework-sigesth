<?php if($allowuploads) :?>
    <?php if (isset($alert)):?>
        <?=$alert;?>
    <?php endif; ?>
    <h2 class="clearfix">L_Add_<?=$baseurl;?>
        <div class="fr">
            <a href="/medias">L_Medias</a>
        </div>
    </h2>
    <form method="post" action="" enctype="multipart/form-data" id="send">
        
        <div class="grid-6-small-3 has-gutter">
            <div id="preview" class="col-all">?</div>
            <p class="col-all">
                <input type="hidden" name="dir" value="">
                <input type="file" name="file[]" id="browse" multiple/>
            </p>
        </div>
        <p>
            <label for="visibility">L_visibility</label>
            <select name="visibility" id="visibility">
                <option value="public">L_public</option>
                <option value="private">L_private</option>
            </select>
        </p>
        <p>
            <input type="submit" value="L_UPLOAD" id="submitButton">
            <input type="hidden" name="baseurl" value="<?php echo $baseurl ?>">
            <input type="hidden" name="lead" value="<?php echo $leadon ?>">
            <input type="hidden" name="token" value="<?=$token;?>">
        </p>
        <p>
            <small>L_max_size = <?=trim(ini_get('upload_max_filesize'));?></small>
        </p>
        <p>
            <small>L_Allowed_extensions = <?=implode('; ',$allowExtensions);?></small>
        </p>
    </form>
<?php else : ?>

    <p>L_File_uploads_are_disabled_in_your_php.ini_file._Please_enable_them.</p>
<?php endif;?>